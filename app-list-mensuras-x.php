<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(20);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}
//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------

$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "get-archivo") {
    $ctx = stream_context_create(array('http' => array(
        'timeout' => 20000, //1200 Seconds is 20 Minutes
    ),
    ));

    if (file_exists($_REQUEST["archivo_pdf"])) {
        echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
    } else {
        echo "";
    }
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "delete-archivo") {
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id=" . $_REQUEST["arc_id"]);
    auditar($db, "archivo", "baja", $_REQUEST["arc_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "delete-presentacion") {
    //$db->ExecuteSQL("UPDATE parcelas_propietarios SET ppr_eliminado=1 WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    $db->ExecuteSQL("UPDATE mensura_presentaciones  SET mpp_eliminada=1 WHERE mpp_id=" . $_REQUEST["mpp_id"]);
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id_fuente=" . $_REQUEST["mpp_id"]);
    auditar($db, "presentación", "baja", $_REQUEST["mpp_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "habilitar-tasacion") {
    //$db->ExecuteSQL("UPDATE parcelas_propietarios SET ppr_eliminado=1 WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    // $db->ExecuteSQL("UPDATE parcelas  SET par_tasacion_habilitada=".$_REQUEST["tipo_tasacion"].",par_tasacion_habilitada_fecha=Now() WHERE par_id=" . $_REQUEST["par_id"]);
    $condicion = "";
    if($_REQUEST["retasar"] == "retasar") {
        if($_REQUEST["tipo_tasacion"] == 1 || $_REQUEST["tipo_tasacion"] == 5) {
            $condicion = "mpp.mpp_presentada_tasacion=0";
        } else {
            $condicion = "(mpp.mpp_respuesta_tasacion = 'APROBADA TASACION DNV' OR mpp.mpp_respuesta_tasacion = 'APROBADA TASACION TTN')";
        }
    } else if($_REQUEST["retasar"] == "devolucion") {
        $condicion = "(mpp.mpp_presentada_tasacion=0 OR mpp.mpp_respuesta_tasacion = 'APROBADA TASACION DNV')";
    } else {
        $condicion = "mpp.mpp_presentada_tasacion=0";
    }
    $sql = "SELECT 
                mpp.mpp_id, 
                mpp.mpp_fecha_presentacion, 
                mpp.mpp_aprobada_dnv_fecha,
                mpp.mpp_observada_catastro_fecha,
                mpp.par_id,
                arc.cat_id
            FROM
                mensura_presentaciones mpp,
                archivos arc 
            WHERE
                mpp.par_id=".$_REQUEST["par_id"] . " AND mpp.mpp_aprobada_dnv_fecha IS NOT NULL AND mpp.mpp_observada_dnv_fecha IS NULL AND " . $condicion . " AND arc.arc_fuente='MENS' AND arc.arc_id_fuente=mpp.mpp_id";

    $db->ExecuteSQL($sql);
    $presentaciones_arr = $db->getRows();

    if(count($presentaciones_arr) > 0) {
        $insert = "INSERT INTO tasaciones_solicitudes (ppt_tipo_tasacion_solicitada,ppt_fecha_solicitud,ppt_usuario_solicitante,par_id,ppt_estado) VALUES ("
            .$_REQUEST["tipo_tasacion"]
            .",Now(),'"
            .$_SESSION["expropiar_usuario_login"]."',"
            .$_REQUEST["par_id"].",1)";

        $db->ExecuteSQL("START TRANSACTION");
        $result = $db->ExecuteSQL($insert);
        if ($result != 1) {
            $db->ExecuteSQL("ROLLBACK");
        } else {
            $solicitud_tasacion = $db->returnInsertId();
            try {
                foreach ($presentaciones_arr as $presentacion) {
                    $result_relation = $db->ExecuteSQL("INSERT INTO tasaciones_solicitudes_detalle (mpp_id, ppt_id, pptdet_estado) VALUES(" . $presentacion["mpp_id"]. "," . $solicitud_tasacion . ",1)");
                    $result_relation = $db->ExecuteSQL("UPDATE mensura_presentaciones SET mpp_presentada_tasacion = 1, mpp_presentada_tasacion_fecha=Now(), mpp_presentada_tasacion_usuario='" . $_SESSION["expropiar_usuario_login"] . "' WHERE mpp_id=" . $presentacion["mpp_id"]);
                }
            } catch (Exception $ex) {
                $db->ExecuteSQL("ROLLBACK");
                $arr["res"] = false;
                $arr["msg"] = "Error[66] al solicitar tasación (ERR $ex)." ;
                echo json_encode($arr);
                exit;
            }
            $result_change_Status = $db->ExecuteSQL("UPDATE parcelas SET par_tasacion_habilitada = 0 WHERE par_id=" . $_REQUEST["par_id"]);
            $db->ExecuteSQL("COMMIT");
        }

        auditar($db, "presentación", "solicitar-tasación", $_REQUEST["par_id"], json_encode($_REQUEST));
        $arr["res"] = true;
        $arr["msg"] = "Se habilitó la tasacion para esta parcela." ;
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = false;
        $arr["msg"] = "No hay presentaciones nuevas para enviar." ;
        echo json_encode($arr);
        exit;
    }

    
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {

    $filtro_dis = "";
    if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
        $filtro_dis = " AND obr_distrito ='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ";
    }

    if ($lstObra != "") {
        $filtro_obra = " AND obr.obr_id='$lstObra' " . $filtro_dis ;
    } else {
        $filtro_obra = $filtro_dis;

    }
    if ($lstEstado != "" && $lstEstado != 1) {
        $filtro_estado = " AND ultimo_estado=$lstEstado";
    } else if ($lstEstado != "" || $lstEstado == 1) {
        $filtro_estado = " AND ultimo_estado IS NULL";
    }
    if ($txtNumeroOrden != "") {
        $filtro_numero_orden_parcela = " AND par.par_numero_orden=$txtNumeroOrden";
    }

    $sql = "SELECT DISTINCT
                par.*,
                (SELECT mes_id FROM mensura_presentaciones WHERE par_id=par.par_id AND mpp_eliminada=0 ORDER BY mpp_id DESC LIMIT 1) ultimo_estado,
                obr.obr_idsigo,
                obr.obr_nombre
            FROM
                parcelas par,
                obras obr
            WHERE
                par.obr_id=obr.obr_id
                AND obr.obr_eliminada=0
                AND par.par_eliminada=0
                $filtro_obra
                $filtro_numero_orden_parcela

            HAVING 1
                $filtro_estado

            ORDER BY
                par.par_id,par_numero_orden";

    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[98] en consulta a la base de datos ($result).";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$parcelas_arr = $db->getRows();

$html = <<<eod
<table class="table ">
<caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions bold">Total de parcelas: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
	<thead class="bg-dark text-light">
		<tr>
            <th class="vert-align" nowrap width="1%"></th>
            <th class="vert-align" nowrap width="1%"></th>
            <th class="vert-align text-center" nowrap width="1%" >Catastro<br>(plano, mejoras, croquis)</th>
            <th class="vert-align" nowrap>ID</th>
            <th class="vert-align" nowrap>ID Sigo</th>
            <th class="vert-align" nowrap>Nombre obra</th>
            <th class="vert-align" nowrap>N° de orden parcela</th>
            <th class="vert-align" nowrap>Fecha alta</th>
            <th class="vert-align" nowrap>Usuario alta</th>
		</tr>
	</thead>
eod;

for ($i = 0; $i < count($parcelas_arr); $i++) {

    $ti = $tf = $tools = $tool_ver = $tool_editar = $tool_eliminar = $estado_actual = "";
    $data1 = encrypt_decrypt("encrypt", $parcelas_arr[$i]['par_id']);

    $db->ExecuteSQL("SELECT *
    FROM
        mensura_presentaciones mpp,
        personas_fisicas per,
        archivos arc,
        mensura_estados mes,
        categorizacion_archivos cat
    WHERE
        mpp.per_id=per.per_id
        AND mpp.mes_id=mes.mes_id
        AND mpp.mpp_id=arc.arc_id_fuente
        AND arc.cat_id=cat.cat_id
        AND arc.arc_fuente='MENS'
        AND mpp.mpp_eliminada=0
        AND arc.arc_eliminado=0
        AND mpp.par_id=" . $parcelas_arr[$i]["par_id"] . " ORDER BY mpp.mpp_id DESC");
    $presentaciones_arr = $db->getRows();

    if (count($presentaciones_arr) > 0) {
        $tool_toogle = <<<eod
            &nbsp;&nbsp;<a href="" ><i onClick="$('#trAdjuntos{$i}').toggle();$('#trPresentaciones{$i}').toggle();if ($(this).hasClass('fa-chevron-down')){ $(this).toggleClass('fa-chevron-up')};return false;"  data-toggle="tooltip" title="ver archivos adjuntos" class="fa fa-chevron-down" style="font-size:0.9rem;color:#212121" ></i></a>&nbsp;&nbsp;
eod;
    } else {
        $tool_toogle = "";
    }

    // BUSCO SI LA PARCELA TIENE UNA TASACION REALIZADA
    $db->ExecuteSQL("SELECT * FROM tasaciones WHERE par_id=" . $parcelas_arr[$i]["par_id"] ." AND tas_deleted=0 ORDER BY tas_fecha_tasacion DESC LIMIT 1");
    $tasaciones_arr = $db->getRows();

    

    $clase_estatus_presentacion_plano = $clase_estatus_aprobada_mejoras = $clase_estatus_aprobada_croquis = "text-secondary";

    
    //--------------------------------------

    for ($p = 0; $p < count($presentaciones_arr); $p++) {

        //-- bolitas con estatus de la presentacion: amarilla -> presentada catastro   verde -> aprobada catstro   roja -> observada catastro
        if ($presentaciones_arr[$p]["cat_id"] == 13 && $presentaciones_arr[$p]["mes_id"] == 6) { //PLANO && APROBADA CATASTRO
            $clase_estatus_presentacion_plano = "text-success";
    

        } else if ($presentaciones_arr[$p]["cat_id"] == 13 && $presentaciones_arr[$p]["mes_id"] == 7) { //PLANO && OBSERVADA CATASTRO
            $clase_estatus_presentacion_plano = "text-danger";

        } else if ($presentaciones_arr[$p]["cat_id"] == 13 && $presentaciones_arr[$p]["mes_id"] == 5) { //PLANO && PRESENTADA CATASTRO
            $presento_plano = 1;
            $clase_estatus_presentacion_plano = "text-warning";

        }if ($presentaciones_arr[$p]["cat_id"] == 25 && $presentaciones_arr[$p]["mes_id"] == 6) { //MEJORAS && APROBADA CATASTRO
            $clase_estatus_aprobada_mejoras = "text-success";

        } else if ($presentaciones_arr[$p]["cat_id"] == 25 && $presentaciones_arr[$p]["mes_id"] == 7) { //MEJORAS && OBSERVADA CATASTRO
            $clase_estatus_aprobada_mejoras = "text-danger";
        } else if ($presentaciones_arr[$p]["cat_id"] == 25 && $presentaciones_arr[$p]["mes_id"] == 5) { //MEJORAS && PRESENTADA CATASTRO
            $presento_mejoras = 1;
            $clase_estatus_aprobada_mejoras = "text-warning";

        }
    }
//-- si la parcela no fue ya habilitada para tasación entonces ponemos el boton para agergar una nueva presentacion
    if ($parcelas_arr[$i]["par_tasacion_habilitada"] == 1) {
        $tool_editar .= <<<eod
        &nbsp;&nbsp;<button onClick="$.habilitarTasacion({$parcelas_arr[$i]["par_id"]},{$i},'tasar')" class="btn btn-sm btn-default" data-toggle="tooltip" title="habilitar tasación"><i class="fas fa-dollar-sign text-success btn-icon"></i></button>
eod;
    } else if($parcelas_arr[$i]["par_tasacion_habilitada"] == 2) {
        $tool_editar .= <<<eod
        &nbsp;&nbsp;<button onClick="$.habilitarTasacion({$parcelas_arr[$i]["par_id"]},{$i},'devolucion')" class="btn btn-sm btn-default" data-toggle="tooltip" title="habilitar tasación"><i class="fas fa-dollar-sign text-success btn-icon"></i></button>
eod;
    } else if($parcelas_arr[$i]["par_tasacion_habilitada"] == 3) {
        $tool_editar .= <<<eod
        &nbsp;&nbsp;<button onClick="$.habilitarTasacion({$parcelas_arr[$i]["par_id"]},{$i},'retasar')" class="btn btn-sm btn-default" data-toggle="tooltip" title="habilitar tasación"><i class="fas fa-dollar-sign text-success btn-icon"></i></button>
eod;
    }


    // BUSCO SI LA PARCELA TIENE UNA SOLICITUD NO DEVUELTA
    $db->ExecuteSQL("SELECT * FROM tasaciones_solicitudes WHERE par_id=" . $parcelas_arr[$i]["par_id"] ." ORDER BY ppt_fecha_solicitud DESC LIMIT 1");
    $tasaciones_soli_arr = $db->getRows();


    if(count($tasaciones_soli_arr) == 0 || (count($tasaciones_soli_arr) > 0 && ($tasaciones_soli_arr[0]["ppt_estado"] == 7 || $tasaciones_soli_arr[0]["ppt_estado"] == 2))) {
        $tool_editar .= <<<eod
        &nbsp;&nbsp;<button onClick="$.abrirVentanaNuevaPresentacion('{$data1}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="nueva presentación"><i class="fas fa-plus text-primary btn-icon"></i></button>
eod;
    }


    
    if(count($tasaciones_arr) > 0){
        $tool_editar .= <<<eod
        <button onClick="$.realizarTasacion({$parcelas_arr[$i]["par_id"]},'show','mensura')" class="btn btn-sm btn-default" data-toggle="tooltip" title="VER TASACIÓN"><i class="fas fa-search-dollar text-primary btn-icon"></i></button>
eod;    
    }

    switch ($atributo_perfil) {
        case 1:
            $tools .= $tool_ver;
            break;
        case 2:
            $tools .= $tool_ver . $tool_editar;
            break;
        case 3:
            $tools .= $tool_ver . $tool_editar . $tool_eliminar;
            break;
        default:
            $tools = "";
            break;
    }

    $html .= <<<eod
			<tr>
                <td  nowrap class="vert-align text-left" width="1%">{$ti}{$tool_toogle}{$tf}</td>
                <td  nowrap class="vert-align text-left">{$ti}<span id="spnTools{$i}">{$tools}</span>{$tf}</td>
                <td nowrap class="vert-align text-center">{$ti}
                    <i  data-toogle="tooltip" title="Plano" class="fa fa-circle {$clase_estatus_presentacion_plano}"></i>
                    <i  data-toogle="tooltip" title="Mejoras" class="fa fa-circle {$clase_estatus_aprobada_mejoras}"></i>
                    <i  data-toogle="tooltip" title="Croquis" class="fa fa-circle {$clase_estatus_aprobada_croquis}"></i>
                {$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_id"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["obr_idsigo"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["obr_nombre"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_numero_orden"]}{$tf}</td>
                <td class="vert-align">{$ti}{$ymd2dmy($parcelas_arr[$i]["fecha"])}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["usuario"]}{$tf}</td>
	    	</tr>
eod;

//---------------------------------------------------------------------------------------------------------------------
    if (count($presentaciones_arr) > 0) {

        $html .= <<<eod
        <tr id="trPresentaciones{$i}" style="display:none">
            <td  colspan="2"></td>
            <td colspan="10">
                <table class="table table-sm table-borderless" style="font-size:0.8rem;background:#FFFDE7">
                 <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions bold">Presentaciones</span></caption>

eod;
        $tools = $tool_observar = $tool_aprobar_dnv = $tool_eliminar = $estado = "";
        for ($p = 0; $p < count($presentaciones_arr); $p++) {
            $tool_aprobar = "";
            $tool_observar = "";
            $tool_eliminar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.eliminarPresentacion({$presentaciones_arr[$p]["mpp_id"]},{$p});return false" data-toggle="tooltip" title="eliminar presentación"><i class="fas fa-trash text-secondary"></i></a>
eod;

            if ($presentaciones_arr[$p]["mes_id"] == 1 || $presentaciones_arr[$p]["mes_id"] == "") {

            } else if ($presentaciones_arr[$p]["mes_id"] == 2) { //PRESENTADA DNV
                //-- presentamos botones de accion para aprobacion observación por parte de la DNV
                $tool_aprobar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('aprobar','DNV',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="aprobar presentación DNV"><i class="fas fa-check-circle text-info btn-icon"></i></a>
eod;
                $tool_observar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('observar','DNV',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="observar presentación"><i class="fas fa-check-circle text-danger btn-icon"></i></a>
eod;

            } else if ($presentaciones_arr[$p]["mes_id"] == 3) { //APROBADA DNV
                //-- presentamos botones de accion para la aprobacion u observación por parte de catastro
                $tool_aprobar = <<<eod
                    <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('presentar','CATASTRO',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="presentar en CATASTRO"><i class="fas fa-arrow-alt-circle-left text-info btn-icon"></i></a>
eod;

            } else if ($presentaciones_arr[$p]["mes_id"] == 5) { //PREDENTADA CATASTRO
                //-- presentamos botones de accion para la aprobacion u observación por parte de catastro
                $tool_aprobar = <<<eod
                    <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('aprobar','CATASTRO',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="catastro aprobó presentación"><i class="fas fa-thumbs-up text-success btn-icon"></i></a>
eod;
                $tool_observar = <<<eod
                    <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('observar','CATASTRO',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="catastro observó presentación"><i class="fas fa-thumbs-down text-danger btn-icon"></i></a>
eod;

            } else if ($presentaciones_arr[$p]["mes_id"] == 4) { //OBSERVADA DNV
                $tool_observar = $tool_aprobar = "";

            } else if ($presentaciones_arr[$p]["mes_id"] == 5) { //PRESENTADA CATASTRO
                $tool_aprobar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('aprobar','CATASTRO',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="catastro aprobó presentación"><i class="fas fa-thumbs-up text-success btn-icon"></i></a>
eod;
                $tool_observar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion('observar','CATASTRO',{$presentaciones_arr[$p]["mpp_id"]},{$presentaciones_arr[$p]["par_id"]});return false" data-toggle="tooltip" title="catastro observó presentación"><i class="fas fa-thumbs-down text-danger btn-icon"></i></a>
eod;

            } else if ($presentaciones_arr[$p]["mes_id"] == 6) { //APROBADA CATASTRO
                $tool_aprobar = $tool_observar = "";

            } else if ($presentaciones_arr[$p]["mes_id"] == 7) { //OBSERVADA CATASTRO
                $tool_observar = $tool_aprobar = "";

            }

            switch ($atributo_perfil) {
                case 1:
                    $tools = "";
                    break;
                case 2:
                    $tools = "";
                    break;
                case 3:
                    $tools = $tool_aprobar . $tool_observar . $tool_eliminarx;
                    break;
                default:
                    $tools = "";
                    break;
            }

            //DEJO SOLO EL BOTON PRESENTAR CATASTRO EN TIPO PLANO PARCELA
            if($presentaciones_arr[$p]["mes_id"] == 3 && ($presentaciones_arr[$p]["cat_descripcion"] == "MEJORAS" || $presentaciones_arr[$p]["cat_descripcion"] == "CROQUIS")) {
                $tools = "";
            }

            $html .= <<<eod
            <tr>
                <th nowrap class="text-center"></th>
                <th>ID</th>
                <th>Fecha presentación</th>
                <th nowrap>Presentó</th>
                <th nowrap>Medio de presentación</th>
                <th nowrap>Persona presentó</th>
                <th>Observaciones presentación</th>
                <th nowrap>Fecha alta</th>
                <th nowrap>Usuario alta</th>
            </tr>
            <tr>
                <td class="text-center vert-align"  width="10%">{$tools}</td>

                <td class="vert-align">{$presentaciones_arr[$p]["mpp_id"]}</td>
                <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_fecha_presentacion"])}</td>
                <td nowrap class="vert-align">
                    {$presentaciones_arr[$p]["cat_descripcion"]}&nbsp;<button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["arc_path"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                </td>
                <td nowrap class="vert-align">{$presentaciones_arr[$p]["mpp_medio"]} {$presentaciones_arr[$p]["mpp_mesa_entrada_nota"]}</td>
                <td nowrap nowrap class="vert-align">{$presentaciones_arr[$p]["per_nombre"]} {$presentaciones_arr[$p]["per_apellido"]}</td>
                <td nowrap class="vert-align">{$presentaciones_arr[$p]["mpp_observaciones"]}</td>
                <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["fecha"])}</td>
                <td nowrap class="vert-align">{$presentaciones_arr[$p]["usuario"]}</td>
            </tr>
eod;

            $html_aprobada_dnv = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">aprobada DNV</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_aprobada_dnv_fecha"])}</td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_aprobada_dnv_observaciones"]}</b></td>
                    <td nowrap class="vert-align">usuario alta: {$presentaciones_arr[$p]["mpp_aprobada_dnv_usuario"]}</td>
eod;
            if($presentaciones_arr[$p]["mpp_aprobada_dnv_archivo"] != "" && !is_null($presentaciones_arr[$p]["mpp_aprobada_dnv_archivo"])) {
                $html_aprobada_dnv .= <<<eod
                    <td nowrap class="vert-align">
                        <button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["mpp_aprobada_dnv_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                    </td>
                </tr>
eod;
            }

            $html_observada_dnv = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">observada DNV</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_observada_dnv_fecha"])}</td>
                    <td nowrap class="vert-align">motivo: <b>{$presentaciones_arr[$p]["mpp_observada_dnv_motivo"]}</b></td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_observada_dnv_observaciones"]}</b></td>
                    <td nowrap class="vert-align">usuario alta: {$presentaciones_arr[$p]["mpp_observada_dnv_usuario"]}</td>
eod;
            if($presentaciones_arr[$p]["mpp_observada_dnv_archivo"] != "" && !is_null($presentaciones_arr[$p]["mpp_observada_dnv_archivo"])) {
                $html_observada_dnv .= <<<eod
                <td nowrap class="vert-align">
                    <button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["mpp_observada_dnv_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                </td>
            </tr>
eod;
            }

            $html_presentada_catastro = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">presentada CATASTRO</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_presentada_catastro_fecha"])}</td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_presentada_catastro_observaciones"]}</b></td>
                    <td nowrap class="vert-align">usuario alta: {$presentaciones_arr[$p]["mpp_presentada_catastro_usuario"]}</td>
eod;
            if($presentaciones_arr[$p]["mpp_presentada_catastro_archivo"] != "" && !is_null($presentaciones_arr[$p]["mpp_presentada_catastro_archivo"])){
                $html_presentada_catastro .= <<<eod
                    <td nowrap class="vert-align">
                        <button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["mpp_presentada_catastro_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                    </td>
                </tr>
eod;
            }
            $html_aprobada_catastro = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">aprobada CATASTRO</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_aprobada_catastro_fecha"])}</td>
                    <td nowrap class="vert-align">{$presentaciones_arr[$p]["mpp_aprobada_catastro_observaciones"]}</td>
                    <td nowrap class="vert-align">usuario alta: {$presentaciones_arr[$p]["mpp_aprobada_catastro_usuario"]}</td>
eod;
            if($presentaciones_arr[$p]["mpp_aprobada_catastro_archivo"] != "" && !is_null($presentaciones_arr[$p]["mpp_aprobada_catastro_archivo"])){
                $html_aprobada_catastro .= <<<eod
                    <td nowrap class="vert-align">
                        <button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["mpp_aprobada_catastro_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                    </td>
                </tr>
eod;
            }
            $html_observada_catastro = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">observada CATASTRO</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_observada_catastro_fecha"])}</td>
                    <td nowrap class="vert-align">motivo: <b>{$presentaciones_arr[$p]["mpp_observada_catastro_motivo"]}</b></td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_observada_catastro_observaciones"]}</b></td>
                    <td nowrap class="vert-align">usuario alta: {$presentaciones_arr[$p]["mpp_observada_catastro_usuario"]}</td>
eod;
            if($presentaciones_arr[$p]["mpp_observada_catastro_archivo"] != "" && !is_null($presentaciones_arr[$p]["mpp_observada_catastro_archivo"])){
                $html_observada_catastro .= <<<eod
                    <td nowrap class="vert-align">
                        <button onClick="$.descargarArchivo('{$presentaciones_arr[$p]["mpp_observada_catastro_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                    </td>
                </tr>
eod;
            }
            
            $html_presentada_ttn = "";
            if(!is_null($presentaciones_arr[$p]["mpp_presentada_tasacion_fecha"])) {

                $html_presentada_ttn = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">presentada TASACIÓN</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_presentada_tasacion_fecha"])}</td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_presentada_tasacion_observaciones"]}</b></td>
                    <td nowrap class="vert-align">usuario alta: <b>{$presentaciones_arr[$p]["mpp_presentada_tasacion_usuario"]}</b></td>
                </tr>
eod;   
            }
            $html_respuesta_tasacion = "";
            if(!is_null($presentaciones_arr[$p]["mpp_respuesta_tasacion_fecha"])) {
                $html_respuesta_tasacion = <<<eod
                <tr>
                    <td nowrap class="vert-align bold">respuesta TASACIÓN</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentaciones_arr[$p]["mpp_respuesta_tasacion_fecha"])}</td>
                    <td nowrap class="vert-align">respuesta: <b>{$presentaciones_arr[$p]["mpp_respuesta_tasacion"]}</b></td>
                    <td nowrap class="vert-align">observaciones: <b>{$presentaciones_arr[$p]["mpp_respuesta_tasacion_observacion"]}</b></td>
                </tr>
eod;    
            }

            if ($presentaciones_arr[$p]["mes_id"] == 3) { //APROBADA DNV
                $html .= <<<eod
            <tr style="font-size:0.8em">
                <td colspan="2"></td>
                <td colspan="10">
                    <table style=" border: 1px solid #ccc;border-collapse: collapse">
                        {$html_aprobada_dnv}
                        {$html_presentada_ttn}
                        {$html_respuesta_tasacion}
                    </table>
                </td>
            </tr>
eod;
            } else if ($presentaciones_arr[$p]["mes_id"] == 4) { // OBSERVADA DNV
                $html .= <<<eod
                <tr style="font-size:0.8em">
                <td colspan="2"></td>
                <td colspan="10">
                    <table style=" border: 1px solid #ccc;border-collapse: collapse">
                    {$html_observada_dnv}
                    </table>
                </td>
            </tr>
eod;
            } else if ($presentaciones_arr[$p]["mes_id"] == 5) { // PRESENTADA CATASTRO
                $html .= <<<eod
                <tr style="font-size:0.8em">
                <td colspan="2"></td>
                <td colspan="10">
                    <table style=" border: 1px solid #ccc;border-collapse: collapse">
                    {$html_aprobada_dnv}
                    {$html_presentada_catastro}
                    {$html_presentada_ttn}
                    {$html_respuesta_tasacion}
                    </table>
                </td>
            </tr>
eod;
            } else if ($presentaciones_arr[$p]["mes_id"] == 6) { // APROBADA CATASTRO
                $html .= <<<eod
            <tr style="font-size:0.8em">
                <td colspan="2"></td>
                <td colspan="10">
                    <table style=" border: 1px solid #ccc;border-collapse: collapse">
                    {$html_aprobada_dnv}
                    {$html_presentada_catastro}
                    {$html_aprobada_catastro}
                    {$html_presentada_ttn}
                    {$html_respuesta_tasacion}
                    </table>
                </td>
            </tr>
eod;
            } else if ($presentaciones_arr[$p]["mes_id"] == 7) { // OBSERVADA CATASTRO
                $html .= <<<eod
                <tr style="font-size:0.8em">
                <td colspan="2"></td>
                <td colspan="10">
                    <table style=" border: 1px solid #ccc;border-collapse: collapse">
                    {$html_aprobada_dnv}
                    {$html_presentada_catastro}
                    {$html_observada_catastro}
                    {$html_presentada_ttn}
                    {$html_respuesta_tasacion}
                    </table>
                </td>
            </tr>
eod;
            }
        } //for ($p = 0; $p < count($presentaciones_arr); $p++) {

        $html .= <<<eod
            </table>
        </td>
    </tr>
eod;
    }

//---------------------------------------------------------------------------------------------------------------------
} //for ($i = 0; $i < count($parcelas_arr); $i++) {

$html .= <<<eod
    </table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
