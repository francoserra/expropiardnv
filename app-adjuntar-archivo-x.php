<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "upload-archivo") {

    $month = date("m");
    $year = date("Y");
    $dir = "$month-$year";
    if (!file_exists("archivos/" . $dir)) {
        mkdir("archivos/" . $dir, 0700);
        chmod("archivos/" . $dir, 0777);
    }
    $output_dir = "archivos/" . $dir;
    $partes_ruta = pathinfo($_FILES['myfile']['name']);
    $archivo = session_id() . date("dmYHis") . "." . $partes_ruta['extension'];
    $uploadfile = $output_dir . "/" . $archivo;
    $ret = array();
    if (isset($_FILES["myfile"])) {
        $error = $_FILES["myfile"]["error"];
        unlink($uploadfile);
        if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
            $_FILES["myfile"]["error"] = "";
            $arr["res"] = 1;
            $arr["archivo"] = $uploadfile;
            print json_encode($arr);
            exit;
        }
    }

    $arr["res"] = 0;
    $arr["msg"] = "Error al adjuntar archivo (" . $_FILES["myfile"]["error"] . ")";
    print json_encode($arr);
    exit;

}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "procesar-upload") {
    $db->ExecuteSQL("START TRANSACTION");
    $result = $db->ExecuteSQL("INSERT INTO archivos (
            arc_fuente,
            arc_id_fuente,
            cat_id,
            arc_path,
            arc_descripcion,
            fecha,
            usuario)
        VALUES(
            '" . $_REQUEST["fuente"] . "',
            " . $_REQUEST["id_fuente"] . ",
            $lstCategoriaArchivo,
            '" . $_REQUEST["archivo"] . "',
            UPPER('$txtDescripcion'),
            Now(),
            '" . $_SESSION["expropiar_usuario_login"] . "'
        )");

    if ($result == 1) {
        $arc_id=$db->returnInsertId();
        auditar($db, "archivo", "alta", $arc_id, json_encode($_REQUEST));

        if($_REQUEST["fuente"] == "PARC" && ($lstCategoriaArchivo == 23 || $lstCategoriaArchivo == 24)) {
            if($lstCategoriaArchivo == 23) {
                $columna = 'pav_permiso_paso';
            } else if($lstCategoriaArchivo == 24) {
                $columna = 'pav_notificacion_expropiacion';
            }

            $result = $db->ExecuteSQL("UPDATE parcelas_avance_relevamiento SET $columna = 1 WHERE par_id=" . $_REQUEST["id_fuente"]);
        }
        
        if($result != 1) {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "ERROR[79] al adjuntar archivo (ERR $result).";
            print json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("COMMIT");
            $arr["res"] = 1;
            $arr["msg"] = "Archivo adjuntado exitosamente.";
            print json_encode($arr);
            exit;
        }
    } else {
        $db->ExecuteSQL("ROLLBACK");
        $arr["res"] = 0;
        $arr["msg"] = "ERROR[79] al adjuntar archivo (ERR $result).";
        print json_encode($arr);
        exit;

    }

}
//---------------------------------------------------------------------------------------------------------------------
