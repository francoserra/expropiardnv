<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados

$id = encrypt_decrypt("decrypt", $_GET["data"]);

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");



?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaNuevaPresentacion">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Nueva presentación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmNuevaPresentacion" name="frmNuevaPresentacion">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipo" class="col-3 col-form-label text-right">Tipo de presentación</label>
                        <div class="col-9">
                            <select class="form-control  text-uppercase" id="lstTipo" name="lstTipo" required>
                                <option value="plano">Plano</option>
                                <option value="croquis">Croquis</option>
                                <option value="mejora">Mejora</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required" required>
                        <label for="dtFechaPresentacion" class="col-3 col-form-label text-right">Fecha presentación</label>
                        <div class="col-9">
                            <input id="dtFechaPresentacion" name="dtFechaPresentacion" class="form-control" type="date" max="<?=date('Y-m-d')?>"required value="" />
                        </div>
                    </div>

                    <div class="form-group row mb-1 required">
                        <label for="lstMedio" class="col-3 col-form-label text-right">Medio de presentación</label>
                        <div class="col-9">
                            <select class="form-control  text-uppercase" id="lstMedio" name="lstMedio" required>

                                <option value="mail">Mail</option>
                                <option value="mesa-entrada">Mesa de entrada</option>
                                <option value="otro">Otro</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtNumeroNota" class="col-3 col-form-label text-right">N° de nota&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-9">
                            <input id="txtNumeroNota" name="txtNumeroNota" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>

                    <div class="form-group row mb-1 required">
                        <label for="txtPersona" class="col-3 col-form-label text-right">Persona que presentó</label>
                        <div class="col-9">
                            <div class="input-group">
                                <input id="txtPersona" name="txtPersona" class="form-control input-fieldsX text-uppercase" type="text" pattern="\d*" value="" readonly style="background:transparent!important">
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas('txtPersona', 0)"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="uploadArchivo" class="col-3 col-form-label text-right">Archivo</label>
                        <div class="col-md-9">
                            <div id="uploadArchivo" class="btn btn-primary"><span class="fa fa-upload"></span>&nbsp;Adjuntar archivo</div>
                        </div>
                    </div>

                    <div class="form-group row mb-1 ">
                        <label for="txaObservaciones" class="col-3 col-form-label text-right">Observaciones&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-9">
                            <textarea id="txaObservaciones" name="txaObservaciones" class="form-control text-uppercase" type="text" maxlength="1000" rows="6" style="resize:none;overflow:hidden"></textarea>
                        </div>
                    </div>

                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaNuevaPresentacion').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_persona = 0;
var g_archivo = "";
//var g_archivo2 = "";
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaNuevaPresentacion').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmNuevaPresentacion #txtNumeroNota").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaNuevaPresentacion').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //---------------------------------------------------------------------
    //--ABRIMOS BUSCADOR DE PERSONAS
    $.buscarPersonas = function(campoTexto, from) {
        tabla_busqueda = 'personas_fisicas';
        filtro_busqueda1 = "per_nombre";
        filtro_busqueda2 = "per_apellido";
        campo_mostrar1 = "per_dni";
        campo_mostrar2 = "per_nombre";
        campo_mostrar3 = "per_apellido";
        $.mostrarVentanaSearch('Buscar persona', tabla_busqueda, 'per_id', filtro_busqueda1, filtro_busqueda2, campo_mostrar1, campo_mostrar2, campo_mostrar3, campoTexto);
    }
    //---------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmNuevaPresentacion #txtPersona").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val().length > 0) {
            //
            let arr = $("#txtPersona").val().split("|");
            if (arr.length > 1) {
                $("#txtPersona").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_persona = arr[0];
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmNuevaPresentacion").submit(function(e) {
        e.preventDefault();
        if (g_archivo == "") {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>Tenés que adjuntar un archivo.</span>"
            });
        } else if (g_id_persona ==0) {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>Tenés que seleccionar una persona.</span>"
            });
        }
        else {
            vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-add-mensura-x.php", {
                                id: "<?=$id?>",
                                id_persona: g_id_persona,
                                archivo: g_archivo,
                                datastring: $("#frmNuevaPresentacion").serialize()
                            },
                            function(response) {
                                $.hideLoading();
                                let json = $.parseJSON(response);
                                vex.dialog.alert({
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                                });
                                $("#mdlVentanaNuevaPresentacion").modal('hide');
                            });
                    } else {}
                }
            }) //vex.dialog.confirm({
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-- upload
    var settings = {
        url: "app-adjuntar-archivo-x.php",
        method: "POST",
        fileName: "myfile",
        uploadStr: '<i class="fa fa-paperclip"></i>&nbsp;&nbsp;click para adjuntar (max. 100MB)',
        multiple: false,
        showProgress: true,
        maxFileCount: 1,
        dragDrop: false,
        showDelete: true,
        showAbort: true,
        showError: 'pdf,dxf,doc',
        maxFileSize: 100000000,
        dynamicFormData: function() {
            var data = {
                accion: "upload-archivo",
                fuente: "<?=$fuente?>"
            };
            return data;
        },
        onSubmit: function(data, files) {
            $("#frmNuevaPresentacion #uploadArchivo").hide();
        },
        onSelect: function(files) {
            //console.log(files);//json
        },
        afterUploadAll: function() {},
        onCancel: function() {
            $("#frmNuevaPresentacion #uploadArchivo").show();
            uploadObj.reset();
            g_archivo = "";
        },
        onSuccess: function(files, data, xhr) {
            let json = $.parseJSON(data);
            if (json["res"] == 1) {
                g_archivo = json["archivo"];
                $("#frmNuevaPresentacion #uploadArchivo").hide();
            } else if (json["res"] == 0) {
                $("#frmNuevaPresentacion #uploadArchivo").show();
                uploadObj.reset();
                g_archivo = "";
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                });
            }
        },
        onError: function(files, status, errMsg) {
            $("#frmNuevaPresentacion #uploadArchivo").show();
            g_archivo = "";
            uploadObj.reset();
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + errMsg + "</span>"
            });
        },
        deleteCallback: function(data, pd) {
            g_archivo = "";
            $("#frmNuevaPresentacion #uploadArchivo").show();
            pd.statusbar.hide();
        }
    };
    var uploadObj = $("#frmNuevaPresentacion #uploadArchivo").uploadFile(settings);
});
</script>