<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT dti_id,dti_tipo FROM documento_tipos ORDER BY dti_tipo");
$documentos_tipo_arr=$db->getRows();

$db->ExecuteSQL("SELECT eci_id,eci_estado FROM estados_civiles ORDER BY eci_estado");
$estado_civiles_arr=$db->getRows();


if ($id == 0) {
    $titulo_ventana = "Nueva persona física";

    $localidad_id = 0;

} else {
   $db->ExecuteSQL("SELECT * FROM personas_fisicas WHERE per_id=$id ORDER BY per_nombre");
   $datos_arr = $db->getRows();

   $localidad_id = $datos_arr[0]["loc_id"];

   $db->ExecuteSQL("SELECT * FROM localidades WHERE loc_id=$localidad_id");
   $datos_loc = $db->getRows();
    $titulo_ventana = "Modificar persona física";

}

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaPersona">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?=$titulo_ventana?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: auto;overflow-x:hidden ">
                <div id="divVentanaModalSearch"></div>
                <form id="frmPersonaFisica" name="frmPersonaFisica">
                    <div class="form-group row mb-1 required">
                        <label for="txtNombre" class="col-3 col-form-label text-right">Nombre</label>
                        <div class="col-7">
                            <input id="txtNombre" name="txtNombre" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_nombre"]?>" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtApellido" class="col-3 col-form-label text-right">Apellido</label>
                        <div class="col-7">
                            <input id="txtApellido" name="txtApellido" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_apellido"]?>" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstSexo" class="col-3 col-form-label text-right">Género</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstSexo" name="lstSexo" required>
                                <option value=""></option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Mujer" ? "selected" : ""?> value="Mujer">Mujer</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Varón" ? "selected" : ""?> value="Varón">Varón</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Mujer Trans" ? "selected" : ""?> value="Mujer Trans">Mujer Trans</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Varón Trans" ? "selected" : ""?> value="Varón Trans">Varón Trans</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Transexual" ? "selected" : ""?> value="Transexual">Transexual</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Transgénero" ? "selected" : ""?> value="Transgénero">Transgénero</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Travesti" ? "selected" : ""?> value="Travesti">Travesti</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Lesbiana" ? "selected" : ""?> value="Lesbiana">Lesbiana</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Gay" ? "selected" : ""?> value="Gay">Gay</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Género Fluído" ? "selected" : ""?> value="Género Fluído">Género Fluído</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "No Binarie" ? "selected" : ""?> value="No Binarie">No Binarie</option>
                                <option <?=$datos_arr[0]["per_sexo"] == "Otras" ? "selected" : ""?> value="Otras">Otras</option>

                            </select>

                        </div>
                    </div>

                    <div class="form-group row mb-1 required">
                        <label for="lstTipoDocumento" class="col-3 col-form-label text-right">Tipo de Documento</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoDocumento" name="lstTipoDocumento" required>
                                <option value=""></option>
                                <?php
                                    for ($i = 0; $i < count($documentos_tipo_arr); $i++) {
                                    ?>
                                <option value="<?=$documentos_tipo_arr[$i]["dti_id"]?>" <?=$datos_arr[0]["dti_tipo"] == $documentos_tipo_arr[$i]["dti_id"] ? ' selected="selected"' : '';?>><?=$documentos_tipo_arr[$i]["dti_tipo"]?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtDocumento" class="col-3 col-form-label text-right">N° de documento</label>
                        <div class="col-7">
                            <input id="txtDocumento" name="txtDocumento" class="form-control input-fieldsX text-uppercase" type="number" min=1000000 max=99999999 step=1 title="Solo números. Minimo 7 carácteres, máximo 8." value="<?=$datos_arr[0]["per_dni"]?>" <?=$id != 0 ? 'readonly' : '';?> required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtCuil" class="col-3 col-form-label text-right">N° de CUIL</label>
                        <div class="col-7">
                            <input id="txtCuil" name="txtCuil" class="form-control input-fieldsX text-uppercase" type="text" pattern="[0-9]{11,13}" title="Solo números. Minimo 11 carácteres, máximo 13." value="<?=$datos_arr[0]["per_cuil"]?>" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="lstSexo" class="col-3 col-form-label text-right">Persona inhibida&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstInhibida" name="lstInhibida">
                                <option value="0" <?=$datos_arr[0]["per_esta_inhibida"] == 0 ? ' selected="selected"' : '';?>>No</option>
                                <option value="1" <?=$datos_arr[0]["per_esta_inhibida"] == 1 ? ' selected="selected"' : '';?>>Si</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtDomicilio" class="col-3 col-form-label text-right">Domicilio&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtDomicilio" name="txtDomicilio" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_domicilio"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtLocalidad" class="col-3 col-form-label text-right">Localidad&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <div class="input-group">
                                <input id="txtLocalidad" name="txtLocalidad" class="form-control  text-uppercase" type="text" pattern="\d*" value="<?=$datos_loc[0]["loc_nombre"] . ", " . $datos_loc[0]["loc_provincia_nombre"]?>" readonly style="background:transparent!important" required>
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" onClick="$.mostrarVentanaSearch('Buscar localidades','localidades','loc_id','loc_nombre','loc_nombre','loc_nombre','loc_departamento_nombre','loc_provincia_nombre','txtLocalidad');return false"><i class="fa fa-search"></i></button>

                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtCodigoPostal" class="col-3 col-form-label text-right">Código postal&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtCodigoPostal" name="txtCodigoPostal" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_codigo_postal"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="lstEstadoCivil" class="col-3 col-form-label text-right">Estado civil&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstEstadoCivil" name="lstEstadoCivil">
                                <option value=""></option>
                                <?php
                                    for ($i = 0; $i < count($estado_civiles_arr); $i++) {
                                    ?>
                                <option value="<?=$estado_civiles_arr[$i]["eci_id"]?>" <?=$datos_arr[0]["eci_id"] == $estado_civiles_arr[$i]["eci_id"] ? ' selected="selected"' : '';?>><?=$estado_civiles_arr[$i]["eci_estado"]?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-1">
                        <label for="dtNacimiento" class="col-3 col-form-label text-right">Fecha de nacimiento&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="dtNacimiento" name="dtNacimiento" class="form-control" type="date" value="<?=$datos_arr[0]["per_fecha_nacimiento"]?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtEmail" class="col-3 col-form-label text-right">Email&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtEmail" name="txtEmail" class="form-control input-fieldsX text-uppercase" type="email" maxlength="45" value="<?=$datos_arr[0]["per_email"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoCasa" class="col-3 col-form-label text-right">Teléfono Casa&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoCasa" name="txtTelefonoCasa" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_casa"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoLaboral" class="col-3 col-form-label text-right">Teléfono Laboral&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoLaboral" name="txtTelefonoLaboral" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_laboral"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoCelular" class="col-3 col-form-label text-right">Teléfono Celular&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoCelular" name="txtTelefonoCelular" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_celular"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="lstVive" class="col-3 col-form-label text-right">Vive&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstVive" name="lstVive">
                            <option value="" <?=$datos_arr[0]["per_vive"] == "SI" ? ' selected="selected"' : '';?>></option>
                                <option value="SI" <?=$datos_arr[0]["per_vive"] == "SI" ? ' selected="selected"' : '';?>>Si</option>
                                <option value="NO" <?=$datos_arr[0]["per_vive"] == "NO" ? ' selected="selected"' : '';?>>No</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaPersona').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = 0;
var g_name_provincia = "";
$(document).ready(function() {
    $.hideLoading();
    if ("<?=$localidad_id?>" != 0) {
        g_id_localidad = <?=$localidad_id?>;
    }
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaPersona').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmPersonaFisica #txtNombre").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaPersona').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmPersonaFisica").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-persona-fisica-x.php", {
                            id: "<?=$id?>",
                            id_localidad: g_id_localidad,
                            nombre_provincia: g_name_provincia,
                            datastring: $("#frmPersonaFisica").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            console.log(json["msg"]);
                            if (json["res"] == 1) {
                                $.clearForm("frmPersonaFisica");
                                vex.dialog.alert({
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                                });
                            } else {
                                vex.dialog.alert({
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                                });
                            }
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmPersonaFisica #txtLocalidad").focus(function(e) {
        e.preventDefault();
        if ($("#txtLocalidad").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtLocalidad").val().split("|");
            if (arr.length > 1) {
                $("#txtLocalidad").val(arr[1] + ", " + arr[2]+ ", " + arr[3]);
                g_id_localidad = arr[0];
                g_name_provincia = arr[2];
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //--Calculamos el número de cuil en base al documento ingresado
    /*$("#txtDocumento").focusout(function() {
        if ($("#txtDocumento").val().length == 8) {
            $.showLoading();
            $.post("app-add-persona-fisica-x.php", {
                    id: "<?=$id?>",
                    funcion: 'cuit',
                    documento: $("#txtDocumento").val(),
                    sexo: $("#lstSexo").val()
                },
                function(response) {
                    $.hideLoading();
                    let json = $.parseJSON(response);
                    $('#txtCuil').val(json);
                });
        }
    });*/
    //-------------------------------------------------------------------------------------------------------------------
    //-- Campos solo lectura
    $(".readonly").on('keydown paste focus mousedown', function(e) {
        if (e.keyCode != 9) // ignore tab
            e.preventDefault();
        if (e.keyCode == 13) // ignore enter
            e.preventDefault();
    });
});
</script>