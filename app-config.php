<?PHP

/**
 * BASE DE DATOS MYSQL
 */

// $PORT_DB = 3306;
// $HOST_DB = "192.168.98.128";
// $USUARIO_DB = "desa01";
// $PASSWORD_DB = "P4SSw0rd$$";
// $DATABASE_DB = "expropiar";

$PORT_DB = 3306;
$HOST_DB = "localhost";
$USUARIO_DB = "root";
$PASSWORD_DB = "";
$DATABASE_DB = "db_expropiar";



/**
 * BAase de datos SQLSERVER
 */

$PORT_SIGO_DB = 1433;
$HOST_SIGO_DB = "192.168.199.40";
$DATABASE_SIGO_DB = "sigo";
$USUARIO_SIGO_DB = "usu_expropiar";
$PASSWORD_SIGO_DB = "\$D2H9huYR5Xf";



/**
 * PERFILES DE USUARIO
 */

$ADMINISTRADOR = 1;
$OPERADOR= 2;
$OPERADOR_MENSURA=3;
$MENSURA=4;
$TASACION=5;
$CONTADURIA=6;
$CONSULTA=7;
$CONSULTA_EJECUTIVA=8;


// COMENTAR EN PROD
$PERFIL = [
    '1' => 'ADMINISTRADOR',
    '2' => 'OPERADOR',
    '3' => 'OPERADOR_MENSURA',
    '4' => 'MENSURA',
    '5' => 'TASACION',
    '6' => 'CONTADURIA',
    '7' => 'CONSULTA',
    '8' => 'CONSULTA_EJECUTIVA'
];



/**
 * FILAS X PAGINA
 */

$ITEMS_X_PAGINA = 50;

/**
 * PATH CARPETAS
 */

 $PATH_ARCHIVOS="archivos/";
