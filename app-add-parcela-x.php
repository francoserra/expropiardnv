<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <1) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$_REQUEST["id_localidad"] = $_REQUEST["id_localidad"] == "" ? 0 : $_REQUEST["id_localidad"];

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "get-resoluciones") {

    $db->ExecuteSQL("SELECT * FROM obras_resoluciones WHERE obr_id=" . $_REQUEST["obr_id"] . " AND res_eliminada=0 ORDER BY res_id DESC");
    $resoluciones_arr = $db->getRows();


    $html = <<<eod
     <select {$readonly} class="form-control  text-uppercase" id="lstNumeroResolucion" name="lstNumeroResolucion" required>
eod;
    for ($i = 0; $i < count($resoluciones_arr); $i++) {
        $selected = $resoluciones_arr[$i]["res_numero"] == $id ? "selected" : "";
        $html .= <<<eod
        <option value="{$resoluciones_arr[$i]["res_numero"]}" {$selected}>{$resoluciones_arr[$i]["res_numero"]}</option>
eod;
    }
    $html .= <<<eod
    </select>
eod;
    $arr["res"] = 1;
    $arr["html"] = $html;
    print json_encode($arr);
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "add-superficie-parcela") {

if($txtSuperficieAfectacionMts == "") {$txtSuperficieAfectacionMts=0;} 
if($txtpsup_superficie_ha == "") {$txtpsup_superficie_ha=0;} 
if($txtpsup_superficie_a == "") {$txtpsup_superficie_a=0;} 
if($txtpsup_superficie_ca == "") {$txtpsup_superficie_ca=0;} 
if($txtpsup_superficie_dm2 == "") {$txtpsup_superficie_dm2=0;} 
 

    if($_REQUEST["psup_id"] == 0){
        if($_REQUEST["par_id"] == 0){
            $session = session_id();
        } else {
            $session = "";
        }

        $sql = "INSERT INTO parcelas_superficies_afectacion
        (psup_nombre,
        psup_superficie_umedida,
        psup_afectacion,
        psup_superficie_ha,
        psup_superficie_a,
        psup_superficie_ca,
        psup_superficie_dm2,
        psup_descripcion,
        par_id,
        session,
        fecha,
        usuario)
        VALUES('$txtNombreSuperficieExpropiar',
        $lstpsup_superficie_umedida,    
        $txtSuperficieAfectacionMts,
        $txtpsup_superficie_ha,
        $txtpsup_superficie_a,
        $txtpsup_superficie_ca,
        $txtpsup_superficie_dm2,
        '$txtDescripcionPoligonoSuperficie',
        " . $_REQUEST["par_id"] . ",
        '" . $session . "',
        Now(),
        '" . $_SESSION["expropiar_usuario_nombre"] . "')"; 

        $result = $db->ExecuteSQL($sql);

        if ($result == 1) {
            auditar($db, "parcela-superficie-afectacion", "alta", $_REQUEST["id"], json_encode($_REQUEST));
            $arr["res"] = 1;
            $arr["msg"] = "Superficie afectada dada de alta con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $arr["res"] = 0;
            $arr["msg"] = "Error[62] al dar de alta superficie afectada (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    } else {
        $txtDescripcionPoligonoSuperficie = str_replace("'","\'",$txtDescripcionPoligonoSuperficie);
        $sql = "UPDATE parcelas_superficies_afectacion SET
        psup_nombre='$txtNombreSuperficieExpropiar',
        psup_superficie_umedida = $lstpsup_superficie_umedida,
        psup_afectacion = $txtSuperficieAfectacionMts,
        psup_superficie_ha = '$txtpsup_superficie_ha',
        psup_superficie_a = '$txtpsup_superficie_a',
        psup_superficie_ca = '$txtpsup_superficie_ca',
        psup_superficie_dm2 = '$txtpsup_superficie_dm2',
        psup_descripcion = '$txtDescripcionPoligonoSuperficie'
        WHERE psup_id = " . $_REQUEST["psup_id"];

        $result = $db->ExecuteSQL($sql);

        if ($result == 1) {
            auditar($db, "parcela-superficie-afectacion", "alta", $_REQUEST["id"], json_encode($_REQUEST));
            $arr["res"] = 1;
            $arr["msg"] = "Superficie afectada modificada con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $arr["res"] = 0;
            $arr["msg"] = "Error[62] al modificar superficie afectada (ERR $result).";
            echo json_encode($arr);
            exit;
        }
    }

        
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "eliminar-superficie-afectada") {
    $db->ExecuteSQL("UPDATE parcelas_superficies_afectacion SET psup_eliminado=1 WHERE psup_id = " . $_REQUEST["psup_id"]);

    if ($_REQUEST["par_id"] > 0) {
        $db->ExecuteSQL("SELECT SUM(psup_afectacion) as SUMA_TOTAL FROM parcelas_superficies_afectacion WHERE par_id = " . $_REQUEST["par_id"] . " AND psup_eliminado = 0");
        $suma_metros = $db->getRows();
        $db->ExecuteSQL("UPDATE parcelas SET par_superficie_afectacion = " . $suma_metros[0]["SUMA_TOTAL"] . " WHERE par_id = " . $_REQUEST["par_id"]);
    }

    auditar($db, "parcela-superficie-afectacion", "baja", $_REQUEST["psup_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "armar-tabla-superficies-parcela") {

    if ($_REQUEST["par_id"] > 0) {
        $filtro = " AND par_id=" . $_REQUEST["par_id"];
    } else {
        $filtro = " AND session='" . session_id() . "'";
    }

    $db->ExecuteSQL("SELECT * FROM parcelas_superficies_afectacion WHERE psup_eliminado=0 $filtro");
    $rows = $db->getRows();

    $db->ExecuteSQL("SELECT SUM(psup_afectacion) as SUMA_TOTAL FROM parcelas_superficies_afectacion WHERE psup_eliminado = 0 $filtro");
    $suma_metros = $db->getRows();

    $html = <<<eod
    <table class="table table-sm  table-striped">
    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Superficie total afectada: {$suma_metros[0]["SUMA_TOTAL"]}mt2 </mark></span></caption>
    <tr>
        <th nowrap width="1%"></th>
        <th>Nombre superficie a expropiar</th>
        <th>Superficie de afectación (m2)</th>
        <th>Descripción del polígono</th>
    </tr>
eod;
    
    for ($i = 0; $i < count($rows); $i++) {
        
        if ($_REQUEST["readonly"] == 1){
            $tools="";
        }else{
            $data3 = encrypt_decrypt("encrypt", $rows[$i]["psup_id"]);
            $data4 = encrypt_decrypt("encrypt", $rows[$i]["psup_nombre"]);

            $tools=<<<eod
            <button type="button" onClick="$.eliminarSuperficie({$rows[$i]["psup_id"]},$i);return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar superficie afectada"><i class="fas fa-trash text-secondary btn-icon"></i></button>
            <button type="button" onClick="$.editarSuperficie('{$data3}');return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar superficie afectada"><i class="far fa-edit text-secondary btn-icon"></i></button>
            <button type="button" onClick="$.cargaPoligonos('{$rows[$i]["psup_id"]}','{$rows[$i]["psup_nombre"]}');return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="Cargar Descripción del Poligono"><i class="far fa fa-map text-secondary btn-icon"></i></button>
eod;
        }
        $xpsup_descripcion = nl2br($rows[$i]["psup_descripcion"]);
        $html .= <<<eod
    <tr id="tr{$i}">
        <td nowrap class="vert-align text-right" width="1%">{$tools}</td>
        <td class="vert-align">{$rows[$i]["psup_nombre"]}</td>
        <td class="vert-align">{$rows[$i]["psup_afectacion"]}</td>
        <td class="vert-align">{$xpsup_descripcion}</td>
    </tr>
eod;
    }
    $html .= <<<eod
    </table>
eod;
    $arr["res"] = 1;
    $arr["html"] = $html;
    echo json_encode($arr);
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
//-- nuevo registro
if ($_REQUEST["id"] == 0) {

if($txtpar_superficie_ha == "") {$txtpar_superficie_ha=0;} 
if($txtpar_superficie_a == "") {$txtpar_superficie_a=0;} 
if($txtpar_superficie_ca == "") {$txtpar_superficie_ca=0;} 
if($txtpar_superficie_dm2 == "") {$txtpar_superficie_dm2=0;} 
  

    $db->ExecuteSQL("START TRANSACTION");
    $sql = "INSERT INTO parcelas
    (obr_id,
    res_numero,
    par_numero_orden,
    par_identificacion,
    par_numero_expediente_gde,
    par_identificacion_tributaria,
    par_identificacion_parcelaria,
    par_mejoras,
    par_observaciones,
    par_datos_adicionales_ubicacion,
    par_datos_normalizados_ubicacion,
    loc_id,
    par_lugar,
    par_lote_afectado,
    par_superficie_umedida, 
    par_superficie_total_inmueble,
    par_superficie_ha,
    par_superficie_a ,
    par_superficie_ca,
    par_superficie_dm2,
    par_nombre_lote_original,
    par_expediente_provincial,
    gti_id,
    usuario,
    fecha,
    par_descripcion_gravamen,
    par_registro_inmobiliario,
    par_descripcion_inmueble_titulo)
    VALUES
    ($lstObra,
    '$lstNumeroResolucion',
    $txtNumeroOrden,
    '$txtNumeroIdentificacionParcela',
    '$txtNumeroExpedienteGDE',
    '$txtNumeroIdentificacionTributaria',
    '$txtNumeroIdentificacionParcelaria',
    $lstMejoras,
    '$txaObservaciones',
    '$txaDatosAdicionalesUbicacion',
    '$txaUbicacionNormalizados',
    " . $_REQUEST["id_localidad"] . ",
    '',
    '',
    '$lstpar_superficie_umedida',
    '$txtSuperficieTotalInmueble',
    '$txtpar_superficie_ha',
    '$txtpar_superficie_a',
    '$txtpar_superficie_ca',
    '$txtpar_superficie_dm2',
    '$txtLoteNombreOriginal',
    '$txtNumeroExpedienteProvincial',
    $lstTipoGravamen,
    '" . $_SESSION["expropiar_usuario_nombre"] . "',
    Now(),
    '$txtDescripcionGravamen',
    '$lstRegistroInmobiliario',
    '$txtDescripcionSegunTitulo')";

    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $db->ExecuteSQL("ROLLBACK");
        $err = explode("|", $result)[1]; //|1111|kkwjwkdj
        if ($err == 1062) {
            $arr["msg"] = "Error[72] ya existe una parcela con ese N° de orden.";
        } else {
            $arr["msg"] = "Error[68] al dar de alta parcela (ERR $result).";
        }
        $arr["res"] = 0;
        echo json_encode($arr);
        exit;
    } else {
        $par_id = $db->returnInsertId();
        $result = $db->ExecuteSQL("INSERT INTO parcelas_avance_relevamiento
        (pav_id,
            par_id,
            par_numero_orden,
            pav_datos_registrales,
            pav_datos_catastrales,
            pav_datos_propietarios,
            pav_permiso_paso)
        VALUES(
        0,
        $par_id,
        $txtNumeroOrden,
        0,
        0,
        0,
        0)");
        if ($result == 1) {
            $db->ExecuteSQL("UPDATE registros_inmobiliarios SET par_id=$par_id WHERE session='" . session_id() . "'");
            $db->ExecuteSQL("UPDATE parcelas_superficies_afectacion SET par_id=$par_id WHERE session='" . session_id() . "'");
            $db->ExecuteSQL("UPDATE parcelas_superficies_afectacion SET session=null WHERE par_id= " . $par_id);

            $db->ExecuteSQL('SELECT SUM(psup_afectacion) as SUMA_TOTAL FROM parcelas_superficies_afectacion WHERE par_id = ' . $par_id . ' AND psup_eliminado = 0');
            $suma_metros = $db->getRows();
            $db->ExecuteSQL("UPDATE parcelas SET par_superficie_afectacion = " . $suma_metros[0]["SUMA_TOTAL"] . " WHERE par_id = " . $par_id);



            $db->ExecuteSQL("COMMIT");

            auditar($db, "parcela", "alta", $par_id, json_encode($_REQUEST));
            $arr["res"] = 1;
            $arr["msg"] = "Parcela dada de alta con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $err = explode("|", $result)[1]; //|1111|kkwjwkdj
            if ($err == 1062) {
                $arr["msg"] = "Error[102] ya existe una parcela con ese N° interno.";
            } else {
                $arr["msg"] = "Error[92] al dar de alta parcela (ERR $result).";
            }

            $arr["res"] = 0;
            echo json_encode($arr);
            exit;
        }
    }
}

//---------------------------------------------------------------------------------------------------------------------

else if ($_REQUEST["id"] > 0) {

if($txtpar_superficie_ha == "") {$txtpar_superficie_ha=0;} 
if($txtpar_superficie_a == "") {$txtpar_superficie_a=0;} 
if($txtpar_superficie_ca == "") {$txtpar_superficie_ca=0;} 
if($txtpar_superficie_dm2 == "") {$txtpar_superficie_dm2=0;} 

    $sql = "UPDATE parcelas SET
    par_numero_expediente_gde='$txtNumeroExpedienteGDE',
    par_identificacion_tributaria = '$txtNumeroIdentificacionTributaria',
    par_identificacion_parcelaria = '$txtNumeroIdentificacionParcelaria',
    par_mejoras = $lstMejoras,
    par_observaciones = '$txaObservaciones',
    par_datos_adicionales_ubicacion = '$txaDatosAdicionalesUbicacion',
    par_datos_normalizados_ubicacion = '$txaUbicacionNormalizados',
    loc_id = " . $_REQUEST["id_localidad"] . ",
    par_lugar = '<{par_lugar: }>',
    par_lote_afectado = '<{par_lote_afectado: }>',
    par_superficie_umedida = $lstpar_superficie_umedida,
    par_superficie_total_inmueble = '$txtSuperficieTotalInmueble',
    par_superficie_ha  = '$txtpar_superficie_ha',
    par_superficie_a   = '$txtpar_superficie_a',
    par_superficie_ca  = '$txtpar_superficie_ca',
    par_superficie_dm2 = '$txtpar_superficie_dm2',
    par_superficie_afectacion =  '$txtSuperficieAfectacion',
    par_nombre_lote_original =    '$txtLoteNombreOriginal',
    par_expediente_provincial =   '$txtNumeroExpedienteProvincial',
    gti_id=$lstTipoGravamen,
    usuario = '" . $_SESSION["expropiar_usuario_nombre"] . "',
    fecha = Now(),
    par_descripcion_gravamen = '$txtDescripcionGravamen',
    par_registro_inmobiliario =   '$lstRegistroInmobiliario',
    par_descripcion_inmueble_titulo = '$txtDescripcionSegunTitulo'
    WHERE par_id = " . $_REQUEST["id"];

    $result = $db->ExecuteSQL($sql);
    if ($result == 1) {
        $db->ExecuteSQL("UPDATE registros_inmobiliarios SET par_id=" . $_REQUEST["id"] . ",session=NULL WHERE session='" . session_id() . "'");
        $db->ExecuteSQL("UPDATE parcelas_superficies_afectacion SET par_id=" . $_REQUEST["id"] . ",session=NULL WHERE session='" . session_id() . "'");

        $db->ExecuteSQL('SELECT SUM(psup_afectacion) as SUMA_TOTAL FROM parcelas_superficies_afectacion WHERE par_id = ' . $_REQUEST["id"] . ' AND psup_eliminado = 0');
        $suma_metros = $db->getRows();
        $db->ExecuteSQL("UPDATE parcelas SET par_superficie_afectacion = " . $suma_metros[0]["SUMA_TOTAL"] . " WHERE par_id = " . $_REQUEST["id"]);

        $arr["res"] = 1;
        $arr["msg"] = "Parcela modificada con éxito.";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 0;
        $arr["msg"] = "Error[109] al modificar parcela (ERR $result).";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
