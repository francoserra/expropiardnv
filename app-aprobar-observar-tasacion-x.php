<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil < 2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


function setEstadoSolicitud($estado_solicitud, $estado_bandera){
    $db2 = new clsDB();
    $db2->setHost($GLOBALS["HOST_DB"]);
    $db2->setUsername($GLOBALS["USUARIO_DB"]);
    $db2->setPassword($GLOBALS["PASSWORD_DB"]);
    $db2->setDatabase($GLOBALS["DATABASE_DB"]);
    $db2->setDebugmode(false);
    $db2->openDB();
    $db2->ExecuteSQL("SET NAMES 'utf8'");

    if(!is_null($estado_bandera)){
        if($estado_solicitud == 5){
            $detalles_sol = 1;
        } else {
            //BUSCO DETALLES EN ESTADO 1 O 4
            $sql = "SELECT * FROM tasaciones_solicitudes_detalle WHERE ppt_id=" . $_REQUEST["ppt_id"] . " AND (pptdet_estado=".$estado_bandera . " OR pptdet_estado=5)";
            $db2->ExecuteSQL($sql);
            $detalles_sol = count($db2->getRows());
        }
    } else {
        $detalles_sol = 1;
    }

    if($detalles_sol == 1){
        //---------------------------------------------
        // CAMBIO EL ESTADO DE LA SOLICITUD GENERAL
        $result = $db2->ExecuteSQL("UPDATE tasaciones_solicitudes
        SET
            ppt_estado=$estado_solicitud
        WHERE
            ppt_id=" . $_REQUEST["ppt_id"]);
        
        if($result == 1) {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "upload-archivo") {

    $month = date("m");
    $year = date("Y");
    $dir = "$month-$year";
    if (!file_exists("archivos/" . $dir)) {
        mkdir("archivos/" . $dir, 0700);
        chmod("archivos/" . $dir, 0777);
    }
    $output_dir = "archivos/" . $dir;
    $partes_ruta = pathinfo($_FILES['myfile']['name']);
    $archivo = session_id() . date("dmYHis") . "." . $partes_ruta['extension'];
    $uploadfile = $output_dir . "/" . $archivo;
    $ret = array();
    if (isset($_FILES["myfile"])) {
        $error = $_FILES["myfile"]["error"];
        unlink($uploadfile);
        if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
            $_FILES["myfile"]["error"] = "";
            $arr["res"] = 1;
            $arr["archivo"] = $uploadfile;
            print json_encode($arr);
            exit;
        }
    }

    $arr["res"] = 0;
    $arr["msg"] = "Error al adjuntar archivo (" . $_FILES["myfile"]["error"] . ")";
    print json_encode($arr);
    exit;

}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "observar") {
    $db->ExecuteSQL("SELECT mpp_id FROM tasaciones_solicitudes_detalle WHERE pptdet_id=" . $_REQUEST["pptdet_id"]);
    $detalle_mmp = $db->getRows();

    if ($_REQUEST["ente"] == "DNV") {
        $db->ExecuteSQL("START TRANSACTION");
        $result = $db->ExecuteSQL("UPDATE tasaciones_solicitudes_detalle
SET
    pptdet_estado=2 /*observada dnv*/,
    pptdet_observada_dnv_observacion=UPPER('$txaObservaciones'),
    pptdet_observada_dnv_fecha='" . $dtFechaPresentacion . "',
    pptdet_observada_dnv_motivo='$lstMotivo',
    pptdet_observada_dnv_archivo='" . $_REQUEST["archivo"] . "',
    pptdet_observada_dnv_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    pptdet_id=" . $_REQUEST["pptdet_id"]);
        if ($result == 1) {
            if(setEstadoSolicitud(2, 1)){
                $db->ExecuteSQL("UPDATE mensura_presentaciones SET mpp_respuesta_tasacion='OBSERVADA TASACION DNV', mpp_respuesta_tasacion_fecha=Now(), mpp_respuesta_tasacion_observacion='" . $txaObservaciones . "', mpp_respuesta_tasacion_archivo='" . $_REQUEST["archivo"] . "', mpp_respuesta_tasacion_usuario='" . $_SESSION["expropiar_usuario_login"] . "' WHERE mpp_id=" . $detalle_mmp[0]["mpp_id"]);
                $db->ExecuteSQL("UPDATE parcelas SET par_tasacion_habilitada = 2 WHERE par_id=" . $_REQUEST["par_id"]);
                $db->ExecuteSQL("COMMIT");
                $arr["res"] = 1;
                $arr["msg"] = "Tasación de presentación observada con éxito.";
                echo json_encode($arr);
                exit;
            } else {
                $db->ExecuteSQL("ROLLBACK");
                $arr["res"] = 0;
                $arr["msg"] = "Error[50] al observar tasacíon de presentación (ERR $result).";
                echo json_encode($arr);
                exit;
            }
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar tasacíon de presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    } else if ($_REQUEST["ente"] == "TTN") {
        $db->ExecuteSQL("START TRANSACTION");
        $result = $db->ExecuteSQL("UPDATE tasaciones_solicitudes_detalle
SET
    pptdet_estado=5 /*observada TTN*/,
    pptdet_observada_ttn_observacion=UPPER('$txaObservaciones'),
    pptdet_observada_ttn_fecha='" . $dtFechaPresentacion . "',
    pptdet_observada_ttn_motivo='$lstMotivo',
    pptdet_observada_ttn_archivo='" . $_REQUEST["archivo"] . "',
    pptdet_observada_ttn_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    pptdet_id=" . $_REQUEST["pptdet_id"]);
        if ($result == 1) {
            if(setEstadoSolicitud(5, 4)){
                $db->ExecuteSQL("UPDATE mensura_presentaciones SET mpp_respuesta_tasacion='OBSERVADA TASACION TTN', mpp_respuesta_tasacion_fecha=Now(), mpp_respuesta_tasacion_observacion='" . $txaObservaciones . "', mpp_respuesta_tasacion_archivo='" . $_REQUEST["archivo"] . "',mpp_respuesta_tasacion_usuario='" . $_SESSION["expropiar_usuario_login"] . "' WHERE mpp_id=" . $detalle_mmp[0]["mpp_id"]);
                $db->ExecuteSQL("COMMIT");
                $arr["res"] = 1;
                $arr["msg"] = "Tasación de presentacion observada con éxito.";
                echo json_encode($arr);
                exit;
            } else {
                $db->ExecuteSQL("ROLLBACK");
                $arr["res"] = 0;
                $arr["msg"] = "Error[50] al observar tasacíon de presentación (ERR $result).";
                echo json_encode($arr);
                exit;
            }
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar tasacíon de presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
    //----------------------------------------------
} else if ($_REQUEST["accion"] == "aprobar") {
    $db->ExecuteSQL("SELECT mpp_id FROM tasaciones_solicitudes_detalle WHERE pptdet_id=" . $_REQUEST["pptdet_id"]);
    $detalle_mmp = $db->getRows();

    if ($_REQUEST["ente"] == "DNV") {
        $db->ExecuteSQL("START TRANSACTION");
        $result = $db->ExecuteSQL("UPDATE tasaciones_solicitudes_detalle
SET
    pptdet_estado=3 /*aprobada dnv*/,
    pptdet_aprobada_dnv_observacion=UPPER('$txaObservaciones'),
    pptdet_aprobada_dnv_fecha='" . $dtFechaPresentacion . "',
    pptdet_aprobada_dnv_archivo='" . $_REQUEST["archivo"] . "',
    pptdet_aprobada_dnv_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    pptdet_id=" . $_REQUEST["pptdet_id"]);
        if ($result == 1) {
            if(setEstadoSolicitud(3, 1)){
                $db->ExecuteSQL("UPDATE mensura_presentaciones SET mpp_respuesta_tasacion='APROBADA TASACION DNV', mpp_respuesta_tasacion_fecha=Now(), mpp_respuesta_tasacion_observacion='" . $txaObservaciones . "', mpp_respuesta_tasacion_archivo='" . $_REQUEST["archivo"] . "',mpp_respuesta_tasacion_usuario='" . $_SESSION["expropiar_usuario_login"] . "' WHERE mpp_id=" . $detalle_mmp[0]["mpp_id"]);
                $db->ExecuteSQL("COMMIT");
                $arr["res"] = 1;
                $arr["msg"] = "Tasación de presentación aprobada con éxito.";
                echo json_encode($arr);
                exit;
            } else {
                $db->ExecuteSQL("ROLLBACK");
                $arr["res"] = 0;
                $arr["msg"] = "Error[50] al aprobaar tasacíon de presentación (ERR $result).";
                echo json_encode($arr);
                exit;
            }
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al aprobaar tasacíon de presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }
        
    } else if ($_REQUEST["ente"] == "TTN") {
        $db->ExecuteSQL("START TRANSACTION");
        $result = $db->ExecuteSQL("UPDATE tasaciones_solicitudes_detalle
SET
    pptdet_estado=6 /*aprobada TTN*/,
    pptdet_aprobada_ttn_observacion=UPPER('$txaObservaciones'),
    pptdet_aprobada_ttn_fecha='" . $dtFechaPresentacion . "',
    pptdet_aprobada_ttn_archivo='" . $_REQUEST["archivo"] . "',
    pptdet_aprobada_ttn_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    pptdet_id=" . $_REQUEST["pptdet_id"]);
        if ($result == 1) {
            if(setEstadoSolicitud(6, 4)){
                $db->ExecuteSQL("UPDATE mensura_presentaciones SET mpp_respuesta_tasacion='APROBADA TASACION TTN', mpp_respuesta_tasacion_fecha=Now(), mpp_respuesta_tasacion_observacion='" . $txaObservaciones . "', mpp_respuesta_tasacion_archivo='" . $_REQUEST["archivo"] . "',mpp_respuesta_tasacion_usuario='" . $_SESSION["expropiar_usuario_login"] . "' WHERE mpp_id=" . $detalle_mmp[0]["mpp_id"]);
                $db->ExecuteSQL("COMMIT");
                $arr["res"] = 1;
                $arr["msg"] = "Tasación de presentación aprobada con éxito.";
                echo json_encode($arr);
                exit;
            } else {
                $db->ExecuteSQL("ROLLBACK");
                $arr["res"] = 0;
                $arr["msg"] = "Error[50] al aprobaar tasacíon de presentación (ERR $result).";
                echo json_encode($arr);
                exit;
            }
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al aprobaar tasacíon de presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
} else if ($_REQUEST["accion"] == "presentar") {
    if ($_REQUEST["ente"] == "TTN") {
        $db->ExecuteSQL("START TRANSACTION");
        
        if(setEstadoSolicitud(4, null)){
            //BUSCO LOS DETALLES DE LA SOLICITUD
            $db->ExecuteSQL("SELECT * FROM tasaciones_solicitudes_detalle WHERE ppt_id=" . $_REQUEST["ppt_id"]);
            $detalle_solicitud_arr = $db->getRows();

            for ($i=0; $i < count($detalle_solicitud_arr); $i++) { 
                $result_det = $db->ExecuteSQL("UPDATE tasaciones_solicitudes_detalle
                    SET
                        pptdet_estado=4 /*presentada TTN*/,
                        pptdet_presentada_ttn_observacion=UPPER('$txaObservaciones'),
                        pptdet_presentada_ttn_fecha='" . $dtFechaPresentacion . "',
                        pptdet_presentada_ttn_archivo='" . $_REQUEST["archivo"] . "',
                        pptdet_presentada_ttn_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
                    WHERE
                        pptdet_id=" . $detalle_solicitud_arr[$i]["pptdet_id"]);
                
                if($result_det != 1){
                    $db->ExecuteSQL("ROLLBACK");
                    $arr["res"] = 0;
                    $arr["msg"] = "Error[50] al observar presentar a TTN (ERR $result_det).";
                    echo json_encode($arr);
                    exit;
                }
            }
            $db->ExecuteSQL("COMMIT");
            $arr["res"] = 1;
            $arr["msg"] = "Presentación a TTN realizada con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentar a TTN (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
}
