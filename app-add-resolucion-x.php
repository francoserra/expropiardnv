<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}if ($perfil_readonly) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------

$result = $db->ExecuteSQL("INSERT INTO obras_resoluciones
(res_id,
obr_id,
res_numero,
res_observaciones,
fecha,
usuario)
VALUES
(0,
".$_REQUEST["id"].",
'$txtResolucion',
'',
Now(),
'" . $_SESSION["expropiar_usuario_login"] . "')");

if ($result==1) {
    auditar($db, "resolución", "alta", $_REQUEST["id"], json_encode($_REQUEST));
    $arr["res"] = 1;
    $arr["msg"] = "Resolución dada de alta con éxito.";
    echo json_encode($arr);
    exit;
} else {
    $arr["res"] = 0;
    $arr["msg"] = "Error[52] al dar de alta resolución (ERR $result).";
    echo json_encode($arr);
    exit;
}
