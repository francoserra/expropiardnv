<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- nuevo registro
if($_REQUEST["accion"] == 'insert') {
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA REPRESENTANTES
    //------------------------------------------------------------------------------------------------------------------
    $sql = "INSERT INTO personas_datos_financieros
    ($txtColumna,
    pdf_cut_numero,
    pdf_cut_estado,
    pdf_banco,
    pdf_cbu,
    pdf_tipo_cuenta,
    pdf_tipo_moneda)
    VALUES
    ('$txtCuit',
    '$txtCutNumero',
    $lstEstadoCut,
    UPPER('$txtBanco'),
    '$txtCbu',
    '$lstTipoCuenta',
    '$lstTipoMoneda')";
    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al asignar dato financiero (ERR $result).";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        $arr["msg"] = "Dato financiero asignado con éxito.";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if($_REQUEST["accion"] == 'tipo_representacion') {
    $tipo = $_REQUEST["tipo_rep"];
    $db->ExecuteSQL("SELECT * FROM representacion_tipo WHERE rti_tipo = '$tipo' ORDER BY rti_tipo");
    $representacion_tipos = $db->getRows();
    
    for ($i=0; $i < count($representacion_tipos); $i++) { 
        echo '<option value="'.$representacion_tipos[$i]['rti_id'].'">'.$representacion_tipos[$i]['rti_representacion'].'</option>';
    }
}

