<?php
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function checkAutorizacion($perfil, $script)
{
    //--
    $arr = array();

    $db = new clsDB();
    $db->setHost($GLOBALS["HOST_DB"]);
    $db->setUsername($GLOBALS["USUARIO_DB"]);
    $db->setPassword($GLOBALS["PASSWORD_DB"]);
    $db->setDatabase($GLOBALS["DATABASE_DB"]);
    $db->setDebugmode(false);
    $db->openDB();

    $script = str_replace("-x", "", $script);
    //-- chequeo acceso al modulo
    $sql = "SELECT * FROM autorizacion_sistema
	WHERE
    $perfil > 0
	AND aut_script IS NOT NULL
    AND ('$script' LIKE CONCAT(aut_script, '%'))
	    OR
        (aut_script_abm IS NOT NULL AND aut_script_abm LIKE CONCAT('%','$script', '%'))";
    $db->ExecuteSQL($sql);
    $row = $db->getRows();
    if ($row[0]["$perfil"] == 0) {
        return 0;
    } else if ($row[0]["$perfil"] == 1) {
        return 1;
    } else if ($row[0]["$perfil"] == 2) {
        return 2;
    } else if ($row[0]["$perfil"] == 3) {
        return 3;
    } else {
        return null;
    }

}
//---------------------------------------------------------------------------------------------------------------------

function getQRCode($texto, $archivopng)
{
    unlink($archivopng);
    QRcode::png($texto, $archivopng, QR_ECLEVEL_L, 3);
    if (file_exists($archivopng)) {
        return true;
    } else {
        return false;
    }

}

//---------------------------------------------------------------------------------------------------------------------

function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2)
{
    // Cálculo de la distancia en grados
    $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

    // Conversión de la distancia en grados a la unidad escogida (kilómetros, millas o millas naúticas)
    switch ($unit) {
        case 'km':
            $distance = $degrees * 111.13384; // 1 grado = 111.13384 km, basándose en el diametro promedio de la Tierra (12.735 km)
            break;
        case 'mi':
            $distance = $degrees * 69.05482; // 1 grado = 69.05482 millas, basándose en el diametro promedio de la Tierra (7.913,1 millas)
            break;
        case 'nmi':
            $distance = $degrees * 59.97662; // 1 grado = 59.97662 millas naúticas, basándose en el diametro promedio de la Tierra (6,876.3 millas naúticas)
    }
    return round($distance, $decimals);
}

//---------------------------------------------------------------------------------------------------------------------

function encodeJWTToken($usuario, $password, $aplicacion, $key)
{

    $token = array(
        "iss" => $aplicacion, //entifica a quien creo el JWT
        "aud" => $aplicacion, //Identifica quien se supone que va a recibir el JWT. Un ejemplo puede ser web, android o ios. Quien use un JWT con este campo debe además de usar el JWT
        "iat" => time(), // Indica cuando fue creado el JWT
        "nbf" => time(), // Indica desde que momento se va a empezar a aceptar un JWT
        "usr" => $usuario,
        "pwd" => $password,
    );

/**
 * IMPORTANT:
 * You must specify supported algorithms for your application. See
 * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
 * for a list of spec-compliant algorithms.
 */
    $jwt = JWT::encode($token, $key);
    return $jwt;
}

//---------------------------------------------------------------------------------------------------------------------

function decodeJWTToken($token, $key)
{
    $decoded = JWT::decode($token, $key, array('HS256'));
    return $decoded;
}

//---------------------------------------------------------------------------------------------------------------------

function checkAccesoAD($usuario_ad, $password_ad)
{
    //-- le peganos a un web service de siper que dado el usuario lo valida y devuelve los grupos al cual pertenece
    $session = curl_init("https://siper.vialidad.gob.ar/go/api/siper-api.php");
    if (!$session) {
        return null;
    }
    curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($session, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($session, CURLOPT_TIMEOUT, 30);
    curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
    $headers = array(
        'Content-Type:application/json',
        'Accept:application/json',
        'Authorization: Basic ' . base64_encode("siper:S1p3r"),
    );
    curl_setopt($session, CURLOPT_HTTPHEADER, $headers);
    $datos_agente_array = array(
        "operation" => "ad-login",
        "usuario_ad" => "$usuario_ad@vialidad.gob.ar",
        "password_ad" => $password_ad,
    );
    curl_setopt($session, CURLOPT_POSTFIELDS, json_encode($datos_agente_array));
    $json = curl_exec($session);
    curl_close($session);
    $datos_agente_arr = json_decode($json, true);
    return $datos_agente_arr;
}

//---------------------------------------------------------------------------------------------------------------------

function auditar($db, $aud_objeto, $aud_accion, $aud_id_objeto, $aud_data = "")
{
    $result = $db->ExecuteSQL("INSERT INTO auditoria 
        (aud_id,
        aud_objeto,
        aud_accion,
        aud_id_objeto,
        aud_data,
        usuario,
        fecha)
        VALUES(
        0,
        '$aud_objeto',
        '$aud_accion',
        '$aud_id_objeto',
        '$aud_data',
        '" . $_SESSION["expropiar_usuario_login"] . "',
        Now())");

    return $result;
}
//---------------------------------------------------------------------------------------------------------------------
function getDatosSigo($db, $id_sigo)
{

  /*  if ($id_sigo == "") {
        $db->ExecuteSQL("dbo.get_obras_expropiar");
    } else {*/
        $db->ExecuteSQL("dbo.get_obras_expropiar @nro_obra=$id_sigo");
   // }

    return $db->getRows();

}
//---------------------------------------------------------------------------------------------------------------------
