<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id_relacion = $arr[0];
$propietario_nombre = $arr[1];
$propietario_id = $arr[2];
$tipo_propietario = $arr[3];

//-- chequeo que usuario tenga permisos 
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil<2) {;exit;}


$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAsignarRepresentantePropietario">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Representantes del propietario <?php echo($propietario_nombre) ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div id="divVentanaModalSearch"></div>
                <form id="frmAsignarRepresentantePropietario" name="frmAsignarRepresentantePropietario">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoPersona" class="col-3 col-form-label text-right">Tipo de persona</label>
                        <div class="col-5">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoPersona" name="lstTipoPersona" required>
                                <option value=""></option>
                                <option value="PF">Persona Física</option>
                                <option value="PJ">Persona Jurídica</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoRepresentante" class="col-3 col-form-label text-right">Tipo de representante</label>
                        <div class="col-5">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoRepresentante" name="lstTipoRepresentante" required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtRepresentantePropietario" class="col-3 col-form-label text-right">Persona</label>
                        <div class="col-9">
                            <div class="input-group">
                                <input id="txtRepresentantePropietario" name="txtRepresentantePropietario" class="form-control  text-uppercase" type="text" pattern="\d*" value="" readonly style="background:transparent!important">
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas('txtRepresentantePropietario', 0)" ;return false><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="personaFirmante">
                        <div class="form-group row mb-1 required">
                            <label for="txtPersonaFirmante" class="col-3 col-form-label text-right">Persona Firmante</label>
                            <div class="col-9">
                                <div class="input-group">
                                    <input id="txtPersonaFirmante" name="txtPersonaFirmante" class="form-control  text-uppercase" type="text" pattern="\d*" value="" readonly style="background:transparent!important">
                                    <div>
                                        &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas('txtPersonaFirmante', 1)" ;return false><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-1 mt-2">
                        <div class="col-12 text-right">
                                <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary"><i class="fas fa-check"></i>&nbsp;&nbsp;Agregar representante</button>
                        </div>
                    </div>
                </form>
                <hr>
                <div class="container" id="listaRepresentantesPropietario"></div>
                <hr>
                <div class="form-group row mb-1">
                    <div class="col-12 text-right">
                        <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAsignarRepresentantePropietario').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var tipo = ""; 
var g_archivo = "";
var g_id_persona = 0;
var g_id_persona_firmante = 0;
$(document).ready(function() {
    $('#personaFirmante').hide()
    $.hideLoading();
    $(".modal").on("hidden.bs.modal", function() {
        //
    });
    $('#mdlVentanaAdjuntarArchivo').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmAdjuntarArchivo #lstTipoArchivo").focus();
        }, 250);
    });

    //---------------------------------------------------------------------
    //--BUSCAMOS REPRESENTANTES DE LA PERSONA
    $.buscarRepresentantesPersona = function() {
        $.showLoading();
        $.post("app-asignar-representante-propietario-x.php", {
            accion: 'get-representantes-propietario',
            id_relacion: "<?=$id_relacion?>",
        },
        function(response) {
            $.hideLoading();
            $('#listaRepresentantesPropietario').html(response);
        });
    } 

    $.buscarRepresentantesPersona();

    //---------------------------------------------------------------------------------------------------------------------
    //-- eliminar representante propietario parcela -------------------------------------------
    $.eliminarRepresentantePropietario = function(pprep_id, $fila) {
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-asignar-representante-propietario-x.php", {
                        accion: "delete-representante-propietario",
                        pprep_id: pprep_id
                    },
                    function(response) {
                        $.hideLoading();
                        $("#trRepresentantePropietario" + $fila).fadeOut();
                    });
                }
            }
        })
    };

    //------------------------------------------table--------------------------
    //--ABRIMOS BUSCADOR DE PERSONAS
    $.buscarPersonas = function(campoTexto, from) {
        var tabla_busqueda = "";
        var filtro_busqueda1 = "";
        var filtro_busqueda2 = "";
        if(from == 0){
            if ($('#lstTipoPersona').val() == 'PF') {
                tabla_busqueda = 'personas_fisicas';
                filtro_busqueda1 = "per_nombre";
                filtro_busqueda2 = "per_apellido";
                campo_mostrar1 = "per_dni";
                campo_mostrar2 = "per_nombre";
                campo_mostrar3 = "per_apellido";
            } else {
                tabla_busqueda = 'personas_juridicas';
                campo_mostrar1 = "per_cuit";
                campo_mostrar2 = "per_razon_social";
                campo_mostrar3 ="";
                filtro_busqueda1 = "per_cuit";
                filtro_busqueda2 = "per_razon_social";
            }
        } else {
            tabla_busqueda = 'personas_fisicas';
            filtro_busqueda2 = "per_apellido";
            filtro_busqueda1 = "per_nombre";
            campo_mostrar1 = "per_dni";
            campo_mostrar2 = "per_nombre";
            campo_mostrar3 = "per_apellido";
        }
    
        $.mostrarVentanaSearch('Buscar persona', tabla_busqueda, 'per_id', filtro_busqueda1, filtro_busqueda2, campo_mostrar1, campo_mostrar2, campo_mostrar3, campoTexto);
    } 

    //---------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmAsignarRepresentantePropietario #txtRepresentantePropietario").focus(function(e) {
        e.preventDefault();
        if ($("#txtRepresentantePropietario").val().length > 0) {
            //  1234|JUAN|PEREZ
            let arr = $("#txtRepresentantePropietario").val().split("|");
            if (arr.length > 1) {
                $("#txtRepresentantePropietario").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_persona = arr[0];
            }
        }
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-- procesamos los datos de la persona firmante
    $("#frmAsignarRepresentantePropietario #txtPersonaFirmante").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersonaFirmante").val().length > 0) {
            //  1234|JUAN|PEREZ
            let arr = $("#txtPersonaFirmante").val().split("|");
            if (arr.length > 1) {
                $("#txtPersonaFirmante").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_persona_firmante = arr[0];
            }
        }
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //--buscamos tipos de representantes segun tipo de persona
    $("#lstTipoPersona").change(function () {  
        if($("#lstTipoPersona").val() == 'PF') {
            tipo = 'PFIS';
            $('#personaFirmante').hide();
            g_id_persona_firmante = 0;
        } else {
            tipo = 'PJUR';
            $('#personaFirmante').show();
        }
        $.showLoading();
        $.post("app-asignar-representante-propietario-x.php", {
            accion: 'tipo_representacion',
            tipo_rep: tipo,
        },
        function(response) {
            $.hideLoading();
            $('#lstTipoRepresentante').html(response);
        });
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmAsignarRepresentantePropietario").submit(function(e) {
        e.preventDefault();
        if($('#txtRepresentantePropietario').val() != ""){
            if($("#lstTipoPersona").val() == 'PF' || ($("#lstTipoPersona").val() == 'PJ' && g_id_persona_firmante != 0)){
                vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-asignar-representante-propietario-x.php", {
                                id_relacion: "<?=$id_relacion?>",
                                id_persona:g_id_persona,
                                tipo_persona_representante:$("#lstTipoPersona").val(),
                                id_persona_firmante:g_id_persona_firmante,
                                id_propietario:"<?=$propietario_id?>",
                                tipo_persona_propietario:"<?=$tipo_propietario?>",
                                accion: 'insert',
                                datastring: $("#frmAsignarRepresentantePropietario").serialize()
                            },
                            function(response) {
                                $.hideLoading();
                                let json = $.parseJSON(response);

                                if(json["res"] == 1){
                                    $.buscarRepresentantesPersona();
                                }
                                vex.dialog.alert({
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;RESULTADO</b></big><br><br>" + json["msg"] + "</span>"
                                });
                            });
                    } else {}
                }
            })
            }
        }
    });
});
</script>