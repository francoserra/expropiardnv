<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//if ($perfil_readonly) {exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");



?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAltaResolucion">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Nueva resolución</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="frmAltaResolucion" name="frmAltaResolucion">
              <!--  <div class="form-group row mb-1 required">
                        <label for="lstObra" class="col-3 col-form-label text-right">Obra</label>
                        <div class="col-9">
                            <select readonly class="form-control  text-uppercase" id="lstObra" name="lstObra" required>
                                
                                <?php
                                    for ($i = 0; $i < count($obras_arr); $i++) {
                                    ?>
                                <option value="<?=$obras_arr[$i]["obr_id"]?>"><?=$obras_arr[$i]["obr_nombre"]?></option>
                                <?php
                                    }
                                    ?>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group row mb-1 required">
                        <label for="txtResolucion" class="col-3 col-form-label text-right">Resolución</label>
                        <div class="col-9">
                            <input style="background: transparent !important" id="txtResolucion" name="txtResolucion" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>

                    <div class="form-group row mb-1 ">
                        <label for="txaObservaciones" class="col-3 col-form-label text-right">Observaciones&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-9">
                            <textarea id="txaObservaciones" name="txaObservaciones" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow:hidden"></textarea>
                        </div>
                    </div>

                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAltaResolucion').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = 0;
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaAltaResolucion').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmAltaResolucion #txtResolucion").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaAltaResolucion').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmAltaResolucion").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-resolucion-x.php", {
                            id: "<?=$id?>",
                            datastring: $("#frmAltaResolucion").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $("#mdlVentanaAltaResolucion").modal('hide');
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
});
</script>