<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


$dtNacimiento=$dtNacimiento==""?"NULL":"'$dtNacimiento'";

//---------------------------------------------------------------------------------------------------------------------

//-- nuevo registro
if ($_REQUEST["id"] == 0) {
    //------------------------------------------------------------------------------------------------------------------
    //-- CALCULA EL CUIL DE LA PERSONA SEGUN DOCUMENTO Y DEVUELVE EL VALOR A LA VISTA PARA MOSTRAR EN CAMPO
    if(isset($_REQUEST['funcion'])) {
        if($_REQUEST['funcion'] == 'cuit') {
            $cuilPersona = dni2cuil($_REQUEST['documento'], $_REQUEST['sexo']); //calculo numero de cuil mediante dni
            echo json_encode($cuilPersona);
            return;
        }
    }

    //BUSCAMOS EL ID DE LA PROVINCIA
    $id_provincia = getProvincia($_REQUEST["nombre_provincia"], 'nombre');

    if(!isset($lstInhibida)) {
        $lstInhibida = 0;
    }

    if(!isset($lstEstadoCivil) || $lstEstadoCivil == '') {  
        $lstEstadoCivil = 0;
    } else {
        $lstEstadoCivil = intval($lstEstadoCivil);
    }
    // var_dump($lstEstadoCivil);exit;
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA PERSONAS FISICAS
    //------------------------------------------------------------------------------------------------------------------

    $sql = "INSERT INTO personas_fisicas
    (per_dni,
    per_cuil,
    dti_tipo,
    per_sexo,
    per_nombre,
    per_apellido,
    per_domicilio,
    loc_id,
    per_codigo_postal,
    eci_id,
    per_fecha_nacimiento,
    per_telefono_casa,
    per_telefono_laboral,
    per_telefono_celular,
    per_email,
    per_esta_inhibida,
    per_vive)
    VALUES
    ($txtDocumento,
    '$txtCuil',
    $lstTipoDocumento,
    '$lstSexo',
    UPPER('$txtNombre'),
    UPPER('$txtApellido'),
    UPPER('$txtDomicilio'),
    " . $_REQUEST["id_localidad"] . ",
    '$txtCodigoPostal',
    '$lstEstadoCivil',
    $dtNacimiento,
    '$txtTelefonoCasa',
    '$txtTelefonoLaboral',
    '$txtTelefonoCelular',
    LOWER('$txtEmail'),
    $lstInhibida,
    '$lstVive')";
    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al dar de alta la persona (ERR $result)." ;
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        $arr["msg"] = "Persona dada de alta con éxito.";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------

else if ($_REQUEST["id"] > 0) {
    if(!isset($lstInhibida)) {
        $lstInhibida = 0;
    }

    if(!isset($lstEstadoCivil) || $lstEstadoCivil == '') {  
        $lstEstadoCivil = 0;
    } else {
        $lstEstadoCivil = intval($lstEstadoCivil);
    }
    
    $sql = "UPDATE personas_fisicas
    SET 
    per_dni = $txtDocumento,
    per_cuil = '$txtCuil',
    dti_tipo = $lstTipoDocumento,
    per_sexo = '$lstSexo',
    per_nombre = UPPER('$txtNombre'),
    per_apellido = UPPER('$txtApellido'),
    per_domicilio =  UPPER('$txtDomicilio'),
    loc_id = " . $_REQUEST["id_localidad"] . ",
    per_codigo_postal = '$txtCodigoPostal',
    eci_id = '$lstEstadoCivil',
    per_fecha_nacimiento = $dtNacimiento,
    per_telefono_casa = '$txtTelefonoCasa',
    per_telefono_laboral = '$txtTelefonoLaboral',
    per_telefono_celular = '$txtTelefonoCelular',
    per_email = LOWER('$txtEmail'),
    per_esta_inhibida = $lstInhibida,
    per_vive='$lstVive'
    WHERE per_id = " . $_REQUEST["id"]; 

    $result = $db->ExecuteSQL($sql);
    if ($result == 1) {
        $arr["res"] = 1;
        $arr["msg"] = "Persona modificada con éxito.";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 0;
        $arr["msg"] = "Error[109] al modificar persona (ERR $result). $sql";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
