<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

// //-- los parametros que se le pasa a la ventana vienen encriptados
// $arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
// $id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");
?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaParcelaActividad">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Selección de parcela</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmParcela" name="frmParcela">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoBusqueda" class="col-4 col-form-label text-right">Tipo de busqueda</label>
                        <div class="col-7">
                            <select class="form-control  text-uppercase" id="lstTipoBusqueda" name="lstTipoBusqueda" required>
                                <option selected value="numero">Por n° de orden</option>
                                <option value="obra">Por obra</option>
                                <option value="propietario">Por propietario</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required" id="numero_parcela">
                        <label for="txtNumeroParcelaBusqueda" class="col-4 col-form-label text-right">N° de orden</label>
                        <div class="col-7">
                            <div class="input-group">
                                <input id="txtNumeroParcelaBusqueda" name="txtNumeroParcelaBusqueda" class="form-control  text-uppercase" type="text" maxlength="45" value="" required>
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary " onClick="$.buscarParcelaNumero();return false"><i class="fa fa-check"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="" id="obra_parcela" style="display:none;">
                        <div class="form-group row mb-1 required">
                            <label for="lstListaObras" class="col-4 col-form-label text-right">Lista de obras</label>
                            <div class="col-7">
                                <div class="input-group">
                                    <select class="form-control text-uppercase" id="lstListaObras" name="lstListaObras" required></select>
                                    <div>
                                        &nbsp;<button type="button" class="btn btn-primary " onClick="$.buscarParcelaObra();return false"><i class="fa fa-check"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="propietario_parcela" style="display:none;">
                        <div class="form-group row mb-1 required">
                            <label for="lstTipoPersona" class="col-4 col-form-label text-right">Tipo persona propietario</label>
                            <div class="col-7">
                                <select class="form-control input-fieldsX text-uppercase" id="lstTipoPersona" name="lstTipoPersona">
                                    <option value="personas_fisicas">PERSONA FÍSICA</option>
                                    <option value="personas_juridicas">PERSONA JURÍDICA</option>
                                </select>
                            </div>
                        </div>
                   
                    <div class="form-group row mb-1 required" >
                        <label for="txtPersona" class="col-4 col-form-label text-right">Propietario</label>
                        <div class="col-7">
                            <div class="input-group">
                                <input id="txtPersona" name="txtPersona" class="form-control  text-uppercase" type="text" pattern="\d*" value="<?=$datos_loc[0]["loc_nombre"] . ", " . $datos_loc[0]["loc_provincia_nombre"]?>" readonly style="background:transparent!important" required>
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" onClick="$.mostrarVentanaSearch('Buscar personas',$('#lstTipoPersona').val(),'per_id','per_nombre','per_apellido','per_nombre','per_apellido','per_id','txtPersona');return false"><i class="fa fa-search"></i></button>
                                    &nbsp;<button type="button" class="btn btn-primary" onClick="$.buscarParcelaPropietario();return false"><i class="fa fa-check"></i></button>

                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="form-group row mb-1 required" id="resultado_parcela">
                        <label for="txtResultadoParcela" class="col-4 col-form-label text-right">Parcela buscada</label>
                        <div class="col-7">
                            <input id="txtResultadoParcela" name="txtResultadoParcela" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="" disabled required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required" id="lista_parcelas" style="display:none;">
                        <label for="lstParcelasResultado" class="col-4 col-form-label text-right">Listado de parcelas</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstParcelasResultado" name="lstParcelasResultado" required>
                            </select>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="button" class="btn btn-primary btn-lg" onClick="$.abrirVentanaNuevaActividad()"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaParcelaActividad').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
var g_id_parcela = 0;
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    //-------------------------------------------------------------------------------
    //seleccion de tipo de busqueda
    $('#lstTipoBusqueda').on('change', function() {
        g_id_parcela = 0
        $('#lstParcelasResultado').html('');
        if ($('#lstTipoBusqueda').val() == 'numero') {
            $("#numero_parcela").show();
            $("#resultado_parcela").show();
            $('#txtNumeroParcelaBusqueda').val('')
            $('#txtResultadoParcela').val('')
            $("#propietario_parcela").hide();
            $("#obra_parcela").hide();
            $("#lista_parcelas").hide();
        } else if ($('#lstTipoBusqueda').val() == 'obra') {
            $("#obra_parcela").show();
            $("#lista_parcelas").show();
            $("#numero_parcela").hide();
            $("#propietario_parcela").hide();
            $("#resultado_parcela").hide();
            $.listarObras();
        } else if ($('#lstTipoBusqueda').val() == 'propietario') {
            $("#propietario_parcela").show();
            $("#lista_parcelas").show();
            $("#numero_parcela").hide();
            $("#obra_parcela").hide();
            $("#resultado_parcela").hide();
        }
    })
    $('#lstParcelasResultado').on('change', function() {
        g_id_parcela = $('#lstParcelasResultado').val();
    })
    //-----------------------------------------------------------------------------------------------------------------
    //--BUSQUEDA DE PARCELA POR NUMERO
    //-----------------------------------------------------------------------------------------------------------------
    $.buscarParcelaNumero = function() {
        $.showLoading();
        $.post("app-add-actividad-x.php", {
                numero_parcela: $('#txtNumeroParcelaBusqueda').val(),
                accion: 'get_parcelas_numero',
            },
            function(response) {
                $.hideLoading();
                let json = $.parseJSON(response);
                console.log(json)
                if (json.length > 0) {
                    g_id_parcela = json[0].id_encriptado
                    $("#txtResultadoParcela").val("Parcela " + json[0].par_numero_orden);
                } else {
                    $('#txtResultadoParcela').val('')
                    g_id_parcela = 0
                }
            });
    }
    //-----------------------------------------------------------------------------------------------------------------
    //--BUSQUEDA DE PARCELA POR OBRA
    //-----------------------------------------------------------------------------------------------------------------
    $.listarObras = function() {
        $.showLoading();
        $.post("app-add-actividad-x.php", {
                accion: 'listar_obras',
            },
            function(response) {
                $.hideLoading();
                let json = $.parseJSON(response);
                $('#lstListaObras').html(json);
            });
    };
    $.buscarParcelaObra = function() {
        if ($('#lstListaObras').val() != '') {
            $.showLoading();
            $.post("app-add-actividad-x.php", {
                    id_obra: $('#lstListaObras').val(),
                    accion: 'get_parcelas_obra',
                },
                function(response) {
                    $.hideLoading();
                    let json = $.parseJSON(response);
                    $('#lstParcelasResultado').html(json);
                    g_id_parcela = $('#lstParcelasResultado').val();
                });
        }
    }
    //-----------------------------------------------------------------------------------------------------------------
    //--BUSQUEDA DE PARCELA POR PROPIETARIO
    //-----------------------------------------------------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmParcela #txtPersona").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtPersona").val().split("|");
            if (arr.length > 1) {
                $("#txtPersona").val(arr[1] + ", " + arr[2]);
                g_id_persona = arr[0];
            }
        }
    });
    //-- buscamos las parcelas del propietario seleccionado
    $.buscarParcelaPropietario = function() {
        $.showLoading();
        $.post("app-add-actividad-x.php", {
                id_propietario: g_id_persona,
                accion: 'get_parcelas_propietario',
            },
            function(response) {
                $.hideLoading();
                let json = $.parseJSON(response);
                $("#lstParcelasResultado").html(json);
                g_id_parcela = $('#lstParcelasResultado').val();
            });
    }
    //-----------------------------------------------------------------------------------------------------------------
    //--- abriri modal alta de actividad -------------------------------
    $.abrirVentanaNuevaActividad = function() {
        $.showLoading();
        if (g_id_parcela != 0) {
            $('#mdlVentanaParcelaActividad').modal('hide');
            $("#divVentanaModal").load("app-add-actividad.php?data=" + g_id_parcela, function() {
                $("#mdlVentanaAddActividad").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        } else {
            $.hideLoading();
            vex.dialog.alert('Debe seleccionar una parcela')
        }
    };
    //---------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
});
</script>