<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {header("Location: login");exit;}

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- distritos para el filtro
$db->ExecuteSQL("SELECT * FROM distritos ORDER BY dis_nombre");
$distritos_arr = $db->getRows();



$filtro_dis = "";
if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
    $filtro_dis = " AND obr_distrito ='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ";
}
$db->ExecuteSQL("SELECT obr_id,obr_nombre,obr_idsigo FROM obras WHERE obr_nombre <>'' ".$filtro_dis." ORDER BY obr_id DESC");
$obras_arr = $db->getRows();

$db->ExecuteSQL("SELECT  * FROM tasaciones_solicitudes_estados");
$tasaciones_estados_arr = $db->getRows();

?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>Tasación: ExpropiAR</title>
        <link rel="icon" href="img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="description" content="">
        <meta name="author" content="DNV">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/all.css" rel="stylesheet">
        <link href="css/style.css?v=<?=time()?>" rel="stylesheet">
        <link href="css/jquery.uploadfile.css" rel="stylesheet">
        <script src="js/vex.combined.js"></script>
        <link rel="stylesheet" href="css/vex.css?v=<?=time()?>" />
        <link rel="stylesheet" href="css/vex-theme-wireframe.css?v=<?=time()?>" />
        <script>
        vex.defaultOptions.className = 'vex-theme-wireframe';
        </script>
        <script src="js/jquery.min.js"></script>

    </head>

    <body oncontextmenu="return false">

        <div id="divMenu" class="fixed-top">
            <!-- incluimos el menu con o sin barra de filtros segun la variable habilitar_barra_filtros.   ESTO SIEMPRE VA !!!!! -->
            <?php $habilitar_barra_filtros = true;include "inc/app-menu.php"?>
        </div>

        <div class="container-fluid">
            <div class="row" style="margin-top:100px;padding:10px">
                <div class="col">
                    <!-- ##################################################################################################################### -->
                    <!-- INICIO FORM CON LOS CAMPOS PARA EL FILTRO -->
                    <!-- ##################################################################################################################### -->
                    <div id="divForm" style="display:none">
                        <form id="frmForm" class="form-inline no-print">
                            <div class="form-group ">
                                <label for="lstObra">&nbsp;&nbsp;Obra&nbsp;&nbsp;</label>
                                <select class="form-control  text-uppercase" id="lstObra" name="lstObra" style="width:350px">
                                    <option value=""></option>
                                    <?php
for ($i = 0; $i < count($obras_arr); $i++) {
    ?>
                                    <option value="<?=$obras_arr[$i]["obr_id"]?>"><?=$obras_arr[$i]["obr_nombre"] . " (id sigo: " . $obras_arr[$i]["obr_idsigo"] . ")"?></option>
                                    <?php
}
?>
                                </select>
                            </div>
                            <div class="form-group ">&nbsp;&nbsp;N° de orden parcela&nbsp;&nbsp;</label>
                                <input class="form-control text-uppercase" id="txtNumeroOrden" name="txtNumeroOrden" type="number" placeholder="" style="width:100px">
                            </div>
                            <div class="form-group">
                                <label for="lstEstado">&nbsp;&nbsp;Estado de la presentación&nbsp;&nbsp;</label>
                                <select class="form-control" id="lstEstado" name="lstEstado" style="width:200px">
                                    <option value=""></option>
                                    <?php
for ($i = 0; $i < count($tasaciones_estados_arr); $i++) {
    ?>
                                    <option value="<?=$tasaciones_estados_arr[$i]["pptestado_id"]?>"><?=$tasaciones_estados_arr[$i]["pptestado_nombre"]?></option>
                                    <?php
}
?>
                                </select>
                            </div>
                            <div class="form-group">
                                &nbsp;&nbsp;<button id="btnSubmit" data-loading-text="" type="button" class="btn btn-primary  "><i class="fa fa-search"></i></button>
                                <button id="btnLimpiar" data-toggle="tooltip" title="limpia filtros de búsqueda" type="button" class="btn btn-link btn-sm text-dark" onClick="$.clearForm('frmForm');return false;"><span class="fa fa-eraser" style="font-size:1.5em"></span></button>
                            </div>
                        </form>
                    </div>
                    <!-- ##################################################################################################################### -->
                    <!-- FIN FORM CON LOS CAMPOS PARA EL FILTRO -->
                    <!-- ##################################################################################################################### -->

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA TABLA CON LOS REGISTROS DE LA CONSULTA ( GRILLA PRINCIPAL) -->
                    <!-- ##################################################################################################################### -->
                    <div id="divTable" class="colu-100"></div>

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA MODAL PARA EL ALTA DE PARCELA -->
                    <!-- ##################################################################################################################### -->
                    <div id="divVentanaModal"></div>

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA MODAL PARA MOSTRAR LOS ARCHIVOS ADJUNTOS -->
                    <!-- ##################################################################################################################### -->
                    <div class="modal fade modal-wide" id="mdlPreviewPDF" role="dialog" tabindex="-1" aria-hidden="true">
                        <div class="modal-dialog modal-xl modal-center" style=" width:100%; margin: auto;">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <div id="divContent">
                                        <object id="objPDF" type="application/pdf" data="" width="100%" height="700px">No Support</object>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="divPDFBoton" class="text-right">
                                            <button id="btnCancelarPreviewPDF" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlPreviewPDF').modal('hide');"><span class="glyphicon glyphicon-remove"></span><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ##################################################################################################################### -->
                    <!-- BARRA DE HERRAMIENTAS DERECHA Y ABAJO -->
                    <!-- ##################################################################################################################### -->
                    <footer class="no-print">
                        <div id="divRightToolsBar" class="caja-htas-derecha btn-primary">
                            <div id="divSubir" style="display:none"><a data-toggle="tooltip" title="subir" href="#" class="back-to-top"><br><span class="fa fa-chevron-circle-up" style="font-size:2.7em;color:#f1c40f;"></span><br><br></a></div>
                        </div>
                    </footer>
                    <!-- ##################################################################################################################### -->

                </div>
            </div>
        </div>
    </body>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.uploadfile.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/lib.js?v=<?=time()?>"></script>
    <script type="text/javascript">
    var g_archivo_pdf = "";
    $(document).ready(function() {
        //---------------------------------------------------------------------------------------------------------------------
        //-- timeout ajax
        $.ajaxSetup({
            timeout: 20000,
            error: function(xhr) {}
        });
        //--- pongo en la barra de filtros todos los campos del formulario de busqueda
        $("#contentBarraFiltros").html($("#divForm").html());
        //---------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------
        //--- abrir modal para adjuntar archivo -----------------------
        $.abrirVentanaNuevaPresentacion = function(data) {
            $.showLoading();
            $("#divVentanaModal").load("app-add-mensura.php?data=" + data, function() {
                $("#mdlVentanaNuevaPresentacion").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        };
        //---------------------------------------------------------------------------------------------------------------------
        //-- abrir modal para ver archivo adjuntado ------------------
        $.mostrarPDF = function(archivo_pdf) {
            g_archivo_pdf = archivo_pdf;
            $.showLoading();
            $('#mdlPreviewPDF').modal('show');
        };
        $('#mdlPreviewPDF').on('shown.bs.modal', function() {
            $.post("app-list-mensuras-x.php", {
                    accion: "get-archivo",
                    archivo_pdf: g_archivo_pdf
                },
                function(response) {
                    $.hideLoading();
                    $('#objPDF').attr("data", response);
                });
            $(".modal-dialog").draggable({
                cursor: "move",
                handle: ".dragable_touch",
            });
        });
        $(document).on('hidden.bs.modal', function(e) {
            $("#objPDF").attr("data", "");
            g_archivo_pdf = "";
            $(e.target).removeData('bs.modal');
        });
        //---------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------
        $.eliminarArchivo = function(arc_id, $fila) {
            vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-list-mensuras-x.php", {
                                accion: "delete-archivo",
                                arc_id: arc_id
                            },
                            function(response) {
                                $.hideLoading();
                                $("#trArchivo" + $fila).fadeOut();
                            });
                    }
                }
            })
        };
        //---------------------------------------------------------------------------------------------------------------------
        $.presentarAprobarObservarPresentacion = function(ppt_id, accion, ente, par_id, pptdet_id) {
            $.showLoading();
            $("#divVentanaModal").load("app-aprobar-observar-tasacion.php?accion=" + accion + "&ppt_id=" + ppt_id + "&ente=" + ente + "&pptdet_id=" + pptdet_id + "&par_id=" + par_id, function() {
                $("#mdlVentanaObservarTasacion").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        } 
        //---------------------------------------------------------------------------------------------------------------------
        $.realizarTasacion = function(ppt_id,par_id,action,from) {
            $.showLoading();
            $("#divVentanaModal").load("app-realizar-tasacion.php?ppt_id=" + ppt_id + "&par_id=" + par_id + "&action="+ action + "&from=" + from, function() {
                $("#mdlRealizarTasacion").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        } 
        //---------------------------------------------------------------------------------------------------------------------
        function download(filename) {
            $.post("app-list-tasaciones-x.php", {
                    accion: "get-archivo",
                    archivo_pdf: filename
                },
                function(response) {
                    var element = document.createElement('a');
                    element.setAttribute('href', response);
                    element.setAttribute('download', filename);
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                });
        }
        $.descargarArchivo = function(archivo) {
            download(archivo);
        };
        //---------------------------------------------------------------------------------------------------------------------
        //---------------------------------------------------------------------------------------------------------------------
        //-- ejecuta la consulta al hacer click en la lupita ---------
        $("#btnSubmit").click(function(e) {
            e.preventDefault();
            $.consultar(0);
        });
        //--
        $.consultar = function(page) {
            $("html, body").animate({
                scrollTop: 0
            }, "fast");
            let primera_vez = page == 0 ? 1 : 0;
            $.showLoading();
            $.post("app-list-tasaciones-x.php?page=" + page, {
                    primera_vez: primera_vez,
                    datastring: $("#frmForm").serialize()
                })
                .done(function(response) {
                    $.hideLoading();
                    let json = $.parseJSON(response);
                    if (json["res"] == 1) {
                        $("#divTable").html(json["html"]);
                        $("#divRightToolsBarExportar").show();
                    } else {
                        vex.dialog.alert({
                            unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                        });
                    }
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                    $.hideLoading();
                    vex.dialog.alert({
                        unsafeMessage: "<big><b>ERROR DE COMUNICACIÓN</b></big><br><br>Por favor, volvé a intentar.</span>"
                    });
                });
        };
    });
    </script>

</html>