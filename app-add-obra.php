<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <1) {exit;}

if ($perfil_readonly) {exit;}

if ($id == 0) {
    $titulo_ventana = "Nueva obra";

} else {
    $titulo_ventana = "Ver obra";

    $db = new clsDB();
    $db->setHost($GLOBALS["HOST_DB"]);
    $db->setUsername($GLOBALS["USUARIO_DB"]);
    $db->setPassword($GLOBALS["PASSWORD_DB"]);
    $db->setDatabase($GLOBALS["DATABASE_DB"]);
    $db->setDebugmode(false);
    $db->openDB();
    $db->ExecuteSQL("SET NAMES 'utf8'");

    $db->ExecuteSQL("SELECT *,(SELECT COUNT(*) FROM parcelas WHERE obr_id=obras.obr_id) cantidad_parcelas, (SELECT SUM(CAST(par_superficie_afectacion AS DECIMAL)) FROM parcelas WHERE obr_id=obras.obr_id)  superficie_afectada  FROM obras WHERE obr_id=$id");
    $obra_arr=$db->getRows();
    $id_sigo = $obra_arr[0]["obr_idsigo"];
    $db->ExecuteSQL("SELECT res_numero FROM obras_resoluciones WHERE obr_id=$id ORDER BY res_id DESC LIMIT 1");
    $primera_resolucion=$db->getRows()[0]["res_numero"];

    //BUSCAMOS LA PERSONA QUE SE CONTRATO EN CASO DE QUE LA MENSURA SEA "DNV, CONTRATADA A EMPRESA"
    if($obra_arr[0]["obr_mensura_cargo"] == "DNV, contratadas a empresa"){
        if($obra_arr[0]["obr_tipo_persona_contratada"] == "PF"){
            $tabla_buscada = "personas_fisicas";
        } else {
            $tabla_buscada = "personas_juridicas";
        }
        $db->ExecuteSQL("SELECT * FROM $tabla_buscada WHERE per_id=" . $obra_arr[0]['obr_persona_contratada']);
        $persona_contratada = $db->getRows();
    }
}

?>
<style>
.modal {
    padding: 0 !important;
}

.modal-dialog {
    max-width: 60% !important;

    padding: 0;
    margin: auto;
}
</style>
<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaObra">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?=$titulo_ventana?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="overflow-y: auto;overflow-x:hidden ">
                <div id="divVentanaModalSearch"></div>
                <form id="frmObra" name="frmObra">
                    <fieldset <?=$_GET["readonly"] == 1 ? "disabled" : ""?>>
                        <div class="form-group row mb-1 ">
                            <label for="txtIDSigo" class="col-3 col-form-label text-right">ID Sigo</label>
                            <div class="col-9">
                                <div class="input-group">
                                    <input id="txtIDSigo" name="txtIDSigo" class="form-control  text-uppercase" type="number" pattern="\d*" value="<?=$id_sigo?>">
                                    <div>
                                        &nbsp;<button type="button" class="btn btn-primary" id="btnBuscarIDSigo" name="btnBuscarIDSigo"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtExpedienteMadre" class="col-3 col-form-label text-right">Expediente madre</label>
                            <div class="col-9">
                                <input readonly id="txtExpedienteMadre" name="txtExpedienteMadre" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-1 ">
                            <label for="txtDistrito" class="col-3 col-form-label text-right">Distrito</label>
                            <div class="col-9">
                                <input readonly id="txtDistrito" name="txtDistrito" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtNombre" class="col-3 col-form-label text-right">Nombre de la obra</label>
                            <div class="col-9">
                                <input readonly id="txtNombre" name="txtNombre" class="form-control  text-uppercase" type="text" maxlength="100" value="">
                            </div>
                        </div>
                        <div class="form-group row mb-1 ">
                            <label for="txtTipoGestion" class="col-3 col-form-label text-right">Tipo de gestión</label>
                            <div class="col-9">
                                <input readonly id="txtTipoGestion" name="txtTipoGestion" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtContratistaConcesionario" class="col-3 col-form-label text-right">Contratista / Concesionario</label>
                            <div class="col-9">
                                <input readonly id="txtContratistaConcesionario" name="txtContratistaConcesionario" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtNumeroLicitacion" class="col-3 col-form-label text-right">N° de licitación</label>
                            <div class="col-3">
                                <input readonly id="txtNumeroLicitacion" name="txtNumeroLicitacion" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>

                            <label for="txtLicitacion" class="col-3 col-form-label text-right">Licitación</label>
                            <div class="col-3">
                                <input readonly id="txtLicitacion" name="txtLicitacion" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtFechaInicio" class="col-3 col-form-label text-right">Fecha inicio obra</label>
                            <div class="col-3">
                                <input readonly id="txtFechaInicio" name="txtFechaInicio" class="form-control  text-uppercase" type="text" value="">
                            </div>

                            <label for="txtFechaFin" class="col-3 col-form-label text-right">Fecha fin obra</label>
                            <div class="col-3">
                                <input readonly id="txtFechaFin" name="txtFechaFin" class="form-control  text-uppercase" type="text" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtPlazoObraOriginal" class="col-3 col-form-label text-right">Plazo obra original</label>
                            <div class="col-3">
                                <input readonly id="txtPlazoObraOriginal" name="txtPlazoObraOriginal" class="form-control  text-uppercase" type="text" value="">
                            </div>

                            <label for="txtPlazoObraVigente" class="col-3 col-form-label text-right">Plazo obra vigente</label>
                            <div class="col-3">
                                <input readonly id="txtPlazoObraVigente" name="txtPlazoObraVigente" class="form-control  text-uppercase" type="text" value="">
                            </div>
                        </div>

                        <div class="form-group row mb-1 ">
                            <label for="txtCantidadAfectaciones" class="col-3 col-form-label text-right">Cantidad de afectaciones</label>
                            <div class="col-3">
                                <input readonly id="txtCantidadAfectaciones" name="txtCantidadAfectaciones" class="form-control  text-uppercase" type="number" min="0" max="99999" placeholder="0" value="<?=$obra_arr[0]["cantidad_parcelas"]?>">
                            </div>

                            <label for="txtSuperficieTotalAfectada" class="col-3 col-form-label text-right">Superficie total afectada (m2)&nbsp;&nbsp;&nbsp;</label>
                            <div class="col-3">
                                <input readonly id="txtSuperficieTotalAfectada" name="txtSuperficieTotalAfectada" class="form-control  text-uppercase" type="number" min="0.00" max="9999999999.99" step="0.01" placeholder="0.00" value="<?=$obra_arr[0]["superficie_afectada"]?>">
                            </div>
                        </div>
                        <div class="form-group row mb-1 required">
                            <label for="txtResolucion" class="col-3 col-form-label text-right">Resolución</label>
                            <div class="col-3">
                                <input required id="txtResolucion" name="txtResolucion" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$primera_resolucion?>">
                            </div>
                        </div>

                        <div class="form-group row mb-1 required">
                            <label for="txtPartidaPresupuestaria" class="col-3 col-form-label text-right">Partida presupuestaria</label>
                            <div class="col-9">
                                <input required d="txtPartidaPresupuestaria" name="txtPartidaPresupuestaria" class="form-control  text-uppercase" type="text" value="<?=$obra_arr[0]["obr_partida_presupuestaria"]?>">
                            </div>
                        </div>
                        <!-- <div class="form-group row mb-1 ">
                        <label for="txtCargoMensura" class="col-3 col-form-label text-right">Mensura a cargo de</label>
                        <div class="col-9">
                            <input readonly  id="txtCargoMensura" name="txtCargoMensura" class="form-control  text-uppercase" type="text" value="">
                        </div>
                    </div> -->

                        <div class="form-group row mb-1 required">
                            <label for="txtCargoMensura" class="col-3 col-form-label text-right">Mensura a cargo de</label>
                            <div class="col-9">
                                <select required class="form-control  text-uppercase" id="txtCargoMensura" name="txtCargoMensura" onchange="$.seleccionarPersonaContratada()">
                                    <option value=""></option>
                                    <option <?=$obra_arr[0]["obr_mensura_cargo"]=="Contratista/Consecionaria"?"selected":""?> value="Contratista/Consecionaria">Contratista/Concesionaria</option>
                                    <option <?=$obra_arr[0]["obr_mensura_cargo"]=="DNV, contratadas a empresa"?"selected":""?> value="DNV, contratadas a empresa">DNV, contratadas a empresa</option>
                                    <option <?=$obra_arr[0]["obr_mensura_cargo"]=="DNV, por administración"?"selected":""?> value="DNV, por administración">DNV, por administración</option>

                                </select>
                            </div>
                        </div>
                        <div id="contratadas" <?=$obra_arr[0]["obr_mensura_cargo"]=="DNV, contratadas a empresa"?"":"hidden"?>>
                            <div class="form-group row mb-1">
                                <label for="lstTipoPersonaContratada" class="col-3 col-form-label text-right">Tipo de persona</label>
                                <div class="col-9">
                                    <select class="form-control  text-uppercase" id="lstTipoPersonaContratada" name="lstTipoPersonaContratada">
                                        <option <?=$obra_arr[0]["obr_tipo_persona_contratada"]=="PF"?"selected":""?> value="PF" selected>Persona Física</option>
                                        <option <?=$obra_arr[0]["obr_tipo_persona_contratada"]=="PJ"?"selected":""?> value="PJ">Persona Jurídica</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row mb-1">
                                <label for="txtPersonaContratada" class="col-3 col-form-label text-right">Persona</label>
                                <div class="col-9">
                                    <div class="input-group">
                                        <input id="txtPersonaContratada" name="txtPersonaContratada" class="form-control  text-uppercase" type="text" pattern="\d*" value="<?=$obra_arr[0]["obr_tipo_persona_contratada"]=="PF"?$persona_contratada[0]["per_dni"]. ', ' . $persona_contratada[0]["per_nombre"] . ', ' . $persona_contratada[0]["per_apellido"]:$persona_contratada[0]["per_cuit"] . ', ' . $persona_contratada[0]["per_razon_social"]?>" readonly style="background:transparent!important">
                                        <div>
                                            &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas('txtPersonaContratada')" ;return false><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>

                        <div class="form-group row mb-1 ">
                            <label for="txaObservaciones" class="col-3 col-form-label text-right">Observaciones&nbsp;</label>
                            <div class="col-9">
                                <textarea id="txaObservaciones" name="txaObservaciones" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow:hidden"><?=$obra_arr[0]["obr_observaciones"]?></textarea>
                            </div>
                        </div>

                        <hr>
                        <div class="form-group row mb-1" style="display:<?=$_GET["readonly"]==1 ? "none" : ""?>">
                            <div class="col-12 text-right">
                                <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                                &nbsp;&nbsp;&nbsp;
                                <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaObra').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    if ("<?=$id?>" == 0) {
        $('#contratadas').removeAttr('hidden');
        $('#contratadas').hide();
    }
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaObra').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmObra #txtIDSigo").focus();
        }, 250);
     
        if ("<?=$id_sigo?>" != "") {
   
            $("#frmObra #btnBuscarIDSigo").val(<?= $id_sigo?>);
            setTimeout(() => {
                $("#frmObra #btnBuscarIDSigo").trigger("click");
            }, 500);
        }
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaObra').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $.seleccionarPersonaContratada = function() {
        if($('#txtCargoMensura').val() == 'DNV, contratadas a empresa') {
            $('#contratadas').show();
        } else {
            $('#contratadas').hide();
        }
    }
    //---------------------------------------------------------------------
    //--ABRIMOS BUSCADOR DE PERSONAS
    $.buscarPersonas = function(campoTexto) {
        var tabla_busqueda = "";
        var filtro_busqueda1 = "";
        var filtro_busqueda2 = "";
        if ($('#lstTipoPersonaContratada').val() == 'PF') {
            tabla_busqueda = 'personas_fisicas';
            filtro_busqueda1 = "per_nombre";
            filtro_busqueda2 = "per_apellido";
            campo_mostrar1 = "per_dni";
            campo_mostrar2 = "per_nombre";
            campo_mostrar3 = "per_apellido";
        } else {
            tabla_busqueda = 'personas_juridicas';
            campo_mostrar1 = "per_cuit";
            campo_mostrar2 = "per_razon_social";
            campo_mostrar3 ="";
            filtro_busqueda1 = "per_cuit";
            filtro_busqueda2 = "per_razon_social";
        }
    
        $.mostrarVentanaSearch('Buscar persona', tabla_busqueda, 'per_id', filtro_busqueda1, filtro_busqueda2, campo_mostrar1, campo_mostrar2, campo_mostrar3, campoTexto);
    } 
    //---------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmObra #txtPersonaContratada").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersonaContratada").val().length > 0) {
            //  1234|JUAN|PEREZ
            let arr = $("#txtPersonaContratada").val().split("|");
            if (arr.length > 1) {
                $("#txtPersonaContratada").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_persona = arr[0];
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmObra").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-obra-x.php", {
                            id: "<?=$id?>",
                            id_persona_contratada: g_id_persona,
                            datastring: $("#frmObra").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $('#mdlVentanaObra').modal("hide");
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmObra #btnBuscarIDSigo").click(function(e) {
        e.preventDefault();
        if ($("#frmObra #txtIDSigo").val() == "") {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>Tenés que ingresar un ID Sigo</span>"
            });
            return false;
        }
        let id_sigo = $("#frmObra #txtIDSigo").val();
        $("#frmObra").get(0).reset();
        $("#frmObra #txtIDSigo").val(id_sigo);
        $.showLoading();
        $.post("app-add-obra-x.php", {
                accion: "buscar-idsigo",
                id_sigo: id_sigo,
                readonly: "<?=$_GET["readonly"]?>",
                datastring: $("#frmObra").serialize()
            },
            function(response) {
                $.hideLoading();
                let json = $.parseJSON(response);
                if (json["res"] == 0) {
                    vex.dialog.alert({
                        unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                    });
                } else {
                    $("#frmObra #txtDistrito").val(json[0]["DISTRITO"]);
                    $("#frmObra #txtExpedienteMadre").val(json[0]["MADRE"]);
                    $("#frmObra #txtNombre").val(json[0]["NOMBRE"]);
                    // $("#frmObra #txtResolucion").val(json[0]["GESTION"]);
                    $("#frmObra #txtTipoGestion").val(json[0]["GESTION"]);
                    $("#frmObra #txtContratistaConcesionario").val(json[0]["BENIFICIARIO"]);
                    $("#frmObra #txtNumeroLicitacion").val(json[0]["NRO LICITACION"]);
                    $("#frmObra #txtLicitacion").val(json[0]["LICITACION"]);
                    $("#frmObra #txtFechaInicio").val(json[0]["INICIO"]);
                    $("#frmObra #txtFechaFin").val(json[0]["FIN"]);
                    $("#frmObra #txtPlazoObraOriginal").val(json[0]["PLAZO ORI"]);
                    $("#frmObra #txtPlazoObraVigente").val(json[0]["PLAZO VIGENTE"]);
                    //  $("#frmObra #txtCargoMensura").val(json[0]["obr_mensura_cargo"]);
                    $("#txtResolucion").focus();
                }
            });
    });
    //--------------------------------------------------------------------------------------------------------------------------------
});
</script>