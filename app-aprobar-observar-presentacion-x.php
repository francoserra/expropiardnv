<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil < 2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "upload-archivo") {

    $month = date("m");
    $year = date("Y");
    $dir = "$month-$year";
    if (!file_exists("archivos/" . $dir)) {
        mkdir("archivos/" . $dir, 0700);
        chmod("archivos/" . $dir, 0777);
    }
    $output_dir = "archivos/" . $dir;
    $partes_ruta = pathinfo($_FILES['myfile']['name']);
    $archivo = session_id() . date("dmYHis") . "." . $partes_ruta['extension'];
    $uploadfile = $output_dir . "/" . $archivo;
    $ret = array();
    if (isset($_FILES["myfile"])) {
        $error = $_FILES["myfile"]["error"];
        unlink($uploadfile);
        if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
            $_FILES["myfile"]["error"] = "";
            $arr["res"] = 1;
            $arr["archivo"] = $uploadfile;
            print json_encode($arr);
            exit;
        }
    }

    $arr["res"] = 0;
    $arr["msg"] = "Error al adjuntar archivo (" . $_FILES["myfile"]["error"] . ")";
    print json_encode($arr);
    exit;

}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "observar") {
    if ($_REQUEST["ente"] == "DNV") {
        $result = $db->ExecuteSQL("UPDATE mensura_presentaciones
SET
    mes_id=4 /*observada dnv*/,
    mpp_observada_dnv_observaciones=UPPER('$txaObservaciones'),
    mpp_observada_dnv_fecha='" . $dtFechaPresentacion . "',
    mpp_observada_dnv_motivo='$lstMotivo',
    mpp_observada_dnv_archivo='" . $_REQUEST["archivo"] . "',
    mpp_observada_dnv_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    mpp_id=" . $_REQUEST["mpp_id"]);
        if ($result == 1) {

            $arr["res"] = 1;
            $arr["msg"] = "Presentación observada con éxito.";
            echo json_encode($arr);
            exit;
        } else {

            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    } else if ($_REQUEST["ente"] == "CATASTRO") {
        $result = $db->ExecuteSQL("UPDATE mensura_presentaciones
SET
    mes_id=7 /*observada dnv*/,
    mpp_observada_catastro_observaciones=UPPER('$txaObservaciones'),
    mpp_observada_catastro_fecha='" . $dtFechaPresentacion . "',
    mpp_observada_catastro_motivo='$lstMotivo',
    mpp_observada_catastro_archivo='" . $_REQUEST["archivo"] . "',
    mpp_observada_catastro_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    mpp_id=" . $_REQUEST["mpp_id"]);
        if ($result == 1) {

            $arr["res"] = 1;
            $arr["msg"] = "Presentación observada con éxito.";
            echo json_encode($arr);
            exit;
        } else {

            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
    //---------------------------------------------
} else if ($_REQUEST["accion"] == "aprobar") {
    if ($_REQUEST["ente"] == "DNV") {
        $result = $db->ExecuteSQL("UPDATE mensura_presentaciones
SET
    mes_id=3 /*aprobada dnv*/,
    mpp_aprobada_dnv_observaciones=UPPER('$txaObservaciones'),
    mpp_aprobada_dnv_fecha='" . $dtFechaPresentacion . "',
    #mpp_aprobada_dnv_motivo='$lstMotivo',
    mpp_aprobada_dnv_archivo='" . $_REQUEST["archivo"] . "',
    mpp_aprobada_dnv_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    mpp_id=" . $_REQUEST["mpp_id"]);
        if ($result == 1) {
            // BUSCO SI LA PARCELA TIENE UNA SOLICITUD REALIZADA
            $db->ExecuteSQL("SELECT * FROM tasaciones_solicitudes WHERE par_id=" . $_REQUEST["par_id"] ." ORDER BY ppt_fecha_solicitud DESC LIMIT 1");
            $solicitud_tas = $db->getRows();

            $funcion_habilitar_tasacion = false;
            if(count($solicitud_tas) == 0) {
                // VERIFICO SI TIENE PLANO Y ESTA APROBADO DNV
                $db->ExecuteSQL("SELECT * FROM 
                        mensura_presentaciones mpp,
                        archivos arc
                    WHERE 
                        mpp.par_id=" . $_REQUEST["par_id"] . " AND 
                        arc.arc_id_fuente = mpp.mpp_id AND
                        arc.cat_id = 13 AND
                        mpp.mpp_presentada_tasacion = 0 ORDER BY mpp.mpp_fecha_presentacion, mpp.mpp_id DESC LIMIT 1");

                $presentacion_plano = $db->getRows();

                // VERIFICO SI TIENE CROQUIS Y ESTA APROBADO DNV
                $db->ExecuteSQL("SELECT * FROM 
                        mensura_presentaciones mpp,
                        archivos arc
                    WHERE 
                        mpp.par_id=" . $_REQUEST["par_id"] . " AND 
                        arc.arc_id_fuente = mpp.mpp_id AND
                        arc.cat_id = 26 AND
                        mpp.mpp_presentada_tasacion = 0 ORDER BY mpp_fecha_presentacion, mpp.mpp_id DESC LIMIT 1");

                $presentacion_croquis = $db->getRows();

                // VERIFICO SI TIENE MEJORAS Y ESTA APROBADO DNV
                $db->ExecuteSQL("SELECT * FROM 
                        mensura_presentaciones mpp,
                        archivos arc
                    WHERE 
                        mpp.par_id=" . $_REQUEST["par_id"] . " AND 
                        arc.arc_id_fuente = mpp.mpp_id AND
                        arc.cat_id = 25 AND
                        mpp.mpp_presentada_tasacion = 0 ORDER BY mpp_fecha_presentacion, mpp.mpp_id DESC LIMIT 1");

                $presentacion_mejoras = $db->getRows();

                // SI NO HAY PLANO
                if(count($presentacion_plano) == 0) {
                    // VERIFICO QUE HAYA CROQUIS
                    if(count($presentacion_croquis) > 0) {
                        // SI HAY CROQUIS VALIDO QUE ESTE APROBADO POR DNV
                        if($presentacion_croquis[0]["mes_id"] > 2 && $presentacion_croquis[0]["mes_id"] != 4) {
                            // SI ESTA APROBADO POR DNV, VERIFICO QUE NO HAYA MEJORA, Y SI LA HAY QUE ESTE APROBADA DNV
                            if(count($presentacion_mejoras) == 0 || (count($presentacion_mejoras) > 0 && $presentacion_mejoras[0]["mes_id"] > 2 && $presentacion_mejoras[0]["mes_id"] != 4)) {
                                $funcion_habilitar_tasacion = true;
                            } else {
                                $funcion_habilitar_tasacion = false;
                            }
                        } else {
                            $funcion_habilitar_tasacion = false;
                        }
                    } else {
                        $funcion_habilitar_tasacion = false;
                    }
                } else {
                    // SI HAY PLANO, VERIFICO QUE ESTA APROBADO DNV
                    if($presentacion_plano[0]["mes_id"] > 2 && $presentacion_plano[0]["mes_id"] != 4) {
                        // SI ESTA APROBADO DNV, VERIFICO SI HAY O NO MEJORA. SI LA HAY VERIFICO QUE ESTE APROBADA DNV
                        if(count($presentacion_mejoras) == 0 || (count($presentacion_mejoras) > 0 && $presentacion_mejoras[0]["mes_id"] > 2 && $presentacion_mejoras[0]["mes_id"] != 4)) {
                            $funcion_habilitar_tasacion = true;
                        } else {
                            $funcion_habilitar_tasacion = false;
                        }
                    } else {
                        $funcion_habilitar_tasacion = false;
                    }
                }

                if($funcion_habilitar_tasacion) {
                    $resulta_habilitada = $db->ExecuteSQL("UPDATE parcelas SET par_tasacion_habilitada = 1 WHERE par_id=" . $_REQUEST["par_id"]);
                }
            }

            $arr["res"] = 1;
            $arr["msg"] = "Presentación observada con éxito.";
            echo json_encode($arr);
            exit;
        } else {

            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    } else if ($_REQUEST["ente"] == "CATASTRO") {
        $result = $db->ExecuteSQL("UPDATE mensura_presentaciones
SET
    mes_id=6 /*aprobada catastro*/,
    mpp_aprobada_catastro_observaciones=UPPER('$txaObservaciones'),
    mpp_aprobada_catastro_fecha='" . $dtFechaPresentacion . "',
    #mpp_aprobada_catastro_motivo='$lstMotivo',
    mpp_aprobada_catastro_archivo='" . $_REQUEST["archivo"] . "',
    mpp_aprobada_catastro_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    mpp_id=" . $_REQUEST["mpp_id"]);
        if ($result == 1) {

            $arr["res"] = 1;
            $arr["msg"] = "Presentación observada con éxito.";
            echo json_encode($arr);
            exit;
        } else {

            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
} else if ($_REQUEST["accion"] == "presentar") {
    if ($_REQUEST["ente"] == "CATASTRO") {
        $result = $db->ExecuteSQL("UPDATE mensura_presentaciones
SET
    mes_id=5 /*presentada catastro*/,
    mpp_presentada_catastro_observaciones=UPPER('$txaObservaciones'),
    mpp_presentada_catastro_fecha='" . $dtFechaPresentacion . "',
   # mpp_presentada_catastro_motivo='$lstMotivo',
    mpp_presentada_catastro_archivo='" . $_REQUEST["archivo"] . "',
    mpp_presentada_catastro_usuario='" . $_SESSION["expropiar_usuario_nombre"] . "'
WHERE
    mpp_id=" . $_REQUEST["mpp_id"]);
        if ($result == 1) {

            $arr["res"] = 1;
            $arr["msg"] = "Presentación observada con éxito.";
            echo json_encode($arr);
            exit;
        } else {

            $arr["res"] = 0;
            $arr["msg"] = "Error[50] al observar presentación (ERR $result).";
            echo json_encode($arr);
            exit;
        }

    }
}
