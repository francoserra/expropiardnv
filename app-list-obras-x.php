<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "inc/cdb_sqlsrv.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(20);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}
//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------
$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "get-permiso-paso") {

   /* for ($i = 1; $i < 10000; $i++) {
        $arr[$i] = -1;
    }*/

    $db->ExecuteSQL("SELECT par_numero_orden FROM parcelas WHERE par_eliminada=0 AND obr_id=" . $_REQUEST["obr_id"]);
    $rows = $db->getRows();
    for ($i = 0; $i < count($rows); $i++) {
        $arr[$rows[$i]["par_numero_orden"]] = 0;
    }

    $db->ExecuteSQL("SELECT par_numero_orden,pav_permiso_paso
    FROM parcelas_avance_relevamiento
    WHERE  par_id IN ( SELECT par_id FROM parcelas WHERE par_eliminada=0 AND obr_id=" . $_REQUEST["obr_id"] . ")");
    $rows = $db->getRows();
    for ($i = 0; $i < count($rows); $i++) {
        $arr[$rows[$i]["par_numero_orden"]] = $rows[$i]["pav_permiso_paso"];
    }
    print json_encode($arr);
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "get-archivo") {
    $ctx = stream_context_create(array('http' => array(
        'timeout' => 20000, //1200 Seconds is 20 Minutes
    ),
    ));

    if (file_exists($_REQUEST["archivo_pdf"])) {
        // var_dump($_REQUEST["tipo_archivo"]);exit;
        if($_REQUEST["tipo_archivo"] == "pdf" && ($_REQUEST["accion_archivo"] == "descarga" || $_REQUEST["accion_archivo"] == "lectura")) {
            echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
        } else if($_REQUEST["tipo_archivo"] == "kml" && $_REQUEST["accion_archivo"] == "descarga"){
            echo "data:application/vnd.google-earth.kml+xml kml;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
        } else if($_REQUEST["tipo_archivo"] == "kml" && $_REQUEST["accion_archivo"] == "lectura") {
            echo base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
        }
    } else {
        echo "";
    }
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-archivo") {
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id=" . $_REQUEST["arc_id"]);
    auditar($db, "archivo", "baja", $_REQUEST["arc_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-resolucion") {
    $db->ExecuteSQL("UPDATE obras_resoluciones SET res_eliminada=1 WHERE res_id=" . $_REQUEST["res_id"]);
    auditar($db, "resolucion", "baja", $_REQUEST["res_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-obra") {
    $db->ExecuteSQL("UPDATE obras SET obr_eliminada=1 WHERE obr_id=" . $_REQUEST["obr_id"]);
    auditar($db, "obra", "baja", $_REQUEST["obr_id"], json_encode($_REQUEST));
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id_fuente=" . $_REQUEST["obr_id"] . " AND arc_fuente='OBRA'");
    auditar($db, "archivo", "baja", $_REQUEST["obr_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {

    if ($lstDistrito != "") {
        $filtro_distrito = " AND obr.obr_distrito='$lstDistrito'";
    }

    if ($txtIDSigo != "") {
        $filtro_idsigo = " AND obr.obr_idsigo=$txtIDSigo";
    }

    if ($txtNombreObra != "") {
        $filtro_nombre_obra = " AND LOWER(obr.obr_nombre) LIKE'%" . strtolower($txtNombreObra) . "%'";
    }

    if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
        $filtro_distrito_obra = " AND obr.obr_distrito='".$_SESSION["expropiar_usuario_distrito_nombre"]."'";
    }

    $sql = "SELECT obr.*,
                (SELECT res.res_numero FROM obras_resoluciones res WHERE res.obr_id=obr.obr_id ORDER BY res.res_id DESC LIMIT 1) res_numero,
                usr.usr_nombre
            FROM
                obras obr, usuarios usr
            WHERE
                obr.usuario=usr.usr_login
                AND obr.obr_eliminada=0
                $filtro_distrito
                $filtro_idsigo
                $filtro_nombre_obra
                $filtro_distrito_obra
            ORDER BY
                obr.obr_id DESC";
    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[59] en consulta a la base de datos ($result)";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}

//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$obras_arr = $db->getRows();

$html = <<<eod
<table class="table table-hover table-stripped ">
<caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Total de obras: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
	<thead class="bg-dark text-light">
		<tr>
            <th nowrap width="1%"></th>
            <th>ID</th>
            <th nowrap>ID Sigo</th>
            <th nowrap>Distrito</th>
            <th nowrap>Nombre</th>
            <th nowrap>Exp. madre</th>
            <th nowrap>Tipo de gestión</th>
            <th nowrap>Contratista</th>
            <th nowrap>N° de licitación</th>
            <th nowrap>Licitación</th>
            <th nowrap>Fecha inicio</th>
            <th nowrap>Fecha fin</th>
            <th nowrap>Plazo original</th>
            <th nowrap>Plazo vigente</th>
            <th nowrap>Resolución</th>
            <th nowrap>Cant. afectaciones</th>
            <th nowrap>Sup. afectada (m2)</th>
            <th nowrap>Part. presup.</th>
            <th nowrap>Mesura a cargo</th>
            <th nowrap>Persona Contratada</th>
            <th nowrap>Observaciones</th>
            <th nowrap>Fecha creación</th>
            <th nowrap>Usuario creó</th>
		</tr>
	</thead>
eod;

// $db1 = new clsDBSQLSRV();
// $db1->setHost($GLOBALS["HOST_SIGO_DB"]);
// $db1->setUsername($GLOBALS["USUARIO_SIGO_DB"]);
// $db1->setPassword($GLOBALS["PASSWORD_SIGO_DB"]);
// $db1->setDatabase($GLOBALS["DATABASE_SIGO_DB"]);
// $db1->setDebugmode(false);
// $db1->openDB();

for ($i = 0; $i < count($obras_arr); $i++) {
    $ti = $tf = $tools = $tool_toogle = $tool_ver = $tool_editar = $tool_elimimar = "";

    // $sigo_arr = getDatosSigo($db1, $obras_arr[$i]["obr_idsigo"]);

    $data1 = encrypt_decrypt("encrypt", "OBRA|" . $obras_arr[$i]['obr_id'] . "|DOCUMENTO");
    $data2 = encrypt_decrypt("encrypt", "OBRA|" . $obras_arr[$i]['obr_id'] . "|PLANO");
    $data3 = encrypt_decrypt("encrypt", $obras_arr[$i]['obr_id']);

    $db->ExecuteSQL("SELECT arc.*,cat.*,usr.*
        FROM archivos arc, categorizacion_archivos cat,usuarios usr
        WHERE arc.cat_id=cat.cat_id AND arc.usuario=usr.usr_login AND arc.arc_eliminado=0 AND arc.arc_fuente='OBRA' AND arc.arc_id_fuente=" . $obras_arr[$i]["obr_id"]);
    $archivos_arr = $db->getRows();

    $db->ExecuteSQL("SELECT * FROM obras_resoluciones WHERE res_eliminada=0 AND obr_id=" . $obras_arr[$i]['obr_id']);
    $resoluciones_arr = $db->getRows();

    //BUSCAMOS LA PERSONA QUE SE CONTRATO EN CASO DE QUE LA MENSURA SEA "DNV, CONTRATADA A EMPRESA"
    if ($obras_arr[$i]["obr_mensura_cargo"] == "DNV, contratadas a empresa") {
        if ($obras_arr[$i]["obr_tipo_persona_contratada"] == "PF") {
            $tabla_buscada = "personas_fisicas";
        } else {
            $tabla_buscada = "personas_juridicas";
        }
        $db->ExecuteSQL("SELECT * FROM $tabla_buscada WHERE per_id=" . $obras_arr[$i]['obr_persona_contratada']);
        $persona_contratada = $db->getRows();
    }
    if (count($archivos_arr) > 0 || count($resoluciones_arr) > 0) {
        $tool_toogle = <<<eod
        &nbsp;&nbsp;<a href="" ><i onClick="$('#trAdjuntos{$i}').toggle();$('#trResoluciones{$i}').toggle();if ($(this).hasClass('fa-chevron-down')){ $(this).toggleClass('fa-chevron-up')};return false;" data-toggle="tooltip" title="ver archivos adjuntos" class="fa fa-chevron-down" style="font-size:0.9rem;color:#212121" ></i></a>&nbsp;&nbsp;
eod;
    }
    //-- aca habilitamos los botones a nivel fila de grilla segun el perfil del usuario

    $tool_ver .= <<<eod
        &nbsp;&nbsp;<button onClick="$.abrirVentanaObra('{$data3}',1)" class="btn btn-sm btn-default" data-toggle="tooltip" title="ver datos obra"><i class="fas fa-eye text-primary btn-icon"></i></button>
eod;
    $tool_editar = <<<eod
	    &nbsp;&nbsp;<button onClick="$.abrirVentanaAdjuntarArchivo('{$data1}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="adjuntar documento"><i class="fas fa-file-alt text-primary btn-icon"></i></button>
	    &nbsp;&nbsp;<button onClick="$.abrirVentanaAdjuntarArchivo('{$data2}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="adjuntar plano"><i class="fas fa-pencil-ruler text-primary btn-icon"></i></button>
	    &nbsp;&nbsp;<button onClick="$.abrirVentanaAltaResolucion('{$data3}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="nueva resolución"><i class="fas fa-file-prescription text-primary btn-icon"></i></button>
eod;
    $tool_eliminar = <<<eod
        &nbsp;&nbsp;<button onClick="$.eliminarObra({$obras_arr[$i]["obr_id"]},$i)" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar obra"><span class="fas fa-trash text-secondary btn-icon"></span></button>
eod;

    switch ($atributo_perfil) {
        case 1:
            $tools = $tool_toogle . $tool_ver;
            break;
        case 2:
            $tools = $tool_toogle . $tool_ver . $tool_editar;
            break;
        case 3:
            $tools = $tool_toogle . $tool_ver . $tool_editar . $tool_eliminar;
            break;
        default:
            $tools = "";
            break;
    }

    $html .= <<<eod
        <tbody id="trObra{$i}">
			<tr>
                <td nowrap class="vert-align text-right" width="1%">{$ti}{$tools}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_id"]}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_idsigo"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$obras_arr[$i]["obr_distrito"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$obras_arr[$i]["obr_nombre"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["MADRE"]}{$tf}</td>

                <td class="vert-align">{$ti}{$sigo_arr[0]["GESTION"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$sigo_arr[0]["BENIFICIARIO"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["NRO LICITACION"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["LICITACION"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["INICIO"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["FIN"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["PLAZO ORI"]}{$tf}</td>
                <td class="vert-align">{$ti}{$sigo_arr[0]["PLAZO VIGENTE"]}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["res_numero"]}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_cantidad_afectaciones"]}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_superficie_afectada"]}{$tf}</td>
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_partida_presupuestaria"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$obras_arr[$i]["obr_mensura_cargo"]}{$tf}</td>
eod;

    if ($obras_arr[$i]["obr_tipo_persona_contratada"] == "PF") {
        $html .= <<<eod
                        <td nowrap class="vert-align">{$ti}{$persona_contratada[0]["per_nombre"]} {$persona_contratada[0]["per_apellido"]}{$tf}</td>
eod;
    } else if ($obras_arr[$i]["obr_tipo_persona_contratada"] == "PJ") {
        $html .= <<<eod
                        <td nowrap class="vert-align">{$ti}{$persona_contratada[0]["per_razon_social"]}{$tf}</td>
eod;
    } else {
        $html .= <<<eod
                    <td nowrap class="vert-align">{$ti}N/A{$tf}</td>
    eod;
    }
    $html .= <<<eod
                <td class="vert-align">{$ti}{$obras_arr[$i]["obr_observaciones"]}{$tf}</td>
                <td class="vert-align">{$ti}{$ymd2dmy($obras_arr[$i]["fecha"])}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$obras_arr[$i]["usr_nombre"]}{$tf}</td>
	    	</tr>
eod;
    //---------------------------------------------------------------------------------------------------------------------
    if (count($archivos_arr) > 0) {
        $html .= <<<eod
            <tr id="trAdjuntos{$i}" style="display:none">
                <td></td>
                <td colspan="12">
                    <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
                    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Documentación</span></caption>
                    <tr>
                        <td nowrap class="text-right bold" width="15%"></td>
                        <td class="bold">ID</td>
                        <td class="bold" width="15%">Tipo</td>
                        <td class="bold" width="15%">Categoría</td>
                        <td nowrap class="bold" width="30%">Descripción</td>
                        <td nowrap class="bold" width="10%">Fecha adjuntó</td>
                        <td nowrap class="bold">Usuario adjuntó</td>
                    </tr>
eod;

        for ($a = 0; $a < count($archivos_arr); $a++) {
            $tools = $tool_toogle = $tool_ver = $tool_editar = $tool_elimimar = "";

            $data = encrypt_decrypt("encrypt", $archivos_arr[$a]['arc_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarArchivo({$archivos_arr[$a]["arc_id"]},{$a});return false" data-toggle="tooltip" title="eliminar archivo"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;
            $tool_ver = <<<eod
            <a class="btn btn-default btn-sm"  href=""  onClick="$.mostrarPDF('{$archivos_arr[$a]["arc_path"]}',{$obras_arr[$i]["obr_id"]});return false;" data-toggle="tooltip" title="ver archivo"><i class="fa fa-eye text-primary"></i></a>
eod;
            $tool_download = <<<eod
            <a class="btn btn-default btn-sm"  href=""  onClick="$.descargarArchivo('{$archivos_arr[$a]["arc_path"]}');return false;" data-toggle="tooltip" title="descargar archivo"><i class="fa fa-download text-primary"></i></a>
eod;
            switch ($atributo_perfil) {
                case 1:
                    $tools = $tool_ver . $tool_download;
                    break;
                case 2:
                    $tools = $tool_ver . $tool_download;
                    break;
                case 3:
                    $tools = $tool_ver . $tool_download . $tool_eliminar;
                    break;
                default:
                    $tools = "";
                    break;
            }

            $html .= <<<eod
                    <tr id="trArchivo{$a}">
                        <td nowrap class="text-right vert-align" width="15%">{$tools}</td>
                        <td class="vert-align">{$archivos_arr[$a]["arc_id"]}</td>
                        <td class="vert-align">{$archivos_arr[$a]["cat_tipo"]}</td>
                        <td class="vert-align">{$archivos_arr[$a]["cat_descripcion"]}</td>
                        <td  nowrap class="vert-align">{$archivos_arr[$a]["arc_descripcion"]}</td>
                        <td class="vert-align">{$ymd2dmy($archivos_arr[$a]["fecha"])}</td>
                        <td nowrap class="vert-align">{$archivos_arr[$a]["usr_nombre"]}</td>
                    </tr>
eod;
        } //for ($a = 0; $a < count($archivos_arr); $a++) {
        $html .= <<<eod
                </table>
                <td colspan="12"></td>
            </td>
        </tr>

eod;
    }
    //---------------------------------------------------------------------------------------------------------------------
    if (count($resoluciones_arr) > 0) {
        $html .= <<<eod
            <tr id="trResoluciones{$i}" style="display:none">
                <td></td>
                <td colspan="12">
                    <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
                    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Resoluciones</span></caption>
                    <tr>
                        <td nowrap class="text-right bold" width="15%"></td>
                        <td class="bold">ID</td>
                        <td class="bold" width="15%">N° de resolución</td>
                        <td class="bold" width="15%">Observaciones</td>
                        <td nowrap class="bold" width="10%">Fecha alta</td>
                        <td nowrap class="bold">Usuario alta</td>
                    </tr>
eod;

        for ($a = 0; $a < count($resoluciones_arr); $a++) {
            $tools = $tool_toogle = $tool_ver = $tool_editar = $tool_elimimar = "";

            $data = encrypt_decrypt("encrypt", $resoluciones_arr[$a]['arc_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarResolucion({$resoluciones_arr[$a]["res_id"]},{$a});return false" data-toggle="tooltip" title="eliminar resolución"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;

            switch ($atributo_perfil) {
                case 1:
                    $tools = "";
                    break;
                case 2:
                    $tools = "";
                    break;
                case 3:
                    $tools = $tool_eliminar;
                    break;
                default:
                    $tools = "";
                    break;
            }
            $html .= <<<eod
                    <tr id="trResolucion{$a}">
                        <td width="15%" nowrap class="text-right vert-align">{$tools}</td>
                        <td width="1%" class="vert-align">{$resoluciones_arr[$a]["res_id"]}</td>
                        <td class="vert-align">{$resoluciones_arr[$a]["res_numero"]}</td>
                        <td class="vert-align">{$resoluciones_arr[$a]["res_observaciones"]}</td>
                        <td class="vert-align">{$ymd2dmy($resoluciones_arr[$a]["fecha"])}</td>
                        <td nowrap class="vert-align">{$resoluciones_arr[$a]["usuario"]}</td>
                    </tr>
eod;
        } //for ($a = 0; $a < count($archivos_arr); $a++) {
        $html .= <<<eod
                </table>
                <td colspan="12"></td>
            </td>
        </tr>

eod;
    }
    //---------------------------------------------------------------------------------------------------------------------
    $html .= <<<eod
    </tbody>
eod;
} //for ($i = 0; $i < count($obras_arr); $i++) {

$html .= <<<eod
</table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
