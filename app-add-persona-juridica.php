<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT pjt_id,pjt_tipo FROM personas_juridicas_tipo ORDER BY pjt_tipo");
$personas_juridicas_tipo_arr=$db->getRows();

$db->ExecuteSQL("SELECT eci_id,eci_estado FROM estados_civiles ORDER BY eci_estado");
$estado_civiles_arr=$db->getRows();


if ($id == 0) {
    $titulo_ventana = "Nueva persona jurídica";

    $localidad_id = 0;

} else {
    $db->ExecuteSQL("SELECT * FROM personas_juridicas WHERE per_id=$id ORDER BY per_razon_social");
    $datos_arr = $db->getRows();

    $localidad_id = $datos_arr[0]["loc_id"];

    $db->ExecuteSQL("SELECT * FROM localidades WHERE loc_id=$localidad_id");
    $datos_loc = $db->getRows();

    $titulo_ventana = "Modificar persona jurídica";

}

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaPersona">
    <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?=$titulo_ventana?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"  style="overflow-y: auto;overflow-x:hidden">
                <div id="divVentanaModalSearch"></div>
                <form id="frmPersonaJuridica" name="frmPersonaJuridica">
                    <div class="form-group row mb-1 required">
                        <label for="txtRazonSocial" class="col-3 col-form-label text-right">Razón social</label>
                        <div class="col-7">
                            <input id="txtRazonSocial" name="txtRazonSocial" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_razon_social"]?>" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtCuit" class="col-3 col-form-label text-right">N° de CUIT</label>
                        <div class="col-7">
                            <input id="txtCuit" name="txtCuit" class="form-control input-fieldsX text-uppercase" <?=$id != 0 ? ' disabled="disabled"' : '';?> type="text" pattern="[0-9]{11,13}" title="Solo números. Minimo 11 carácteres, máximo 13." value="<?=$datos_arr[0]["per_cuit"]?>" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="lstInhibida" class="col-3 col-form-label text-right">Inhibida&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstInhibida" name="lstInhibida">
                                <option value="0" <?=$datos_arr[0]["per_esta_inhibida"] == 0 ? ' selected="selected"' : '';?>>No</option>
                                <option value="1" <?=$datos_arr[0]["per_esta_inhibida"] == 1 ? ' selected="selected"' : '';?>>Si</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoPersonaJuridica" class="col-3 col-form-label text-right">Tipo persona jurídica</label>
                        <div class="col-7">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoPersonaJuridica" name="lstTipoPersonaJuridica" required>
                                <option value=""></option>
                                <?php
                                    for ($i = 0; $i < count($personas_juridicas_tipo_arr); $i++) {
                                    ?>
                                <option value="<?=$personas_juridicas_tipo_arr[$i]["pjt_id"]?>" <?=$datos_arr[0]["pjt_id"] == $personas_juridicas_tipo_arr[$i]["pjt_id"] ? ' selected="selected"' : '';?>><?=$personas_juridicas_tipo_arr[$i]["pjt_tipo"]?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="dtInicioActividad" class="col-3 col-form-label text-right">Inicio de actividad&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="dtInicioActividad" name="dtInicioActividad" class="form-control" type="date" value="<?=$datos_arr[0]["per_inicio_actividad"]?>" />
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtDomicilio" class="col-3 col-form-label text-right">Domicilio&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtDomicilio" name="txtDomicilio" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_domicilio_legal"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtLocalidad" class="col-3 col-form-label text-right">Localidad&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <div class="input-group">
                                <input id="txtLocalidad" name="txtLocalidad" class="form-control  text-uppercase" type="text" pattern="\d*" value="<?=$datos_loc[0]["loc_nombre"] . ", " . $datos_loc[0]["loc_provincia_nombre"]?>" readonly style="background:transparent!important" required>
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" onClick="$.mostrarVentanaSearch('Buscar localidades','localidades','loc_id','loc_nombre','loc_provincia_nombre','loc_nombre','loc_provincia_nombre','loc_id','txtLocalidad');return false"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtCodigoPostal" class="col-3 col-form-label text-right">Código postal&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtCodigoPostal" name="txtCodigoPostal" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$datos_arr[0]["per_codigo_postal"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtEmail" class="col-3 col-form-label text-right">Email&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtEmail" name="txtEmail" class="form-control input-fieldsX text-uppercase" type="email" maxlength="45" value="<?=$datos_arr[0]["per_email"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoPrincipal" class="col-3 col-form-label text-right">Teléfono Principal&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoPrincipal" name="txtTelefonoPrincipal" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_principal"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoLaboral" class="col-3 col-form-label text-right">Teléfono Laboral&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoLaboral" name="txtTelefonoLaboral" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_laboral"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTelefonoCelular" class="col-3 col-form-label text-right">Teléfono Celular&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="txtTelefonoCelular" name="txtTelefonoCelular" class="form-control input-fieldsX text-uppercase" type="number" maxlength="45" value="<?=$datos_arr[0]["per_telefono_celular"]?>">
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaPersona').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = 0;
var g_name_provincia = "";
$(document).ready(function() {
    $.hideLoading();
    if ("<?=$localidad_id?>" != 0) {
        g_id_localidad = <?=$localidad_id?>;
    }
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaPersona').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmPersonaJuridica #txtRazonSocial").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaPersona').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmPersonaJuridica").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-persona-juridica-x.php", {
                            id: "<?=$id?>",
                            id_localidad: g_id_localidad,
                            nombre_provincia: g_name_provincia,
                            datastring: $("#frmPersonaJuridica").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            console.log(json["msg"]);
                            if (json["res"] == 1) {
                                $.clearForm("frmPersonaJuridica");
                            }
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmPersonaJuridica #txtLocalidad").focus(function(e) {
        e.preventDefault();
        if ($("#txtLocalidad").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtLocalidad").val().split("|");
            if (arr.length > 1) {
                $("#txtLocalidad").val(arr[1] + ", " + arr[2]);
                g_id_localidad = arr[0];
                g_name_provincia = arr[2];
            }
        }
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-- Campos solo lectura
    $(".readonly").on('keydown paste focus mousedown', function(e) {
        if (e.keyCode != 9) // ignore tab
            e.preventDefault();
        if (e.keyCode == 13) // ignore enter
            e.preventDefault();
    });
});
</script>