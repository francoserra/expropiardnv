<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(20);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}
//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------

$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "get-archivo") {
    $ctx = stream_context_create(array('http' => array(
        'timeout' => 20000, //1200 Seconds is 20 Minutes
    ),
    ));

    if (file_exists($_REQUEST["archivo_pdf"])) {
        echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
    } else {
        echo "";
    }
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-archivo") {
    // BUSCO EL ARCHIVO
    $db->ExecuteSQL("SELECT * FROM archivos WHERE arc_id=" . $_REQUEST["arc_id"]);
    $archivo = $db->getRows();

    if(count($archivo) > 0) {
        $db->ExecuteSQL("START TRANSACTION");

        if($archivo[0]["cat_id"] == 23 || $archivo[0]["cat_id"] == 23) {
            if($archivo[0]["cat_id"] == 23) {
                $columna = "pav_permiso_paso";
            } else if($archivo[0]["cat_id"] == 23) {
                $columna = "pav_notificacion_expropiacion";
            }

            $db->ExecuteSQL("SELECT * FROM archivos WHERE arc_id_fuente=" . $archivo[0]["arc_id_fuente"] . " AND cat_id=" . $archivo[0]["cat_id"] . "AND arc_eliminado=0");
            $mas_archivos = $db->getRows();
            if(count($mas_archivos) == 1) {
                $result = $db->ExecuteSQL("UPDATE parcelas_avance_relevamiento SET $columna=0 WHERE arc_id=" . $archivo[0]["arc_id"]);

                if($result != 1) {
                    $db->ExecuteSQL("ROLLBACK");
                    echo false;
                    exit;
                }
            }
        }

        $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id=" . $_REQUEST["arc_id"]);
        auditar($db, "archivo", "baja", $_REQUEST["arc_id"], json_encode($_REQUEST));
        $db->ExecuteSQL("COMMIT");
        echo true;
        exit;
    } else {
        echo false;
        exit;
    }

    
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-propietario") {
    //$db->ExecuteSQL("UPDATE parcelas_propietarios SET ppr_eliminado=1 WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    $db->ExecuteSQL("DELETE FROM  parcelas_propietarios WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    auditar($db, "propietario", "baja", $_REQUEST["ppr_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-parcela") {
    $db->ExecuteSQL("UPDATE parcelas SET par_eliminada=1 WHERE par_id=" . $_REQUEST["par_id"]);
    auditar($db, "parcela", "baja", $_REQUEST["par_id"], json_encode($_REQUEST));
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id_fuente=" . $_REQUEST["par_id"] . " AND arc_fuente='PARC'");
    auditar($db, "archivo", "baja", $_REQUEST["par_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {

    $filtro_dis = "";
    if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
        $filtro_dis = " AND obr_distrito ='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ";
    }

    if ($lstObra != "") {
        $filtro_obra .= " AND obr.obr_id='$lstObra' " . $filtro_dis ;
    } else {
        $filtro_obra .= $filtro_dis;

    }

    if ($txtNumeroOrden != "") {
        $filtro_inumero_parcela = " AND par.par_numero_orden=$txtNumeroOrden";
    }

    if ($lstEstatus == "tod0") {
        $filtro_estatus = " AND pav.pav_datos_registrales+pav.pav_datos_catastrales+pav_datos_propietarios=0";
    } else if ($lstEstatus == "tod1") {
        $filtro_estatus = " AND pav.pav_datos_registrales+pav.pav_datos_catastrales+pav_datos_propietarios=3";
    } else if ($lstEstatus == "dre0") {
        $filtro_estatus = " AND pav.pav_datos_registrales=0";
    } else if ($lstEstatus == "dre1") {
        $filtro_estatus = " AND pav.pav_datos_registrales=1";
    } else if ($lstEstatus == "dca0") {
        $filtro_estatus = " AND pav.pav_datos_catastrales=0";
    } else if ($lstEstatus == "dca1") {
        $filtro_estatus = " AND pav.pav_datos_catastrales=1";
    } else if ($lstEstatus == "dpr0") {
        $filtro_estatus = " AND pav.pav_datos_propietarios=0";
    } else if ($lstEstatus == "dpr1") {
        $filtro_estatus = " AND pav.pav_datos_propietarios=1";
    }

    if ($lstPropietario != "") {
        $tipo = substr($lstPropietario, 0, 2);
        $per_id = substr($lstPropietario, 2);
        if ($tipo == "PF") {
            $filtro_propietario = " AND par.par_id IN (SELECT par_id FROM parcelas_propietarios WHERE ppr_tipo='PF' AND (per_id_propietario = $per_id OR per_id_representante=$per_id))";
        } else if ($tipo == "PJ") {
            $filtro_propietario = " AND par.par_id IN (SELECT par_id FROM parcelas_propietarios WHERE ppr_tipo='PJ' AND (per_id_propietario = $per_id OR per_id_representante=$per_id))";
        }

    }
    $sql = "SELECT
                par.*,
                obr.obr_idsigo,
                obr.obr_nombre,
                gti.gti_tipo,
                loc.loc_nombre,
                pav.pav_datos_registrales,
                pav.pav_datos_catastrales,
                pav_datos_propietarios,
                pav_permiso_paso
            FROM
                parcelas par
                LEFT OUTER JOIN gravamenes_tipo gti ON par.gti_id=gti.gti_id
                LEFT OUTER JOIN localidades loc ON par.loc_id=loc.loc_id ,
                obras obr,
                parcelas_avance_relevamiento pav
            WHERE
                par.obr_id=obr.obr_id
                AND par.par_id=pav.par_id
                AND obr.obr_eliminada=0
                AND par.par_eliminada=0
                $filtro_obra
                $filtro_inumero_parcela
                $filtro_estatus
                $filtro_propietario
            ORDER BY
                par.par_id DESC";

    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[98] en consulta a la base de datos ($result).";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$parcelas_arr = $db->getRows();

$html = <<<eod
<table class="table table-hover">
<caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Total de parcelas: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
	<thead class="bg-dark text-light">
		<tr>
            <th nowrap width="1%"></th>

            <th nowrap width="3%" data-toogle="tooltip" title="Datos registrales">(DR)</th>
            <th nowrap width="3%" data-toogle="tooltip" title="Datos catastraless">(DC)</th>
            <th nowrap width="3%" data-toogle="tooltip" title="Datos propietarios">(DP)</th>
            <th nowrap width="3%" data-toogle="tooltip" title="Permiso de ocupación">(PO)</th>
            <th nowrap width="3%" data-toogle="tooltip" title="Notificación de expropiación">(NE)</th>
            <th nowrap width="1%">ID</th>
            <th nowrap>ID Sigo</th>
            <th nowrap>Nombre obra</th>
            <th nowrap>N° de resolución</th>
            <th nowrap>N° de orden</th>
            <th nowrap>N° de identificación</th>
           <!-- <th nowrap>N° parcela</th>
            <th nowrap>Localidad</th>
            <th nowrap>Tipo parcela</th>
            <th nowrap>Lugar</th>
            <th nowrap>Lote afectado</th>
            <th nowrap>Sup. total inmueble</th>
            <th nowrap>Sup. afectación</th>
            <th nowrap>Mejoras</th>
            <th nowrap>Tipo gravámen</th> -->

            <th nowrap>Observaciones</th>
            <th nowrap>Fecha creación</th>
            <th nowrap>Usuario creó</th>
		</tr>
	</thead>
	<tbody>
eod;

for ($i = 0; $i < count($parcelas_arr); $i++) {

    $ti = $tf = $tools=$tool_ver = $tool_editar = $tool_eliminar = "";
    $data1 = encrypt_decrypt("encrypt", "PARC|" . $parcelas_arr[$i]['par_id'] . "|DOCUMENTO");
    $data2 = encrypt_decrypt("encrypt", "PARC|" . $parcelas_arr[$i]['par_id'] . "|PLANO");
    $data3 = encrypt_decrypt("encrypt", $parcelas_arr[$i]['par_id'] . "|" . $parcelas_arr[$i]['par_numero_orden']);
    $db->ExecuteSQL("SELECT arc.*,cat.*,usr.*
    FROM archivos arc, categorizacion_archivos cat,usuarios usr
    WHERE arc.cat_id=cat.cat_id AND arc.usuario=usr.usr_login AND arc.arc_eliminado=0 AND arc.arc_fuente='PARC' AND arc.arc_id_fuente=" . $parcelas_arr[$i]["par_id"]);
    $archivos_arr = $db->getRows();

    $db->ExecuteSQL("SELECT
        ppr.*,
        pre.pre_relacion,
        CONCAT(pfi.per_nombre,' ',pfi.per_apellido) nombre_persona_fisica,
        pju.per_razon_social razon_social
        FROM
        parcelas_propietarios ppr LEFT OUTER JOIN personas_fisicas pfi ON ppr.per_id_propietario = pfi.per_id
        LEFT OUTER JOIN personas_juridicas pju ON ppr.per_id_propietario = pju.per_id,
        parcela_relacion pre
        WHERE
        ppr.pre_id = pre.pre_id
        AND ppr.ppr_eliminado=0
        AND ppr.par_id=" . $parcelas_arr[$i]["par_id"]);
    $propietarios_arr = $db->getRows();

    //-- aca habilitamos los botones a nivel fila de grilla segun el perfil del usuario

    if (count($archivos_arr) > 0 || count($propietarios_arr) > 0) {
        $tools = <<<eod
            &nbsp;&nbsp;<a href="" ><i onClick="$('#trAdjuntos{$i}').toggle();$('#trPropietarios{$i}').toggle();if ($(this).hasClass('fa-chevron-down')){ $(this).toggleClass('fa-chevron-up')};return false;"  data-toggle="tooltip" title="ver archivos adjuntos" class="fa fa-chevron-down" style="font-size:0.9rem;color:#212121" ></i></a>&nbsp;&nbsp;
eod;
    }
    $tool_ver .= <<<eod
        &nbsp;&nbsp;<button onClick="$.abrirVentanaParcela('{$data3}',1)" class="btn btn-sm btn-default" data-toggle="tooltip" title="ver datos parcela"><i class="fas fa-eye text-primary btn-icon"></i></button>
        eod;
    $tool_editar = <<<eod
        &nbsp;&nbsp;<button onClick="$.abrirVentanaParcela('{$data3}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar parcela"><i class="fas fa-edit text-primary btn-icon"></i></button>
        &nbsp;&nbsp;<button onClick="$.abrirVentanaAdjuntarArchivo('{$data1}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="adjuntar documento"><i class="fas fa-file-alt text-primary btn-icon"></i></button>
	    &nbsp;&nbsp;<button onClick="$.abrirVentanaAdjuntarArchivo('{$data2}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="adjuntar plano"><i class="fas fa-pencil-ruler text-primary btn-icon"></i></button>
	    &nbsp;&nbsp;<button onClick="$.abrirVentanaCambioEstatus('{$data3}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="cambiar estado"><i class="fa fa-retweet text-primary btn-icon"></i></button>

        &nbsp;&nbsp;<button onClick="$.abrirVentanaAsignarPropietarios('{$data3}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="asignar propietarios"><i class="far fa-address-book text-primary btn-icon"></i></button>
eod;

    $tool_eliminar = <<<eod
        &nbsp;&nbsp;<button onClick="$.eliminarParcela({$parcelas_arr[$i]['par_id']},{$i})" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar parcela"><i class="fas fa-trash text-secondary btn-icon"></i></button>
eod;

    switch ($atributo_perfil) {
        case 1:
            $tools .= $tool_ver;
            break;
        case 2:
            $tools .= $tool_ver . $tool_editar;
            break;
        case 3:
            $tools .= $tool_ver . $tool_editar . $tool_eliminar;
            break;
        default:
           // $tools = "";
            break;
    }

    $db->ExecuteSQL('SELECT * FROM archivos WHERE arc_id_fuente = ' . $parcelas_arr[$i]["par_id"] . ' AND cat_id = 23 AND arc_eliminado = 0');
    $permiso_ocupacion = $db->getRows();

    $db->ExecuteSQL('SELECT * FROM archivos WHERE arc_id_fuente = ' . $parcelas_arr[$i]["par_id"] . ' AND cat_id = 24 AND arc_eliminado = 0');
    $notificacion_expropiacion = $db->getRows();

    if ($parcelas_arr[$i]["pav_datos_registrales"] == 1) {
        $clase_estatus_datos_registrales = "text-success";
    } else {
        $clase_estatus_datos_registrales = "text-danger";
    }
    if ($parcelas_arr[$i]["pav_datos_catastrales"] == 1) {
        $clase_estatus_datos_catastrales = "text-success";
    } else {
        $clase_estatus_datos_catastrales = "text-danger";
    }
    if ($parcelas_arr[$i]["pav_datos_propietarios"] == 1) {
        $clase_estatus_datos_propietarios = "text-success";
    } else {
        $clase_estatus_datos_propietarios = "text-danger";
    }
    
    if ($parcelas_arr[$i]["pav_permiso_paso"] == 1) {
        $clase_estatus_permiso_paso = "text-success";
    } else {
        $clase_estatus_permiso_paso = "text-danger";
    }
    if (count($notificacion_expropiacion) > 0) {
        $clase_estatus_notificacion_expropiacion = "text-success";
    } else {
        $clase_estatus_notificacion_expropiacion = "text-danger";
    }
    $html .= <<<eod
        <tbody id="trParcela{$i}">
			<tr>
                <td nowrap class="vert-align text-right" width="1%">{$ti}{$tools}{$tf}</td>

                <td class="vert-align"  data-toogle="tooltip" title="Datos registrales" >{$ti}<i class="fa fa-circle {$clase_estatus_datos_registrales}"></i>{$tf}</td>
                <td class="vert-align"  data-toogle="tooltip" title="Datos catastrales">{$ti}<i class="fa fa-circle {$clase_estatus_datos_catastrales}"></i>{$tf}</td>
                <td class="vert-align"  data-toogle="tooltip" title="Datos propietarios">{$ti}<i class="fa fa-circle {$clase_estatus_datos_propietarios}"></i>{$tf}</td>
                <td class="vert-align"  data-toogle="tooltip" title="Permiso de ocupación">{$ti}<i class="fa fa-circle {$clase_estatus_permiso_paso}"></i>{$tf}</td>
                <td class="vert-align"  data-toogle="tooltip" title="Notificación de expropiación">{$ti}<i class="fa fa-circle {$clase_estatus_notificacion_expropiacion}"></i>{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_id"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["obr_idsigo"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["obr_nombre"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["res_numero"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_numero_orden"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_identificacion"]}{$tf}</td>
               <!-- <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_numero_parcela"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["loc_nombre"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["pti_tipo"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_lugar"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_lote_afectado"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_superficie_total_inmueble"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_superficie_afectacion"]}{$tf}</td>
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_mejoras"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["gti_tipo"]}{$tf}</td> -->
                <td class="vert-align">{$ti}{$parcelas_arr[$i]["par_observaciones"]}{$tf}</td>
                <td class="vert-align">{$ti}{$ymd2dmy($parcelas_arr[$i]["fecha"])}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["usuario"]}{$tf}</td>
	    	</tr>
eod;
//---------------------------------------------------------------------------------------------------------------------
    if (count($archivos_arr) > 0) {
        $html .= <<<eod
    <tr id="trAdjuntos{$i}" style="display:none">
        <td></td>
        <td colspan="10">
            <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
            <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Archivos</span></caption>
            <tr>
                <td nowrap class="text-center bold" width="15%"></td>
                <td class="bold" >ID</td>
                <td class="bold" width="15%">Tipo</td>
                <td class="bold" width="15%">Categoría</td>
                <td class="bold" width="30%">Descripción</td>
                <td nowrap class="bold" width="10%">Fecha adjuntó</td>
                <td class="bold" >Usuario adjuntó</td>
            </tr>
eod;
        $tools = $tool_ver = $tool_editar = $tool_eliminar = "";
        for ($a = 0; $a < count($archivos_arr); $a++) {

            $data = encrypt_decrypt("encrypt", $archivos_arr[$a]['pac_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarArchivo({$archivos_arr[$a]["arc_id"]},{$a});return false" data-toggle="tooltip" title="eliminar archivo"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;
            $tool_ver = <<<eod
            &nbsp;&nbsp;&nbsp;<a class="btn btn-default btn-sm"  href=""  onClick="$.mostrarPDF('{$archivos_arr[$a]["arc_path"]}',{$a});return false;" data-toggle="tooltip" title="ver archivo"><i class="fa fa-eye text-primary"></i></a>
            <a class="btn btn-default btn-sm"  href=""  onClick="$.descargarArchivo('{$archivos_arr[$a]["arc_path"]}');return false;" data-toggle="tooltip" title="descargar archivo"><i class="fa fa-download text-primary"></i></a>
eod;

            switch ($atributo_perfil) {
                case 1:
                    $tools = $tool_ver;
                    break;
                case 2:
                    $tools = $tool_ver;
                    break;
                case 3:
                    $tools = $tool_ver . $tool_eliminar;
                    break;
                default:
                    $tools = "";
                    break;
            }

            $html .= <<<eod
            <tr id="trArchivo{$a}">
                <td nowrap class="text-right vert-align">{$tools}</td>
                <td class="vert-align">{$archivos_arr[$a]["arc_id"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["cat_tipo"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["cat_descripcion"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["arc_descripcion"]}</td>
                <td class="vert-align">{$ymd2dmy($archivos_arr[$a]["fecha"])}</td>
                <td nowrap class="vert-align">{$archivos_arr[$a]["usr_nombre"]}</td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
        </td>
    </tr>
    </tbody>
eod;
    }
//---------------------------------------------------------------------------------------------------------------------
    if (count($propietarios_arr) > 0) {
        $html .= <<<eod
    <tr id="trPropietarios{$i}" style="display:none">
        <td></td>
        <td colspan="10">
            <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
            <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Propietarios</span></caption>

            <tr>
                <td nowrap class="text-center bold" width="15%"></td>
                <td class="bold" >ID</td>
                <td class="bold" width="15%">Tipo persona</td>
                <td class="bold" width="15%">Porcentaje</td>
                <td nowrap class="bold" width="15%">Relación con el bien</td>
                <td class="bold" width="30%">Nombre o Razón Social</td>
                <td class="bold" width="30%">Representante</td>
                <td nowrap class="bold" width="10%">Fecha alta</td>
                <td nowrap class="bold" >Usuario alta</td>
            </tr>
eod;
        $tools = $tool_ver = $tool_editar = $tool_eliminar = "";
        for ($a = 0; $a < count($propietarios_arr); $a++) {

            $tools_propietario = "";
            $data = encrypt_decrypt("encrypt", $propietarios_arr[$a]['ppr_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarPropietario({$propietarios_arr[$a]["ppr_id"]},{$a});return false" data-toggle="tooltip" title="eliminar propietario"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;
eod;


            $nombre_o_razon_social = $propietarios_arr[$a]["ppr_tipo"] == "PF" ? $propietarios_arr[$a]["nombre_persona_fisica"] : $propietarios_arr[$a]["razon_social"];
            if ($propietarios_arr[$a]['per_id_representante'] == "1") {
                $data_prop = encrypt_decrypt("encrypt", $propietarios_arr[$a]['ppr_id'] . "|" . $nombre_o_razon_social . "|" . $propietarios_arr[$a]['per_id_propietario'] . "|" . $propietarios_arr[$a]['ppr_tipo']);

                $tools_propietario = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.asignarRepresentantePropietario('{$data_prop}')" data-toggle="tooltip" title="representates de propietario"><i class="fas fa-user text-secondary"></i></a>
eod;
            }
            switch ($atributo_perfil) {
                case 1:
                    $tools = "";
                    break;
                case 2:
                    $tools = "";
                    break;
                case 3:
                    $tools =$tools_propietario.$tool_eliminar;
                    break;
                default:
                    $tools = "";
                    break;
            }
            $html .= <<<eod
            <tr id="trPropietarios{$a}">
                <td nowrap class="text-right vert-align">{$tools}</td>
                <td class="vert-align">{$propietarios_arr[$a]["ppr_id"]}</td>
                <td class="vert-align">{$propietarios_arr[$a]["ppr_tipo"]}</td>
                <td class="vert-align">{$number_format($propietarios_arr[$a]["ppr_porcentaje"],2,",",".")}</td>
                <td nowrap class="vert-align">{$propietarios_arr[$a]["pre_relacion"]}</td>
                <td class="vert-align">{$nombre_o_razon_social}</td>
                <td class="vert-align"></td>
                <td nowrap class="vert-align">{$ymd2dmy($propietarios_arr[$a]["fecha"])}</td>
                <td nowrap class="vert-align">{$propietarios_arr[$a]["usuario"]}</td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
        </td>
    </tr>
    </tbody>
eod;
    }
//---------------------------------------------------------------------------------------------------------------------
} //for ($i = 0; $i < count($parcelas_arr); $i++) {

$html .= <<<eod
    </tbody>
</table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
