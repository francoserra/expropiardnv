<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- nuevo registro
if($_REQUEST["accion"] == 'insert') {
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA REPRESENTANTES
    //------------------------------------------------------------------------------------------------------------------

    if($_REQUEST["id_persona"] == $_REQUEST["id_propietario"] && $_REQUEST["tipo_persona_propietario"] == $_REQUEST["tipo_persona_representante"]){
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al asignar un representante. El propietario y el representante son la misma persona";
        echo json_encode($arr);
        exit;
    }

    $db->ExecuteSQL("SELECT * FROM parcelas_propietarios_representantes WHERE ppr_id = " . $_REQUEST["id_relacion"] . " AND per_tipo = '$lstTipoPersona' AND per_id_representante = " . $_REQUEST["id_persona"] . " AND pprep_eliminado = 0");
    $getData = $db->getRows();

    if(count($getData) > 0){ 
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al asignar un representante. Es probable que ya exista dicho representante para el propietario.";
        echo json_encode($arr);
        exit;
    }

    if($_REQUEST["id_persona_firmante"] == 0){
        $persona_firmante = 0;
        $id_firmante = 'null';
    } else {
        $persona_firmante = 1;
        $id_firmante = $_REQUEST["id_persona_firmante"];

        if($_REQUEST["id_persona_firmante"] == $_REQUEST["id_propietario"] && $_REQUEST["tipo_persona_propietario"] == "PF"){
            $arr["res"] = 0;
            $arr["msg"] = "Error[66] al asignar un firmante. El firmante y el propietario son la misma persona";
            echo json_encode($arr);
            exit;
        }
    }

    $sql = "INSERT INTO parcelas_propietarios_representantes
        (ppr_id,
        per_tipo,
        per_id_representante,
        pprep_tipo,
        pprep_firmante,
        per_id_firmante,
        fecha,
        usuario)
        VALUES
        (" . $_REQUEST["id_relacion"] . ",
        '$lstTipoPersona',
        " . $_REQUEST["id_persona"] . ",
        '$lstTipoRepresentante',
        $persona_firmante,
        $id_firmante,
        Now(),
        '" . $_SESSION["expropiar_usuario_nombre"] . "')";
        $result = $db->ExecuteSQL($sql);

        if ($result != 1) {
            $arr["res"] = 0;
            $arr["msg"] = "Error[66] al asignar un representante (ERR $result).";
            echo json_encode($arr);
            exit;
        } else {
            $arr["res"] = 1;
            $arr["msg"] = "Representante asignado con éxito.";
            echo json_encode($arr);
            exit;
        }
}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-representante-propietario") {
    $db->ExecuteSQL("UPDATE parcelas_propietarios_representantes SET pprep_eliminado=1 WHERE pprep_id=" . $_REQUEST["pprep_id"]);
    auditar($db, "representante propietario parcela", "baja", $_REQUEST["pprep_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if($_REQUEST["accion"] == 'tipo_representacion') {
    $tipo = $_REQUEST["tipo_rep"];
    $db->ExecuteSQL("SELECT * FROM representacion_tipo WHERE rti_tipo = '$tipo' ORDER BY rti_tipo");
    $representacion_tipos = $db->getRows();
    
    for ($i=0; $i < count($representacion_tipos); $i++) { 
        echo '<option value="'.$representacion_tipos[$i]['rti_representacion'].'">'.$representacion_tipos[$i]['rti_representacion'].'</option>';
    }
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if($_REQUEST["accion"] == 'get-representantes-propietario') {
    $id_relacion = $_REQUEST["id_relacion"];
    $db->ExecuteSQL("SELECT * FROM parcelas_propietarios_representantes WHERE ppr_id = " . $id_relacion . " AND pprep_eliminado = 0");
    $representantes_propietarios = $db->getRows();

    $table = <<<eod
    <table class="table table-sm table-striped">
        <thead class="bg-dark text-light">
            <tr>
                <td nowrap class="text-center bold" width="7%"></td>
                <td class="bold">Tipo Persona</td>
                <td class="bold">CUIT/CUIL</td>
                <td class="bold">Representante</td>
                <td class="bold">Tipo Representacion</td>
                <td class="bold">Firmante</td>
            </tr>
        </thead>
        <tbody>
eod;
    for ($i=0; $i < count($representantes_propietarios); $i++) { 
        $ti = $tf = $tools = "";

        $tools .= <<<eod
        <button onClick="$.eliminarRepresentantePropietario({$representantes_propietarios[$i]['pprep_id']},{$i})" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar representante propietario"><i class="fas fa-trash text-secondary btn-icon"></i></button>
eod;
        if($representantes_propietarios[$i]["per_tipo"] == "PF"){
            $tabla_busqueda = "personas_fisicas";
            $tipo_rep = "PERSONA FÍSICA";
        } else {
            $tabla_busqueda = "personas_juridicas";
            $tipo_rep = "PERSONA JURÍDICA";
        }

        //OBTENGO LOS DATOS DE LA PERSONA REPRESENTANTE
        $db->ExecuteSQL("SELECT * FROM $tabla_busqueda WHERE per_id = " . $representantes_propietarios[$i]["per_id_representante"]);
        $datos_representante = $db->getRows();

        if($representantes_propietarios[$i]["pprep_firmante"] == 1){
            //OBTENGO LOS DATOS DE LA PERSONA FIRMANTE
            $db->ExecuteSQL("SELECT * FROM personas_fisicas WHERE per_id = " . $representantes_propietarios[$i]["per_id_firmante"]);
            $datos_firmante = $db->getRows();
        }

        
        $table .= <<<eod
        <tr id="trRepresentantePropietario{$i}">
            <td nowrap class="vert-align text-center">{$ti}{$tools}{$tf}</td>
            <td nowrap class="vert-align">{$ti}{$tipo_rep}{$tf}</td>
eod;
        
        if($representantes_propietarios[$i]["per_tipo"] == "PF"){
            $table .= <<<eod
            <td nowrap class="vert-align">{$ti}{$datos_representante[0]["per_cuil"]}{$tf}</td>
            <td nowrap class="vert-align">{$ti}{$datos_representante[0]["per_nombre"]} {$datos_representante[0]["per_apellido"]}{$tf}</td>
eod;
        } else {
            $table .= <<<eod
            <td nowrap class="vert-align">{$ti}{$datos_representante[0]["per_cuit"]}{$tf}</td>
            <td nowrap class="vert-align">{$ti}{$datos_representante[0]["per_razon_social"]}{$tf}</td>
eod;
        }
        $table .= <<<eod
            <td nowrap class="vert-align">{$ti}{$representantes_propietarios[$i]["pprep_tipo"]}{$tf}</td>
eod;
        if($representantes_propietarios[$i]["pprep_firmante"] == 1){
            $table .= <<<eod
            <td nowrap class="vert-align">{$ti}{$datos_firmante[0]["per_dni"]} - {$datos_firmante[0]["per_nombre"]} {$datos_firmante[0]["per_apellido"]}{$tf}</td>
eod;
        } else {
            $table .= <<<eod
            <td nowrap class="vert-align">{$ti}N/A{$tf}</td>
eod;
        }
    }

    echo $table;
}

