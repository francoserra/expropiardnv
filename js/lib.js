$.showLoading = function () {
  $('body').append(
    '<div id="divSpinner12345678" class="text-center" style="position: absolute;top: 0%; right: 0; bottom: 0; left: 0;margin: auto;height:100vh;width: 100%;z-index:99999999"><div class="spinner-grow text-secondary " role="status" style="position: absolute;top: 40%; left: 50%;margin: auto;height:120px;width:120px"><span class="sr-only">Loading...</span></div></div>'
  )
}
//---------------------------------------------------------------------------------------------------------------------

$.hideLoading = function () {
  $('#divSpinner12345678').remove()
}
//---------------------------------------------------------------------------------------------------------------------
$.dmy2ymd = function (fecha) {
  return fecha
    .split('-')
    .reverse()
    .join('-')
}

//---------------------------------------------------------------------------------------------------------------------

$(window).scroll(function () {
  if ($(window).scrollTop() > 300 || $(window).scrollLeft() > 10) {
    $('#divSubir').fadeIn('slow')
  } else {
    $('#divSubir').fadeOut('slow')
  }
})
$('a.back-to-top').click(function () {
  $('html, body').animate(
    {
      scrollTop: 0,
      scrollLeft: 0
    },
    200
  )
  return false
})

//---------------------------------------------------------------------------------------------------------------------

$.showAlerta = function (tipo, titulo, mensaje, timeout = 5000) {
  if (tipo == 'ok') var s = 'OK'
  else if (tipo == 'error') var s = 'Error'
  else if (tipo == 'info') var s = 'Info'
  $('#spnAlerta' + s + 'Titulo').html(titulo)
  $('#spnAlerta' + s + 'Mensaje').html(mensaje)
  $('#divAlerta' + s)
    .show()
    .delay(timeout)
    .fadeOut()
}
//---------------------------------------------------------------------------------------------------------------------

$.fn.serializeIncludeDisabled = function () {
  let disabled = this.find(':input:disabled').removeAttr('disabled')
  let serialized = this.serialize()
  disabled.attr('disabled', 'disabled')
  return serialized
}
//---------------------------------------------------------------------------------------------------------------------
//-- [ENTER] pasa al siguiente campo
$.extend(jQuery.expr[':'], {
  focusable: function (el, index, selector) {
      return $(el).is('a, button, :input, [tabindex]');
  }
});

$(document).on('keypress', 'input,select', function (e) {
  if (e.which == 13) {
      e.preventDefault();
      // Get all focusable elements on the page
      var $canfocus = $(':focusable');
      var index = $canfocus.index(this) + 1;
      if (index >= $canfocus.length) index = 0;
      $canfocus.eq(index).focus();
  }
});
//---------------------------------------------------------------------------------------------------------------------

$.clearForm = function (myform) {
  $(':input', '#' + myform)
    .not(':button, :submit, :reset, :hidden')
    .val('')
    .removeAttr('checked')
    .removeAttr('selected')
}
//---------------------------------------------------------------------------------------------------------------------
//-- inicio modal search -------------------------------------------

$.mostrarVentanaSearch = function (titulo_ventana,tabla,campo_id,campo_busqueda1,campo_busqueda2,campo_mostrar1,campo_mostrar2,campo_mostrar3,id_campo_respuesta) {
    $.showLoading();
    $('#divVentanaModalSearch').load('app-search.php?titulo_ventana=' +encodeURIComponent(titulo_ventana) +'&tabla=' + tabla +'&campo_id=' +campo_id +'&campo_busqueda1=' +campo_busqueda1+'&campo_busqueda2=' +campo_busqueda2 +'&campo_mostrar1=' +campo_mostrar1+'&campo_mostrar2=' +campo_mostrar2+'&campo_mostrar3=' +campo_mostrar3,
    function () {
      $('#mdlVentanaSearch').modal({
        backdrop: 'static',
        keyboard: false
      })
      $('#mdlVentanaSearch').on('hidden.bs.modal', function () {
        //-- al seleccionar desde la modal searh se asigna el INFO en la variable g_search_response
        $('#' + id_campo_respuesta).val(g_search_response)
        setTimeout(() => {
          $('#' + id_campo_respuesta).focus()
        }, 300)
      })
    }
  )
}
//-- fin modal search -------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
document.body.style.zoom = '100%'
