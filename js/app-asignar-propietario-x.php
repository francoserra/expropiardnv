<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$arr = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($arr["pasa"] == 0) {exit;} else { $puede_modificar = $arr["pasa"] == 2 ? true : false;}
if (!$puede_modificar) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- nuevo registro
if ($_REQUEST["accion"] == 'insert') {
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA REPRESENTANTES
    //------------------------------------------------------------------------------------------------------------------
    $sql = "INSERT INTO personas_representantes
    (prp_tipo,
    per_id,
    rti_id,
    pjur_id)
    VALUES
    ('$lstTipoPersona',
    " . $_REQUEST["id_persona"] . ",
    $lstTipoRepresentante,
    " . $_REQUEST["id"] . ")";
    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al asignar un representante (ERR $result). $sql";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        $arr["msg"] = "Representante asignado con éxito.";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == 'tipo_representacion') {
    $tipo = $_REQUEST["tipo_rep"];
    $db->ExecuteSQL("SELECT * FROM representacion_tipo WHERE rti_tipo = '$tipo' ORDER BY rti_tipo");
    $representacion_tipos = $db->getRows();

    for ($i = 0; $i < count($representacion_tipos); $i++) {
        echo '<option value="' . $representacion_tipos[$i]['rti_id'] . '">' . $representacion_tipos[$i]['rti_representacion'] . '</option>';
    }
}
