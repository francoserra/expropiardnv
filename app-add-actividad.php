<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id_parcela = $arr[0];

//-- chequeo que usuario tenga permisos 
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- distritos para el filtro
$db->ExecuteSQL("SELECT * FROM actividades_tipo_tarea ORDER BY ati_tarea");
$tipos_tareas_arr = $db->getRows();

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAddActividad">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Nueva actividad</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div id="divVentanaModalSearch"></div>
                <form id="frmNuevaActividad" name="frmNuevaActividad">
                    <input id="txtIdParcela" name="txtIdParcela" type="hidden" value="<?=$id_parcela?>">  
                    <div class="form-group row mb-1 required">
                        <label for="dtFechaActividad" class="col-3 col-form-label text-right">Fecha de actividad&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <input id="dtFechaActividad" name="dtFechaActividad" class="form-control" type="date" required value=""/>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoActividad" class="col-3 col-form-label text-right">Tipo de actividad</label>
                        <div class="col-7">
                            <select class="form-control " id="lstTipoActividad" name="lstTipoActividad" required>
                                <option value=""></option>
                                <?php
                                    for ($i = 0; $i < count($tipos_tareas_arr); $i++) {
                                ?>
                                <option value="<?=$tipos_tareas_arr[$i]["ati_id"]?>"><?=$tipos_tareas_arr[$i]["ati_tarea"]?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtDescripcion" class="col-3 col-form-label text-right">Descripción&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-7">
                            <textarea class="form-control input-fieldsX text-uppercase" id="txtDescripcion" name="txtDescripcion" rows="4" value="<?=$datos_arr[0]["per_domicilio"]?>" required></textarea>
                        </div>
                    </div>

                    
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAddActividad').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    $(".modal").on("hidden.bs.modal", function() {
        //
    });
    $('#mdlVentanaAddActividad').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#mdlVentanaAddActividad #lstTipoPersona").focus();
        }, 250);
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmNuevaActividad").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-actividad-x.php", {
                            id: "<?=$id?>",
                            accion: 'insert',
                            datastring: $("#frmNuevaActividad").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;RESULTADO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        });
                } else {}
            }
        })
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmNuevaActividad #txtPersona").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtPersona").val().split("|");
            if (arr.length > 1) {
                $("#txtPersona").val(arr[1] + ", " + arr[2]);
                g_id_persona = arr[0];
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //-- buscamos las parcelas del propietario seleccionado
    $("#txtPersona").focusout(function () {
        $.showLoading();
        $.post("app-add-actividad-x.php", {
            id_propietario: g_id_persona,
            accion: 'get_parcelas_propietario',
        },
        function(response) {
            $.hideLoading();
            let json = $.parseJSON(response);
            $("#lstParcelas").html(json);
        });
    });
});
</script>