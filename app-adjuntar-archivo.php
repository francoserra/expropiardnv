<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$fuente = $arr[0];// OBRA PARC PJUR PFIS
$id_fuente = $arr[1];// id 
$cat_tipo=$arr[2];// documento o plano
//-- chequeo que usuario tenga permisos 
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT * FROM categorizacion_archivos WHERE cat_fuente='$fuente' AND cat_tipo='$cat_tipo' ORDER BY cat_descripcion");
$categorias_archivos_arr = $db->getRows();


?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAdjuntarArchivo">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Adjuntar <?=$cat_tipo=="DOCUMENTO"?"documento":"plano"?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="frmAdjuntarArchivo" name="frmAdjuntarArchivo">

                    <div class="form-group row mb-1 required">
                        <label for="lstCategoriaArchivo" class="col-3 col-form-label text-right">Tipo de <?=$cat_tipo=="DOCUMENTO"?"documento":"plano"?></label>
                        <div class="col-9">
                            <select id="lstCategoriaArchivo" name="lstCategoriaArchivo" class="form-control  text-uppercase" required>
                                <option value="" <?=count($categorias_archivos_arr)==1?"disabled":""?>></option>
                                <?php
for ($i = 0; $i < count($categorias_archivos_arr); $i++) {
?>
                                <option value="<?=$categorias_archivos_arr[$i]["cat_id"]?>"><?=$categorias_archivos_arr[$i]["cat_descripcion"]?></option>
                                <?php
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtDescripcion" class="col-3 col-form-label text-right">Descripción&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-9">
                            <input id="txtDescripcion" name="txtDescripcion" class="form-control  text-uppercase" type="text" maxlenght="100" value="">
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="uploadArchivo" class="col-3 col-form-label text-right">Archivo <?=$cat_tipo=="DOCUMENTO"?"PDF":"PDF,KMZ,KML"?></label>
                        <div class="col-md-9">
                            <div id="uploadArchivo" class="btn btn-primary"><span class="fa fa-upload"></span>&nbsp;Adjuntar archivo</div>
                        </div>
                    </div>

                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAdjuntarArchivo').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>

                </form>
            </div>
            <!-- <div class="modal-footer">
                <div class="col-12 text-right">

                </div>
            </div> -->
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_archivo = "";
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaAdjuntarArchivo').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    $('#mdlVentanaAdjuntarArchivo').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmAdjuntarArchivo #lstTipoArchivo").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-- upload
    var settings = {
        url: "app-adjuntar-archivo-x.php",
        method: "POST",
        fileName: "myfile",
        uploadStr: '<i class="fa fa-paperclip"></i>&nbsp;&nbsp;click para adjuntar (max. 100MB)',
        multiple: false,
        showProgress: true,
        maxFileCount: 1,
        dragDrop: false,
        showDelete: true,
        showAbort: true,
        showError: true,
        allowedTypes: <?=$cat_tipo=="DOCUMENTO"?"'pdf'":"'pdf,kmz,kml'"?>,
        maxFileSize: 100000000,
        dynamicFormData: function() {
            var data = {
                accion: "upload-archivo",
                fuente:"<?=$fuente?>"
            };
            return data;
        },
        onSubmit: function(data, files) {
            $("#frmAdjuntarArchivo #uploadArchivo").hide();
        },
        onSelect: function(files) {
            //console.log(files);//json
        },
        afterUploadAll: function() {},
        onCancel: function() {
            $("#frmAdjuntarArchivo #uploadArchivo").show();
            uploadObj.reset();
            g_archivo = "";
        },
        onSuccess: function(files, data, xhr) {
            let json = $.parseJSON(data);
            if (json["res"] == 1) {
                g_archivo = json["archivo"];
                $("#frmAdjuntarArchivo #uploadArchivo").hide();
            } else if (json["res"] == 0) {
                $("#frmAdjuntarArchivo #uploadArchivo").show();
                uploadObj.reset();
                g_archivo = "";
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                });
            }
        },
        onError: function(files, status, errMsg) {
            $("#frmAdjuntarArchivo #uploadArchivo").show();
            g_archivo = "";
            uploadObj.reset();
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + errMsg + "</span>"
            });
        },
        deleteCallback: function(data, pd) {
            g_archivo = "";
            $("#frmAdjuntarArchivo #uploadArchivo").show();
            pd.statusbar.hide();
        }
    };
    var uploadObj = $("#frmAdjuntarArchivo #uploadArchivo").uploadFile(settings);
    //-- submit formulario
    $("#frmAdjuntarArchivo").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-adjuntar-archivo-x.php", {
                            accion: "procesar-upload",
                            fuente: "<?=$fuente?>",
                            id_fuente: "<?=$id_fuente?>",
                            archivo: g_archivo,
                            datastring: $("#frmAdjuntarArchivo").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>",
                                callback: function(data) {
                                    //-- reset upload
                                    $("#frmAdjuntarArchivo #uploadArchivo").show();
                                    uploadObj.reset();
                                    g_archivo = "";
                                    //-- limpoiamos formulario
                                    $.clearForm("frmAdjuntarArchivo");
                                }
                            });
                        });
                } else {
                    //$('#mdlVentanaAdjuntarArchivo').modal('hide');
                }
            }
        }) //vex.dialog.confirm({
    });
});
</script>