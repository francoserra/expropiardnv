<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
//$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
//$id = $arr[0];
$par_id = $_GET["par_id"];
$tipo = $_GET["tipo"];
//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
//$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}
//if ($perfil_readonly) {exit;}

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaRegistroInmobiliario" style="z-index:99999999!important">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Nueva registro inmobiliario <?=$tipo?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmRegistroInmobiliario" name="frmRegistroInmobiliario">

                    <div class="form-group row mb-1">
                        <label for="txtNumero" class="col-3 col-form-label text-right">Número</label>
                        <div class="col-9">
                            <input id="txtNumero" name="txtNumero" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtFolio" class="col-3 col-form-label text-right">Folio</label>
                        <div class="col-9">
                            <input id="txtFolio" name="txtFolio" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtTomo" class="col-3 col-form-label text-right">Tomo</label>
                        <div class="col-9">
                            <input id="txtTomo" name="txtTomo" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtAnio" class="col-3 col-form-label text-right">Año</label>
                        <div class="col-9">
                            <input id="txtAnio" name="txtAnio" class="form-control  text-uppercase" type="text"  value="">
                        </div>
                    </div>
                    <div class="form-group row mb-1">
                        <label for="txtPlanilla" class="col-3 col-form-label text-right">Planilla</label>
                        <div class="col-9">
                            <input id="txtPlanilla" name="txtPlanilla" class="form-control  text-uppercase" type="text" maxlength="45" value="">
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaRegistroInmobiliario').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaRegistroInmobiliario').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        if ("<?=$tipo?>" == "FOLIO REAL" || "<?=$tipo?>" == "MINUTA") {
            $("#frmRegistroInmobiliario #txtFolio").attr("disabled", "disabled");
            $("#frmRegistroInmobiliario #txtTomo").attr("disabled", "disabled");
            $("#frmRegistroInmobiliario #txtPlanilla").attr("disabled", "disabled");
        }
        setTimeout(() => {
            $("#frmRegistroInmobiliario #txtNumero").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaRegistroInmobiliario').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    $.eliminar = function(reg_id, fila) {
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-registro-inmobiliario-x.php", {
                            accion: "eliminar",
                            reg_id: reg_id
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            $("#tr" + fila).hide();
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    }
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmRegistroInmobiliario").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-registro-inmobiliario-x.php", {
                            par_id: "<?=$par_id?>",
                            tipo: "<?=$tipo?>",
                            datastring: $("#frmRegistroInmobiliario").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            //console.log(json["msg"]);
                            if (json["res"] == 1) {
                                let msg=json["msg"];
                                $.post("app-add-registro-inmobiliario-x.php", {
                                        accion: "armar-tabla-registros-inmobiliarios",
                                        par_id: "<?=$par_id?>"
                                    },
                                    function(response) {
                                        let json = $.parseJSON(response);
                                        $("#divParcelaResgistrosInmobiliarios").html(json["html"]);
                                        $('#mdlVentanaRegistroInmobiliario').modal("hide");
                                        /*vex.dialog.alert({
                                            unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + msg + "</span>"
                                        });*/
                                    });
                            } else {
                                vex.dialog.alert({
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"]+ "</span>"
                                });
                            }
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
    //--------------------------------------------------------------------------------------------------------------------------------
});
</script>