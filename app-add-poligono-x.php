<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
//$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {header("Location: login");exit;}//if ($perfil_readonly) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "eliminar") {
    $db->ExecuteSQL("UPDATE parcelas_poligono SET eliminado=1 WHERE poligono_id=" . $_REQUEST["poligono_id"]);
    auditar($db, "parcelas_poligono", "baja", $_REQUEST["reg_id"], json_encode($_REQUEST));
    echo true;
    exit;

}
//---------------------------------------------------------------------------------------------------------------------

else if ($_REQUEST["accion"] == "armar-tabla-poligonos") {

    if ($_REQUEST["psup_id"] > 0) {
        $filtro = " AND psup_id=" . $_REQUEST["psup_id"];
    } 

    $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE eliminado=0 $filtro order by orden");
    $rows = $db->getRows();
    $html = <<<eod
    <table class="table table-sm  table-striped">
    <tr>
        <th></th>
        <th nowrap>lado</th>
        <th nowrap>Punto</th>
        <th nowrap>Punto Sig.</th>
        <th nowrap>Angulo Grad</th>
        <th nowrap>Angulo Min</th>
        <th nowrap>Angulo Seg</th>
        <th nowrap>Longitud</th>
        <th nowrap>Tipo</th>
        <th nowrap>Cuerda</th>
        <th nowrap>Flecha</th>
        <th nowrap>Radio</th>
        <th nowrap>Lindero</th>
       

    </tr>
eod;
for ($i = 0; $i < count($rows); $i++) {

    if ( isset($_REQUEST["readonly"])){
        $tools="";
    }else{
        $tools=<<<eod
        <button type="button" onClick="$.eliminarPoligonos({$rows[$i]["poligono_id"]},$i);return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar polígono"><i class="fas fa-trash text-secondary btn-icon"></i></button>

        <button type="button" onClick="$.editarPoligonos({$rows[$i]["poligono_id"]},$i);return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar polígono"><i class="fas fa-edit text-secondary btn-icon"></i></button>

        <button type="button" onClick="$.subir({$rows[$i]["poligono_id"]},$i);return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar polígono"><i class="fas fa-arrow-up text-secondary btn-icon"></i></button>
        <button type="button" onClick="$.bajar({$rows[$i]["poligono_id"]},$i);return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar polígono"><i class="fas fa-arrow-down text-secondary btn-icon"></i></button>
    
eod;
    }
    
        $html .= <<<eod
    <tr id="tr{$i}">
        <td class="vert-align">{$tools}</td>
        <td class="vert-align">{$ti}{$rows[$i]["lado"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["punto"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["punto_sig"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["angulo_grad"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["angulo_min"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["angulo_seg"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["longitud"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["tipo"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["cuerda"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["flecha"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["radio"]}{$tf}</td>
        <td class="vert-align">{$ti}{$rows[$i]["lindero"]}{$tf}</td>

    </tr>
eod;
    }
    $html .= <<<eod
    </table>
eod;
    $arr["res"] = 1;
    $arr["html"] = $html;
    echo json_encode($arr);
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "add-poligono") {
//$_REQUEST["psup_nombre"]
$db->ExecuteSQL("select max(orden) as orden from parcelas_poligono where psup_id = ". $_REQUEST["psup_id"] );
    $orden=$db->getRows()[0]["orden"] +1;

    $txtpunto = strtoupper($txtpunto);
    $txtpunto_sig = strtoupper($txtpunto_sig);

    if($txtangulo_grad == ""){$txtangulo_grad = 0;}
    if($txtangulo_min == ""){$txtangulo_min = 0;}
    if($txtangulo_seg == ""){$txtangulo_seg = 0;}
    if($txtcuerda == ""){$txtcuerda = 0;}
    if($txtflecha == ""){$txtflecha = 0;}
    if($txtradio == ""){$txtradio = 0;}
    $txtlindero = str_replace("'","&#39;",$txtlindero);

    if($poligono_id == ""){
        $sql = "INSERT INTO parcelas_poligono
        (par_id,
        psup_id,    
        lado,
        punto,
        punto_sig,
        angulo_grad,
        angulo_min,
        angulo_seg,
        longitud,
        tipo,
        cuerda,
        flecha,
        radio,
        lindero,
        orden,
        fecha,
        usuario)
        VALUES(

        " . $_REQUEST["par_id"] . ",
        " . $_REQUEST["psup_id"] . ",
        '$lstlado', 
        '$txtpunto', 
        '$txtpunto_sig', 
        '$txtangulo_grad', 
        '$txtangulo_min', 
        '$txtangulo_seg', 
        '$txtlongitud', 
        '$lsttipo', 
        '$txtcuerda', 
        '$txtflecha', 
        '$txtradio', 
        '$txtlindero', 
        $orden,
        Now(),
        '" . $_SESSION["expropiar_usuario_nombre"] . "')";

        $result = $db->ExecuteSQL($sql);

        if ($result != 1) {
            $arr["res"] = 0;
            //$arr["msg"] = "Error[62] al dar de alta el Polígono (ERR $result).";
            $arr["msg"] =$sql;
            echo json_encode($arr);
            exit;
        }

        $Descripcion_poligono =   getDescripcionPoligono($db, $_REQUEST["psup_id"] , $_REQUEST["psup_nombre"] );

        $sql = 'update parcelas_superficies_afectacion set psup_descripcion = "'.$Descripcion_poligono.'" where psup_id = ' . $_REQUEST["psup_id"];
        $result = $db->ExecuteSQL($sql);

        if ($result == 1) {
            auditar($db, "parcelas_poligono", "alta", $_REQUEST["id"], json_encode($_REQUEST));
           
            $arr["res"] = 1;
            $arr["msg"] = "Polígono dado de alta con éxito.";
            //  $arr["html"] = $html;
            echo json_encode($arr);
            exit;
        } else {
            $arr["res"] = 0;
            $arr["msg"] = "Error[62] al dar de alta el Polígono (ERR $result).";
            echo json_encode($arr);
            exit;
        }
    }
    else{
        $sql = "UPDATE parcelas_poligono
        SET 
        lado='$lstlado', 
        punto='$txtpunto', 
        punto_sig='$txtpunto_sig', 
        angulo_grad='$txtangulo_grad', 
        angulo_min='$txtangulo_min', 
        angulo_seg='$txtangulo_seg', 
        longitud='$txtlongitud', 
        tipo='$lsttipo', 
        cuerda='$txtcuerda', 
        flecha='$txtflecha', 
        radio='$txtradio', 
        lindero='$txtlindero', 
        fecha=Now(), 
        usuario='" . $_SESSION["expropiar_usuario_nombre"] . "' 

        WHERE poligono_id = " . $poligono_id; 

        $result = $db->ExecuteSQL($sql);
        if ($result != 1) {
            $arr["res"] = 0;
            $arr["msg"] = "Error[62] al dar de alta el Polígono (ERR $result).";
            echo json_encode($arr);
            exit;
        }

        $Descripcion_poligono =   getDescripcionPoligono($db, $_REQUEST["psup_id"] , $_REQUEST["psup_nombre"] );

        $sql = 'update parcelas_superficies_afectacion set psup_descripcion = "'.$Descripcion_poligono.'" where psup_id = ' . $_REQUEST["psup_id"];
        $result = $db->ExecuteSQL($sql);

        if ($result == 1) {
            $arr["res"] = 1;
            $arr["msg"] = "Polígono modificada con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $arr["res"] = 0;
            $arr["msg"] = "Error[109] al modificar Polígono (ERR $result). $sql";
            echo json_encode($arr);
            exit;
        }
        
    }
}
else if ($_REQUEST["accion"] == "get-poligono") {
    $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE poligono_id= ".$_REQUEST["ppol_id"]);
    $rows = $db->getRows();
    echo json_encode($rows);
    exit;

}
else if ($_REQUEST["accion"] == "subir-poligono") {
    
    $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE poligono_id= ".$_REQUEST["ppol_id"]);
    $rows = $db->getRows();
    $orden = $rows[0]["orden"];
    $psup_id = $rows[0]["psup_id"];
    
    if($orden > 1){
        $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE psup_id= ".$psup_id." and orden < ".$orden." order by orden desc  limit 1 " );
        $rows2 = $db->getRows();
        
        if(count($rows2)){       
            $anterior = $rows2[0]["orden"];
            $db->ExecuteSQL("update parcelas_poligono set orden = ".$anterior." where poligono_id = ".$_REQUEST["ppol_id"]);
            $db->ExecuteSQL("update parcelas_poligono set orden = ".$orden." where poligono_id = ".$rows2[0]["poligono_id"]);

            $Descripcion_poligono =   getDescripcionPoligono($db, $psup_id , $_REQUEST["psup_nombre"] );

            $sql = 'update parcelas_superficies_afectacion set psup_descripcion = "'.$Descripcion_poligono.'" where psup_id = ' . $psup_id;
            $result = $db->ExecuteSQL($sql);
            
        }

    }

    echo json_encode("OK ");
    exit;

}
else if ($_REQUEST["accion"] == "bajar-poligono") {
    
    $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE poligono_id= ".$_REQUEST["ppol_id"]);
    $rows = $db->getRows();
    $orden = $rows[0]["orden"];
    $psup_id = $rows[0]["psup_id"];
    
    //if($orden > 1){
        $db->ExecuteSQL("SELECT * FROM parcelas_poligono WHERE psup_id= ".$psup_id." and orden > ".$orden." order by orden desc  limit 1 " );
        $rows2 = $db->getRows();
        
        if(count($rows2)){       
            $posterior = $rows2[0]["orden"];
            $db->ExecuteSQL("update parcelas_poligono set orden = ".$posterior." where poligono_id = ".$_REQUEST["ppol_id"]);
            $db->ExecuteSQL("update parcelas_poligono set orden = ".$orden." where poligono_id = ".$rows2[0]["poligono_id"]);

            $Descripcion_poligono =   getDescripcionPoligono($db, $psup_id , $_REQUEST["psup_nombre"] );

            $sql = 'update parcelas_superficies_afectacion set psup_descripcion = "'.$Descripcion_poligono.'" where psup_id = ' . $psup_id;
            $result = $db->ExecuteSQL($sql);
            
        }

    //}

    echo json_encode("OK ");
    exit;

}
  