<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

// //-- los parametros que se le pasa a la ventana vienen encriptados
// $arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
// $id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");
?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaTipoPersona">
    <div class="modal-dialog  modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Selección</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmParcela" name="frmParcela">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoPersona" class="col-4 col-form-label text-right">Tipo de persona</label>
                        <div class="col-8">
                            <select class="form-control  text-uppercase" id="lstTipoPersona" name="lstTipoPersona" required>
                                <option value="fisica">Persona Física</option>
                                <option value="juridica">Persona Jurídica</option>
                            </select>
                        </div>
                    </div>
                    
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="button" class="btn btn-primary btn-lg" onClick="$.abrirVentanaNuevaPersona()"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaTipoPersona').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
var g_id_localidad = 0;
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    //--- abriri modal alta de personas -------------------------------
    $.abrirVentanaNuevaPersona = function() {
        $.showLoading();
        if($('#lstTipoPersona').val() != "") {
            $('#mdlVentanaTipoPersona').modal('hide');    
            $("#divVentanaModal").load($('#lstTipoPersona').val() == "fisica" ? "app-add-persona-fisica.php" : "app-add-persona-juridica.php"  , function() {
                $("#mdlVentanaPersona").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        } else {
            $.hideLoading();
            vex.dialog.alert('Debe seleccionar un tipo de persona')
        }
    };
    //---------------------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
});
</script>