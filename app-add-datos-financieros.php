<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$cuit_cuil = $arr[0];
$col_cuitcuil = $arr[1];
//-- chequeo que usuario tenga permisos 
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAgregarDatosFinancieros">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar datos financieros</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div id="divVentanaModalSearch"></div>
                <form id="frmAsignarDatosFinancieros" name="frmAsignarDatosFinancieros">
                    <input id="txtColumna" name="txtColumna" type="hidden" value="<?=$col_cuitcuil?>">
                    <div class="form-group row mb-1">
                        <label for="txtCuit" class="col-3 col-form-label text-right">CUIT/CUIL&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-6">
                            <input id="txtCuit" name="txtCuit" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$cuit_cuil?>" readonly>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtCutNumero" class="col-3 col-form-label text-right">N° Cuenta Única Tesoro&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-6">
                            <input id="txtCutNumero" name="txtCutNumero" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstEstadoCut" class="col-3 col-form-label text-right">Estado Cuenta Tesoro</label>
                        <div class="col-6">
                            <select class="form-control input-fieldsX text-uppercase" id="lstEstadoCut" name="lstEstadoCut" required>
                                <option value=""></option>
                                <option value="1">Aplicado</option>
                                <option value="0">Desactivado</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtBanco" class="col-3 col-form-label text-right">Entidad Bancaria&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-6">
                            <input id="txtBanco" name="txtBanco" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtCbu" class="col-3 col-form-label text-right">N° CBU&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-6">
                            <input id="txtCbu" name="txtCbu" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="" required>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoCuenta" class="col-3 col-form-label text-right">Tipo de Cuenta</label>
                        <div class="col-6">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoCuenta" name="lstTipoCuenta" required>
                                <option value=""></option>
                                <option value="CAJA AHORRO">Caja de Ahorro</option>
                                <option value="CUENTA CORRIENTE">Cuenta Corriente</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoMoneda" class="col-3 col-form-label text-right">Tipo de Moneda</label>
                        <div class="col-6">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoMoneda" name="lstTipoMoneda" required>
                                <option value=""></option>
                                <option value="PESOS">(AR$) - Pesos Argentinos</option>
                                <option value="DOLAR">(USD) - Dolar</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAgregarDatosFinancieros').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_archivo = "";
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    $(".modal").on("hidden.bs.modal", function() {
        //
    });
    $('#mdlVentanaAgregarDatosFinancieros').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmAsignarDatosFinancieros #lstTipoArchivo").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaAgregarDatosFinancieros').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmAsignarDatosFinancieros").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-datos-financieros-x.php", {
                            id: "<?=$id?>",
                            accion: 'insert',
                            datastring: $("#frmAsignarDatosFinancieros").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;RESULTADO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        });
                } else {}
            }
        })
    });
});
</script>