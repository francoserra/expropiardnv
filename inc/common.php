<?php
$number_format = "number_format";
$sec2hm = "sec2hm";
$ymd2dmy = "ymd2dmy";
$strtoupper = "strtoupper";
$strtolower = "strtolower";
$meses = array("", "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "setiembre", "octubre", "noviembre", "diciembre");

define("CONECTION_ERROR", -1);

define("TIMEOUT_ADVICE", 1); //dias de vigencia de losa avisos

define("ROWS_YES", 1);
define("ROWS_NO", 0);
define("EXIST", 1);
define("OK", 1);
define("NO_EXIST", -99);
define("EMPTYY", -99);
define("USER_NO_EXIST", -999);
define("LOGIN_EXIST", 199);
define("SUCCESS", 1);
define("UNSUCCESS", -1);
define("PASSWORD_ERROR", -1);
define("ACCESS_ERROR", -2);

//---------------------------------------------------------------------------------------------------------------------

function loggerLog($msg)
{
    file_put_contents(dirname(__DIR__) . "/log/logger.log", date("d-m-Y H:i:s") . "\t$msg\n", FILE_APPEND);
    return true;
}

//---------------------------------------------------------------------------------------------------------------------
function logSQLError($msg)
{
    //if ( file_exists("/var/www/gesttora/log/logsi") ){
    $ahora = date("d-m-Y h:i:s");
    $rta = file_put_contents(dirname(__DIR__) . "/log/sql.error.log", "$ahora\n$msg\n\n", FILE_APPEND);

    return true;
    //}
}
//---------------------------------------------------------------------------

function eliminarAcentos($cadena)
{
    $no_permitidas = array("á", "é", "í", "ó", "ú", "Á", "É", "Í", "Ó", "Ú", "ñ", "À", "Ã", "Ì", "Ò", "Ù", "Ã™", "Ã ", "Ã¨", "Ã¬", "Ã²", "Ã¹", "ç", "Ç", "Ã¢", "ê", "Ã®", "Ã´", "Ã»", "Ã‚", "ÃŠ", "ÃŽ", "Ã”", "Ã›", "ü", "Ã¶", "Ã–", "Ã¯", "Ã¤", "«", "Ò", "Ã", "Ã„", "Ã‹", "Ñ");
    $permitidas = array("a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "n", "N", "A", "E", "I", "O", "U", "a", "e", "i", "o", "u", "c", "C", "a", "e", "i", "o", "u", "A", "E", "I", "O", "U", "u", "o", "O", "i", "a", "e", "U", "I", "A", "E");
    $texto = str_replace($no_permitidas, $permitidas, $cadena);
    return $texto;
}

//---------------------------------------------------------------------------
# tiene que recibir la hora inicial y la hora final
//$horaini="10:05:20";
//$horafin="14:05:20";
function restarHoras($horaini, $horafin)
{
    $horai = substr($horaini, 0, 2);
    $mini = substr($horaini, 3, 2);
    $segi = substr($horaini, 6, 2);

    $horaf = substr($horafin, 0, 2);
    $minf = substr($horafin, 3, 2);
    $segf = substr($horafin, 6, 2);

    $ini = ((($horai * 60) * 60) + ($mini * 60) + $segi);
    $fin = ((($horaf * 60) * 60) + ($minf * 60) + $segf);

    $dif = $fin - $ini;

    $difh = floor($dif / 3600);
    $difm = floor(($dif - ($difh * 3600)) / 60);
    $difs = $dif - ($difm * 60) - ($difh * 3600);
    return ($difh * 60) + $difm + ($difs / 60); // date("H-i-s",mktime($difh,$difm,$difs));
}

//---------------------------------------------------------------------------

function dni2cuil($dni, $sexo)
{

    if (strlen($dni) < 7 || strlen($dni) > 8) {
        return "";
    } else if (strlen($dni) == 7) {
        $dni = "0" . $dni;
    }
    for ($i = 1; $i <= 3; $i++) {
        if ($sexo == "M") {
            if ($i == 1) {
                $cIniciales = "20";
            } else if ($i == 2) {
                $cIniciales = "23";
            } else if ($i == 3) {
                $cIniciales = "24";
            }

        } else {
            if ($i == 1) {
                $cIniciales = "27";
            } else if ($i == 2) {
                $cIniciales = "23";
            } else if ($i == 3) {
                $cIniciales = "24";
            }

        }
        for ($j = 0; $j <= 9; $j++) {
            /*--crea un cuit y va probando digitos verificadores */
            $cCuit = $cIniciales . $dni . $j;
            $c1 = substr($cCuit, 0, 1);
            $c2 = substr($cCuit, 1, 1);
            $c3 = substr($cCuit, 2, 1);
            $c4 = substr($cCuit, 3, 1);
            $c5 = substr($cCuit, 4, 1);
            $c6 = substr($cCuit, 5, 1);
            $c7 = substr($cCuit, 6, 1);
            $c8 = substr($cCuit, 7, 1);
            $c9 = substr($cCuit, 8, 1);
            $c10 = substr($cCuit, 9, 1);
            $c11 = substr($cCuit, 10, 1);
            $nSumatoria = (5 * $c1 + 4 * $c2 + 3 * $c3 + 2 * $c4 + 7 * $c5 + 6 * $c6 + 5 * $c7 + 4 * $c8 + 3 * $c9 + 2 * $c10);
            $nResto = $nSumatoria % 11;
            $nVerificador = 11 - $nResto;
            if ($nVerificador == $c11 || ($nResto == 0 && $c11 == 0)) {
                $lStop = true;
                break;
            }
        }
        if ($lStop) {
            break;
        }

    }
    return ($cCuit);
}

//-------------------------------------------------------------------------

function num_letra($num, $fem = false, $dec = true)
{

    $matuni[2] = "dos";
    $matuni[3] = "tres";
    $matuni[4] = "cuatro";
    $matuni[5] = "cinco";
    $matuni[6] = "seis";
    $matuni[7] = "siete";
    $matuni[8] = "ocho";
    $matuni[9] = "nueve";
    $matuni[10] = "diez";
    $matuni[11] = "once";
    $matuni[12] = "doce";
    $matuni[13] = "trece";
    $matuni[14] = "catorce";
    $matuni[15] = "quince";
    $matuni[16] = "dieciseis";
    $matuni[17] = "diecisiete";
    $matuni[18] = "dieciocho";
    $matuni[19] = "diecinueve";
    $matuni[20] = "veinte";
    $matunisub[2] = "dos";
    $matunisub[3] = "tres";
    $matunisub[4] = "cuatro";
    $matunisub[5] = "quin";
    $matunisub[6] = "seis";
    $matunisub[7] = "sete";
    $matunisub[8] = "ocho";
    $matunisub[9] = "nove";

    $matdec[2] = "veint";
    $matdec[3] = "treinta";
    $matdec[4] = "cuarenta";
    $matdec[5] = "cincuenta";
    $matdec[6] = "sesenta";
    $matdec[7] = "setenta";
    $matdec[8] = "ochenta";
    $matdec[9] = "noventa";
    $matsub[3] = 'mill';
    $matsub[5] = 'bill';
    $matsub[7] = 'mill';
    $matsub[9] = 'trill';
    $matsub[11] = 'mill';
    $matsub[13] = 'bill';
    $matsub[15] = 'mill';
    $matmil[4] = 'millones';
    $matmil[6] = 'billones';
    $matmil[7] = 'de billones';
    $matmil[8] = 'millones de billones';
    $matmil[10] = 'trillones';
    $matmil[11] = 'de trillones';
    $matmil[12] = 'millones de trillones';
    $matmil[13] = 'de trillones';
    $matmil[14] = 'billones de trillones';
    $matmil[15] = 'de billones de trillones';
    $matmil[16] = 'millones de billones de trillones';

    $num = trim((string) @$num);
    if ($num[0] == '-') {
        $neg = 'menos ';
        $num = substr($num, 1);
    } else {
        $neg = '';
    }

    while ($num[0] == '0') {
        $num = substr($num, 1);
    }

    if ($num[0] < '1' or $num[0] > 9) {
        $num = '0' . $num;
    }

    $zeros = true;
    $punt = false;
    $ent = '';
    $fra = '';
    for ($c = 0; $c < strlen($num); $c++) {
        $n = $num[$c];
        if (!(strpos(".,'''", $n) === false)) {
            if ($punt) {
                break;
            } else {
                $punt = true;
                continue;
            }

        } elseif (!(strpos('0123456789', $n) === false)) {
            if ($punt) {
                if ($n != '0') {
                    $zeros = false;
                }

                $fra .= $n;
            } else {
                $ent .= $n;
            }

        } else {
            break;
        }

    }
    $ent = '     ' . $ent;
    if ($dec and $fra and !$zeros) {
        $fin = ' con';
        $fra = $fra < 10 ? $fra . "0" : $fra;
        $fin .= ' ' . $fra . '/100 centavos';
        /*
    for ($n = 0; $n < strlen($fra); $n++) {
    if (($s = $fra[$n]) == '0')
    $fin .= ' cero';
    elseif ($s == '1')
    $fin .= $fem ? ' una' : ' un';
    else
    $fin .= ' ' . $matuni[$s];
    $fin .= ' '.$fra.' / 100 centavos';
     */
    } else {
        $fin = '';
    }

    if ((int) $ent === 0) {
        return 'Cero ' . $fin;
    }

    $tex = '';
    $sub = 0;
    $mils = 0;
    $neutro = false;
    while (($num = substr($ent, -3)) != '   ') {
        $ent = substr($ent, 0, -3);
        if (++$sub < 3 and $fem) {
            $matuni[1] = 'una';
            $subcent = 'os';
        } else {
            $matuni[1] = $neutro ? 'un' : 'uno';
            $subcent = 'os';
        }
        $t = '';
        $n2 = substr($num, 1);
        if ($n2 == '00') {
        } elseif ($n2 < 21) {
            $t = ' ' . $matuni[(int) $n2];
        } elseif ($n2 < 30) {
            $n3 = $num[2];
            if ($n3 != 0) {
                $t = 'i' . $matuni[$n3];
            }

            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        } else {
            $n3 = $num[2];
            if ($n3 != 0) {
                $t = ' y ' . $matuni[$n3];
            }

            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        }
        $n = $num[0];
        if ($n == 1) {
            $t = ' ciento' . $t;
        } elseif ($n == 5) {
            $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
        } elseif ($n != 0) {
            $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
        }
        if ($sub == 1) {
        } elseif (!isset($matsub[$sub])) {
            if ($num == 1) {
                $t = ' mil';
            } elseif ($num > 1) {
                $t .= ' mil';
            }
        } elseif ($num == 1) {
            $t .= ' ' . $matsub[$sub] . '?n';
        } elseif ($num > 1) {
            $t .= ' ' . $matsub[$sub] . 'ones';
        }
        if ($num == '000') {
            $mils++;
        } elseif ($mils != 0) {
            if (isset($matmil[$sub])) {
                $t .= ' ' . $matmil[$sub];
            }

            $mils = 0;
        }
        $neutro = true;
        $tex = $t . $tex;
    }
    $tex = $neg . substr($tex, 1) . $fin;
    return ucfirst($tex);
}

//---------------------------------------------------------------------------------------------------------------------
function microtime_float()
{
    list($useg, $seg) = explode(" ", microtime());
    return ((float) $useg + (float) $seg);
}

//---------------------------------------------------------------------------------------------------------------------
function logDebug($msg)
{
    //if ( file_exists("/var/www/gesttora/log/logsi") ){
    $ahora = date("d-m-Y h:i:s");
    $rta = file_put_contents("/tmp/sql.log", "$ahora\n$msg\n\n", FILE_APPEND);
    return true;
    //}
}

//---------------------------------------------------------------------------------------------------------------------
function Strip($value)
{
    if (get_magic_quotes_gpc() == 0) {
        return $value;
    } else {
        return stripslashes($value);
    }

}

//---------------------------------------------------------------------------------------------------------------------
function FormatDate($DateToFormat, $FormatMask)
{
    global $ShortWeekdays;
    global $Weekdays;
    global $ShortMonths;
    global $Months;

    if ($DateToFormat < 0) {
        $DateToFormat = "";
    }

    if (is_array($FormatMask) && strlen($DateToFormat)) {
        $masks = array(
            "GeneralDate" => "n/j/y, h:i:s A", "LongDate" => "l, F j, Y",
            "ShortDate" => "n/j/y", "LongTime" => "g:i:s A",
            "ShortTime" => "H:i", "d" => "j", "dd" => "d",
            "m" => "n", "mm" => "m", "yy" => "y", "yyyy" => "Y",
            "h" => "g", "hh" => "h", "H" => "G", "HH" => "H",
            "nn" => "i", "ss" => "s", "AM/PM" => "A", "am/pm" => "a",
        );
        $FormatedDate = "";
        for ($i = 0; $i < sizeof($FormatMask); $i++) {
            if (isset($masks[$FormatMask[$i]])) {
                $FormatedDate .= date($masks[$FormatMask[$i]], $DateToFormat);
            } else {
                switch ($FormatMask[$i]) {
                    case "ddd":
                        $FormatedDate .= $ShortWeekdays[date("w", $DateToFormat)];
                        break;
                    case "dddd":
                        $FormatedDate .= $Weekdays[date("w", $DateToFormat)];
                        break;
                    case "w":
                        $FormatedDate .= (date("w", $DateToFormat) + 1);
                        break;
                    case "ww":
                        $FormatedDate .= ceil((6 + date("z", $DateToFormat) - date("w", $DateToFormat)) / 7);
                        break;
                    case "mmm":
                        $FormatedDate .= $ShortMonths[date("n", $DateToFormat) - 1];
                        break;
                    case "mmmm":
                        $FormatedDate .= $Months[date("n", $DateToFormat) - 1];
                        break;
                    case "q":
                        $FormatedDate .= ceil(date("n", $DateToFormat) / 3);
                        break;
                    case "y":
                        $FormatedDate .= (date("z", $DateToFormat) + 1);
                        break;
                    case "n":
                        $FormatedDate .= intval(date("i", $DateToFormat));
                        break;
                    case "s":
                        $FormatedDate .= intval(date("s", $DateToFormat));
                        break;
                    case "A/P":
                        $am = date("A", $DateToFormat);
                        $FormatedDate .= $am[0];
                        break;
                    case "a/p":
                        $am = date("a", $DateToFormat);
                        $FormatedDate .= $am[0];
                        break;
                    case "GMT":
                        $gmt = date("Z", $DateToFormat) / (60 * 60);
                        if ($gmt >= 0) {
                            $gmt = "+" . $gmt;
                        }

                        $FormatedDate .= $gmt;
                        break;
                    default:
                        $FormatedDate .= $FormatMask[$i];
                        break;
                }
            }
        }
    } else {
        $FormatedDate = $DateToFormat;
    }
    return $FormatedDate;
}

//---------------------------------------------------------------------------------------------------------------------
function date_to_ddmmyyyyhhmmss($DateToFormat)
{
    //yyyy-mm-dd
    return (substr($DateToFormat, 8, 2) . "-" . substr($DateToFormat, 5, 2) . "-" . substr($DateToFormat, 0, 4) . " " . substr($DateToFormat, 11, 8));
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
function date_to_ddmmyyyy($DateToFormat)
{
    //yyyy-mm-dd
    return (substr($DateToFormat, 8, 2) . "-" . substr($DateToFormat, 5, 2) . "-" . substr($DateToFormat, 0, 4));
}
//---------------------------------------------------------------------------------------------------------------------
function dmy_hns__to__ymd_hns($dato)
{
    $tempa1 = array();
    $tempa1 = explode("-", substr($dato, 0, 10));
    $date = trim($tempa1[2] . "-" . $tempa1[1] . "-" . $tempa1[0]);
    $time = trim(substr($dato, 11, 10));
    return "$date $time";
}
//---------------------------------------------------------------------------------------------------------------------
function generate_password($len)
{
    $salt = "abcdefghijklmnopqrstuvwxyz1234567890";
    srand((double) microtime() * 1000000);
    $i = 0;
    $pass = "";
    while ($i < $len) {
        // largo de password
        $num = rand() % 33;
        $tmp = substr($salt, $num, 1);
        $pass = $pass . $tmp;
        $i++;
    }
    return $pass;
}

//---------------------------------------------------------------------------------------------------------------------
function decode_url($url)
{
    $a = split('&', $url);
    $i = 0;
    $j = 0;
    while ($i < count($a)) {
        $b = split('=', $a[$i]);
        $v1 = htmlspecialchars(urldecode($b[0]));
        $v2 = htmlspecialchars(urldecode($b[1]));
        $rta[$v1] = $v2;
        $i++;
    }
    return $rta;
}
//---------------------------------------------------------------------------------------------------------------------
function dmy2ymd($fecha, $separador = "-")
{
    return (substr($fecha, 6, 4) . $separador . substr($fecha, 3, 2) . $separador . substr($fecha, 0, 2));

}
//---------------------------------------------------------------------------------------------------------------------
function ymd2dmy($fecha, $separador = "-")
{
    return (substr($fecha, 8, 2) . $separador . substr($fecha, 5, 2) . $separador . substr($fecha, 0, 4));

}
//---------------------------------------------------------------------------------------------------------------------
function getResponse($socket)
{
    do {
        $line = fgets($socket, 4096);
        $wrets[] = $line;
        $info = stream_get_meta_data($socket);
    } while ($line != "\r\n");
    return $wrets;
}

//---------------------------------------------------------------------------------------------------------------------
function sec2hms($sec, $padHours = true)
{

    // holds formatted string
    $hms = "";

    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600);

    // add to $hms, with a leading 0 if asked for
    $hms .= ($padHours)
    ? str_pad($hours, 2, "0", STR_PAD_LEFT) . ':'
    : $hours . ':';

    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in
    // minutes past the hour: to get that, we need to
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60);

    // then add to $hms (with a leading 0 if needed)
    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT) . ':';

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    $seconds = intval($sec % 60);

    // add to $hms, again with a leading 0 if needed
    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hms;

}
function sec2hm($sec, $padHours = true)
{

    // holds formatted string
    $hm = "";

    // there are 3600 seconds in an hour, so if we
    // divide total seconds by 3600 and throw away
    // the remainder, we've got the number of hours
    $hours = intval(intval($sec) / 3600);

    // add to $hms, with a leading 0 if asked for
    $hm .= ($padHours)
    ? str_pad($hours, 2, "0", STR_PAD_LEFT) . ':'
    : $hours . ':';

    // dividing the total seconds by 60 will give us
    // the number of minutes, but we're interested in
    // minutes past the hour: to get that, we need to
    // divide by 60 again and keep the remainder
    $minutes = intval(($sec / 60) % 60);

    // then add to $hms (with a leading 0 if needed)
    $hm .= str_pad($minutes, 2, "0", STR_PAD_LEFT);

    // seconds are simple - just divide the total
    // seconds by 60 and keep the remainder
    // $seconds = intval($sec % 60);

    // add to $hms, again with a leading 0 if needed
    //  $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

    // done!
    return $hm;

}
//---------------------------------------------------------------------------------------------------------------------
function daydiff($from, $to)
{
    // dd/mm/yyyy o dd-mm-yyyy
    $s1 = mktime(0, 0, 0, date(intval(substr($to, 3, 2)) + 1), date(intval(substr($to, 0, 2))), date(intval(substr($to, 6, 4))));
    $s2 = mktime(0, 0, 0, date(intval(substr($from, 3, 2)) + 1), date(intval(substr($from, 0, 2))), date(intval(substr($from, 6, 4))));
    //print date("d",$s1-$s2);
    return ($s1 - $s2) / 86400;
}
//---------------------------------------------------------------------------------------------------------------------
function secdiff($start, $end)
{
// yyyy-dd-mm hh:nn:ss

    //calcula diferencias en segundos entre dos fechas
    $s1 = mktime(substr($start, 11, 2), substr($start, 14, 2), substr($start, 17, 2), substr($start, 5, 2) + 1, substr($start, 8, 2), substr($start, 0, 4));
    $s2 = mktime(substr($end, 11, 2), substr($end, 14, 2), substr($end, 17, 2), substr($end, 5, 2) + 1, substr($end, 8, 2), substr($end, 0, 4));
    return ($s2 - $s1);
}
//---------------------------------------------------------------------------------------------------------------------
function encrypt_decrypt($action, $string)
{
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = 'Ggkl_9910029-$#33FDf!!';
    $secret_iv = 'Kgkl_9910029-$#33FDf!!';
    // hash
    $key = hash('sha256', $secret_key);

    // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    if ($action == 'encrypt') {
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    } else if ($action == 'decrypt') {
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    }
    return $output;
}
//---------------------------------------------------------------------------------------------------------------------
function sysInfo()
{
    exec("cat /proc/cpuinfo", $details);
    $arr = explode(":", $details[4]);
    $cpu = "<p align='center'>Procesador: <b>$arr[1]</b></p>";
    /*
    total       used       free     shared    buffers     cached
    Mem:           502        348        154          0         11        215
     */
    exec("free -mt", $details1);
    $mem = "<p align='center'>Memoria: <b>" . trim(substr($details1[1], 13, 10)) . " MB</b></p>";

    exec("df --total -h", $details2);
    /*
    S.ficheros            Size  Used Avail Use% Montado en
    /dev/sda1              71G  1,8G   65G   3% /
    tmpfs                 982M     0  982M   0% /lib/init/rw
    udev                  977M  368K  977M   1% /dev
    tmpfs                 982M     0  982M   0% /dev/shm
    total                  74G  1,8G   68G   3%
     */
    for ($i = 0; $i < count($details2); $i++) {
        $ps1 = strpos($details2[$i], "total");
        if ($ps1 === false) {
            continue;
        } else {
            $hd = substr($details2[$i], 22, 5);

        }
    }
    $hd = "<p align='center'>Disco: <b>$hd</b></p>";
    return $cpu . $mem . $hd;
}

//--------------------------------------------------------------------
function putMemCache($variable, $valor)
{
    $memcache = new Memcache();
    $memcache->connect("localhost", 11211);
    $datos = $memcache->get($variable);
    $memcache->set($variable, $valor, false, 60 * 60 * 24); //cachados por 24 horas
    return true;
}
//--------------------------------------------------------------------
function getMemCache($variable)
{
    $memcache = new Memcache();
    $memcache->connect("localhost", 11211);
    $datos = $memcache->get($variable);
    if (!$datos) {
        return false;
    } else {
        //--Ya se encuentra en memoria
        return $datos;
    }
}
//--------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

function flushMemCache()
{
    $memcache = new Memcache();
    $memcache->connect("localhost", 11211);
    $memcache->flush();
    return true;
}

//--------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------

function deleteMemCache($variable)
{
    $memcache = new Memcache();
    $memcache->connect("localhost", 11211);
    $memcache->delete($variable);
    return true;
}

//--------------------------------------------------------------------

class ObjectToXML
{
    private $dom;
    public function __construct($obj)
    {
        $this->dom = new DOMDocument("1.0", "UTF8");
        $root = $this->dom->createElement(get_class($obj));
        foreach ($obj as $key => $value) {
            $node = $this->createNode($key, $value);
            if ($node != null) {
                $root->appendChild($node);
            }

        }
        $this->dom->appendChild($root);
    }
    private function createNode($key, $value)
    {
        $node = null;
        if (is_string($value) || is_numeric($value) || is_bool($value) || $value == null) {
            //if($value == NULL) $node = $this->dom->createElement($key);
            //else
            $node = $this->dom->createElement($key, (string) $value);
        } else {
            $node = $this->dom->createElement($key);
            //if($value != NULL) {
            foreach ($value as $key => $value) {
                $sub = $this->createNode($key, $value);
                if ($sub != null) {
                    $node->appendChild($sub);
                }

            }
            //}
        }
        return $node;
    }
    public function __toString()
    {
        return $this->dom->saveXML();
    }
}
//--------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------
function escape($string)
{
    if (get_magic_quotes_gpc()) {
        $string = stripslashes($string);
    }
    return mysql_real_escape_string($string);
}

//---------------------------------------------------------------------------------------------------------------------
function getGoogleMapsCodificacion($domicilio)
{

    $domicilio = str_replace(" ", "+", urlencode($domicilio));

    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $domicilio . "&sensor=false&key=AIzaSyDzvmlHmTEhWAZu8MNuR1hlOn58Klo_WZQ";
    $contenido = file_get_contents($url);
    $response = json_decode($contenido, true);
    if ($response['status'] != 'OK') {
        return null;
    }

    $geometry = $response['results'][0]['geometry'];
    $address_components = $response['results'][0]['formatted_address'];

    $longitude = $geometry['location']['lat'];
    $latitude = $geometry['location']['lng'];
    $formatted_address =
    $array = array(
        'latitude' => $geometry['location']['lng'],
        'longitude' => $geometry['location']['lat'],
        'location_type' => $geometry['location_type'],
        'administrative_area_level_1' => $response['results'][0]['address_components'][2]["long_name"],
        'formatted_address' => $address_components,
    );

    return $array;

}

//---------------------------------------------------------------------------------------------------------------------
function getGoogleMapsDistancia($origen, $destino, $mode = "driving")
{
    $origen = str_replace(" ", "+", urlencode($origen));
    $destino = str_replace(" ", "+", urlencode($destino));
    $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=$origen&destinations=$destino&mode=$mode&language=es-ES&sensor=false";
    $contenido = file_get_contents($url);
    $response = json_decode($contenido, true);
    if ($response['status'] != 'OK') {
        return null;
    }
    return $response['rows'][0]['elements'][0]['distance']['value'];
}

//---------------------------------------------------------------------------------------------------------------------
function ping($ip, $timeout = 5)
{
    $starttime = microtime(true);
    $file = fsockopen($ip, 80, $errno, $errstr, $timeout);
    $stoptime = microtime(true);
    $status = 0;

    if (!$file) {
        $status = -1;
    }
    // Site is down
    else {
        fclose($file);
        $status = ($stoptime - $starttime) * 1000;
        $status = floor($status);
    }
    return $status;
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
// fecha nacimiento formato YYYY-MM-DD
function calcularEdad($fecha_nacimiento)
{
//yyyy-mm-dd
    $fecha = time() - strtotime($fecha_nacimiento);
    $edad = floor((($fecha / 3600) / 24) / 360);
    return $edad == 0 ? 1 : $edad;
}
//---------------------------------------------------------------------------------------------------------------------//Y-m-d
function diferenciaDias($inicio, $fin)
{
    $date1 = new DateTime($inicio);
    $date2 = new DateTime($fin);
    $diff = $date1->diff($date2);
// will output 2 days
    return ($diff->days + 1);
}

//---------------------------------------------------------------------------------------------------------------------//Y-m-d
function sumarDiasAFecha($fecha, $dias)
{
    $f = date_create($fecha);
    date_add($f, date_interval_create_from_date_string("$dias days"));
    return date_format($f, 'Y-m-d');

}
//---------------------------------------------------------------------------------------------------------------------
/*
echo "1-".$_SERVER['SERVER_ADDR']."<br>";
echo "2-".get_ip_cliente()."<br>";//202.9 //proxy
echo "3-".$_SERVER['HTTP_X_FORWARDED_HOST']."<br>";
echo "4-".$_SERVER['HTTP_X_FORWARDED_SERVER']."<br>";
echo "5-".$_SERVER['REMOTE_ADDR']."<br>";

SIN PROXY
1-192.168.98.106
2-
3-
4-
5-192.168.202.69

CON PROXY Y PC (IP .121)
1-192.168.98.106
2-192.168.201.1213
3-
4-
5-192.168.202.8
 */

// Function to get the client ip address
function get_ip_cliente()
{
    $ipaddress = '';
    if ($_SERVER['HTTP_CLIENT_IP']) {
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    } else if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else if ($_SERVER['HTTP_X_FORWARDED']) {
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    } else if ($_SERVER['HTTP_FORWARDED_FOR']) {
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    } else if ($_SERVER['HTTP_FORWARDED']) {
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    } else if ($_SERVER['REMOTE_ADDR']) {
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    } else {
        $ipaddress = '';
    }

    return ($_SERVER['HTTP_CLIENT_IP'] . "," . $_SERVER['HTTP_X_FORWARDED_FOR'] . "," . $_SERVER['HTTP_X_FORWARDED'] . "," . $_SERVER['HTTP_FORWARDED_FOR'] . "," . $_SERVER['HTTP_FORWARDED'] . "," . $_SERVER['REMOTE_ADDR']);
}

//---------------------------------------------------------------------------------------------------------------------

function arr_search($array, $key, $value)
{
    for ($i = 0; $i < count($array); $i++) {
        if ($array[$i][$key] == $value) {
            return $i;
        }
    }
    return -1;
}

//---------------------------------------------------------------------------------------------------------------------
function getProvincia($valor_buscado, $referencia)
{ //$referencia = tipo de dato que viene en la variable $valor_buscado
    $objProvincias = (object) [
        'BUENOS AIRES' => 6,
        'CIUDAD AUTÓNOMA DE BUENOS AIRES' => 2,
        'CATAMARCA' => 10,
        'CHACO' => 22,
        'CHUBUT' => 26,
        'CÓRDOBA' => 14,
        'CORRIENTES' => 18,
        'ENTRE RIOS' => 30,
        'FORMOSA' => 34,
        'JUJUY' => 38,
        'LA PAMPA' => 42,
        'LA RIOJA' => 46,
        'MENDOZA' => 50,
        'MISIONES' => 54,
        'NEUQUÉN' => 58,
        'RIO NEGRO' => 62,
        'SALTA' => 66,
        'SAN JUAN' => 70,
        'SAN LUIS' => 74,
        'SANTA CRUZ' => 78,
        'SANTA FE' => 82,
        'SANTIAGO DEL ESTERO' => 86,
        'TIERRA DEL FUEGO, ANTÁRTIDA E ISLAS DEL ATLÁNTICO SUR' => 94,
        'TUCUMÁN' => 90,
    ];
    if ($referencia == 'nombre') {
        return $objProvincias->$valor_buscado;
    } else if ($referencia == 'id') {
        foreach ($objProvincias as $key => $id) {
            if ($id == $valor_buscado) {
                return $key;
            }
        }
    }
}
