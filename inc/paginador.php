<?php
if ($pagina_anterior < 0) {
    $disabled = "disabled";
    $onClick = "return false;";
} else {
    $disabled = "";
    $onClick = "$.consultar({$pagina_anterior});return false;";
}

$htmlf = <<<eod
	<nav style=" position: fixed!important;left:50%!important;bottom:-40px!important;opacity:0.8!important;transform: translate(-50%, -50%);margin: 0 auto;">
		  <ul class="pagination pagination-lg justify-content-center">
			<li class="page-item {$disabled}">
			<a class="page-link"  href="" onClick="{$onClick}">&laquo;</a></li>
		
eod;
for ($i = $pagina; $i < $pagina + 5; $i++) {
    $x = $i + 1;
    if ($i >= $_SESSION["total_paginas"]) {
        $disabled = "disabled";
        $onClick = "return false;";
    } else {
        $disabled = "";
        $onClick = "$.consultar($i);return false;";
    }
    if ($pagina == $i) {
        $disabled = "active";
    } else {
        $class = "";
    }

    $htmlf .= <<<eod
			<li class="page-item {$disabled}"><a  class="page-link" href="" onClick="{$onClick}">{$x}</a></li>
eod;
}

if ($pagina_siguiente == $_SESSION["total_paginas"]) {
    $disabled = "disabled";
    $onClick = "return false;";
} else {
    $disabled = "";
    $onClick = "$.consultar({$pagina_siguiente});return false;";

}
$htmlf .= <<<eod
			<li class="page-item {$disabled}"><a class="page-link"  href="" onClick="{$onClick}">&raquo;</a></li>
		  </ul>
		  </nav>
		

eod;
if ($_SESSION['total_paginas'] <= 1) {
    $htmlf = "";
}
$html=$html."<div style='margin-bottom:150px!important'>&nbsp;</div>".$htmlf;
