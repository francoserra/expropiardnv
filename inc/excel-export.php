<?php
session_start();
include "cdb.php";
include "../app-config.php";
include "common.php";
ini_set('memory_limit', '256M');
error_reporting(0);
set_time_limit(60);
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- obtengo nombres campos
$db->ExecuteSQL($_SESSION["sql"]);
//file_put_contents("/tmp/aaa",$result);
$col = $db->getColumnData();
$csv_export = '"';
for ($i = 0; $i < count($col); $i++) {
    $csv_export .= $col[$i] . '","';
}
$csv_export = substr($csv_export, 0, -1);
//-- final de linea
$csv_export .= '
';

//-- obtengo los registros
$db->ExecuteSQL($_SESSION["sql"]);
$rows = $db->getRows();

foreach ($rows as $val) {
    $buffer = fopen('php://temp', 'r+');
    fputcsv($buffer, $val);
    rewind($buffer);
    $csv_export .= fgets($buffer);
    fclose($buffer);
}

$archivo = date('dmYHis') . ".csv";
header("Content-type: text/x-csv");
header("Content-Disposition: attachment; filename=" . $archivo . "");
echo ($csv_export);
