<link href="css/toastr.css" rel="stylesheet" />
<script src="js/toastr.min.js"></script>

<script type="text/javascript">
var g_idle_seconds_counter;
$(document).ready(function() {
    var g_idle_seconds_counter = 0;
    document.onclick = function() {
        g_idle_seconds_counter = 0;
    };
    document.onmousemove = function() {
        g_idle_seconds_counter = 0;
    };
    document.onkeypress = function() {
        g_idle_seconds_counter = 0;
    };
    /*
     * logout de usuario, lo manda a la pantalla de login
     */
    $.logout = function() {
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    location.href = "login";
                } else {}
            }
        }) //vex.dialog.confirm({
    }
    /**
     * Chequea la actividad en pantalla del usuario, superado cierto tiempo se cierra automaticamente la sesion y lo manda al login
     */
    function checkIdleTime() {
        g_idle_seconds_counter++;
        if (g_idle_seconds_counter >= 3600) {
            location.href = "login";
        }
    }
    window.setInterval(checkIdleTime, 10000);
    /**
     * Actualiza estatus online ( esto se hace para evitar que ingresen mas de un agente con el mismo usuario)
     */
    $.actualizarLogout = function() {
        $.post("app-x.php", {
                accion: "actualizar-logout"
            },
            function(response) {});
    };
    setInterval(function() {
        $.actualizarLogout();
    }, 5000);
    $.showInfoObra = function() {
        toastr["info"]("No implementado", "ATENCIÓN");
    }
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": 10000,
        "extendedTimeOut": 0,
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "tapToDismiss": false
    }
    /* $('.dropdown').click(function() {
         $('.dropdown-menu').toggleClass('show');
     });*/
});
</script>

<!-- ##################################################################################################### -->
<!-- menu -->
<!-- ##################################################################################################### -->

<nav class="navbar navbar-expand-lg navbar-dark bg-primary  py-0  w-100" style="font-size:1.1em">
    <a class="text-light navbar-brand" href="#" onClick="$.logout();return false;" data-toggle="tooltip" title="Salir" style="text-decoration: none; background-color: transparent !important;"><span class="fa fa-power-off fa-2x"></span></a>

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuRelevamiento') != -1 ? 'active' : 'disabled'?>">
                <a id="mnuRelevamiento" class="nav-link dropdown-toggle <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuRelevamiento') != -1 ? 'active' : 'disabled'?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-check"></i>&nbsp;&nbsp;Relevamiento</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a id="mnuSubmenuGestionObras" class="dropdown-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSubmenuGestionObras') != -1 ? '' : 'disabled'?>" href="obras"><i class="fas fa-road"></i>&nbsp;&nbsp;Gestión de obras</a>

                    <div class="dropdown-divider"></div>
                    <a id="mnuSubmenuGestionParcelas" class="dropdown-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSubmenuGestionParcelas') != -1 ? '' : 'disabled'?>" href="parcelas" href="parcelas"><i class="fa fa-puzzle-piece"></i>&nbsp;&nbsp;Gestión de parcelas</a>

                    <div class="dropdown-divider"></div>
                    <a id="mnuSubmenuGestionPersonas" class="dropdown-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSubmenuGestionPersonas') != -1 ? '' : 'disabled'?>" href="personas"><i class="fas fa-user"></i>&nbsp;&nbsp;Gestión de personas</a>
                </div>
            </li>
            <li class="nav-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuMensura') != -1 ? 'active' : 'disabled'?>">
                <a id="mnuMensura" class="nav-link  <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuMensura') != -1 ? '' : 'disabled'?>" href="mensura"><i class="fas fa-pencil-ruler"></i>&nbsp;&nbsp;Mensura</a>
            </li>
            <li class="nav-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuTasacion') != -1 ? 'active' : ''?>">
                <a id="mnuTasacion" class="nav-link  <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuTasacion') != -1 ? '' : 'disabled'?>" href="tasacion"><i class="fas fa-dollar-sign"></i>&nbsp;&nbsp;Tasación</a>
            </li>
            <li class="nav-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuConvenioLiberacion') != -1 ? 'active' : ''?>">
                <a id="mnuConvenioLiberacion" class="nav-link  <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuConvenioLiberacion') != -1 ? '' : 'disabled'?>" href="#"><i class="fas fa-handshake"></i>&nbsp;&nbsp;Convenio y Liberación</a>
            </li>
            <li class="nav-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuReportes') != -1 ? 'active' : ''?>">
                <a id="mnuReportes" class="nav-link  <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuReportes') != -1 ? '' : 'disabled'?>" href="#"><i class="fas fa-list-alt"></i>&nbsp;&nbsp;Reportes</a>
            </li>

            <li class="nav-item dropdown <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSistema') != -1 ? 'active' : ''?>">
                <a id="mnuSistema" class="nav-link dropdown-toggle <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSistema') != -1 ? '' : 'disabled'?>" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-globe"></i>&nbsp;&nbsp;Sistema</a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    <a id="mnuSubmenuActividades" class="dropdown-item <?=arr_search($_SESSION['expropiar_menu_habilitado'], 'aut_menu', 'mnuSubmenuActividades') != -1 ? '' : 'disabled'?>" href="actividades"><i class="fas fa-calendar"></i>&nbsp;&nbsp;Gestión de actividades</a>

                </div>
            </li>

        </ul>
        <ul class="nav navbar-nav navbar-right" style="margin-right:10px!important">
            <li style="margin-top:0px!important;margin-right:20px!important">
                <a data-togogle="tooltip" title="alta de actividad" class="btn btn-sm btn-info" href="#" onClick="$.abrirVentanActividad();return false;"><i class="fa fa-calendar text-light" style="font-size:1.7em"></i></a>
            </li>
        </ul>

        <form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="search" placeholder="ID Sigo" aria-label="Buscar">
            <button class="btn btn-outline-light" onClick="$.showInfoObra();return false"><i class="fas fa-search text-light"></i></button>
        </form>
    </div>
</nav>

<table id="divBarraFiltros" cellpadding="5px" style="display:<?=!$habilitar_barra_filtros ? "none" : ""?>;border:1px solid #ccc;width:100%;margin-top:0px;background:#E0E0E0">
    <tr>
        <td id="contentBarraFiltros"></td>
    </tr>
</table>

<script>
    $.abrirVentanActividad = function() {
        $.showLoading();
        $("#divVentanaModal").load("app-select-parcela-actividad.php", function() {
            $("#mdlVentanaParcelaActividad").modal({
                backdrop: "static",
                keyboard: false
            });
        })
    };
</script>