<?php

//--------------------------------------------------------------------------------------------------------

class clsDBSQLSRV
{

    public $host;
    public $port;
    public $user;
    public $pass;
    public $database;

    public $connection;
    public $retVal;
    public $errornum;
    public $errormsg;
    public $debugMode;

    public $timer_start;
    public $timer_end;
    public $elapsed_time;

    public $total_queries;
    public $num_fields;
    public $field_name;

    public function clsDBSQLSRV()
    {
        $this->connection = 0;
        $this->retVal = 0;
        $this->port = 1433;
        $this->debugMode = false;
    }

    public function setUsername($sUsername)
    {
        $this->user = $sUsername;
    }
    public function setPassword($sPassword)
    {
        $this->pass = $sPassword;
    }
    public function setHost($sHost, $iPort = 1433)
    {
        $this->host = $sHost;
        $this->port = $iPort;
    }
    public function setDatabase($sDatabase)
    {
        $this->database = $sDatabase;
    }
    public function setDebugMode($bDebugmode)
    {
        $this->debugMode = $bDebugmode;
    }
    public function getTotalQueries()
    {
        return $this->total_queries;
    }
    public function openDB()
    {
        $connectionOptions = array(
            "Database" => $this->database,
            "Uid" => $this->user,
            "PWD" => $this->pass,
            "CharacterSet" => "UTF-8",
            "MultipleActiveResultSets" => false
        );

        $this->connection = sqlsrv_connect($this->host.",".$this->port, $connectionOptions);

        if (!$this->connection) {
            logSQLError( json_encode(sqlsrv_errors()) );
         // print_r( sqlsrv_errors());
            return false;
        }
        return $this->connection;
    }
    public function closeDB()
    {
        sqlsrv_close($this->connection);
    }

    public function closeRS()
    {

        if (intval($this->retVal) != 0 && intval($this->retVal) != 1) {
            sqlsrv_free_stmt($this->retVal);
        }
    }

    public function ping()
    {
        return mysqli_ping($this->connection);

    }
    public function ExecuteSQL($strSQL)
    {
        $this->closeRS();
        $this->startWatch();
        $this->retVal = sqlsrv_query($this->connection, $strSQL, array(), array("Scrollable" => SQLSRV_CURSOR_KEYSET ));
        $this->stopWatch();
  
        if ($this->debugMode) {
            echo $this->elapsed_time . $strSQL . "\n";
        }
        $this->total_queries++;
        if (false === $this->retVal) {

            if (($errors = sqlsrv_errors()) != null) {
                foreach ($errors as $error) {
                  
                    $this->errornum = $error['code'];
                    logSQLError($strSQL . " ; " . $error[ 'message'] .";".$error[ 'SQLSTATE']);
                }
            }else{
                logSQLDebug($strSQL . " ; " . $this->errornum); 
            }
           
            return $this->errornum;
        }

        return 1; //SUCCESS

    }
    public function ExecuteSP($stmt, $params)
    {
        $this->closeRS();
        $this->startWatch();
        $this->retVal = sqlsrv_query($this->connection, $stmt, $params);

        $this->stopWatch();
        if ($this->retVal === false) {
            if (($errors = sqlsrv_errors()) != null) {
                foreach ($errors as $error) {
             
                 
                    $this->errornum = $error['code'];
                    logSQLError($stmt . " ; " . $error[ 'message'] .";".$error[ 'SQLSTATE']);
                }
            }else{
                logSQLDebug($stmt . " ; " . $this->errornum);
            }
      
            return $this->errornum;
        }

        while ($obj = sqlsrv_fetch_object($this->retVal)) {
            return $obj->output;
        }
    }
    public function returnInsertId()
    {
        $this->retVal = sqlsrv_query($this->connection, "SELECT SCOPE_IDENTITY() insert_id");
        $this->executeSQL("SELECT SCOPE_IDENTITY()");
        $row = $this->getRows();
        return $row[0]["insert_id"];
        //   sqlsrv_next_result($this->connection);
        // sqlsrv_fetch($this->connection);
        //  return sqlsrv_get_field($this->connection, 1);
    }

    public function getColumnData()
    {

        /* Get field information for all fields */
        while ($finfo = mysqli_fetch_field($this->retVal)) {
            $columns[] = $finfo->name;

        }
        return $columns;
    }

    public function getRows()
    {
        if (0 !== sqlsrv_num_rows($this->retVal)){
        while ($row = sqlsrv_fetch_array($this->retVal)) {

            $rows[] = $row;
        }
    }
        return $rows;
    }
    public function startWatch()
    {
        list($foo, $bar) = explode(' ', microtime());
        $this->timer_start = $foo + $bar;
        unset($foo);
        unset($bar);
    }

    public function stopWatch()
    {
        list($foo, $bar) = explode(' ', microtime());
        $this->timer_end = $foo + $bar;
        unset($foo);
        unset($bar);
        $this->elapsed_time = round($this->timer_end - $this->timer_start, 5);
    }
}

//--------------------------------------------------------------------------------------------------------
