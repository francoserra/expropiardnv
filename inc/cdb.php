<?php

class clsDB
{

    public $host;
    public $port;
    public $user;
    public $pass;
    public $database;

    public $connection;
    public $retVal;
    public $errornum;
    public $errormsg;
    public $debugMode;

    public $timer_start;
    public $timer_end;
    public $elapsed_time;

    public $total_queries;
    public $num_fields;
    public $field_name;

    public function clsDB()
    {
        $this->connection = 0;
        $this->retVal = 0;
        $this->port = 3306;
        $this->debugMode = false;
    }

    public function setUsername($sUsername)
    {
        $this->user = $sUsername;
    }
    public function setPassword($sPassword)
    {
        $this->pass = $sPassword;
    }
    public function setHost($sHost, $iPort = 3306)
    {
        $this->host = $sHost;
        $this->port = $iPort;
    }
    public function setDatabase($sDatabase)
    {
        $this->database = $sDatabase;
    }
    public function setDebugMode($bDebugmode)
    {
        $this->debugMode = $bDebugmode;
    }
    public function getTotalQueries()
    {
        return $this->total_queries;
    }
    public function openDB()
    {
        $this->connection = mysqli_connect($this->host, $this->user, $this->pass, $this->database, $this->port);

        if (!$this->connection) {
            return false;
        }

        return $this->connection;
    }
    public function closeDB()
    {
        mysqli_close($this->connection);
    }

    public function closeRS()
    {
        if (intval($this->retVal) != 0 && intval($this->retVal) != 1) {
            mysqli_free_result($this->retVal);
        }
    }

    public function ping()
    {
        return mysqli_ping($this->connection);

    }
    public function ExecuteSQL($strSQL)
    {
        $this->closeRS();
        $this->startWatch();
// excuta a strSQL
        //$strSQL=mysqli_real_escape_string($this->connection,$strSQL);
        $this->retVal = mysqli_query($this->connection, $strSQL);
// executa a strSQL
        $this->stopWatch();
        if ($this->debugMode) {
            echo $this->elapsed_time . $strSQL . "\n";
        }
        $this->total_queries++;
        if (false == $this->retVal) {
            $this->errortxt = $this->connection->error;

            $this->errornum = "|".mysqli_errno($this->connection)."|";
            logDebug($strSQL . " ; " . $this->errornum."-".$this->errortxt);
            return $this->errornum.$this->errortxt;
        }

        return 1; //SUCCESS

    }
    public function ExecuteSQLMulti($strSQL)
    {
        $this->closeRS();
        $this->startWatch();
// excuta a strSQL
        $this->retVal = mysqli_multi_query($this->connection, $strSQL);
// executa a strSQL
        $this->stopWatch();
        if ($this->debugMode) {
            echo $this->elapsed_time . " - <b>" . $strSQL . "</b><br>\n";
        }
        $this->total_queries++;
        if (false == $this->retVal) {
            $this->errornum = $this->connection->error;
            logDebug($strSQL . " ; " . $this->errornum);
            return $this->errornum;
        }
        return 1; //SUCCESS

    }
    public function returnInsertId()
    {
        return mysqli_insert_id($this->connection);
    }

    public function getColumnData()
    {

        while ($finfo = mysqli_fetch_field($this->retVal)) {
            $columns[] = $finfo->name;

        }
        return $columns;
    }

    public function getRows()
    {
        $x = 0;
        $object = array();
        foreach ($this->retVal as $column => $value) {
            $object[$column] = $value;
        }
        mysqli_next_result($this->connection);
        return $object;
    }

    public function startWatch()
    {
        list($foo, $bar) = explode(' ', microtime());
        $this->timer_start = $foo + $bar;
        unset($foo);
        unset($bar);
    }

    public function stopWatch()
    {
        list($foo, $bar) = explode(' ', microtime());
        $this->timer_end = $foo + $bar;
        unset($foo);
        unset($bar);
        $this->elapsed_time = round($this->timer_end - $this->timer_start, 5);
    }
}
/*

class clsDB
{

public $host;
public $port;
public $user;
public $pass;
public $database;

public $connection;
public $retVal;
public $errornum;
public $errormsg;
public $debugMode;

public $timer_start;
public $timer_end;
public $elapsed_time;

public $total_queries;
public $num_fields;
public $field_name;

public function clsDB()
{
$this->connection = 0;
$this->retVal = 0;
$this->port = 3306;
$this->debugMode = false;
}

public function setUsername($sUsername)
{
$this->user = $sUsername;
}

public function setPassword($sPassword)
{
$this->pass = $sPassword;
}

public function setHost($sHost, $iPort = 3306)
{
$this->host = $sHost;
$this->port = $iPort;
}

public function setDatabase($sDatabase)
{
$this->database = $sDatabase;
}

public function setDebugMode($bDebugmode)
{
$this->debugMode = $bDebugmode;
}

public function getTotalQueries()
{
return $this->total_queries;
}

public function openDB()
{

$this->connection = mysql_connect($this->host . ":" .
$this->port, $this->user, $this->pass); // or  die('Could not connect: ' . mysql_error());
mysql_select_db($this->database, $this->connection);
return $this->connection;
}

public function closeDB()
{
mysql_close($this->connection) or false;
}

public function closeRS()
{
if ($this->retVal != 0 && $this->retVal != 1) {
mysql_free_result($this->retVal) or false;
}
}

public function ping()
{
return mysql_ping($this->connection);

}

public function ExecuteSQL($strSQL)
{
global $DEBUG;
$this->closeRS();

$this->startWatch();

// excuta a strSQL
$this->retVal = mysql_query($strSQL, $this->connection);

// executa a strSQL
$this->stopWatch();

if ($this->debugMode) {
echo $this->elapsed_time . " - <b>" . $strSQL . "</b><br>\n";
}

$this->total_queries++;

if (false == $this->retVal) {

$this->errornum = mysql_errno($this->connection);
if ($DEBUG == 1) {
logDebug($strSQL . " ; " . $this->errornum);
}

return $this->errornum;
}
if ($DEBUG == 1) {
logDebug($strSQL . " ; 1");
}

return 1; //SUCCESS

}
public function ExecuteMultiSQL($strSQL)
{

// excuta a strSQL
$this->retVal = mysqli_multi_query($this->connection, $strSQL);

if (false == $this->retVal) {
$this->errornum = mysql_errno($this->connection);
return $this->errornum;
}

return 1; //SUCCESS

}
public function ExecuteSQLX($strSQL)
{

$this->closeRS();

$this->startWatch();

// excuta a strSQL
$this->retVal = mysql_unbuffered_query($strSQL, $this->connection);
// executa a strSQL
$this->stopWatch();

if ($this->debugMode) {
echo $this->elapsed_time . " - <b>" . $strSQL . "</b><br>\n";
}

$this->total_queries++;

if (false == $this->retVal) {
$this->errornum = mysql_errno($this->connection);
return $this->errornum;
}

return 1; //SUCCESS

}
public function returnMysql_info()
{
return mysql_info($this->connection);
}

public function returnInsertId()
{
return mysql_insert_id($this->connection);
}

public function isRsEmpty()
{
return (!mysql_num_rows($this->retVal)) ? 1 : 0;
}

public function getRows()
{
$num_fields = mysql_num_fields($this->retVal);
$this->num_fields = $num_fields;
$x = 0;
while ($row = mysql_fetch_array($this->retVal)) {
for ($j = 0; $j < $num_fields; $j++) {
$name = mysql_field_name($this->retVal, $j);

$object[$x][$name] = $row[$name];
}
$x++;
}
if ($x > 0) {
return $object;
}
}

public function getColumnData()
{

$i = 0;
while ($i < mysql_num_fields($this->retVal)) {
$data = mysql_fetch_field($this->retVal, $i);
$meta[] = $data->name;
$i++;
}
return $meta;
}

public function startWatch()
{
list($foo, $bar) = explode(' ', microtime());
$this->timer_start = $foo + $bar;
unset($foo);
unset($bar);
}

public function stopWatch()
{
list($foo, $bar) = explode(' ', microtime());
$this->timer_end = $foo + $bar;
unset($foo);
unset($bar);
$this->elapsed_time = round($this->timer_end - $this->timer_start, 5);
}

}

function openDefaultDB()
{

$db = new clsDB;

$db->setUsername("root");
$db->setPassword("");
$db->setHost("localhost");
$db->setDatabase("DATABASENAME");
$db->setDebugmode(true);

$db->openDB();

return $db;

}

 */
