<?php
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);
unset($_SESSION["expropiar_usuario_id"]);

//-- abrimos base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
if (!$db->openDB()) {
    $arr["res"] = 0;
    $arr["msg"] = "Error[19] al conectarse a la base de datos.";
    echo json_encode($arr);
    exit;
}

//-- deserializamos los datos del formulario de login
parse_str($_REQUEST["datastring"]);

//----------------------------------------------------------------------------

if ($_REQUEST["accion"] == "login") {
    //-- google recaptcha 3: SOLO SE HABILITA EN PRODUCCION
    /* $recaptcha_url = 'https://www.google.com/recaptcha/api/siteverify';
    $recaptcha_secret = '6LcUsaEUAAAAAFHDtJ9iuD4X6aw0P6r1TgOueUov';
    $recaptcha_response = $_POST['recaptcha_response'];
    $ctx = stream_context_create(array('http' => array(
    'timeout' => 30,
    ),
    ));
    $recaptcha = file_get_contents($recaptcha_url . '?secret=' . $recaptcha_secret . '&response=' . $recaptcha_response, false, $ctx);
    $recaptcha = json_decode($recaptcha);
    if ($recaptcha->score >= 0.5) {
    // Verified - send email
    } else {
    $arr["res"] = 0;
    $arr["msg"] = "Error[46] acceso no permitido .";
    echo json_encode($arr);
    exit;
    }*/

    if (strpos($txtUsuario, "@") === false) {
        $usuario_ad = $txtUsuario;
    } else {
        $usuario_ad = explode("@", $txtUsuario)[0];
    }
    $usuario_ad = strtolower($usuario_ad);

    //-- vemos si el usuario se encuetra logeado, el mismo usuario no puede tener 2 sesiones abiertas al mismo tiempo !
    $db->ExecuteSQL("SELECT TOP 1 DATEDIFF(second, agl_fecha_logout, Now()) tiempo FROM login_control WHERE usuario='$usuario_ad' ORDER BY agl_id DESC ");
    $row = $db->getRows();
    if (count($row) > 0) {
        if ($row[0]["tiempo"] < 10) {
            $arr["res"] = 0;
            $arr["msg"] = "Error[65] el usuario <b>$usuario_ad</b> se encuentra logeado.";
            echo json_encode($arr);
            exit;
        }
    }
    //-- le pegamos al AD con el usuario y password ingresados en el login
    $datos_agente_arr = checkAccesoAD($usuario_ad, $txtPassword);
    //-- ups.... no pasa
    if ($datos_agente_arr == null) {
        $arr["res"] = 0;
        $arr["msg"] = "ERROR[70] usuario / contraseña inválidos.";
        echo json_encode($arr);
        exit;
    }

    //-- ok, validado
    if ($datos_agente_arr["pasa"] == 1) {
        $tmp_arr = json_decode($datos_agente_arr["grupos_ad"]);
        $perfil_usuario = "";
        for ($i = 0; $i < count($tmp_arr); $i++) {
            if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Consulta") {
                $perfil_usuario = $GLOBALS["CONSULTA"];
                $perfil_usuario_nombre = "CONSULTA";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Operador") {
                $perfil_usuario = $GLOBALS["OPERADOR"];
                $perfil_usuario_nombre = "OPERADOR";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Consulta_Ejecutiva") {
                $perfil_usuario = $GLOBALS["CONSULTA_EJECUTIVA"];
                $perfil_usuario_nombre = "CONSULTA_EJECUTIVA";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_administrador") {
                $perfil_usuario = $GLOBALS["ADMINISTRADOR"];
                $perfil_usuario_nombre = "ADMINISTRADOR";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Supervisor") {
                $perfil_usuario = $GLOBALS["SUPERVISOR"];
                $perfil_usuario_nombre = "SUPERVISOR";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Operador") {
                $perfil_usuario = $GLOBALS["OPERADOR"];
                $perfil_usuario_nombre = "OPERADOR";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Mensura") {
                $perfil_usuario = $GLOBALS["MENSURA"];
                $perfil_usuario_nombre = "MENSURA";
                break;
            } else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Operador_Mensura") {
                $perfil_usuario = $GLOBALS["OPERADOR_MENSURA"];
                $perfil_usuario_nombre = "OPERADOR_MENSURA";
                break;
            }else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Tasación") {
                $perfil_usuario = $GLOBALS["TASACION"];
                $perfil_usuario_nombre = "TASACION";
                break;
            }else if ($tmp_arr[$i] == "DNV-U-ExpropiAR_Contaduría") {
                $perfil_usuario = $GLOBALS["CONTADURIA"];
                $perfil_usuario_nombre = "CONTADURIA";
                break;
            }
        }

        if ($perfil_usuario == "") {
            //-- descomentar cuando se pruebe con un usuario realque tenga un perfil creado en el AD
            $arr["res"] = 0;
            $arr["msg"] = "ERROR[108] Usuario no tiene un perfil asociado.";
            echo json_encode($arr);
            exit;
        }

        $grupo_ad = $tmp_arr[$i];

        //-- comentar las 2 sig. lineas cuando se pase a produccion !!!!!
        if (isset($_REQUEST["perfil"])) {
            $perfil_usuario = $_REQUEST["perfil"];
            $perfil_usuario_nombre = $PERFIL[$perfil_usuario];
            $_SESSION["perfil_usuario"] = $perfil_usuario;
            //$datos_agente_arr["distrito_agente"] = "02";
        }


        $db->ExecuteSQL("SELECT dis_nombre FROM distritos WHERE dis_numero='" . $datos_agente_arr["distrito_agente"] . "'");
        $nombre_distrito = $db->getRows()[0]["dis_nombre"];
        //-- seteamos variables de session
        $_SESSION["expropiar_usuario_id"] = crc32($usuario_ad);
        $_SESSION["expropiar_usuario_login"] = $usuario_ad;
        $_SESSION["expropiar_usuario_grupo_ad"] = $grupo_ad;
        $_SESSION["expropiar_usuario_login_fecha"] = date("d/m/Y H:i");
        $_SESSION["expropiar_usuario_nombre"] = $datos_agente_arr["nombre_agente"];
        $_SESSION["expropiar_usuario_foto"] = $datos_agente_arr["foto_agente"];
        $_SESSION["expropiar_usuario_distrito"] = $datos_agente_arr["distrito_agente"];
        $_SESSION["expropiar_usuario_distrito_nombre"] = $nombre_distrito;
        $_SESSION["expropiar_usuario_perfil"] = $perfil_usuario;
        $_SESSION["expropiar_usuario_perfil_nombre"] = $perfil_usuario_nombre;

        //-- guardamos la IP desde donde accede el usuario
        $log_ip = get_ip_cliente();
        $db->ExecuteSQL("INSERT INTO login_logs (id,fecha,ip,usuario) VALUES (0,Now(),'$log_ip','$usuario_ad')");

        //-- registramos el login
        $db->ExecuteSQL("DELETE FROM login_control WHERE usuario='$usuario_ad'");
        $db->ExecuteSQL("INSERT INTO login_control (agl_fecha_login,agl_fecha_logout,usuario,agl_ip)  VALUES (Now(),Now(),'$usuario_ad','$log_ip')");

        //-- guardamos el login y perfil del usuario para llevar un registro de los usuario y su perfil ( NO SE GUARDA LA PASSWORD !!!! )
        $db->ExecuteSQL("DELETE FROM usuarios WHERE  usr_login='$usuario_ad'");
        $result = $db->ExecuteSQL("INSERT INTO usuarios (usr_login, usr_nombre,usr_fecha_login,usr_distrito,usr_perfil) VALUES ('$usuario_ad','" . $datos_agente_arr["nombre_agente"] . "',Now(),'" . $_SESSION["expropiar_usuario_distrito_nombre"] . "','$perfil_usuario_nombre')");

        //--
        $arr["res"] = 1;
        $arr["msg"] = "";
        $arr["nombre"] = $datos_agente_arr["nombre_agente"];
        $arr["foto"] = $datos_agente_arr["foto_agente"];
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 0;
        $arr["msg"] = "Error[156] usuario / contraseña inválidos.";
        echo json_encode($arr);
        exit;
    }
}
