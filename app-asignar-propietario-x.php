<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- nuevo registro
if ($_REQUEST["accion"] == 'insert') {
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA REPRESENTANTES
    //------------------------------------------------------------------------------------------------------------------

    /*   $sql = "INSERT INTO personas_representantes
    (prp_id,
    prp_tipo,
    per_id,
    rti_id)
    VALUES
    (0,
    $lstRol,
    '$lstTipoPersona',
    $lstTieneRepresentante)";*/

    $db->ExecuteSQL("SELECT SUM(ppr_porcentaje) total_porcentaje FROM parcelas_propietarios WHERE ppr_eliminado=0 AND par_id=" . $_REQUEST["id"]);
    if ($db->getRows()[0]["total_porcentaje"] > 100) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[48] se superó el 100%.";
        echo json_encode($arr);
        exit;
    }
    $sql = "INSERT INTO parcelas_propietarios
    (ppr_id,
    par_id,
    ppr_tipo,
    pre_id,
    per_id_propietario,
    per_id_representante,
    ppr_porcentaje,
    fecha,
    usuario)
   VALUES
    (0,
    " . $_REQUEST["id"] . ",
    '$lstTipoPersona',
    $lstRol,
    " . $_REQUEST["id_persona"] . ",
    '$lstTieneRepresentante',
    '$txtPorcentaje',
   Now(),
   '" . $_SESSION["expropiar_usuario_nombre"] . "')";

    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {

        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al asignar un propietario. Es probable que ya exista un propietario.";
        echo json_encode($arr);
        exit;
    } else {

        $db->ExecuteSQL("SELECT 100-SUM(ppr_porcentaje) total_porcentaje_asignado FROM parcelas_propietarios WHERE par_id=".$_REQUEST["id"]);
        $total_porcentaje_asignado = $db->getRows()[0]["total_porcentaje_asignado"];
        $arr["res"] = 1;
        $arr["msg"] = "Propietario asignado con éxito.";
        $arr["total_porcentaje_asignado"] = $total_porcentaje_asignado;
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == 'tipo_representacion') {
    $tipo = $_REQUEST["tipo_rep"];
    $db->ExecuteSQL("SELECT * FROM representacion_tipo WHERE rti_tipo = '$tipo' ORDER BY rti_tipo");
    $representacion_tipos = $db->getRows();

    for ($i = 0; $i < count($representacion_tipos); $i++) {
        echo '<option value="' . $representacion_tipos[$i]['rti_id'] . '">' . $representacion_tipos[$i]['rti_representacion'] . '</option>';
    }
}
