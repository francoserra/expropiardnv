<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(10);

if (!isset($_SESSION["expropiar_usuario_id"]) || $_SESSION["expropiar_usuario_id"] < 1) {
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "actualizar-logout") {
    $db = new clsDB();
    $db->setHost($GLOBALS["HOST_DB"], $GLOBALS["PORT_DB"]);
    $db->setUsername($GLOBALS["USUARIO_DB"]);
    $db->setPassword($GLOBALS["PASSWORD_DB"]);
    $db->setDatabase($GLOBALS["DATABASE_DB"]);
    $db->setDebugmode(false);
    $db->openDB();
    $db->ExecuteSQL("SET NAMES 'utf8'");
    $db->ExecuteSQL("UPDATE login_control SET agl_fecha_logout=Now() WHERE age_id=" . $_SESSION["siper_usuario_id"]);
    echo 1;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "search") {

    $db = new clsDB();
    $db->setHost($GLOBALS["HOST_DB"], $GLOBALS["PORT_DB"]);
    $db->setUsername($GLOBALS["USUARIO_DB"]);
    $db->setPassword($GLOBALS["PASSWORD_DB"]);
    $db->setDatabase($GLOBALS["DATABASE_DB"]);
    $db->setDebugmode(false);
    $db->openDB();
    $db->ExecuteSQL("SET NAMES 'utf8'");
    //--
    $tabla = $_REQUEST["tabla"];
    $campo_id = $_REQUEST["campo_id"];
    $campo_busqueda1 = $_REQUEST["campo_busqueda1"];
    $campo_busqueda2 = $_REQUEST["campo_busqueda2"];
    $campo_mostrar1 = $_REQUEST["campo_mostrar1"];
    $campo_mostrar2 = $_REQUEST["campo_mostrar2"];
    $campo_mostrar3 = $_REQUEST["campo_mostrar3"];
    $search = strtoupper($_REQUEST["search"]);

    if (trim($campo_mostrar3) != "") {
        $sql_add = "UPPER($campo_mostrar1) $campo_mostrar1,UPPER($campo_mostrar2) $campo_mostrar2,UPPER($campo_mostrar3) $campo_mostrar3";
        $order_by = "$campo_mostrar2,$campo_mostrar3";
    } else {
        $sql_add = "UPPER($campo_mostrar1) $campo_mostrar1,UPPER($campo_mostrar2) $campo_mostrar2";
        $order_by = $campo_mostrar2;
    }

    $sql = "SELECT
        $campo_id,$sql_add
    FROM
        $tabla
    WHERE
        (UPPER($campo_busqueda1) LIKE '%$search%' OR UPPER($campo_busqueda2) LIKE '%$search%' )
    ORDER BY
        $order_by
    LIMIT 100";

    $result = $db->ExecuteSQL($sql);
    $rows = $db->getRows();
    file_put_contents("/var/www/html/expropiar/tmp/aaa",$sql);
    $html .= <<<eod
    <table class="table table-sm table-striped table-hover" style="cursor:default">
    <tr>
        <th></th>
        <th></th>
    </tr>
eod;
    for ($i = 0; $i < count($rows); $i++) {
        $largo_search = strlen($_REQUEST["search"]);
        $ps = strpos($rows[$i]["$campo_mostrar1"], $search);
        if ($ps !== false) {
            $mostrar1 = substr($rows[$i]["$campo_mostrar1"], 0, $ps) . "<span class='table-warning'>" . substr($rows[$i]["$campo_mostrar1"], $ps, $largo_search) . "</span>" . substr($rows[$i]["$campo_mostrar1"], $ps + $largo_search);
        } else {
            $mostrar1 = $rows[$i]["$campo_mostrar1"];
        }

        $ps = strpos($rows[$i]["$campo_mostrar2"], $_REQUEST["search"]);
        if ($ps !== false) {
            $mostrar2 = substr($rows[$i]["$campo_mostrar2"], 0, $ps) . "<span class='bg-success'>" . substr($rows[$i]["$campo_mostrar2"], $ps, $largo_search) . "</span>" . substr($rows[$i]["$campo_mostrar2"], $ps + $largo_search);
        } else {
            $mostrar2 = $rows[$i]["$campo_mostrar2"];
        }

        $ps = strpos($rows[$i]["$campo_mostrar3"], $_REQUEST["search"]);
        if ($ps !== false) {
            $mostrar3 = substr($rows[$i]["$campo_mostrar3"], 0, $ps) . "<span class='bg-success'>" . substr($rows[$i]["$campo_mostrar3"], $ps, $largo_search) . "</span>" . substr($rows[$i]["$campo_mostrar3"], $ps + $largo_search);
        } else {
            $mostrar3 = $rows[$i]["$campo_mostrar3"];
        }

        $html .= <<<eod
        <tr onClick="g_search_response='{$rows[$i]["$campo_id"]}|{$rows[$i]["$campo_mostrar1"]}|{$rows[$i]["$campo_mostrar2"]}|{$rows[$i]["$campo_mostrar3"]}';$('#mdlVentanaSearch').modal('hide')">

            <td class="vert-align">{$mostrar1}</td>
            <td class="vert-align">{$mostrar2}</td>
            <td class="vert-align">{$mostrar3}</td
        </tr>
eod;
    }
    $html .= <<<eod
</table>

eod;

    echo $html;
    exit;
}
