<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------
$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "cambiar-estado-actividad") {
    $db->ExecuteSQL("UPDATE actividades SET act_cumplida = " . $_REQUEST["estado_nuevo"] . ", act_resolucion = '" . strtoupper($_REQUEST["resolucion_actividad"]) . "' WHERE act_id = " . $_REQUEST["act_id"]);
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-actividad") {
    $db->ExecuteSQL("DELETE FROM actividades WHERE act_id = " . $_REQUEST["act_id"]);
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR DESCRIPCION
    //------------------------------------------------------------------------

    if ($txtDescripcion != "") {
        $filtro_descripcion = " AND act.act_descripcion LIKE '%" . strtoupper($txtDescripcion) . "%' ";
    }

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR TIPO ACTIVIDAD
    //------------------------------------------------------------------------

    if ($lstTipoActividad != "") {
        $filtro_tipo = " AND act.ati_id = " . $lstTipoActividad;
    }

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR FECHA ACTIVIDAD
    //------------------------------------------------------------------------

    if ($dtFechaActividad != "") {
        $filtro_fecha = " AND act.act_fecha = '" . $dtFechaActividad . "'";
    }

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR FECHA ACTIVIDAD
    //------------------------------------------------------------------------

    if ($lstEstado != "") {
        $filtro_estado = " AND act.act_cumplida = " . $lstEstado;
    }

    //------------------------------------------------------------------------
    // CONSULTA A LA BASE DE DATOS
    //------------------------------------------------------------------------
    $sql = "SELECT act.*, par.par_numero_orden
            FROM
                actividades act, parcelas par
            WHERE
                act.usuario = '" . $_SESSION["expropiar_usuario_login"] . "'
                AND par.par_id = act.par_id
                $filtro_descripcion
                $filtro_tipo
                $filtro_fecha
                $filtro_estado
            ORDER BY
                act.act_fecha ASC";

    //------------------------------------------------------------------------
    //------------------------------------------------------------------------

    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[98] en consulta a la base de datos ($result)";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$actividades_arr = $db->getRows();

$html = <<<eod
<table class="table table-hover table-stripped ">
<caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Total de actividades: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
	<thead class="bg-dark text-light">
		<tr>
            <th nowrap width="1%"></th>

            <th nowrap>ID</th>
            <th nowrap>Parcela</th>
            <th nowrap>Fecha de actividad</th>
            <th nowrap>Tipo de actividad</th>
            <th nowrap>Descripción de actividad</th>
            <th nowrap>Estado actividad</th>
            <th nowrap>Resolución de actividad</th>
            <th nowrap>Fecha de registro</th>
		</tr>
	</thead>
	<tbody>
eod;

for ($i = 0; $i < count($actividades_arr); $i++) {
    $ti = $tf = $tools = $tool_editar = $tool_eliminar = "";

    //BUSQUEDA TIPOS DE TAREAS
    $db->ExecuteSQL("SELECT ati.* FROM actividades_tipo_tarea ati WHERE ati.ati_id = " . $actividades_arr[$i]["ati_id"]);
    $tipo_actividades_arr = $db->getRows();

    $data1 = encrypt_decrypt("encrypt", $actividades_arr[$i]['act_id']);

    $tool_editar = <<<eod
            &nbsp;&nbsp;<button onClick="$.cambiarEstadoActividad('{$actividades_arr[$i]["act_id"]}', 1)" class="btn btn-sm btn-default" data-toggle="tooltip" title="MARCAR CUMPLIDA"><i class="fas fa-check-circle text-success btn-icon"></i></button>
eod;

    $tool_eliminar = <<<eod
	    &nbsp;&nbsp;<button onClick="$.eliminarActividad({$actividades_arr[$i]["act_id"]},$i)" class="btn btn-sm btn-default" data-toggle="tooltip" title="ELIMINAR ACTIVIDAD"><span class="fas fa-trash text-secondary btn-icon"></span></button>
eod;

    switch ($atributo_perfil) {
        case 1:
            $tools = "";
            break;
        case 2:
            $tools = $tool_editar;
            break;
        case 3:
            $tools = $tool_editar . $tool_eliminar;
            break;
        default:
            $tools = "";
            break;
    }
    if ($actividades_arr[$i]["act_cumplida"] != 0) { // si se cumplio ya no se puede editar
        $tool_editar = "";
    }

    $html .= <<<eod
        <tbody id="trActividad{$i}">
			<tr>
                <td nowrap class="vert-align text-right" width="1%">{$ti}{$tools}{$tf}</td>
                <td class="vert-align">{$ti}{$actividades_arr[$i]["act_id"]}{$tf}</td>
                <td class="vert-align text-uppercase">{$ti}{$actividades_arr[$i]["par_numero_orden"]}{$tf}</td>
                <td class="vert-align">{$ti}{$ymd2dmy($actividades_arr[$i]["act_fecha"])}{$tf}</td>
                <td class="vert-align">{$ti}{$tipo_actividades_arr[0]["ati_tarea"]}{$tf}</td>
                <td class="vert-align">{$ti}{$actividades_arr[$i]["act_descripcion"]}{$tf}</td>
eod;
    if ($actividades_arr[$i]["act_cumplida"] == 0) {
        $html .= <<<eod
            <td class="vert-align text-primary">{$ti}<span class="badge badge-secondary">NO CUMPLIDA</span>{$tf}</td>
eod;
    } else {
        $html .= <<<eod
            <td class="vert-align text-pink">{$ti}<span class="badge badge-success">CUMPLIDA</span>{$tf}</td>
eod;
    }

    $html .= <<<eod
    <td class="vert-align">{$ti}{$actividades_arr[$i]["act_resolucion"]}{$tf}</td>
    <td class="vert-align">{$ti}{$ymd2dmy($actividades_arr[$i]["fecha"])}{$tf}</td>
    </tr>
    eod;
}
$html .= <<<eod
</tbody>
</table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
