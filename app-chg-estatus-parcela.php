<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];
$parcela_numero_orden=$arr[1];
//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}
if ($perfil_readonly) {exit;}



?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaEstatusParcela">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Actualizar estatus parcela</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            
                <form id="frmEstatusParcela" name="frmEstatusParcela">

                    <div class="form-group row mb-1 required">
                        <label for="lstTipo" class="col-3 col-form-label text-right">Tipo de datos</label>
                        <div class="col-9">
                            <select class="form-control  text-uppercase" id="lstTipo" name="lstTipo" required>
                                <option value="dre">(DR) Datos registrales</option>
                                <option value="dca">(DC) Datos castastrales</option>
                                <option value="dpr">(DP) Datos propietarios</option>
                                <option value="dpp">(PP) Permiso de paso</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row mb-1 required">
                        <label for="lstEstatus" class="col-3 col-form-label text-right">Estatus</label>
                        <div class="col-9">
                            <select class="form-control  text-uppercase" id="lstEstatus" name="lstEstatus" required>
                                <option value="1">Completo</option>
                                <option value="0">Incompleto</option>
                            </select>
                        </div>
                    </div>


                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaEstatusParcela').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = 0;
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaEstatusParcela').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmEstatusParcela #txtIDSigo").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaEstatusParcela').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmEstatusParcela").submit(function(e) {
        e.preventDefault();

        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-chg-estatus-parcela-x.php", {
                            id: "<?=$id?>",
                            parcela_numero_orden:"<?=$parcela_numero_orden?>",
                            datastring: $("#frmEstatusParcela").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                          
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $("#mdlVentanaEstatusParcela").modal("hide");
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
  
});
</script>