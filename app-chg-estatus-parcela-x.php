<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------

if ($lstTipo == "dre") {
    $campo = "pav_datos_registrales";
} else if ($lstTipo == "dca") {
    $campo = "pav_datos_catastrales";
} else if ($lstTipo == "dpr") {
    $campo = "pav_datos_propietarios";
} else if ($lstTipo == "dpp") {
    $campo = "pav_permiso_paso";
}

$result = $db->ExecuteSQL("UPDATE parcelas_avance_relevamiento SET par_numero_orden=".$_REQUEST["parcela_numero_orden"].", $campo=$lstEstatus WHERE par_id=" . $_REQUEST["id"]);
if ($result==1) {
    auditar($db, "parcela", "estatus", $_REQUEST["id"], json_encode($_REQUEST));
    $arr["res"] = 1;
    $arr["msg"] = "Estatus datos parcela actualizado con éxito.";
    echo json_encode($arr);
    exit;
} else {
    $arr["res"] = 0;
    $arr["msg"] = "Error[45] al actualizar estatus datos parcela.";
    echo json_encode($arr);
    exit;
}
