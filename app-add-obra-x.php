<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <1) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "buscar-idsigo") {

    
    if ($_REQUEST["readonly"] != 1) {
 
        $db->ExecuteSQL("SELECT obr_id FROM obras WHERE obr_idsigo=" . $_REQUEST["id_sigo"]);
        if (count($db->getRows()) > 0) {
            $arr["res"] = 0;
            $arr["msg"] = "Esta obra ya está dada de alta.";
            print json_encode($arr);
            exit;
        }
    }
    $db1 = new clsDBSQLSRV();
    $db1->setHost($GLOBALS["HOST_SIGO_DB"]);
    $db1->setUsername($GLOBALS["USUARIO_SIGO_DB"]);
    $db1->setPassword($GLOBALS["PASSWORD_SIGO_DB"]);
    $db1->setDatabase($GLOBALS["DATABASE_SIGO_DB"]);
    $db1->setDebugmode(false);
    $db1->openDB();
    
    $row = getDatosSigo($db1, $_REQUEST["id_sigo"]);
    if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
        if ($row[0]["DISTRITO"] != $_SESSION["expropiar_usuario_distrito_nombre"]) {
            $arr["res"] = 0;
            $arr["msg"] = "Esta obra pertenece al distrito " . $row[0]["DISTRITO"] . ".";
            print json_encode($arr);
            exit;
        }
    }
    if (count($row) == 0) {
        $arr["res"] = 0;
        $arr["msg"] = "ID Sigo no encontrado.";
        print json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        print json_encode($row);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------

//-- nuevo registro
if ($_REQUEST["id"] == 0) {
    $idPersonaContrada = $_REQUEST["id_persona_contratada"];
    if($txtCargoMensura != "DNV, contratadas a empresa"){
        $idPersonaContrada = "NULL";
        $lstTipoPersonaContratada = "";
    }
    
    $db->ExecuteSQL("START TRANSACTION");
    $sql = "INSERT INTO obras
    (obr_id,
    obr_idsigo,
    obr_nombre,
    obr_distrito,
    obr_partida_presupuestaria,
    obr_mensura_cargo,
    obr_persona_contratada,
    obr_tipo_persona_contratada,
    obr_observaciones,
    obr_eliminada,
    fecha,
    usuario)
    VALUES
    (0,
        $txtIDSigo,
        '$txtNombre',
        '$txtDistrito',
        '$txtPartidaPresupuestaria',
        '$txtCargoMensura',
        $idPersonaContrada,
        '$lstTipoPersonaContratada',
        '$txaObservaciones',
        0,
        Now(),
        '" . $_SESSION["expropiar_usuario_login"] . "')";
    $result = $db->ExecuteSQL($sql);
    if ($result != 1) {
        $db->ExecuteSQL("ROLLBACK");
        $err = explode("|", $result)[1]; //|1111|kkwjwkdj
        $arr["res"] = 0;
        if ($err == 1062) {
            $arr["msg"] = "Error[84] ya existe una obra con ese ID Sigo.";
        } else {
            $arr["msg"] = "Error[87] al dar de alta la obra (ERR $err). $sql";

        }
        echo json_encode($arr);
        exit;
    } else {
        $obr_id = $db->returnInsertId();
        $result = $db->ExecuteSQL("INSERT INTO obras_resoluciones
            (res_id,
            obr_id,
            res_numero,
            res_observaciones,
            fecha,
            usuario)
            VALUES
            (0,
            $obr_id,
            '$txtResolucion',
            '',
            Now(),
            '" . $_SESSION["expropiar_usuario_login"] . "')");
        if ($result == 1) {
            $db->ExecuteSQL("COMMIT");
            auditar($db, "obra", "alta", $obr_id, json_encode($_REQUEST));
            $arr["res"] = 1;
            $arr["msg"] = "Obra dada de alta con éxito.";
            echo json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[119] al dar de alta la obra (ERR $err).";
            echo json_encode($arr);
            exit;
        }
    }
}
