<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {header("Location: login");exit;}

//-- los parametros que se le pasa a la ventana de alta o edicion van encriptados
$data = encrypt_decrypt("encrypt", "0|");

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- distritos para el filtro
$db->ExecuteSQL("SELECT * FROM distritos WHERE 1 /*dis_numero='".$_SESSION["expropiar_usuario_distrito"]."'*/  ORDER BY dis_nombre");
$distritos_arr = $db->getRows();

?>
<!DOCTYPE html>
<html>

    <head>
        <meta charset="utf-8">
        <title>Gestión de obras: ExpropiAR</title>
        <link rel="icon" href="img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="description" content="">
        <meta name="author" content="DNV">
        <link href="css/bootstrap.min.css?v=<?=time()?>" rel="stylesheet">
        <link href="css/all.css" rel=" stylesheet">
        <link href="css/style.css?v=<?=time()?>" rel="stylesheet">
        <link href="css/jquery.uploadfile.css" rel="stylesheet">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css">
        <script src="js/vex.combined.js"></script>
        <link rel="stylesheet" href="css/vex.css?v=<?=time()?>" />
        <link rel="stylesheet" href="css/vex-theme-wireframe.css?v=<?=time()?>" />
        <script>
        vex.defaultOptions.className = 'vex-theme-wireframe';
        </script>
        <script src="js/jquery.min.js"></script>
        <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
        <!-- Leaflet-KML -->
        <script src="js/mapa-kml.js"></script>
        <style>
        .text-below-marker {
            color: #FFF;
            font-weight: bold;
        }

        /*  .modal {
            padding: 0 !important;
        }

        .modal-dialog {
            max-width: 95% !important;

            padding: 0;
            margin: auto;
        }*/
        .custom-class-assignedto-modal .modal-dialog {
            width: 95%;
        }

        .custom-class-assignedto-modal .modal-body {
            height: 75vh;
            overflow: hidden;
        }
        </style>
    </head>

    <body oncontextmenu="return false">

        <div id="divMenu" class="fixed-top">
            <!-- incluimos el menu con o sin barra de filtros segun la variable habilitar_barra_filtros.   ESTO SIEMPRE VA !!!!! -->
            <?php $habilitar_barra_filtros = true;include "inc/app-menu.php"?>
        </div>
        <div class="container-fluid">
            <div class="row" style="margin-top:100px;padding:10px">
                <div class="col">
                    <!-- ##################################################################################################################### -->
                    <!-- INICIO FORM CON LOS CAMPOS PARA EL FILTRO -->
                    <!-- ##################################################################################################################### -->
                    <div id="divForm" style="display:none">
                        <form id="frmForm" class="form-inline no-print">
                            <div class="form-group">
                                <label for="lstDistrito">&nbsp;&nbsp;Distrito&nbsp;&nbsp;</label>
                                <select class="form-control" id="lstDistrito" name="lstDistrito">
                                     <option value=""></option> 
                                    <?php
for ($i = 0; $i < count($distritos_arr); $i++) {
    ?>
                                    <option value="<?=$distritos_arr[$i]["dis_nombre"]?>"><?=$distritos_arr[$i]["dis_nombre"]?></option>
                                    <?php
}
?>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="txtIDSigo">&nbsp;&nbsp;ID Sigo&nbsp;&nbsp;</label>
                                <input class="form-control text-uppercase" id="txtIDSigo" name="txtIDSigo" type="number" placeholder="">
                            </div>
                            <div class="form-group ">
                                <label for="txtNombreObra">&nbsp;&nbsp;Nombre de la obra&nbsp;&nbsp;</label>
                                <input class="form-control text-uppercase" id="txtNombreObra" name="txtNombreObra" type="text" maxlength="100" placeholder="" style="min-width:300px!important">
                            </div>
                            <div class="form-group">
                                &nbsp;&nbsp;<button id="btnSubmit" data-loading-text="" type="button" class="btn btn-primary  "><i class="fa fa-search"></i></button>
                                <button id="btnLimpiar" data-toggle="tooltip" title="limpia filtros de búsqueda" type="button" class="btn btn-link btn-sm text-dark" onClick="$.clearForm('frmForm');return false;"><span class="fa fa-eraser" style="font-size:1.5em"></span></button>
                            </div>
                        </form>
                    </div>
                    <!-- ##################################################################################################################### -->
                    <!-- FIN FORM CON LOS CAMPOS PARA EL FILTRO -->
                    <!-- ##################################################################################################################### -->

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA TABLA CON LOS REGISTROS DE LA CONSULTA ( GRILLA PRINCIPAL) -->
                    <!-- ##################################################################################################################### -->
                    <div id="divTable" class="colu-100"></div>

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA MODAL PARA EL ALTA DE OBRA -->
                    <!-- ##################################################################################################################### -->
                    <div id="divVentanaModal"></div>

                    <!-- ##################################################################################################################### -->
                    <!-- DIV DONDE PONEMOS LA MODAL PARA MOSTRAR LOS ARCHIVOS ADJUNTOS -->
                    <!-- ##################################################################################################################### -->
                    <div class="modal fade  box-shadow--8dp" id="mdlPreviewPDF" role="dialog" tabindex="-1" aria-hidden="true" style="z-index=10!important">
                        <div class="modal-dialog modal-xl modal-dialog-centered " role="document">
                            <div class="modal-content  box-shadow--8dp">
                                <div class="modal-body">
                                    <div id="divContent">
                                        <object id="objKML" data="" width="100%" height="700px">

                                            <embed id="objPDF" type="application/pdf" frameborder="0" width="100%" height="700px">

                                        </object>
                                    </div>
                                    <div class="modal-footer">
                                        <div id="divPDFBoton" class="text-right">
                                            <button id="btnCancelarPreviewPDF" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlPreviewPDF').modal('hide');"><span class="glyphicon glyphicon-remove"></span><i class="fa fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- ##################################################################################################################### -->
                    <!-- BARRA DE HERRAMIENTAS DERECHA Y ABAJO -->
                    <!-- ##################################################################################################################### -->
                    <footer class="no-print">
                        <div id="divRightToolsBar" class="caja-htas-derecha btn-primary">
                            <div id="divSubir" style="display:none"><a data-toggle="tooltip" title="subir" href="#" class="back-to-top"><br><span class="fa fa-chevron-circle-up" style="font-size:2.7em;color:#f1c40f;"></span><br><br></a></div>
                            <span style='<?=$atributo_perfil>=2 ? "display:block" : "display:none"?>'>
                                <a class="text-light" data-toggle="tooltip" title="agregar" href="#" onClick="$.abrirVentanaObra('<?=$data?>');return false"><br><i class="fas fa-plus fa-3x"></i></a>
                            </span>
                        </div>
                    </footer>
                    <!-- ##################################################################################################################### -->

                </div>
            </div>
        </div>

    </body>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.uploadfile.js"></script>
    <script src="js/lib.js"></script>
    <script src="js/mapa-kml.js"></script>
    <!--  <script src="js/jquery-ui.min.js"></script> -->
    <script type="text/javascript">
    //--------------------------------------------------------------------------------------------------------------------------------------------------
    var g_archivo_pdf_kml = "";
    var g_obr_id = 0;
    var re = /(?:\.([^.]+))?$/;
    var ext = "";
    $(document).ready(function() {
        //-- timeout ajax
        $.ajaxSetup({
            timeout: 20000,
            error: function(xhr) {}
        });
        //--- pongo en la barra de filtros todos los campos del formulario de busqueda
        $("#contentBarraFiltros").html($("#divForm").html());
        //--------------------------------------------------------------------------------------------------------------------------------
        //--- abriri modal alta de obra -------------------------------
        $.abrirVentanaObra = function(data, readonly = 0) {
            $.showLoading();
            $("#divVentanaModal").load("app-add-obra.php?obr_id=" + g_obr_id + "&data=" + data + "&readonly=" + readonly, function() {
                $("#mdlVentanaObra").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        };
        //---------------------------------------------------------------------------------------------------------------------------------
        //--- abrir modal para adjuntar archivo -----------------------
        $.abrirVentanaAdjuntarArchivo = function(data) {
            $.showLoading();
            $("#divVentanaModal").load("app-adjuntar-archivo.php?data=" + data, function() {
                $("#mdlVentanaAdjuntarArchivo").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        };
        //--------------------------------------------------------------------------------------------------------------------------------
        //--- abrir modal alta de resolucion -------------------------------
        $.abrirVentanaAltaResolucion = function(data) {
            $.showLoading();
            $("#divVentanaModal").load("app-add-resolucion.php?data=" + data, function() {
                $("#mdlVentanaAltaResolucion").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        };
        //---------------------------------------------------------------------------------------------------------------------------------
        //--- abrir modal para info parcela -----------------------
        $.abrirVentanaInfoParcela = function(data, obr_id) {
            $.showLoading();
            $("#divVentanaModal").load("app-add-parcela.php?obr_id=" + obr_id + "&numero_orden=" + data + "&readonly=1", function() {
                $("#mdlVentanaParcela").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        };
        //---------------------------------------------------------------------------------------------------------------------
        function download(filename) {
            let extFile = re.exec(filename)[1];
            console.log(extFile);
            $.post("app-list-obras-x.php", {
                    accion: "get-archivo",
                    tipo_archivo: extFile,
                    accion_archivo: "descarga",
                    archivo_pdf: filename
                },
                function(response) {
                    var element = document.createElement('a');
                    element.setAttribute('href', response);
                    element.setAttribute('download', filename);
                    element.style.display = 'none';
                    document.body.appendChild(element);
                    element.click();
                    document.body.removeChild(element);
                });
        }
        $.descargarArchivo = function(archivo) {
            download(archivo);
        };
        //---------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------
        //-- abrir modal para ver archivo adjuntado ------------------
        var ext;
        $.mostrarPDF = function(archivo_pdf, obr_id) {
            g_archivo_pdf_kml = archivo_pdf;
            g_obr_id = obr_id;
            ext = re.exec(g_archivo_pdf_kml)[1];
            if (ext == 'pdf') {
                
                $('#divContent').html('<object id="objPDF" type="application/pdf" data="" width="100%" height="700px">No Support</object>');
                // $('#divContent').html('<object id="objPDF" type="application/pdf" data="" width="100%" height="700px">No Support</object>');
                // $('#divContent').html('<embed  id="objPDF"  frameborder="0" width="100%" height="700px">');
            } else if (ext == 'kml') {
                $('#divContent').html('<div id="myMap" style="height: 700px; width: 100%;"></div>')
            }
            $('#mdlPreviewPDF').modal('show');
            $.showLoading();
        };
        $('#mdlPreviewPDF').on('shown.bs.modal', function() {
            $.hideLoading();
            if (ext == 'pdf') {
                $.post("app-list-obras-x.php", {
                    accion: "get-archivo",
                    tipo_archivo: 'pdf',
                    accion_archivo: "lectura",
                    archivo_pdf: g_archivo_pdf_kml
                },
                function(response) {
                    $.hideLoading();
                    $('#objPDF').attr("data", response);
                });

                // $('#objPDF').attr("src", g_archivo_pdf_kml);
            } else if (ext == 'kml') {
                $.mostrarMapaKmz();
                /*$.post("app-list-obras-x.php", {
                        accion: "get-archivo",
                        archivo_pdf: g_archivo_pdf_kml
                    },
                    function(response) {
                        $.hideLoading();
                        if (ext == 'kml') {
                           // alert(g_archivo_pdf_kml);
                          
                        } else if (ext == 'pdf') {
                            $('#objKML').attr("src", g_archivo_pdf_kml);
                        }
                    });*/
            }
        });
        $(document).on('hidden.bs.modal', function(e) {
            $("#objPDF").attr("data", "");
            g_archivo_pdf_kml = "";
            $(e.target).removeData('bs.modal');
        });
        //--------------------------------------------------------------------------------------------------------------------------------
        //-- eliminar archivo ----------------------------------------
        $.eliminarArchivo = function(arc_id, $fila) {
            vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-list-obras-x.php", {
                                accion: "delete-archivo",
                                arc_id: arc_id
                            },
                            function(response) {
                                $.hideLoading();
                                $("#trArchivo" + $fila).fadeOut();
                            });
                    }
                }
            })
        }; //--------------------------------------------------------------------------------------------------------------------------------
        //-- eliminar resolucion ----------------------------------------
        $.eliminarResolucion = function(res_id, $fila) {
            vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-list-obras-x.php", {
                                accion: "delete-resolucion",
                                res_id: res_id
                            },
                            function(response) {
                                $.hideLoading();
                                $("#trResolucion" + $fila).fadeOut();
                            });
                    }
                }
            })
        };
        //--------------------------------------------------------------------------------------------------------------------------------
        //-- eliminar obra -------------------------------------------
        $.eliminarObra = function(obr_id, $fila) {
            vex.dialog.confirm({
                message: 'Confirmás ?',
                callback: function(value) {
                    if (value) {
                        $.showLoading();
                        $.post("app-list-obras-x.php", {
                                accion: "delete-obra",
                                obr_id: obr_id
                            },
                            function(response) {
                                $.hideLoading();
                                $("#trObra" + $fila).fadeOut();
                            });
                    }
                }
            })
        };
        //--------------------------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------------------------
        //-- ejecuta la consulta al hacer click en la lupita ---------
        $("#btnSubmit").click(function(e) {
            e.preventDefault();
            $.consultar(0);
        });
        //--
        $.consultar = function(page) {
            $("html, body").animate({
                scrollTop: 0
            }, "fast");
            let primera_vez = page == 0 ? 1 : 0;
            $.showLoading();
            $.post("app-list-obras-x.php?page=" + page, {
                    primera_vez: primera_vez,
                    datastring: $("#frmForm").serialize()
                })
                .done(function(response) {
                    $.hideLoading();
                    let json = JSON.parse(response);
                    if (json["res"] == 1) {
                        $("#divTable").html(json["html"]);
                        $("#divRightToolsBarExportar").show();
                    } else {
                        vex.dialog.alert({
                            unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                        });
                    }
                })
                .fail(function(xhr, textStatus, errorThrown) {
                    //alert(xhr.responseText);
                    $.hideLoading();
                    vex.dialog.alert({
                        unsafeMessage: "<big><b>ERROR DE COMUNICACIÓN</b></big><br><br>Por favor, volvé a intentar.</span>"
                    });
                });
        };
        //--------------------------------------------------------------------------------------------------------
        //-- CARGO EL MAPA KMZ
        $.mostrarMapaKmz = function() {
      
            $.showLoading();
            $.post("app-list-obras-x.php", {
                    accion: "get-permiso-paso",
                    obr_id: g_obr_id
                },
                function(response) {
                    $.hideLoading();
                    let json = JSON.parse(response);
                    //console.log(response);
                    let mymap = L.map('myMap').setView([-32.411310296390724, -63.24005705246602], 15);
                    L.tileLayer('http://mt0.google.com/vt/lyrs=s&x={x}&y={y}&z={z}', {
                        attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors | Vialidad Nacional'
                    }).addTo(mymap);
                    let resultMapa;
                    $.post("app-list-obras-x.php", {
                        accion: "get-archivo",
                        tipo_archivo: "kml",
                        accion_archivo: "lectura",
                        archivo_pdf: g_archivo_pdf_kml
                    },
                    function(response) {
                        $.hideLoading();
                        try {
                            resultMapa = window.atob(response)

                            const parser = new DOMParser();
                            const kml = parser.parseFromString(resultMapa, 'text/xml');
                            const track = new L.KML(kml);
                            mymap.addLayer(track);
                            var selected = 0
                            var poligono = null
                            var color = ''
                            mymap.eachLayer(function(layer) {
                                if (layer instanceof L.Polygon && !(layer instanceof L.Rectangle)) {
                                    let nParcela = layer._popup._content.split('</h2>');
                                    //--
                                    if (json[nParcela[1]] == -1) {
                                        var fcolor = "#CCC";
                                    } else if (json[nParcela[1]] == 1) { // tiene permisos de paso
                                        var fcolor = "#4FA845";
                                        //alert(json[nParcela[1]]+"-"+nParcela[1]);
                                    } else if (json[nParcela[1]] == 0) { // tiene permisos de paso
                                        var fcolor = "#ff0000";
                                        //alert(json[nParcela[1]]+"-"+nParcela[1]);
                                    }
                                    //--
                                    layer.setStyle({
                                        fillColor: fcolor,
                                        weight: 1,
                                        color: 'white',
                                        fillOpacity: 0.7
                                    });
                                    var center = layer.getBounds().getCenter();
                                    L.marker(center, {
                                        icon: L.divIcon({
                                            html: "Parc_N°_" + nParcela[1],
                                            className: 'text-below-marker',
                                        })
                                    }).addTo(mymap);
                                    layer.on('click', function(e) {
                                        // let datos = layer._popup._content.split('</h2>')
                                        layer.closePopup();
                                        if (selected == 0) {
                                            color = layer.options.fillColor
                                            poligono = layer
                                            /*layer.setStyle({
                                                fillColor: '#2731FF'
                                            })*/
                                            selected = 1
                                        } else if (selected == 1) {
                                            if (json[nParcela[1]] == -1) {
                                                vex.dialog.alert({
                                                    unsafeMessage: "<big><b>UPS...</b></big><br><br>Parcela no creada en el sistema.</span>"
                                                });
                                                return false;
                                            }
                                            $.abrirVentanaInfoParcela(nParcela[1], g_obr_id);
                                            poligono.setStyle({
                                                fillColor: color
                                            });
                                            if (layer != poligono) {
                                                color = layer.options.fillColor
                                                poligono = layer
                                                /*layer.setStyle({
                                                    fillColor: '#2731FF'
                                                })*/
                                            } else {
                                                selected = 0
                                                poligono = null
                                            }
                                        }
                                    })
                                } //
                            });
                            var drawnPolygons = L.featureGroup();
                            // Adjust map to show the kml
                            const bounds = track.getBounds();
                            mymap.fitBounds(bounds);
                        } catch (error) {
                            vex.dialog.alert({
                                unsafeMessage: "<big><b>ERROR DE LECTURA</b></big><br><br>Por favor, volvé a intentar.</span>"
                            });
                        }
                    });
                }); //function(response) 
        }
    });
    </script>

</html>