<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- los parametros que se le pasa a la ventana vienen encriptados
//$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$par_id = $_GET["par_id"];
$ppt_id = $_GET["ppt_id"];
$action = $_GET["action"];
$from = $_GET["from"];

if($action == 'show'){
    $title = "Ver Tasación de Parcela";
    if($from == 'mensura') {
        // BUSCO SI LA PARCELA TIENE UNA TASACION REALIZADA TRAYENDO LA ULTIMA
        $db->ExecuteSQL("SELECT * FROM tasaciones WHERE par_id=" . $par_id . " AND tas_deleted=0 ORDER BY tas_fecha_tasacion DESC LIMIT 1");
    } else {
        // BUSCO SI LA PARCELA TIENE UNA TASACION REALIZADA TRAYENDO LA ULTIMA
        $db->ExecuteSQL("SELECT * FROM tasaciones WHERE tas_solicitud_id=" . $ppt_id . " AND tas_deleted=0 ORDER BY tas_fecha_tasacion DESC LIMIT 1");
    }
    $tasacion = $db->getRows();
   
} else {
    $title = "Realizar Tasación de Parcela";
}


?>
 
<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlRealizarTasacion">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?php echo $title; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="frmRealizarTasacion" name="frmRealizarTasacion">
                    <div class="form-group row mb-1 required">
                        <label for="txtValorTasacion" class="col-5 col-form-label text-right">Valor de Tasación ($)</label>
                        <div class="col-6">
                            <input id="txtValorTasacion" name="txtValorTasacion" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$tasacion[0]["tas_valor_tasacion"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtValorTasacionAfectacion" class="col-5 col-form-label text-right">Valor Tasación Superficie Afectación ($)</label>
                        <div class="col-6">
                            <input id="txtValorTasacionAfectacion" name="txtValorTasacionAfectacion" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$tasacion[0]["tas_valor_afectacion"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtValorTasacionMejora" class="col-5 col-form-label text-right">Valor Tasación Mejoras ($)</label>
                        <div class="col-6">
                            <input id="txtValorTasacionMejora" name="txtValorTasacionMejora" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$tasacion[0]["tas_valor_mejoras"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtSuperficieTasación" class="col-5 col-form-label text-right">Superficie de tasación</label>
                        <div class="col-6">
                            <input id="txtSuperficieTasación" name="txtSuperficieTasación" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$tasacion[0]["tas_superficie_tasacion"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtNumeroNotaIf" class="col-5 col-form-label text-right">Número de nota IF</label>
                        <div class="col-6">
                            <input id="txtNumeroNotaIf" name="txtNumeroNotaIf" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$tasacion[0]["tas_numero_nota_if"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtNumeroEspecialTTN" class="col-5 col-form-label text-right">Número especial TTN</label>
                        <div class="col-6">
                            <input id="txtNumeroEspecialTTN" name="txtNumeroEspecialTTN" class="form-control input-fieldsX text-uppercase" type="text" maxlength="45" value="<?=$tasacion[0]["tas_numero_especial_ttn"]?>" required <?=$action == 'show' ? 'readonly' : '';?>>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required" <?=$action == 'show' ? 'hidden' : '';?>>
                        <label for="uploadArchivo" class="col-5 col-form-label text-right">Archivo&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-md-6">
                            <div id="uploadArchivo" class="btn btn-primary" style="margin-bottom:-15px!important"><span class="fa fa-upload"></span>&nbsp;Adjuntar archivo</div>
                        </div>
                    </div>  
                    <div class="form-group row mb-1 required" <?=$tasacion[0]["tas_archivo_adjunto"] == '' ? 'hidden' : '';?> <?=$action == 'insert' ? 'hidden' : '';?>>
                        <label for="txtPersona" class="col-5 col-form-label text-right">Archivo adjunto</label>
                        <div class="col-6">
                            <div>
                                <button type="button" class="btn btn-primary" id="" name="" onClick="$.descargarArchivo('<?=$tasacion[0]["tas_archivo_adjunto"]?>')"><i class="fa fa-download"></i> Descargar Adjunto</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg" <?=$action == 'show' ? 'hidden' : '';?>><i class="fas fa-check" ></i>&nbsp;&nbsp;Aceptar</button>
                            <button id="btnDelete" type="button" class="btn btn-danger btn-lg" <?=$from == 'mensura' ? 'hidden' : '';?> <?=$action == 'insert' ? 'hidden' : '';?> onClick="$.eliminarTasación(<?=$tasacion[0]["tas_id"]?>, <?=$ppt_id?>)"><i class="fas fa-trash"></i>&nbsp;&nbsp;Eliminar Tasación</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="$('#mdlRealizarTasacion').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_archivo = "";
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaObservarPresentacion').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmObservarPresentacion #txtResolucion").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaObservarPresentacion').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    $('#txtValorTasacionAfectacion, #txtValorTasacionMejora, #txtValorTasacion').change(function (e) { 
        e.preventDefault();
        if(!validarSuma()) {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>La sumatoria de valores no es igual al total de tasación.</span>"
            });
        }
    });
    function validarSuma() {
        if($('#txtValorTasacionMejora').val() != "" && $('#txtValorTasacionAfectacion').val() != "") {
            let suma_valores = parseFloat($('#txtValorTasacionMejora').val()) + parseFloat($('#txtValorTasacionAfectacion').val());
            if(parseFloat($('#txtValorTasacion').val()) != parseFloat(suma_valores)) {
                return false;
            }
            return true;
        }
        return true
    }

    //----------------------------------------------------------------------------------------------------------------
    function validarSuperficie() {
        $.post("app-realizar-tasacion-x.php", {
            par_id: "<?=$par_id?>",
            accion:'validar-superficie',
            superficie: $("#txtSuperficieTasación").val()
        },
        function(response) {
            return $.parseJSON(response);
            console.log(response);
        });
    }
    //-----------------------------------------------------------------------------------------------------------------
    function download(filename) {
        $.post("app-list-realizar-tasacion-x.php", {
                accion: "get-archivo",
                archivo_pdf: filename
            },
            function(response) {
                var element = document.createElement('a');
                element.setAttribute('href', response);
                element.setAttribute('download', filename);
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
                document.body.removeChild(element);
            });
        }
    $.descargarArchivo = function(archivo) {
        download(archivo);
    };
     //---------------------------------------------------------------------------------------------------------------------
     $.eliminarTasación = function(tas_id, ppt_id) {
        // $.showLoading();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-realizar-tasacion-x.php", {
                            accion: "delete-tasacion",
                            tas_id: tas_id,
                            ppt_id: ppt_id
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $('#mdlRealizarTasacion').modal('hide');
                        });
                }
            }
        })
    } 
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmRealizarTasacion").submit(function(e) {
        e.preventDefault();
        if(!validarSuma()) {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>La sumatoria de valores no es igual al total de tasación.</span>"
            });
        } else {
            $.post("app-realizar-tasacion-x.php", {
                par_id: "<?=$par_id?>",
                accion:'validar-superficie',
                superficie: $("#txtSuperficieTasación").val()
            },
            function(response) {
                let json = $.parseJSON(response);
                console.log(response);
                if(json["res"] == 0) {
                    vex.dialog.alert({
                        unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                    });
                } else {
                    vex.dialog.confirm({
                        message: 'Confirmás ?',
                        callback: function(value) {
                            if (value) {
                                $.showLoading();
                                $.post("app-realizar-tasacion-x.php", {
                                        par_id: "<?=$par_id?>",
                                        ppt_id: "<?=$ppt_id?>",
                                        accion:'insert-tasacion',
                                        archivo: g_archivo,
                                        datastring: $("#frmRealizarTasacion").serialize()
                                    },
                                    function(response) {
                                        $.hideLoading();
                                        let json = $.parseJSON(response);
                                        vex.dialog.alert({
                                            unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                                        });
                                        $("#mdlRealizarTasacion").modal('hide');
                                    });
                            } else {}
                        }
                    })
                }
            });
        }
    });
     //-----------------------------------------------------------------------------------------------------------------
    //-- upload
    var settings = {
        url: "app-realizar-tasacion-x.php",
        method: "POST",
        fileName: "myfile",
        uploadStr: '<i class="fa fa-paperclip"></i>&nbsp;&nbsp;click para adjuntar (max. 100MB)',
        multiple: false,
        showProgress: true,
        maxFileCount: 1,
        dragDrop: false,
        showDelete: true,
        showAbort: true,
        showError: 'pdf,dxf,doc',
        maxFileSize: 100000000,
        dynamicFormData: function() {
            var data = {
                accion: "upload-archivo",
                fuente: "<?=$fuente?>"
            };
            return data;
        },
        onSubmit: function(data, files) {
            $("#frmRealizarTasacion #uploadArchivo").hide();
        },
        onSelect: function(files) {
            //console.log(files);//json
        },
        afterUploadAll: function() {},
        onCancel: function() {
            $("#frmRealizarTasacion #uploadArchivo").show();
            uploadObj.reset();
            g_archivo = "";
        },
        onSuccess: function(files, data, xhr) {
            let json = $.parseJSON(data);
            if (json["res"] == 1) {
                g_archivo = json["archivo"];
                $("#frmRealizarTasacion #uploadArchivo").hide();
            } else if (json["res"] == 0) {
                $("#frmRealizarTasacion #uploadArchivo").show();
                uploadObj.reset();
                g_archivo = "";
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                });
            }
        },
        onError: function(files, status, errMsg) {
            $("#frmRealizarTasacion #uploadArchivo").show();
            g_archivo = "";
            uploadObj.reset();
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + errMsg + "</span>"
            });
        },
        deleteCallback: function(data, pd) {
            g_archivo = "";
            $("#frmRealizarTasacion #uploadArchivo").show();
            pd.statusbar.hide();
        }
    };
    var uploadObj = $("#frmRealizarTasacion #uploadArchivo").uploadFile(settings);
});
</script>