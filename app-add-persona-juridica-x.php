<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");




$dtInicioActividad = $dtInicioActividad == "" ? "NULL" : "'" . $dtInicioActividad . "'";
//---------------------------------------------------------------------------------------------------------------------

//-- nuevo registro
if ($_REQUEST["id"] == 0) {



    //BUSCAMOS EL ID DE LA PROVINCIA
    $id_provincia = getProvincia($_REQUEST["nombre_provincia"], 'nombre');

    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA PERSONAS FISICAS
    //------------------------------------------------------------------------------------------------------------------
    $sql = "INSERT INTO personas_juridicas
    (per_cuit,
    per_razon_social,
    pjt_id,
    per_domicilio_legal,
    loc_id,
    per_codigo_postal,
    per_inicio_actividad,
    per_telefono_principal,
    per_telefono_laboral,
    per_telefono_celular,
    per_email,
    per_esta_inhibida)
    VALUES
    ('$txtCuit',
    UPPER('$txtRazonSocial'),
    $lstTipoPersonaJuridica,
    UPPER('$txtDomicilio'),
    " . $_REQUEST["id_localidad"] . ",
    '$txtCodigoPostal',
    $dtInicioActividad,
    '$txtTelefonoPrincipal',
    '$txtTelefonoLaboral',
    '$txtTelefonoCelular',
    '$txtEmail',
    $lstInhibida)";
    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = $result;
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        $arr["msg"] = "Persona dada de alta con éxito.";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------

else if ($_REQUEST["id"] > 0) {
    $sql = "UPDATE personas_juridicas
    SET
    per_razon_social = UPPER('$txtRazonSocial'),
    pjt_id = $lstTipoPersonaJuridica,
    per_domicilio_legal = UPPER('$txtDomicilio'),
    loc_id = " . $_REQUEST["id_localidad"] . ",
    per_codigo_postal = '$txtCodigoPostal',
    per_inicio_actividad = $dtInicioActividad,
    per_telefono_principal = '$txtTelefonoPrincipal',
    per_telefono_laboral = '$txtTelefonoLaboral',
    per_telefono_celular = '$txtTelefonoCelular',
    per_email = LOWER('$txtEmail'),
    per_esta_inhibida = $lstInhibida
    WHERE per_id = " . $_REQUEST["id"];

    $result = $db->ExecuteSQL($sql);
    if ($result == 1) {
        $arr["res"] = 1;
        $arr["msg"] = "Persona modificada con éxito.";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 0;
        $arr["msg"] = "Error[109] al modificar Persona (ERR $result).";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
