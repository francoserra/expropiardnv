
INSERT INTO categorizacion_archivos (cat_id, cat_descripcion, cat_fuente, cat_tipo) VALUES ('0', 'PLANO PARCELA', 'MENS', 'PLANO');
INSERT INTO categorizacion_archivos (cat_id, cat_descripcion, cat_fuente, cat_tipo) VALUES ('0', 'MEJORAS', 'MENS', 'DOCUMENTO');


CREATE TABLE agrimensores (
  agr_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  agr_titulo_profesional VARCHAR(45) NULL,
  agr_matricula VARCHAR(45) NULL,
  per_dni INT UNSIGNED NOT NULL COMMENT 'sale de tabla personas_fisicas',
  PRIMARY KEY (agr_id),
  INDEX per_dni (per_dni ASC) VISIBLE)
ENGINE = MyISAM;

CREATE TABLE mensura_flow (
  mfl_id int NOT NULL AUTO_INCREMENT,
  par_id int unsigned NOT NULL,
  est_id varchar(45) DEFAULT NULL COMMENT 'id del estado (sale de tabla mensura_estados)',
  PRIMARY KEY (mfl_id),
  KEY par_id (par_id)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb3;



CREATE TABLE mensura_estados (
  mes_id int unsigned NOT NULL AUTO_INCREMENT,
  mes_estado varchar(45) DEFAULT NULL,
  PRIMARY KEY (mes_id)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb3;


INSERT INTO mensura_estados (mes_estado) VALUES ('SIN PLANO');
INSERT INTO mensura_estados (mes_estado) VALUES ('PRESENTADO DNV');
INSERT INTO mensura_estados (mes_estado) VALUES ('APROBADO DNV');
INSERT INTO mensura_estados (mes_estado) VALUES ('OBSERVADO DNV');
INSERT INTO mensura_estados (mes_estado) VALUES ('APROBADO CATASTRO');
INSERT INTO mensura_estados (mes_estado) VALUES ('OBSERVADO CATASTRO');


UPDATE categorizacion_archivos SET cat_fuente = 'MENS' WHERE (cat_id = '13');
INSERT INTO categorizacion_archivos (cat_descripcion, cat_fuente, cat_tipo) VALUES ('MEJORAS', 'MENS', 'DOCUMENTO');


CREATE TABLE mensura_presentaciones (
  mpp_id int unsigned NOT NULL AUTO_INCREMENT,
  mpp_observaciones varchar(1000) DEFAULT NULL,
  mpp_eliminada int unsigned NOT NULL DEFAULT '0',
  mpp_medio varchar(45) DEFAULT NULL COMMENT 'mail / mesa entrada /otro',
  mpp_mesa_entrada_nota varchar(45) DEFAULT NULL COMMENT 'n° de nota',
  per_id varchar(45) DEFAULT NULL COMMENT 'id del agrimensor (sale de tabla personas_fisicas)',
  usuario varchar(45) DEFAULT NULL,
  fecha datetime DEFAULT NULL,
  PRIMARY KEY (mpp_id),
  KEY per_id (per_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;




INSERT INTO expropiar.autorizacion_sistema (aut_menu, aut_script, aut_script_abm, 1, 2, 3, 4, 9) VALUES ('menuMensura', 'app-list-mensuras', 'app-add-mensura', '3', '1', '1', '1', '3');
UPDATE expropiar.autorizacion_sistema SET aut_menu = 'mnuMensura' WHERE (aut_menu = 'menuMensura');
UPDATE expropiar.autorizacion_sistema SET aut_menu = 'mnuSistema' WHERE (aut_menu = 'menuSistema');


ALTER TABLE `expropiar-testing`.`parcelas` 
ADD COLUMN `par_superficie_umedida` INT NULL DEFAULT NULL AFTER `par_eliminada`,
ADD COLUMN `par_superficie_ha` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_umedida`,
ADD COLUMN `par_superficie_a` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_ha`,
ADD COLUMN `par_superficie_ca` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_a`,
ADD COLUMN `par_superficie_dm2` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_ca`;


ALTER TABLE `expropiar-testing`.`parcelas_superficies_afectacion` 
ADD COLUMN `par_superficie_umedida` INT NULL DEFAULT NULL AFTER `psup_eliminado`,
ADD COLUMN `par_superficie_ha` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_umedida`,
ADD COLUMN `par_superficie_a` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_ha`,
ADD COLUMN `par_superficie_ca` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_a`,
ADD COLUMN `par_superficie_dm2` VARCHAR(30) CHARACTER SET 'utf8' NULL DEFAULT NULL AFTER `par_superficie_ca`;


CREATE TABLE `unidad_medida` (
  `id_unidad_medida` int NOT NULL AUTO_INCREMENT,
  `unidad_medida` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_unidad_medida`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb3;

INSERT INTO `expropiar-testing`.`unidad_medida` (`id_unidad_medida`, `unidad_medida`) VALUES ('1', 'Metros Cuadrados');
INSERT INTO `expropiar-testing`.`unidad_medida` (`id_unidad_medida`, `unidad_medida`) VALUES ('2', 'Hectáreas');

