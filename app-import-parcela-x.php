<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}if ($perfil_readonly) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------
if(!empty($_FILES['fileCSV']['name'])){
    $errors = array();
    $total_registros = 0;
    $total_registros_errores = 0;

    $file_data = fopen($_FILES['fileCSV']['tmp_name'], 'r');
    fgetcsv($file_data); //LECTURA DE PRIMER FILA DE ENCABEZADOS
    while ($row = fgetcsv($file_data, 0, ";")) {
        $total_registros += 1;
        //VERIFICO SI SELECCIONO CORRECTAMENTE LA OBRA
        $db->ExecuteSQL('SELECT obr_idsigo FROM obras WHERE obr_id = ' . intval($_REQUEST["lstObra"]));
        $obra = $db->getRows();

        if(intval($obra[0]["obr_idsigo"]) != intval($row[6])){
            array_push($errors, setArrayErrors("La parcela seleccionada es distinta a la de importación", $row));
            $total_registros_errores += 1;
            continue;
        }

        //VERIFICO SI EL CSV CONTIENE EL NUMERO DE ORDEN Y ES NUMERICO
        if($row[0] == "" || !is_numeric($row[0])) {
            array_push($errors, setArrayErrors("Número de orden no existe o es inválido", $row));
            $total_registros_errores += 1;
            continue;
        }

        // //VERIFICO SI EXISTE PARCELA
        $db->ExecuteSQL('SELECT * FROM parcelas WHERE par_numero_orden = ' . intval($row[0]) . ' AND obr_id = ' . intval($_REQUEST["lstObra"]));
        $parcela = $db->getRows();
        if(count($parcela) > 0){
            array_push($errors, setArrayErrors("Parcela ya existente", $row));
            $total_registros_errores += 1;
            continue;
        }

        //INSERTO PARCELA EN BD
        $result = insertParcela($row, $db);
        if ($result != 1) {
            array_push($errors, setArrayErrors($result, $row));
            $total_registros_errores += 1;
        } 
    }

    fclose($file_data);

    //CALCULO EL TOTAL DE REGISTROS IMPORTADOS
    $total_importadas = $total_registros - $total_registros_errores;

    //RESPUESTA EXITOSA
    $arr["res"] = 1;
    $arr["msg"] = "Importación finalizada. Se importaron " . $total_importadas . "/" . $total_registros . ". Errores: " . $total_registros_errores;
    $arr["errores"] = $errors;
    $arr["file_name"] = $_FILES['fileCSV']['name'];
    echo json_encode($arr);
    exit;
} else {
    $arr["res"] = 0;
    $arr["msg"] = "Error[52] al importar parcelas.";
    echo json_encode($arr);
    exit;
}

function insertParcela($row, $db){
    $db->ExecuteSQL("START TRANSACTION");
    $result = $db->ExecuteSQL("INSERT INTO parcelas
    (obr_id,
    res_numero,
    par_numero_orden,
    par_observaciones,
    par_identificacion,
    par_identificacion_parcelaria,
    par_identificacion_tributaria,
    par_superficie_afectacion,
    fecha,
    usuario)
    VALUES
    (" . $_REQUEST["lstObra"] . ",'" .
    $_REQUEST["lstNumeroResolucion"] . "'," .
    $row[0] . ",'" .
    utf8_encode($row[3]) . "','" .
    $row[1] . "','" .
    $row[4] . "','" .
    $row[5] . "'," .
    $row[2] . ",
    Now(),'" .
    $_SESSION["expropiar_usuario_nombre"] . "')");

    if ($result != 1) {
        $db->ExecuteSQL("ROLLBACK");
    } else {
        $par_id = $db->returnInsertId();
        $result_relation = $db->ExecuteSQL("INSERT INTO parcelas_avance_relevamiento
        (pav_id,
            par_id,
            par_numero_orden,
            pav_datos_registrales,
            pav_datos_catastrales,
            pav_datos_propietarios,
            pav_permiso_paso)
        VALUES(
        0,
        $par_id,
        $row[0],
        0,
        0,
        0,
        0)");

        if($result_relation == 1){
            $db->ExecuteSQL("COMMIT");
        } else {
            $db->ExecuteSQL("ROLLBACK");
            $result = $result_relation;
        }
    }

    return $result;
}

function setArrayErrors($error, $row){
    $tmp_obj = [
        'n_orden' => $row[0],
        'nombre_parcela_proyecto' => $row[1],
        'superficie_afectacion' => $row[2],
        'titular' => utf8_encode($row[3]),
        'id_parcelaria' => $row[4],
        'id_tributaria' => $row[5],
        'id_sigo' => $row[6],
        'error_log' => $error
    ];

    return $tmp_obj;
}
