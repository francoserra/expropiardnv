<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "version.php";
error_reporting(0);
set_time_limit(10);
//-- limpiamos todas las variables de session
session_unset();
?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Login: ExpropiAR</title>
        <link rel="icon" href="img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="description" content="">
        <meta name="author" content="DNV">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/all.css" rel="stylesheet">
        <link href="css/style.css?v=<?=time()?>" rel="stylesheet">
        <script src="js/vex.combined.js"></script>
        <link rel="stylesheet" href="css/vex.css?v=<?=time()?>" />
        <link rel="stylesheet" href="css/vex-theme-wireframe.css?v=<?=time()?>"  />
        <script>
        vex.defaultOptions.className = 'vex-theme-wireframe';
        </script>
        <script src="js/jquery.min.js"></script>
        <!-- google recaptcha 3: SOLO SE HABILITA EN PRODUCCION  -->
        <!--   <script src="https://www.google.com/recaptcha/api.js?render=6LcUsaEUAAAAAJ0mvTymnM9TW_D2wDCOF8oJc6Q_"></script> -->
    </head>

    <body style="background-color:#1670b8!important;overflow:hidden;" oncontextmenu="return false">
        <div id="divMain">
            <!-- logo -->
            <div class="row" style="background-color:#FFF;">
                <table class="table table-borderless" style="background-color:#FFF;margin-left:0%;width:100%;margin-top:10px!important;">
                    <tr>
                        <td class="text-right" valign="top"><img class="responsive" src="img/logo.png" width="5%" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    </tr>
                </table>
            </div>
            <!--  cuadro login -->
            <br>
            <div class="container ">
                <div class="card card-container ">
                    <table class="table table-borderless ">
                        <tr>
                            <td colspan="2" class="text-right"><span style="letter-spacing: 2px;text-shadow: rgba(255,255,255,.1) -1px -1px 1px,rgba(0,0,0,.5) 1px 1px 1px;">expropiAR v<?=$GLOBALS["VERSION"]?></span></td>
                        </tr>
                        <tr>
                            <td width="%30" style="background-color:#ECEFF1!important">
                                <img id="fotoUsuario" src="img/avatar_2x.png" class=' profile-img-card ' style="  border: 4px solid  rgba(255,255,255,0.4); box-shadow: 0 4px 5px 0 rgba(0, 0, 0, .14), 0 1px 10px 0 rgba(0, 0, 0, .12), 0 2px 4px -1px rgba(0, 0, 0, .2)" />
                                <p id="spnNombreUsuario" class="profile-name-card"></p>
                            </td>
                            <td>
                                <form id="frmLogin" class="form ">
                                    <div class="control-group">
                                        <div class="form-group">
                                            <input type="text" id="txtUsuario" name="txtUsuario" class="form-control input-lg" placeholder="ingresá tu usuario" required autofocus>

                                        </div>
                                        <div class="form-group">
                                            <input type="password" id="txtPassword" name="txtPassword" class="form-control input-lg" placeholder="ingresá tu contraseña" required autofocus>
                                        </div>
                                        <button id="btnLogin" class="btn btn-lg btn-primary btn-block" type="submit" style=" padding: 10px 20px;font-size:1.4em!important "><span class="glyphicon glyphicon-log-in"></span>&nbsp;&nbsp;Acceder</button>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>

                <!-- google recaptcha 3: SOLO SE HABILITA EN PRODUCCION  -->
                <!--
                    <div class="form-group">
                        <input type="hidden" name="recaptcha_response" id="recaptchaResponse">
                    </div> 
                -->

            </div>

            <footer class="fixedBottom">

                <div class="text-center" style="padding:10px;font-size:0.8em;z-index:1;width:100%">
                    Presidencia de la Nación | Ministerio de Transporte<br>
                    Dirección Nacional de Vialidad<br>
                    Todos los derechos reservados - <?=date("Y")?>
                </div>
            </footer>
        </div>

        <script src="js/bootstrap.min.js"></script>
        <script src="js/lib.js"></script>
        <script type="text/javascript">
        $(document).ready(function() {
            //--
            $.ajaxSetup({
                timeout: 10000,
                error: function(xhr) {}
            });
            //-- submit login
            $("#frmLogin").submit(function(e) {
            
                $.showLoading();
                e.preventDefault();
                localStorage.removeItem("nombreUsuario");
                localStorage.removeItem("fotoUsuario");
                $.post("app-index-x.php", {
                        accion: "login",
                        //-- google recaptcha 3: SOLO SE HABILITA EN PRODUCCION
                        perfil:"<?=$_GET["perfil"]?>",
                        //recaptcha_response: $("#recaptchaResponse").val(),
                        datastring: $("#frmLogin").serialize()
                    })
                    .done(function(response) {
                        $.hideLoading();
                        let json = $.parseJSON(response);
                        if (json["res"] == 1) {
                            localStorage.setItem("nombreUsuario", json["nombre"]);
                            localStorage.setItem("fotoUsuario", json["foto"]);
               
                            location.href = "inicio"; 
                        } else if (json["res"] == "0") {
                            $.hideLoading();
                            console.log(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        }
                    })
                    .fail(function(xhr, textStatus, errorThrown) {
                        $.hideLoading();
                        vex.dialog.alert({
                            unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>Por favor, volvé a intentar.</span>"
                        });
                    });
            });
            //-- guardo foto y nombre en el local storage
            var nombreUsuario = localStorage.getItem("nombreUsuario");
            if (localStorage.getItem("nombreUsuario") != null) {
                $("#fotoUsuario").attr("src", "data:images/jpeg;base64," + localStorage.getItem("fotoUsuario"));
                $("#spnNombreUsuario").html(localStorage.getItem("nombreUsuario"));
            } else {
                $("#fotoUsuario").attr("src", "img/sin-foto.png");
            }
            $("input:visible:enabled:first").focus();
            //-- google recaptcha 3: SOLO SE HABILITA EN PRODUCCION
            /*   grecaptcha.ready(function() {
                   grecaptcha.execute('6LcUsaEUAAAAAJ0mvTymnM9TW_D2wDCOF8oJc6Q_', {
                       action: 'contact'
                   }).then(function(token) {
                       var recaptchaResponse = document.getElementById('recaptchaResponse');
                       recaptchaResponse.value = token;
                   });
               });*/
        });
        </script>
    </body>

</html>