<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];
//-- chequeo que usuario tenga permisos 
$arr = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($_SERVER['HTTP_REFERER'] == "" ||  $arr["pasa"] == 0) {exit;} else { $puede_modificar = $arr["pasa"] == 2 ? true : false;}
if (!$puede_modificar) {exit;}

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAsignarRepresentante">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar representante</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
            <div id="divVentanaModalSearch"></div>
                <form id="frmAsignarRepresentante" name="frmAsignarRepresentante">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoPersona" class="col-3 col-form-label text-right">Tipo de persona</label>
                        <div class="col-5">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoPersona" name="lstTipoPersona" required>
                                <option value=""></option>
                                <option value="PF">Persona Física</option>
                                <option value="PJ">Persona Jurídica</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoRepresentante" class="col-3 col-form-label text-right">Tipo de representante</label>
                        <div class="col-5">
                            <select class="form-control input-fieldsX text-uppercase" id="lstTipoRepresentante" name="lstTipoRepresentante" required>
                                <option value=""></option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtPersona" class="col-3 col-form-label text-right">Persona</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input id="txtPersona" name="txtPersona" class="form-control input-fieldsX text-uppercase" type="text" pattern="\d*" value="" readonly style="background:transparent!important">
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas()";return false"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAsignarRepresentante').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<script>
var g_archivo = "";
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    $(".modal").on("hidden.bs.modal", function() {
        //
    });
    $('#mdlVentanaAdjuntarArchivo').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmAdjuntarArchivo #lstTipoArchivo").focus();
        }, 250);
    });
    //---------------------------------------------------------------------
    //--ABRIMOS BUSCADOR DE PERSONAS
    $.buscarPersonas = function() {
        var tabla_busqueda = "";
        var filtro_busqueda1 = "";
        var filtro_busqueda2 = "";
        if($('#lstTipoPersona').val() == 'PF') {
            tabla_busqueda = 'personas_fisicas';
            filtro_busqueda1 = "per_nombre";
            filtro_busqueda2 = "per_apellido"
        } else {
            tabla_busqueda = 'personas_juridicas';
            filtro_busqueda1 = "per_razon_social";
            filtro_busqueda2 = "per_cuit"
        }
        $.mostrarVentanaSearch('Buscar personas',tabla_busqueda,'per_id',filtro_busqueda1,filtro_busqueda2,'txtPersona');
    }
    //---------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmAsignarRepresentante #txtPersona").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtPersona").val().split("|");
            if (arr.length > 1) {
                $("#txtPersona").val(arr[2] + ", " + arr[1]);
                g_id_persona = arr[0];
            }
        }
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    //--buscamos tipos de representantes segun tipo de persona
    $("#lstTipoPersona").change(function () {  
        var tipo = ""; 
        if($("#lstTipoPersona").val() == 'PF') {
            tipo = 'PFIS';
        } else {
            tipo = 'PJUR';
        }
        $.showLoading();
        $.post("app-asignar-representante-x.php", {
            accion: 'tipo_representacion',
            tipo_rep: tipo,
        },
        function(response) {
            $.hideLoading();
            $('#lstTipoRepresentante').html(response);
        });
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmAsignarRepresentante").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-asignar-representante-x.php", {
                            id: "<?=$id?>",
                            id_persona:g_id_persona,
                            accion: 'insert',
                            datastring: $("#frmAsignarRepresentante").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;RESULTADO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
});
</script>