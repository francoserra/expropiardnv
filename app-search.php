<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);


//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}
if ($perfil_readonly) {exit;}


 
?>

<div class="modal " tabindex="-1" role="dialog" id="mdlVentanaSearch">
    <div class="modal-dialog  modal-dialog-scrollable modal-dialog-centered" role="document" style=" max-width: 30% !important;">
        <div class="modal-content box-shadow--8dp" style="backgroundxx:#B2DFDB">
            <div class="modal-header">
                <h5 class="modal-title"><?=$_GET["titulo_ventana"]?></h5>
                <button type="button" class="close" data-dismiss="modal1" aria-label="Close" onClick="$('#mdlVentanaSearch').modal('hide')">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="max-height:400px;overflow-x: hidden;overflow-y: auto;padding:5px ">
                <form id="frmFormSearch" name="frmFormSearch" class="form-inline">
                    <div class="form-group">
                        <input required type="text" class="form-control input-sm  text-uppercase" id="txtSearch" name="txtSearch" value="" placeholder="" autocomplete="off" maxlength="50" >
                    </div>
                    <div class="form-group">
                        &nbsp;&nbsp;<button id="btnSubmitSearch" data-loading-text="" type="submit" class="btn  btn-primary"><i class="fa fa-search"></i></button>
                        <button id="btnLimpiarSearch" data-toggle="tooltip" title="limpia filtros de búsqueda" type="button" class="btn btn-link btn-sm" onClick="$.clearForm('frmFormSearch');return false;"><i class="fa fa-eraser fa-2x"></i></button>
                    </div>
                </form>
                <div id="divTableSearch" class="" style="overflow-x: hidden;overflow-y"></div>
            </div>
            <!-- <div class="modal-footer">
                <div class="col-12 text-right">

                </div>
            </div> -->
        </div>
    </div>
</div>

<script>
g_search_response="";
$(document).ready(function() {
    $.hideLoading();

    $('#mdlVentanaSearch').on('shown.bs.modal', function() {
       // $(this).find('form')[0].reset();
       $.clearForm("frmFormSearch");
       setTimeout(() => {
              $("#frmFormSearch #txtSearch").focus();
          }, 500);
    });

    $("#frmFormSearch").submit(function(e) {
        e.preventDefault();
        $.post("app-x.php", { 
                accion: "search",
                search: $("#frmFormSearch #txtSearch").val(),
                tabla:"<?=$_GET['tabla']?>",
                campo_id:"<?=$_GET['campo_id']?>",
                campo_busqueda1:"<?=$_GET['campo_busqueda1']?>",
                campo_busqueda2:"<?=$_GET['campo_busqueda2']?>",
                campo_mostrar1:"<?=$_GET['campo_mostrar1']?>",
                campo_mostrar2:"<?=$_GET['campo_mostrar2']?>",
                campo_mostrar3:"<?=$_GET['campo_mostrar3']?>"
            },
            function(response) {
                $("#divTableSearch").html(response).show();
            });
    });
});
</script>