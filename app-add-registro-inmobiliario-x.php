<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
//$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {header("Location: login");exit;}//if ($perfil_readonly) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "eliminar") {
    $db->ExecuteSQL("UPDATE registros_inmobiliarios SET reg_eliminado=1 WHERE reg_id=" . $_REQUEST["reg_id"]);
    auditar($db, "registro-inmobiliario", "baja", $_REQUEST["reg_id"], json_encode($_REQUEST));
    echo true;
    exit;

}
//---------------------------------------------------------------------------------------------------------------------

else if ($_REQUEST["accion"] == "armar-tabla-registros-inmobiliarios") {

    if ($_REQUEST["par_id"] > 0) {
        $filtro = " AND par_id=" . $_REQUEST["par_id"];
    } else {
        $filtro = " AND session='" . session_id() . "'";
    }

    $db->ExecuteSQL("SELECT * FROM registros_inmobiliarios WHERE reg_eliminado=0 $filtro");
    $rows = $db->getRows();
    $html = <<<eod
    <table class="table table-sm  table-striped">
    <tr>
        <th></th>
        <th>Tipo</th>
        <th>Parcela</th>
        <th>Número</th>
        <th>Folio</th>
        <th>Tomo</th>
        <th>Año</th>
        <th>Planila</th>
    </tr>
eod;
    if ( isset($_REQUEST["readonly"])){
        $tools="";
    }else{
        $tools=<<<eod
        <a href="" class="fas fa-trash text-secondary btn-icon" onClick="$.eliminar({$rows[$i]["reg_id"]},$i);return false;"></a>
eod;
    }
    for ($i = 0; $i < count($rows); $i++) {
        $html .= <<<eod
    <tr id="tr{$i}">
        <td class="vert-align">{$tools}</td>
        <td class="vert-align">{$rows[$i]["reg_tipo"]}</td>
        <td class="vert-align">{$rows[$i]["par_id"]}</td>
        <td class="vert-align">{$rows[$i]["reg_numero"]}</td>
        <td class="vert-align">{$rows[$i]["reg_folio"]}</td>
        <td class="vert-align">{$rows[$i]["reg_tomp"]}</td>
        <td class="vert-align">{$rows[$i]["reg_anio"]}</td>
        <td class="vert-align">{$rows[$i]["reg_planilla"]}</td>
    </tr>
eod;
    }
    $html .= <<<eod
    </table>
eod;
    $arr["res"] = 1;
    $arr["html"] = $html;
    echo json_encode($arr);
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

$sql = "INSERT INTO registros_inmobiliarios
(reg_id,
reg_tipo,
reg_numero,
reg_folio,
reg_tomo,
reg_anio,
reg_planilla,
par_id,
session,
fecha,
usuario)
VALUES(
0,
'" . $_REQUEST["tipo"] . "',
'$txtNumero',
'$txtFolio',
'$txtTomo',
'$txtAnio',
'$txtPlanilla',
" . $_REQUEST["par_id"] . ",
'" . session_id() . "',
Now(),
'" . $_SESSION["expropiar_usuario_nombre"] . "')";

$result = $db->ExecuteSQL($sql);

if ($result == 1) {
    auditar($db, "registro-inmobiliario", "alta", $_REQUEST["id"], json_encode($_REQUEST));
    /* $db->ExecuteSQL("SELECT * FROM registros_inmobiliarios WHERE reg_eliminado=0 AND session='" . session_id() . "'");
    $rows = $db->getRows();
    $html = <<<eod
    <table class="table table-sm  table-striped ">
    <tr>
    <th></th>
    <th>Tipo</th>
    <th>Parcela</th>
    <th>Número</th>
    <th>Folio</th>
    <th>Tomo</th>
    <th>Año</th>
    <th>Planila</th>
    </tr>
    eod;
    for ($i = 0; $i < count($rows); $i++) {
    $html .= <<<eod
    <tr id="tr{$i}">
    <td><a href="" class="fas fa-trash text-secondary btn-icon" onClick="$.eliminar({$rows[$i]["reg_id"]},$i);return false;"></a></td>
    <td>{$rows[$i]["reg_tipo"]}</td>
    <td>{$rows[$i]["par_id"]}</td>
    <td>{$rows[$i]["reg_numero"]}</td>
    <td>{$rows[$i]["reg_folio"]}</td>
    <td>{$rows[$i]["reg_tomp"]}</td>
    <td>{$rows[$i]["reg_anio"]}</td>
    <td>{$rows[$i]["reg_planilla"]}</td>
    </tr>
    eod;
    }
    $html .= <<<eod
    </table>
    eod;*/
    $arr["res"] = 1;
    $arr["msg"] = "Registro inmobiliario dado de alta con éxito.";
    //  $arr["html"] = $html;
    echo json_encode($arr);
    exit;
} else {
    $arr["res"] = 0;
    $arr["msg"] = "Error[62] al dar de alta registro inmobiliario (ERR $result).";
    echo json_encode($arr);
    exit;
}
