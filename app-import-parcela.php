<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//if ($perfil_readonly) {exit;}


//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT obr_id,obr_nombre FROM obras WHERE obr_nombre<>'' AND obr_distrito='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ORDER BY obr_id DESC");
$obras_arr = $db->getRows();

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaImportarParcelas">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Importación de Parcelas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="frmImportParcela" name="frmImportParcela">
                    <div class="form-group row mb-1 required">
                        <label for="lstObra" class="col-3 col-form-label text-right">Obra</label>
                        <div class="col-8">
                            <select <?=$id > 0 ? "readonly" : ""?> class="form-control  text-uppercase" id="lstObra" name="lstObra" onChange="$.getResoluciones($(this).val())" required>
                                <?php
                                    for ($i = 0; $i < count($obras_arr); $i++) {
                                ?>
                                <option value="<?=$obras_arr[$i]["obr_id"]?>" <?=$obras_arr[$i]["obr_id"] == $parcela_arr[0]["obr_id"] ? "selected" : ""?>><?=$obras_arr[$i]["obr_nombre"]?></option>
                                <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstNumeroResolucion" class="col-3 col-form-label text-right">Resolución</label>
                        <div class="col-8" id="divResoluciones">
                            <selected <?=$id > 0 ? "readonly" : ""?> class="form-control  text-uppercase" id="lstNumeroResolucion" name="lstNumeroResolucion" required>
                                <option value=""></option>
                            </selected>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="csv-file" class="col-3 col-form-label text-right">Archivo CSV</label>
                        <div class="col-9">
                            <input class="form-control" type="file" id="fileCSV" name="fileCSV" accept=".csv" required>
                        </div>
                    </div>
                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaImportarParcelas').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = 0;
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaAltaResolucion').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmImportParcela #txtResolucion").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaAltaResolucion').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmImportParcela").submit(function(e) {
        const formData = new FormData(this);
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.ajax({
                        url: "app-import-parcela-x.php",
                        type: "POST",
                        data: formData,
                        datastring: $('#frmImportParcela').serialize(),
                        dataType: 'json',
                        contentType: false,
                        cache: false,
                        processData:false,
                        success : function(json) {
                            $.hideLoading();
                            //INICIO EL BOTON POR DEFECTO PARA EL DIALOG
                            arrButtons = [
                                $.extend({}, vex.dialog.buttons.YES, {
                                    text: 'Aceptar'
                                })
                            ]
                            //SI HAY ERRORES AGREGO BOTON PARA DESCARGAR EL CSV
                            if(json["errores"].length > 0){
                                arrButtons.push(
                                    $.extend({}, vex.dialog.buttons.NO, {
                                        text: 'Descargar Errores'
                                    })
                                )
                            }

                            if(json["res"] == 1){
                                vex.dialog.open({
                                    buttons: arrButtons,
                                    callback: function(data) {
                                        if (!data) {
                                            $.saveCSV(json["errores"], json["file_name"]);
                                        }
                                        $("#mdlVentanaImportarParcelas").modal('hide');
                                    },
                                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                                });
                            }
                        },
                    })
                }
            }
        })
    });

    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    $.saveCSV = function(array_errores, file_name){
        let csvContent = "data:text/csv;charset=utf-8,";
        csvContent += "Carpetin;Parcela s/Proyecto;Superficie Afectada M2;Titulares del Bien;Identificacion Parcelaria;Identificacion Tributaria;ID Sigo;Error Devuelto" + "\r\n";

        array_errores.forEach(function(rowArray) {
            // console.log(rowArray)
            let row = rowArray.n_orden + ";" + rowArray.nombre_parcela_proyecto + ";" + rowArray.superficie_afectacion + ";" + rowArray.titular + ";" + rowArray.id_parcelaria + ";" + rowArray.id_tributaria + ";" + rowArray.id_sigo + ";" + rowArray.error_log;
            csvContent += row + "\r\n";
        });

        var encodedUri = encodeURI(csvContent);
        var link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "ERRORES_" + file_name);
        document.body.appendChild(link); // Required for FF

        link.click(); // This will download the data file named "my_data.csv".
        
    }
    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    $.getResoluciones = function(obr_id) {
        $.showLoading();
        $.post("app-add-parcela-x.php", {
            accion: "get-resoluciones",
            id: "<?=$id?>",
            obr_id: obr_id
        },
        function(response) {
            $.hideLoading();
            let json = $.parseJSON(response);
            if (json["res"] == 0) {
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                });
            }
            if (json["res"] == 1) {
                $('#divResoluciones').html(json["html"]);
            }
        });
    }
    $.getResoluciones("<?=$obras_arr[0]["obr_id"]?>");
});
</script>