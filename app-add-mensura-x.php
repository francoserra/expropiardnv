<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil < 2) {exit;}
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "upload-archivo") {

    $month = date("m");
    $year = date("Y");
    $dir = "$month-$year";
    if (!file_exists("archivos/" . $dir)) {
        mkdir("archivos/" . $dir, 0700);
        chmod("archivos/" . $dir, 0777);
    }
    $output_dir = "archivos/" . $dir;
    $partes_ruta = pathinfo($_FILES['myfile']['name']);
    $archivo = session_id() . date("dmYHis") . "." . $partes_ruta['extension'];
    $uploadfile = $output_dir . "/" . $archivo;
    $ret = array();
    if (isset($_FILES["myfile"])) {
        $error = $_FILES["myfile"]["error"];
        unlink($uploadfile);
        if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
            $_FILES["myfile"]["error"] = "";
            $arr["res"] = 1;
            $arr["archivo"] = $uploadfile;
            print json_encode($arr);
            exit;
        }
    }

    $arr["res"] = 0;
    $arr["msg"] = "Error al adjuntar archivo (" . $_FILES["myfile"]["error"] . ")";
    print json_encode($arr);
    exit;

}

//---------------------------------------------------------------------------------------------------------------------
$sql = "INSERT INTO mensura_presentaciones(
    mpp_observaciones,
    mpp_medio,
    mpp_mesa_entrada_nota,
    per_id,
    usuario,
    fecha,
    par_id,
    mes_id,
    mpp_fecha_presentacion)
VALUES(
    UPPER('$txaObservaciones'),
    UPPER('$lstMedio'),
    UPPER('$txtNumeroNota'),
    " . $_REQUEST["id_persona"] . ",
    '" . $_SESSION["expropiar_usuario_nombre"] . "',
    Now(),
    " . $_REQUEST["id"] . ",
    2 /*PRESENTADA DNV*/,
    '" . $dtFechaPresentacion . "'
)";
$result = $db->ExecuteSQL($sql);

if ($result == 1) {
    $mpp_id = $db->returnInsertId();
    if ($lstTipo == "plano") {
        $cat_id = 13; //PLANO PARCELA sale de tabla categorizacion_archivo
    } else if ($lstTipo == "mejora") {
        $cat_id = 25; //MEJORAS sale de tabla categorizacion_archivo
    } else if ($lstTipo == "croquis") {
        $cat_id = 26; //CROQUIS sale de tabla categorizacion_archivo
    } else {
        $cat_id = 13;
    }

    if ($_REQUEST["archivo"] != "") {
        $result = $db->ExecuteSQL("INSERT INTO archivos (
            arc_fuente,
            arc_id_fuente,
            cat_id,
            arc_path,
            arc_descripcion,
            fecha,
            usuario)
        VALUES(
            'MENS',
            $mpp_id,
            $cat_id,
            '" . $_REQUEST["archivo"] . "',
            '',
            Now(),
            '" . $_SESSION["expropiar_usuario_login"] . "'
        )");
        if ($result != 1) {

            $arr["res"] = 0;
            $arr["msg"] = "Error[111] al dar de alta presentación (ERR $result)";
            echo json_encode($arr);
            exit;
        }
    }

    auditar($db, "presentación", "alta", $_REQUEST["id"], json_encode($_REQUEST));
    $db->ExecuteSQL("COMMIT");
    $arr["res"] = 1;
    $arr["msg"] = "Presentación dada de alta con éxito.";
    //  $arr["html"] = $html;
    echo json_encode($arr);
    exit;
} else {
    $arr["res"] = 0;
    $db->ExecuteSQL("ROLLBACK");
    $arr["msg"] = "Error[125] al dar de alta presentación (ERR $result)";
    echo json_encode($arr);
    exit;
}
