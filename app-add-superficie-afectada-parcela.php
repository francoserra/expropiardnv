<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

$par_id = $_GET["par_id"];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
//$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}
//if ($perfil_readonly) {exit;}

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT * FROM parcelas_superficies_afectacion WHERE psup_id = $id");
$psup_datos = $db->getRows();

$db->ExecuteSQL("SELECT id_unidad_medida,unidad_medida FROM unidad_medida ORDER BY id_unidad_medida desc ");
$unidad_medida_arr = $db->getRows();
 

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaSuperficieAfectada" style="z-index:99999999!important">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Nueva superficia de afectación</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmSuperficieAfectacion" name="frmSuperficieAfectacion">
                    <div class="form-group row mb-1">
                        <label for="txtNombreSuperficieExpropiar" class="col-3 col-form-label text-right">Nombre superficie a expropiar</label>
                        <div class="col-8">
                            <input required id="txtNombreSuperficieExpropiar" name="txtNombreSuperficieExpropiar" class="form-control  text-uppercase" type="text" value="<?=$psup_datos[0]["psup_nombre"]?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="lstpsup_superficie_umedida" class="col-3 col-form-label text-right">Unidad de Medida</label>
                        <div class="col-8">
                            <select class="form-control  text-uppercase" id="lstpsup_superficie_umedida" name="lstpsup_superficie_umedida" onchange="$.HabilitaUM2()">

<?php
for ($i = 0; $i < count($unidad_medida_arr); $i++) {
?>
                                <option value="<?=$unidad_medida_arr[$i]["id_unidad_medida"]?>" <?=$unidad_medida_arr[$i]["id_unidad_medida"] == $psup_datos[0]["psup_superficie_umedida"] ? "selected" : ""?>><?=$unidad_medida_arr[$i]["unidad_medida"]?></option>
                                <?php
}
?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="txtSuperficieAfectacionMts" class="col-3 col-form-label text-right">Superficie de afectación (m2)</label>
                        <div class="col-8">
                            <input id="txtSuperficieAfectacionMts" name="txtSuperficieAfectacionMts" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$psup_datos[0]["psup_afectacion"] != 0 ? $psup_datos[0]["psup_afectacion"] : 0?>" <?php echo $psup_datos[0]["psup_superficie_umedida"] != 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()">
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="txtpsup_superficie_ha" class="col-3 col-form-label text-right">Superficie total del inmueble (Ha)</label>
                        <div class="col-2">
                            <div class="custom-control-inline">
                                <input id="txtpsup_superficie_ha" name="txtpsup_superficie_ha" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$psup_datos[0]["psup_superficie_ha"]?>" <?php echo $psup_datos[0]["psup_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> ha
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="custom-control-inline">
                                <input id="txtpsup_superficie_a" name="txtpsup_superficie_a" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$psup_datos[0]["psup_superficie_a"]?>" <?php echo $psup_datos[0]["psup_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> a
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="custom-control-inline">
                                <input id="txtpsup_superficie_ca" name="txtpsup_superficie_ca" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$psup_datos[0]["psup_superficie_ca"]?>" <?php echo $psup_datos[0]["psup_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()" > ca
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="custom-control-inline">
                                <input id="txtpsup_superficie_dm2" name="txtpsup_superficie_dm2" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$psup_datos[0]["psup_superficie_dm2"]?>" <?php echo $psup_datos[0]["psup_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> dm2
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="txtDescripcionPoligonoSuperficie" class="col-3 col-form-label text-right">Descripción del polígono</label>
                        <div class="col-8">
                            <textarea id="txtDescripcionPoligonoSuperficie" name="txtDescripcionPoligonoSuperficie" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"><?=$psup_datos[0]["psup_descripcion"]?></textarea>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaSuperficieAfectada').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaSuperficieAfectada').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmSuperficieAfectacion #txtNombreSuperficieExpropiar").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaSuperficieAfectada').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmSuperficieAfectacion").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-parcela-x.php", {
                            par_id: "<?=$par_id?>",
                            psup_id: "<?=$id?>",
                            accion: "add-superficie-parcela",
                            datastring: $("#frmSuperficieAfectacion").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            //console.log(json["msg"]);
                            $.getSuperficiesAfectadas(0);
                        });
                } else {}
            }
        })
    });
    //--------------------------------------------------------------------------------------------------------------------------------
    $.HabilitaUM2 = function() {
       // console.log("aaa");
        document.getElementById("txtSuperficieAfectacionMts").value="";
        document.getElementById("txtpsup_superficie_ha").value="";
        document.getElementById("txtpsup_superficie_a").value="";
        document.getElementById("txtpsup_superficie_ca").value="";
        document.getElementById("txtpsup_superficie_dm2").value="";

        var xUM = document.getElementById("lstpsup_superficie_umedida").value;
        if(xUM == 1){
            chabM2 = "";
            chabHa = "true";
            $("#frmParcela #txtSuperficieAfectacionMts").focus();
        } 
        else{
            chabM2 = "true";
            chabHa = "";
            $("#frmParcela #txtpsup_superficie_ha").focus();
        }
     
        document.getElementById("txtSuperficieAfectacionMts").readOnly = chabM2;
        document.getElementById("txtpsup_superficie_ha").readOnly = chabHa;
        document.getElementById("txtpsup_superficie_a").readOnly = chabHa;
        document.getElementById("txtpsup_superficie_ca").readOnly = chabHa;
        document.getElementById("txtpsup_superficie_dm2").readOnly = chabHa;
         

    }
    //-----------------------------------------------------------------------------------------------------------------
    $.Calculo_UM = function() {
        var xUM = document.getElementById("lstpsup_superficie_umedida").value;
        var x_m2 = document.getElementById("txtSuperficieAfectacionMts").value;
        var x_ha = document.getElementById("txtpsup_superficie_ha").value;
        var x_a = document.getElementById("txtpsup_superficie_a").value;
        var x_ca = document.getElementById("txtpsup_superficie_ca").value;
        var x_dm2 = document.getElementById("txtpsup_superficie_dm2").value;
        if(xUM == 1){
            document.getElementById("txtpsup_superficie_ha").value="";
            document.getElementById("txtpsup_superficie_a").value="";
            document.getElementById("txtpsup_superficie_ca").value="";
            document.getElementById("txtpsup_superficie_dm2").value="";
            if(x_m2 == ""){return}
            x_num = x_m2;
            x_entero = Math.trunc(x_num);  
             
            //x_decimal  = x_num - Math.trunc(x_num);
            x_decimal = x_num.toString().split('.')[1];
             
            x_ha = String(x_entero).substring(0, (String(x_entero).length)-4) ;
            if(x_ha ==  ""){x_ha = 0;}
            document.getElementById("txtpsup_superficie_ha").value = x_ha;

            x_a = String(x_entero).substring((String(x_entero).length)-4, (String(x_entero).length)-2) ;
            if(x_a ==  ""){x_a = 0;}
            document.getElementById("txtpsup_superficie_a").value = x_a;

            x_ca = String(x_entero).substring((String(x_entero).length)-2, (String(x_entero).length) ) ;
            if(x_ca ==  ""){x_ca = 0;}
            document.getElementById("txtpsup_superficie_ca").value = x_ca;
         
            if(x_decimal == ""){
                x_dm2 = "0";    
            }
            else{
                x_dm2 = x_decimal;
            }
            ;
            document.getElementById("txtpsup_superficie_dm2").value = x_dm2;
        } 
        else{
            document.getElementById("txtSuperficieAfectacionMts").value="";
            if(x_ha == "" || x_a == "" || x_ca == "" || x_dm2 == ""){return}
            var xtot_m2 = (parseFloat(x_ha)*10000)+parseFloat(x_a)*100+parseFloat(x_ca)+(parseFloat(x_dm2)*0.01); 
            document.getElementById("txtSuperficieAfectacionMts").value=xtot_m2;
             
        }
       


    }
});
</script>