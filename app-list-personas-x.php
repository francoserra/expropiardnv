<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------
$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];
//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "get-archivo") {
    if (file_exists($_REQUEST["archivo_pdf"])) {
        echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"]));
    } else {
        echo "";
    }
    exit;
}

//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-dato-financiero") {
    $db->ExecuteSQL("DELETE FROM personas_datos_financieros WHERE pdf_id=" . $_REQUEST["pdf_id"]);
    auditar($db, "dato financiero", "baja", $_REQUEST["pdf_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-archivo") {
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id=" . $_REQUEST["arc_id"]);
    auditar($db, "archivo", "baja", $_REQUEST["arc_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-representante") {
    $db->ExecuteSQL("DELETE FROM personas_representantes WHERE prp_id=" . $_REQUEST["prp_id"]);
    auditar($db, "representante", "baja", $_REQUEST["prp_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "delete-persona") {
    $db->ExecuteSQL("UPDATE {$_REQUEST["tipo_persona"]} SET per_eliminada=1 WHERE per_id=" . $_REQUEST["per_id"]);
    auditar($db, "persona", "baja", $_REQUEST["per_id"], json_encode($_REQUEST));
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id_fuente=" . $_REQUEST["per_id"]);
    auditar($db, "archivo", "baja", $_REQUEST["per_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR NOMBRE
    //------------------------------------------------------------------------
    if ($txtNombreBusqueda != "") {
        if ($lstTipoPersonaBusqueda == "personas_fisicas") {
            $nombre_apellido = explode(" ", $txtNombreBusqueda);
            if (count($nombre_apellido) == 1) {
                $filtro_nombre .= " AND (LOWER(per.per_nombre) LIKE '%" . $nombre_apellido[0] . "%' OR LOWER(per.per_apellido) LIKE '%" . $nombre_apellido[0] . "%')";
            } else {
                $filtro_nombre .= " AND LOWER(per.per_nombre) LIKE '%" . $nombre_apellido[0] . "%'";
                $filtro_nombre .= " AND LOWER(per.per_apellido) LIKE '%" . $nombre_apellido[1] . "%'";
            }
        } else {
            $filtro_nombre = " AND LOWER(per.per_razon_social) LIKE '%" . $txtNombreBusqueda . "%'";
        }
    }

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR DOCUMENTO|CUIT|CUIL
    //------------------------------------------------------------------------
    if ($txtDocumentoBusqueda != "") {
        if ($lstTipoPersonaBusqueda == "personas_fisicas") {
            if (strlen($txtDocumentoBusqueda) <= 8) {
                $filtro_documento = " AND per.per_dni = $txtDocumentoBusqueda";
            } else if (strlen($txtDocumentoBusqueda) > 8) {
                $filtro_documento = " AND per.per_cuil = $txtDocumentoBusqueda";
            }
        } else {
            $filtro_documento = " AND per.per_cuit = $txtDocumentoBusqueda";
        }

    }

    //------------------------------------------------------------------------
    // FILTRO DE BUSQUEDA POR TIPO DE PERSONA (SIEMPRE DEBE SELECCIONARSE UNO)
    //------------------------------------------------------------------------
    if ($lstTipoPersonaBusqueda != "" && $lstTipoPersonaBusqueda == "personas_fisicas") {
        $busqueda_tipo_persona = ", dti.dti_tipo as tipo_doc";
        $from_busqueda = ", documento_tipos dti";
        $filtro_tipo_persona = " AND dti.dti_id=per.dti_tipo";
    } else {
        $busqueda_tipo_persona = ", pjt.pjt_tipo";
        $from_busqueda = ", personas_juridicas_tipo pjt";
        $filtro_tipo_persona = " AND pjt.pjt_id = per.pjt_id";
    }

    //------------------------------------------------------------------------
    // CONSULTA A LA BASE DE DATOS
    //------------------------------------------------------------------------
    $sql = "SELECT per.*,loc.loc_nombre as localidad_nombre $busqueda_tipo_persona
            FROM
                $lstTipoPersonaBusqueda per LEFT OUTER JOIN localidades loc ON per.loc_id=loc.loc_id $from_busqueda
            WHERE
                per.per_eliminada=0
               # AND loc.loc_id=per.loc_id
                $filtro_tipo_persona
                $filtro_nombre
                $filtro_documento
            ORDER BY
                per.per_id DESC";

    //------------------------------------------------------------------------
    //------------------------------------------------------------------------

    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[98] en consulta a la base de datos ($result)";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$personas_arr = $db->getRows();

//------------------------------------------------------------------------
// SE MUESTRA LAS PERSONAS BUSCADAS SEGUN TIPO DE PERSONA
//------------------------------------------------------------------------
if ($lstTipoPersonaBusqueda == "personas_fisicas") {
    $html = <<<eod
    <table class="table table-hover table-stripped ">
    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Total de personas: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
        <thead class="bg-dark text-light">
            <tr>
                <th nowrap width="1%"></th>
                <th>ID</th>
                <th nowrap>Nombre</th>
                <th nowrap>Apellido</th>
                <th nowrap>Tipo documento</th>
                <th nowrap>N° documento</th>
                <th nowrap>N° cuil</th>
                <th nowrap>Género</th>
                <th nowrap>Domicilio</th>
                <th nowrap>Localidad</th>
                <th nowrap>Provincia</th>
                <th nowrap>Cod Postal</th>
                <th nowrap>Email</th>
                <th nowrap>Teléfono Casa</th>
                <th nowrap>Teléfono Laboral</th>
                <th nowrap>Teléfono Celular</th>
                <th nowrap>Fecha nacimiento</th>
                <th nowrap>Estado civil</th>
                <th nowrap>Inhibida</th>
                <th nowrap>Vive</th>
            </tr>
        </thead>
        <tbody>
eod;
} else {
    $html = <<<eod
    <table class="table table-hover table-stripped ">
    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Total de personas: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
        <thead class="bg-dark text-light">
            <tr>
                <th nowrap width="1%"></th>
                <th>ID</th>
                <th nowrap>Razón Social</th>
                <th nowrap>N° CUIT</th>
                <th nowrap>Tipo Persona Jurídica</th>
                <th nowrap>Inicio Actividad</th>
                <th nowrap>Domicilio</th>
                <th nowrap>Localidad</th>
                <th nowrap>Provincia</th>
                <th nowrap>Cod Postal</th>
                <th nowrap>Email</th>
                <th nowrap>Teléfono Principal</th>
                <th nowrap>Teléfono Laboral</th>
                <th nowrap>Teléfono Celular</th>
                <th nowrap>Inhibida</th>

            </tr>
        </thead>
        <tbody>
eod;
}

for ($i = 0; $i < count($personas_arr); $i++) {
    $ti = $tf = $tools = $tool_editar = $tool_eliminar = "";

    if ($lstTipoPersonaBusqueda == "personas_fisicas") {
        $categoria_archivo = 'PFIS';
        $col_cuilcuit = 'per_cuil';
    } else {
        $categoria_archivo = 'PJUR';
        $asigar_personas = true;
        $col_cuilcuit = 'per_cuit';

        //BUSQUEDA DE PRESONAS REPRESENTANTES SI LA OPCION SELECCIONADA ES DE PERSONAS JURIDICAS
        $db->ExecuteSQL("SELECT prp.*, rti.rti_representacion FROM personas_representantes prp, representacion_tipo rti WHERE rti.rti_id = prp.rti_id AND prp.pjur_id =" . $personas_arr[$i]["per_id"]);
        $representantes_arr = $db->getRows();
    }

    $data = encrypt_decrypt("encrypt", $personas_arr[$i]['per_id']);
    $data1 = encrypt_decrypt("encrypt", $categoria_archivo . '|' . $personas_arr[$i]['per_id'] . "|DOCUMENTO");
    $data2 = encrypt_decrypt("encrypt", $personas_arr[$i]['per_cuil']);
    $data3 = encrypt_decrypt("encrypt", $personas_arr[$i][$col_cuilcuit] . '|' . $col_cuilcuit);

    $db->ExecuteSQL("SELECT arc.*,cat.*,usr.* FROM archivos arc, categorizacion_archivos cat,usuarios usr WHERE arc.cat_id=cat.cat_id AND arc.usuario=usr.usr_login AND arc.arc_eliminado=0 AND arc.arc_fuente='$categoria_archivo' AND arc.arc_id_fuente=" . $personas_arr[$i]["per_id"]);
    $archivos_arr = $db->getRows();

    //BUSCAMOS LOS DATOS FINANCIEROS DE LA PERSONA
    $db->ExecuteSQL("SELECT pdf.* FROM personas_datos_financieros pdf WHERE pdf." . $col_cuilcuit . " = " . $personas_arr[$i][$col_cuilcuit]);
    $datos_financieros_arr = $db->getRows();

    //-- aca habilitamos los botones a nivel fila de grilla segun el perfil del usuario
    $tool_ver = "";
    if (count($archivos_arr) > 0 || count($representantes_arr) > 0 || count($datos_financieros_arr) > 0) {
        $tool_ver .= <<<eod
            &nbsp;&nbsp;<a href="" onClick="$('#trAdjuntos{$i}').toggle();$('#trDatoFinanciero{$i}').toggle();if ($(this).hasClass('fa-chevron-down')){ $(this).toggleClass('fa-chevron-up')};return false;"><i id="xxx" data-toggle="tooltip" title="ver archivos adjuntos" class="fa fa-chevron-down" style="font-size:0.9rem;color:#212121" ></i></a>&nbsp;&nbsp;
eod;
    }
    //ACCIONES GENERALES
    $tool_editar .= <<<eod
        &nbsp;&nbsp;<button onClick="$.editarDatos('{$data}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="editar persona"><i class="fas fa-edit text-primary btn-icon"></i></button>
        &nbsp;&nbsp;<button onClick="$.abrirVentanaAdjuntarArchivo('{$data1}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="adjuntar documento"><i class="fas fa-file-alt text-primary btn-icon"></i></button>
        &nbsp;&nbsp;<button onClick="$.abrirVentanaAsignarDatosFinancieros('{$data3}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="datos financieros"><i class="fas fa-dollar-sign text-primary btn-icon"></i></button>
eod;
    //ACCION PARA AGREGAR REPRESENTANTES SOLO SI LA BUSQUEDA ES DE PERSONAS JURIDICAS
    /* if($asigar_personas) {
    $toolsxxx .= <<<eod
    &nbsp;&nbsp;<button onClick="$.abrirVentanaAsignarRepresentante('{$data2}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="asignar representantes"><i class="far fa-address-book text-primary btn-icon"></i></button>
    eod;
    }*/

//         //ACCION PARA VER EL LISTADO DE PERSONAS REPRESENTATES DE LA PERSONA JURIDICA SELECCIONADA
//         if(count($representantes_arr) > 0) {
//             $tools .= <<<eod
//             &nbsp;&nbsp;<button onClick="$('#trRepresentantes{$i}').toggle();return false;" class="btn btn-sm btn-default" data-toggle="tooltip" title="ver representantes"><i class="fas fa-clipboard-list text-primary btn-icon"></i></button>
// eod;
//         }
    $tool_eliminar = <<<eod
        &nbsp;&nbsp;<button onClick="$.eliminarPersona({$personas_arr[$i]["per_id"]},$i, '$lstTipoPersonaBusqueda')" class="btn btn-sm btn-default" data-toggle="tooltip" title="eliminar"><span class="fas fa-trash text-secondary btn-icon"></span></button>

eod;

    switch ($atributo_perfil) {
        case 1:
            $tools = $tool_ver;
            break;
        case 2:
            $tools = $tool_ver . $tool_editar;
            break;
        case 3:
            $tools = $tool_ver . $tool_editar . $tool_eliminar;
            break;
        default:
            $tools = "";
            break;
    }
    //------------------------------------------------------------------------
    // BUSCAMOS EL NOMBRE DE LA PROVINCIA SEGUN ID
    //------------------------------------------------------------------------
    $db->ExecuteSQL("SELECT * FROM localidades WHERE loc_id = " . $personas_arr[$i]["loc_id"]);
    $provincia_arr = $db->getRows();
    $nombreProvincia = mb_strtoupper($provincia_arr[0]["loc_provincia_nombre"]);


    //------------------------------------------------------------------------

    if ($lstTipoPersonaBusqueda == "personas_fisicas") {
        //------------------------------------------------------------------------
        // BUSCAMOS EL ESTADO CIVIL SEGUN ID
        //------------------------------------------------------------------------
        $nombreEstadoCivil = "";
        if($personas_arr[$i]["eci_id"] > 0) {
            $db->ExecuteSQL("SELECT * FROM estados_civiles WHERE eci_id = " . $personas_arr[$i]["eci_id"]);
            $estado_civil_arr = $db->getRows();
            $nombreEstadoCivil = mb_strtoupper($estado_civil_arr[0]["eci_estado"]);
        }
        
        $html .= <<<eod
        <tbody id="trPersona{$i}">
			<tr>
                <td nowrap class="vert-align text-right" width="1%">{$ti}{$tools}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_id"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_nombre"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_apellido"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["tipo_doc"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_dni"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_cuil"]}{$tf}</td>
            <td class="vert-align ">{$ti}{$personas_arr[$i]["per_sexo"]}{$tf}</td>

                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_domicilio"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["localidad_nombre"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$nombreProvincia}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_codigo_postal"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_email"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_casa"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_laboral"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_celular"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_fecha_nacimiento"]}{$tf}</td>

                <td class="vert-align">{$ti}{$nombreEstadoCivil}{$tf}</td>


eod;
        if ($personas_arr[$i]["per_esta_inhibida"] == 1) {
            $html .= <<<eod
                <td class="vert-align">{$ti}SI{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_vive"]}{$tf}</td>
            </tr>
eod;
        } else {
            $html .= <<<eod
                <td class="vert-align">{$ti}NO{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_vive"]}{$tf}</td>
            </tr>
eod;
        }
    } else {
        $html .= <<<eod
        <tbody id="trPersona{$i}">
			<tr>
                <td nowrap class="vert-align text-right" width="1%">{$ti}{$tools}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_id"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_razon_social"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_cuit"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["pjt_tipo"]}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_inicio_actividad"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_domicilio_legal"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["localidad_nombre"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$nombreProvincia}{$tf}</td>
                <td class="vert-align">{$ti}{$personas_arr[$i]["per_codigo_postal"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_email"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_principal"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_laboral"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$personas_arr[$i]["per_telefono_celular"]}{$tf}</td>
eod;
        if ($personas_arr[$i]["per_esta_inhibida"] == 1) {
            $html .= <<<eod
                <td class="vert-align">{$ti}SI{$tf}</td>
            </tr>
eod;
        } else {
            $html .= <<<eod
                <td class="vert-align">{$ti}NO{$tf}</td>
            </tr>
eod;
        }
    }

    //MOSTRAMOS LISTADO DE DATOS FINANCIEROS
    if (count($datos_financieros_arr) > 0) {
        $html .= <<<eod
            <tr id="trDatoFinanciero{$i}" style="display:none">
                <td></td>
                <td colspan="16">
                    <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
                    <tr>
                        <td nowrap class="text-center bold" width="15%"></td>
                        <td class="bold">ID</td>
                        <td class="bold">CUIT/CUIL</td>
                        <td class="bold">CUT número</td>
                        <td nowrap class="bold">CUT estado</td>
                        <td nowrap class="bold" width="25%">Entidad bancaria</td>
                        <td nowrap class="bold">CBU</td>
                        <td nowrap class="bold">Tipo de cuenta</td>
                        <td nowrap class="bold">Tipo de moneda</td>
                    </tr>
eod;

        for ($d = 0; $d < count($datos_financieros_arr); $d++) {
            $tools = $tool_editar = $tool_eliminar = "";
            $data = encrypt_decrypt("encrypt", $datos_financieros_arr[$d]['pdf_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarDatoFinanciero({$datos_financieros_arr[$d]["pdf_id"]},{$d});return false" data-toggle="tooltip" title="eliminar"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;
            switch ($atributo_perfil) {
                case 1:
                    $tools = "";
                    break;
                case 2:
                    $tools = "";
                    break;
                case 3:
                    $tools = $tool_eliminar;
                    break;
                default:
                    $tools = "";
            }

            $html .= <<<eod
            <tr id="trDato{$d}">
                <td class="text-center vert-align">{$tools}</td>
                <td class="vert-align">{$datos_financieros_arr[$d]["pdf_id"]}</td>
                <td class="vert-align">{$datos_financieros_arr[$d][$col_cuilcuit]}</td>
                <td class="vert-align">{$datos_financieros_arr[$d]["pdf_cut_numero"]}</td>
eod;

            if ($datos_financieros_arr[$d]["pdf_cut_estado"] == 0) {
                $html .= <<<eod
                    <td class="vert-align text-primary">{$ti}<span class="badge badge-secondary">DESACTIVADO</span>{$tf}</td>
eod;
            } else {
                $html .= <<<eod
                    <td class="vert-align">{$ti}<span class="badge badge-primary">APLICADO</span>{$tf}</td>
eod;
            }
            $html .= <<<eod
                <td class="vert-align">{$datos_financieros_arr[$d]["pdf_banco"]}</td>
                <td nowrap class="vert-align">{$datos_financieros_arr[$d]["pdf_cbu"]}</td>
                <td nowrap class="vert-align">{$datos_financieros_arr[$d]["pdf_tipo_cuenta"]}</td>
                <td nowrap class="vert-align">{$datos_financieros_arr[$d]["pdf_tipo_moneda"]}</td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
        </td>
    </tr>
    </tbody>
eod;
    }

    // MOSTRAMOS LISTADO DE REPRESENTANTES
    if (count($representantes_arr) > 0) {
        $html .= <<<eod
            <tr id="trRepresentantes{$i}" style="display:none">
                <td></td>
                <td colspan="12">
                    <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
                    <tr>
                        <td nowrap class="text-center bold" width="15%"></td>
                        <td nowrap class="bold" width="3%">ID</td>
                        <td class="bold">Tipo persona</td>
                        <td class="bold">CUIL/CUIT</td>
                        <td nowrap class="bold">Nombre/Razon social</td>
                        <td nowrap class="bold">Tipo representación</td>
                    </tr>
eod;

        for ($r = 0; $r < count($representantes_arr); $r++) {
            $tools = $tool_editar = $tool_eliminar = "";
            //------------------------------------------------------------------------
            //BUSCAMOS LOS DATOS DEL REPRESENTANTE SEGUN ID Y TIPO DE PERSONA
            //------------------------------------------------------------------------

            //SEGUN TIPO DE PERSONA CAMBIAMOS LA TABLA DE BUSQUEDA
            //$col_cuilcuit CORRESPONDE A LA COLUMNA DONDE SE ENCUENTRA ALMACENADO EL CUIT O CUIL SEGUN TIPO PERSONA
            if ($representantes_arr[$r]["prp_tipo"] == 'PF') {
                $db_table = 'personas_fisicas per';
                $col_cuilcuit = 'per_cuil';
            } else {
                $col_cuilcuit = 'per_cuit';
                $db_table = 'personas_juridicas per';
            }

            $db->ExecuteSQL("SELECT per.* FROM $db_table WHERE per.per_id =" . $representantes_arr[$r]["per_id"]);
            $datos_representante = $db->getRows();

            //------------------------------------------------------------------------
            //ACCIONES PARA LAS PERSONAS REPRESENTANTES
            //------------------------------------------------------------------------
            $data = encrypt_decrypt("encrypt", $representantes_arr[$r]['pac_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarRepresentante({$representantes_arr[$r]["prp_id"]},{$r});return false" data-toggle="tooltip" title="eliminar"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;

            switch ($atributo_perfil) {
                case 1:
                    $tools = "";
                    break;
                case 2:
                    $tools = "";
                    break;
                case 3:
                    $tools = $tool_eliminar;
                    break;
                default:
                    $tools = "";
            }

            //------------------------------------------------------------------------
            //CARGAMOS LA TABLA CON LAS PERSONAS
            //------------------------------------------------------------------------
            $html .= <<<eod
            <tr id="trPersonaRepresentante{$r}">
                <td class="text-center vert-align">{$tools}</td>
                <td nowrap class="vert-align">{$representantes_arr[$r]["prp_id"]}</td>
                <td nowrap class="vert-align">{$representantes_arr[$r]["prp_tipo"]}</td>
                <td nowrap class="vert-align">{$datos_representante[0][$col_cuilcuit]}</td>
eod;
            if ($representantes_arr[$r]["prp_tipo"] == 'PF') {
                $html .= <<<eod
                <td nowrap class="vert-align">{$datos_representante[0]["per_nombre"]} {$datos_representante[0]["per_apellido"]}</td>
eod;
            } else {
                $html .= <<<eod
                <td nowrap class="vert-align">{$datos_representante[0]["per_razon_social"]}</td>
eod;
            }
            $html .= <<<eod
            <td nowrap class="vert-align">{$representantes_arr[$r]["rti_representacion"]}</td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
        </td>
    </tr>
eod;
    }

    //MOSTRAMOS LISTADO DE ARCHIVOS
    if (count($archivos_arr) > 0) {
        $html .= <<<eod
            <tr id="trAdjuntos{$i}" style="display:none">
                <td></td>
                <td colspan="16">
                    <table class="table table-sm table-striped " style="font-size:0.8rem;background:#FFFDE7">
                    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions">Documentación</span></caption>

                    <tr>
                        <td nowrap class="text-center bold" width="15%"></td>
                        <td class="bold">ID</td>
                        <td class="bold" width="15%">Tipo</td>
                        <td class="bold" width="15%">Categoría</td>
                        <td nowrap class="bold" width="30%">Descripción</td>
                        <td nowrap class="bold" width="10%">Fecha adjuntó</td>
                        <td nowrap class="bold">Usuario adjuntó</td>
                    </tr>
eod;

        for ($a = 0; $a < count($archivos_arr); $a++) {
            $tools = $tool_editar = $tool_ver = $tool_eliminar = "";
            $data = encrypt_decrypt("encrypt", $archivos_arr[$a]['pac_id']);
            $tool_eliminar = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.eliminarArchivo({$archivos_arr[$a]["arc_id"]},{$a});return false" data-toggle="tooltip" title="eliminar"><i class="fas fa-trash text-secondary"></i></a>
            &nbsp;&nbsp;&nbsp;
eod;
            $tool_ver = <<<eod
            <a class="btn btn-default btn-sm"  href=""  onClick="$.mostrarPDF('{$archivos_arr[$a]["arc_path"]}',{$a});return false;" data-toggle="tooltip" title="ver archivo"><i class="fa fa-eye text-primary"></i></a>
            <a class="btn btn-default btn-sm"  href=""  onClick="$.descargarArchivo('{$archivos_arr[$a]["arc_path"]}');return false;" data-toggle="tooltip" title="descargar archivo"><i class="fa fa-download text-primary"></i></a>
eod;

            switch ($atributo_perfil) {
                case 1:
                    $tools = $tool_ver;
                    break;
                case 2:
                    $tools = $tool_ver;
                    break;
                case 3:
                    $tools = $tool_ver . $tool_eliminar;
                    break;
                default:
                    $tools = "";
            }

            $html .= <<<eod
            <tr id="trArchivo{$a}">
                <td class="text-center vert-align">{$tools}</td>
                <td class="vert-align">{$archivos_arr[$a]["arc_id"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["cat_tipo"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["cat_descripcion"]}</td>
                <td class="vert-align">{$archivos_arr[$a]["arc_descripcion"]}</td>
                <td class="vert-align">{$ymd2dmy($archivos_arr[$a]["fecha"])}</td>
                <td nowrap class="vert-align">{$archivos_arr[$a]["usr_nombre"]}</td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
        </td>
    </tr>
    </tbody>
eod;
    }

}

$html .= <<<eod
    </tbody>
</table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
