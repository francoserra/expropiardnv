<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- si no se logio lo mando al login
if ( $_SERVER['HTTP_REFERER'] == "" || !isset($_SESSION["expropiar_usuario_id"])) {header("Location: login");}

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


// guardamos en una variable de session todos los items de menu que el usuario tiene habilitado de acuerdo a su perfil
$db->ExecuteSQL("SELECT aut_menu FROM autorizacion_sistema WHERE " . $_SESSION["expropiar_usuario_perfil"] . " > 0");
$_SESSION["expropiar_menu_habilitado"]=$db->getRows();

//-- segun la hora del dia ....
if (date("Hi") >= 100 && date("Hi") <= 1200) {
    $hola = "Buenos días ";
    $db->ExecuteSQL("SELECT * FROM frases_celebres ORDER BY frase");
    $rows = $db->getRows();
} else if (date("Hi") > 1200 && date("Hi") <= 2000) {
    $hola = "Buenas tardes ";
    $db->ExecuteSQL("SELECT * FROM frases_celebres   ORDER BY frase");
    $rows = $db->getRows();
} else {
    $hola = "Buenas noches ";
    $db->ExecuteSQL("SELECT * FROM frases_celebres ORDER BY frase");
    $rows = $db->getRows();
}
$x = mt_rand (0, count($rows) - 1);
$frase_del_dia = $rows[$x]["frase"];
$frase_del_dia = "“" . str_replace("”", "", str_replace('"', "", str_replace("“", "", $frase_del_dia))) . "“";
$autor_frase_del_dia = $rows[$x]["autor"];

$arr_tmp=explode(" ",$_SESSION["expropiar_usuario_nombre"]);
$nombre_usuario=$arr_tmp[count($arr_tmp)-2];

//-- ponemos la barra que va debajo del menu para la colocacion de los filtros de busqueda ?

?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <title>Inicio: ExpropiAR</title>
        <link rel="icon" href="img/logo.png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="expires" content="Sun, 01 Jan 2014 00:00:00 GMT" />
        <meta http-equiv="pragma" content="no-cache" />
        <meta name="description" content="">
        <meta name="author" content="DNV">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/all.css" rel=" stylesheet">
        <link href="css/style.css?v=<?=time()?>" rel="stylesheet">
        <script src="js/vex.combined.js"></script>
        <link rel="stylesheet" href="css/vex.css?v=<?=time()?>" />
        <link rel="stylesheet" href="css/vex-theme-wireframe.css?v=<?=time()?>"  />
        <script>
        vex.defaultOptions.className = 'vex-theme-wireframe';
        </script>
        <script src="js/jquery.min.js"></script>
    </head>

    <body oncontextmenu="return false">

        <div id="divMenu" class="fixed-top">
            <!-- incluimos el menu con o sin barra de filtros segun la variable habilitar_barra_filtros  -->
            <?php $habilitar_barra_filtros = false;include "inc/app-menu.php"?>
        </div>
        <div id="main" class="container-fluid">
            <div style="margin-top:35px">&nbsp;</div>
            <div id="divFraseDelDia">
                <div style="
						position:fixed;
						width:800px;
						top:50%;
						left:50%;
						-ms-transform: translateX(-50%) translateY(-50%);
						-webkit-transform: translate(-50%,-50%);
						transform: translate(-50%,-50%);
						font-size:1.6em;
						color:#FAFAFA;
						z-index:1">
                    <div id="table">
                        <div id="centeralign" class="text-center">
                            <h1 style="color:#607D8B!important"><?=$hola?> <b><?=$nombre_usuario?></b ...></h1>
                            <br>
                            <p id="spnFrase" style="color:#607D8B!important"></p>
                            <br>
                            <p id="spnAutor" style="color:#607D8B!important;font-weight:bold; font-size:22px; "></p>
                        </div>
                    </div>
                </div>
            </div>
    </body>

    <script src="js/bootstrap.min.js"></script>
    <script src="js/lib.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        function typeEffect0(element, speed) {
            var text = $(element).text();
            $(element).html('');
            var i = 0;
            var timer0 = setInterval(function() {
                if (i < text.length) {
                    $(element).append(text.charAt(i));
                    i++;
                } else {
                    clearInterval(timer0);
                }
            }, speed);
        }

        function typeEffect(element, speed) {
            var text = "<?=$frase_del_dia?>"; // $(element).text();
            $(element).html('');
            var i = 0;
            var timer = setInterval(function() {
                if (i < text.length) {
                    $(element).append(text.charAt(i));
                    i++;
                } else {
                    clearInterval(timer);
                    $("#spnAutor").html("<?=$autor_frase_del_dia?>");
                    typeEffect1($('#spnAutor'), speed);
                }
            }, speed);
        }

        function typeEffect1(element, speed) {
            var text = $(element).text();
            $(element).html('');
            var i = 0;
            var timer1 = setInterval(function() {
                if (i < text.length) {
                    $(element).append(text.charAt(i));
                    i++;
                } else {
                    clearInterval(timer1);
                    $("#divMenu").fadeIn();
                }
            }, speed);
        }
        var speed = 25;
        var delay = $('h1').text().length * speed + speed;
        typeEffect0($('h1'), speed);
        setTimeout(function() {
            $('#spnFrase').css('display', 'inline-block');
            //$("#spnFrase").html("<?=$frase_del_dia?>");
            typeEffect($('#spnFrase'), speed);
        }, delay);
    });
    </script>

</html>