<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <1) {exit;}



//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

$db->ExecuteSQL("SELECT pti_id,pti_tipo FROM parcelas_tipo ORDER BY pti_tipo");
$parcelas_tipo_arr = $db->getRows();

$db->ExecuteSQL("SELECT id_unidad_medida,unidad_medida FROM unidad_medida ORDER BY id_unidad_medida desc");
$unidad_medida_arr = $db->getRows();
 
$db->ExecuteSQL("SELECT gti_id ,gti_tipo FROM gravamenes_tipo ORDER BY gti_tipo");
$gravamenes_tipo_arr = $db->getRows();

$filtro_dis = "";
if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
    $filtro_dis = " and obr_distrito ='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ";
}

$db->ExecuteSQL("SELECT obr_id,obr_nombre FROM obras WHERE obr_nombre<>'' ".$filtro_dis." ORDER BY obr_id DESC");
$obras_arr = $db->getRows();

if ($id == 0 && !isset($_GET["numero_orden"])) {
    $titulo_ventana = "Nueva parcela";
    $localidad_id = 0;
} else {
    // $db->ExecuteSQL("SELECT * FROM frases_celebres WHERE id=$id ORDER BY frase");
    // $datos_arr = $db->getRows();
    $titulo_ventana = "Ver/Modificar parcela";
    if (isset($_GET["numero_orden"])) {
        $filtro = " AND par.obr_id=".$_GET["obr_id"]." AND par.par_numero_orden=" . $_GET["numero_orden"];
    } else {
        $filtro = " AND par.par_id=$id";
    }

    $db->ExecuteSQL("SELECT
    par.*,
    obr.obr_idsigo,
    obr.obr_nombre,
    gti.gti_id,
    gti.gti_tipo,
    loc.loc_nombre,
    pav.pav_datos_registrales,
    pav.pav_datos_catastrales,
    pav_datos_propietarios
FROM
    parcelas par
    LEFT OUTER JOIN gravamenes_tipo gti ON par.gti_id=gti.gti_id
    LEFT OUTER JOIN localidades loc ON par.loc_id=loc.loc_id ,
    obras obr,
    parcelas_avance_relevamiento pav
WHERE
    par.obr_id=obr.obr_id
    AND par.par_id=pav.par_id
    AND obr.obr_eliminada=0
    $filtro");
    $parcela_arr = $db->getRows();

    $db->ExecuteSQL("SELECT loc_id,CONCAT(loc_nombre,',',loc_departamento_nombre,',',loc_provincia_nombre) localidad FROM localidades WHERE loc_id=" . $parcela_arr[0]["loc_id"]);
    $row = $db->getRows();
    $localidad = $row[0]["localidad"];
    $localidad_id = $row[0]["loc_id"];

}
?>
<style>
.custom-class-assignedto-modal .modal-body {
    height: 70vh;
    overflow: hidden;
}
</style>
<div class="modal fade custom-class-assignedto-modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaParcela" style="z-index:9999!important">
    <div class="modal-dialog modal-xl modal-dialog-centered modal-dialog-scrollable" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?=$titulo_ventana?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div id="divVentanaModalSearch"></div>

            <form id="frmParcela" name="frmParcela">
                <fieldset <?=$_GET["readonly"] == 1 ? "disabled" : ""?>>
                    <div class="modal-body" style="overflow-y: auto;overflow-x:hidden ">

                        <ul class="nav nav-tabs" role="tabs">
                            <li class="nav-item">
                                <a class="nav-link active" href="#datos_generales" data-toggle="tab">Datos generales</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#datos_registrales" data-toggle="tab">Datos registrales</a>

                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#datos_catastrales" data-toggle="tab">Datos catastrales</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <br>

                            <!--
                            #####################################################################
                            SOLAPA DATOS GENERALES
                            #####################################################################
                        -->
                            <div id="datos_generales" class=" tab-pane active show">
                                <div class="form-group row mb-1 required">
                                    <label for="lstObra" class="col-3 col-form-label text-right">Obra</label>
                                    <div class="col-8">
                                        <select <?=$id > 0 ? "readonly" : ""?> class="form-control  text-uppercase" id="lstObra" name="lstObra" onChange="$.getResoluciones($(this).val())" required>

                                            <?php
for ($i = 0; $i < count($obras_arr); $i++) {
    ?>
                                            <option value="<?=$obras_arr[$i]["obr_id"]?>" <?=$obras_arr[$i]["obr_id"] == $parcela_arr[0]["obr_id"] ? "selected" : ""?>><?=$obras_arr[$i]["obr_nombre"]?></option>
                                            <?php
}
?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row mb-1 required">
                                    <label for="lstNumeroResolucion" class="col-3 col-form-label text-right">Resolución</label>
                                    <div class="col-8" id="divResoluciones">
                                        <selected <?=$id > 0 ? "readonly" : ""?> class="form-control  text-uppercase" id="lstNumeroResolucion" name="lstNumeroResolucion" required>
                                            <option value=""></option>

                                        </selected>
                                    </div>
                                </div>
                                <div class="form-group row mb-1 required">
                                    <label for="txtNumeroOrden" class="col-3 col-form-label text-right">N° de orden</label>
                                    <div class="col-8">
                                        <input <?=$id > 0 ? "readonly" : ""?> id="txtNumeroOrden" name="txtNumeroOrden" class="form-control  text-uppercase" type="number" min=1 max=9999999 value="<?=$parcela_arr[0]["par_numero_orden"]?>" required>
                                    </div>
                                </div>
                                <div class="form-group row mb-1">
                                    <label for="txtNumeroIdentificacionParcela" class="col-3 col-form-label text-right">Identificación de parcela según proyecto</label>
                                    <div class="col-8">
                                        <input id="txtNumeroIdentificacionParcela" name="txtNumeroIdentificacionParcela" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_identificacion"]?>" >
                                    </div>
                                </div>
                                <div class="form-group row mb-1">
                                    <label for="txtNumeroExpedienteGDE" class="col-3 col-form-label text-right">N° de Expediente GDE&nbsp;&nbsp;&nbsp;</label>
                                    <div class="col-8">
                                        <input id="txtNumeroExpedienteGDE" name="txtNumeroExpedienteGDE" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_numero_expediente_gde"]?>">
                                    </div>
                                </div>
                                <div class="form-group row mb-1 ">
                                    <label for="lstMejoras" class="col-3 col-form-label text-right">Mejoras&nbsp;&nbsp;&nbsp;</label>
                                    <div class="col-8">
                                        <select class="form-control  text-uppercase" id="lstMejoras" name="lstMejoras">

                                            <option value="0" <?=$parcela_arr[0]["par_mejoras"] == "0" ? "selected" : ""?>>No</option>
                                            <option value="1" <?=$parcela_arr[0]["par_mejoras"] == "1" ? "selected" : ""?>>Si</option>

                                        </select>
                                    </div>
                                </div>

                                <!--  <div class="form-group row mb-1 required">
                        <label for="txtNumeroParcela" class="col-4 col-form-label text-right">N° de parcela</label>
                        <div class="col-5">
                            <input id="txtNumeroParcela" name="txtNumeroParcela" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_numero_parcela"]?>" required>
                        </div>
                    </div> -->

                                <!--  <div class="form-group row mb-1 required">
                                <label for="lstTipoParcela" class="col-4 col-form-label text-right">Tipo de parcela</label>
                                <div class="col-5">
                                    <select class="form-control  text-uppercase" id="lstTipoParcela" name="lstTipoParcela" required>
                                        <option value=""></option>
                                        <?php
for ($i = 0; $i < count($parcelas_tipo_arr); $i++) {
    ?>
                                        <option value="<?=$parcelas_tipo_arr[$i]["pti_id"]?>" <?=$parcelas_tipo_arr[$i]["pti_id"] == $parcela_arr[0]["pti_id"] ? "selected" : ""?>><?=$parcelas_tipo_arr[$i]["pti_tipo"]?></option>
                                        <?php
}
?>
                                    </select>
                                </div>
                            </div> -->
                                <!--   <div class="form-group row mb-1 required">
                                <label for="txtLugar" class="col-4 col-form-label text-right">Lugar</label>
                                <div class="col-8">
                                    <input id="txtLugar" name="txtLugar" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_lugar"]?>" required>
                                </div>
                            </div>
                            <div class="form-group row mb-1 required">
                                <label for="txtLoteAfectado" class="col-4 col-form-label text-right">Lote afectado</label>
                                <div class="col-8">
                                    <input id="txtLoteAfectado" name="txtLoteAfectado" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_lote_afectado"]?>" required>
                                </div>
                            </div> -->

                                <!--  <div class="form-group row mb-1 ">
                        <label for="txaMejoras" class="col-4 col-form-label text-right">Mejoras&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-8">
                            <textarea id="txaMejoras" name="txaMejoras" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"><?=$parcela_arr[0]["par_mejoras"]?></textarea>
                        </div>
                    </div> -->

                                <div class="form-group row mb-1 ">
                                    <label for="txaObservaciones" class="col-3 col-form-label text-right">Observaciones&nbsp;&nbsp;&nbsp;</label>
                                    <div class="col-8">
                                        <textarea id="txaObservaciones" name="txaObservaciones" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"><?=$parcela_arr[0]["par_observaciones"]?></textarea>
                                    </div>
                                </div>

                            </div> <!-- <div id="datos_generales" class="container tab-pane active"> -->

                            <!--
                            #####################################################################
                            SOLAPA DATOS CATASTRALES
                            #####################################################################
                        -->
                            <div id="datos_catastrales" class=" tab-pane ">

                                <div class="row">
                                    <div class="col col-md-12">

                                        <div class="form-group row mb-1">

                                            <label for="txtNumeroIdentificacionTributaria" class="col-3 col-form-label text-right">Identificación tributaria del inmueble</label>
                                            <div class="col-8">
                                                <input id="txtNumeroIdentificacionTributaria" name="txtNumeroIdentificacionTributaria" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_identificacion_tributaria"]?>">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1 ">

                                            <label for="txtNumeroIdentificacionParcelaria" class="col-3 col-form-label text-right">Identificación parcelaria del inmueble</label>
                                            <div class="col-8">
                                                <input id="txtNumeroIdentificacionParcelaria" name="txtNumeroIdentificacionParcelaria" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_identificacion_parcelaria"]?>">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1">
                                            <label for="txtLocalidad" class="col-3 col-form-label text-right">Ubicación</label>
                                            <div class="col-8">
                                                <div class="input-group">
                                                    <input id="txtLocalidad" name="txtLocalidad" class="form-control  text-uppercase" type="text" pattern="\d*" value="<?=$id > 0 || isset($_GET["numero_orden"]) ? $localidad : ""?>" readonly style="background:transparent!important">
                                                    <div>
                                                        &nbsp;<button type="button" class="btn btn-primary" onClick="$.mostrarVentanaSearch('Buscar localidades','localidades','loc_id','loc_nombre','loc_nombre','loc_nombre','loc_departamento_nombre','loc_provincia_nombre','txtLocalidad');return false"><i class="fa fa-search"></i></button>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input id="txtLocalidadID" name="txtLocalidadID" type="hidden" value="<?=$parcela_arr[0]["loc_id"]?>">

                                        <div class="form-group row mb-1 ">
                                            <label for="txaDatosAdicionalesUbicacion" class="col-3 col-form-label text-right">Datos adicionales de ubicación</label>
                                            <div class="col-8">
                                                <textarea id="txaDatosAdicionalesUbicacion" name="txaDatosAdicionalesUbicacion" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"><?=$parcela_arr[0]["par_datos_adicionales_ubicacion"]?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 ">
                                            <label for="txaUbicacionNormalizados" class="col-3 col-form-label text-right">Datos normalizados de ubicación</label>
                                            <div class="col-8">
                                                <div class="input-group">
                                                    <textarea id="txaUbicacionNormalizados" name="txaUbicacionNormalizados" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"><?=$parcela_arr[0]["par_datos_normalizados_ubicacion"]?></textarea>
                                                    <div>
                                                        &nbsp;<button type="button" class="btn btn-primary" data-toogle="tooltip" title="pegar datos adicionales de ubicación" onCLick="$.setCopyPaste()"><i class="fa fa-paste"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1 ">
                                            <label for="txtLoteNombreOriginal" class="col-3 col-form-label text-right">Nombre original del lote</label>
                                            <div class="col-8">
                                                <input id="txtLoteNombreOriginal" name="txtLoteNombreOriginal" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_nombre_lote_original"]?>">
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 ">
                                            <label for="lstpar_superficie_umedida" class="col-3 col-form-label text-right">Unidad de Medida</label>
                                            <div class="col-8">
                                                <select class="form-control  text-uppercase" id="lstpar_superficie_umedida" name="lstpar_superficie_umedida" onchange="$.HabilitaUM()">

<?php
for ($i = 0; $i < count($unidad_medida_arr); $i++) {
    ?>
                                                    <option value="<?=$unidad_medida_arr[$i]["id_unidad_medida"]?>" <?=$unidad_medida_arr[$i]["id_unidad_medida"] == $parcela_arr[0]["par_superficie_umedida"] ? "selected" : ""?>><?=$unidad_medida_arr[$i]["unidad_medida"]?></option>
                                                    <?php
}
?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 ">
                                            <label for="txtSuperficieTotalInmueble" class="col-3 col-form-label text-right">Superficie total del inmueble (m2)</label>
                                            <div class="col-8">
                                                <div class="custom-control-inline">
                                                    <input id="txtSuperficieTotalInmueble" name="txtSuperficieTotalInmueble" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$parcela_arr[0]["par_superficie_total_inmueble"]?>" <?php echo $parcela_arr[0]["par_superficie_umedida"] != 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()">m2                                                
                                                </div>    
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 ">
                                            <label for="txtSuperficieTotalInmuebleHa" class="col-3 col-form-label text-right">Superficie total del inmueble (Ha)</label>
                                            <div class="col-2">
                                                <div class="custom-control-inline">
                                                    <input id="txtpar_superficie_ha" name="txtpar_superficie_ha" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$parcela_arr[0]["par_superficie_ha"]?>" <?php echo $parcela_arr[0]["par_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> ha
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="custom-control-inline">
                                                    <input id="txtpar_superficie_a" name="txtpar_superficie_a" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$parcela_arr[0]["par_superficie_a"]?>" <?php echo $parcela_arr[0]["par_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> a
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="custom-control-inline">
                                                    <input id="txtpar_superficie_ca" name="txtpar_superficie_ca" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$parcela_arr[0]["par_superficie_ca"]?>" <?php echo $parcela_arr[0]["par_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()" > ca
                                                </div>
                                            </div>
                                            <div class="col-2">
                                                <div class="custom-control-inline">
                                                    <input id="txtpar_superficie_dm2" name="txtpar_superficie_dm2" class="form-control  text-uppercase" type="number" min="0.00" step="0.01" value="<?=$parcela_arr[0]["par_superficie_dm2"]?>" <?php echo $parcela_arr[0]["par_superficie_umedida"] == 1 ? 'readonly' : '' ?> onblur="$.Calculo_UM()"> dm2
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1">
                                            <label for="lstRegistroInmobiliarioTipo" class="col-3 col-form-label text-right">Superficies afectadas</label>
                                            <div class="col-8">
                                                <div id="divSuperficiesAfectadasParcelas" style="max-height:50vh ;overflow-y: auto;">

                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 mt-2">
                                            <div class="col-11 text-right">
                                                    <button id="btnAgregarSuperficie" type="button" class="btn btn-primary"><i class="fas fa-check"></i>&nbsp;&nbsp;Agregar superficie</button>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 mt-4">

                                            <label for="txtNumeroExpedienteProvincial" class="col-3 col-form-label text-right">N° de expediente provincial</label>
                                            <div class="col-8">
                                                <input id="txtNumeroExpedienteProvincial" name="txtNumeroExpedienteProvincial" class="form-control  text-uppercase" type="text" maxlength="45" value="<?=$parcela_arr[0]["par_expediente_provincial"]?>">
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <!--
                            #####################################################################
                            SOLAPA DATOS REGISTRALES
                            #####################################################################
                        -->

                            <div id="datos_registrales" class=" tab-pane ">
                                <div class="row">
                                    <div class="col col-md-10">
                                        <div class="form-group row mb-1">
                                            <label for="txtDescripcionSegunTitulo" class="col-4 col-form-label text-right">Descripción del inmueble según título</label>
                                            <div class="col-8">
                                                <textarea id="txtDescripcionSegunTitulo" name="txtDescripcionSegunTitulo" class="form-control text-uppercase" type="text" maxlength="2000" rows="5" style="resize:none;overflow-y:scrool"><?=$parcela_arr[0]["par_descripcion_inmueble_titulo"]?></textarea>

                                            </div>
                                        </div>
                                        <div class="form-group row mb-1 ">
                                            <label for="lstTipoGravamen" class="col-4 col-form-label text-right">Tipo de gravámen</label>
                                            <div class="col-8">
                                                <select class="form-control  text-uppercase" id="lstTipoGravamen" name="lstTipoGravamen">
                                                    <option value="0"></option>
                                                    <?php
for ($i = 0; $i < count($gravamenes_tipo_arr); $i++) {
    ?>
                                                    <option value="<?=$gravamenes_tipo_arr[$i]["gti_id"]?>" <?=$gravamenes_tipo_arr[$i]["gti_id"] == $parcela_arr[0]["gti_id"] ? "selected" : ""?>><?=$gravamenes_tipo_arr[$i]["gti_tipo"]?></option>
                                                    <?php
}
?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1">
                                            <label for="txtDescripcionGravamen" class="col-4 col-form-label text-right">Descripción del gravamen</label>
                                            <div class="col-8">
                                                <input id="txtDescripcionGravamen" name="txtDescripcionGravamen" class="form-control  text-uppercase" type="text" maxlenght="255" value="<?=$parcela_arr[0]["par_descripcion_gravamen"]?>">
                                            </div>
                                        </div>

                                        <div class="form-group row mb-1">
                                            <label for="lstRegistroInmobiliarioTipo" class="col-4 col-form-label text-right">Registro inmobiliario</label>
                                            <div class="col-8">
                                                <div class="input-group">
                                                    <select class="form-control  text-uppercase" id="lstRegistroInmobiliarioTipo" name="lstRegistroInmobiliarioTipo">
                                                        <option value="" <?=$parcela_arr[0]["par_registro_inmobiliario"] == "" ? "selected" : ""?>></option>
                                                        <option value="CRONOLOGICO" <?=$parcela_arr[0]["par_registro_inmobiliario"] == "CRONOLOGICO" ? "selected" : ""?>>CRONOLÓGICO</option>
                                                        <option value="FOLIO REAL" <?=$parcela_arr[0]["par_registro_inmobiliario"] == "FOLIO REAL" ? "selected" : ""?>>FOLIO REAL</option>
                                                        <option value="MINUTA" <?=$parcela_arr[0]["par_registro_inmobiliario"] == "MINUTA" ? "selected" : ""?>>MINUTA</option>
                                                    </select>
                                                    <div>
                                                        &nbsp;<button type="button" class="btn btn-primary" onClick="$.abrirVentanaRegistroInmobiliario();return false"><i class="fa fa-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-1">
                                            <label for="lstRegistroInmobiliarioTipo" class="col-4 col-form-label text-right"></label>
                                            <div class="col-8">
                                                <div id="divParcelaResgistrosInmobiliarios" style="  max-height:50vh ;overflow-y: auto;">
                                                    <!-- <table class="table table-sm  table-striped ">
                                                    <tr>
                                                        <th></th>
                                                        <th>Tipo</th>
                                                        <th>Parcela</th>
                                                        <th>Número</th>
                                                        <th>Folio</th>
                                                        <th>Tomo</th>
                                                        <th>Año</th>
                                                        <th>Planila</th>
                                                    </tr>
                                                    <tr>
                                                        <td><a href="" class="fas fa-trash text-secondary btn-icon" onClick="alert('UPS....');return false;"></a></td>
                                                        <td>CRONOLOGICA</td>
                                                        <td>n° 2 de Juan Perez</td>
                                                        <td>12345</td>
                                                        <td>4454</td>
                                                        <td>2233</td>
                                                        <td>2003</td>
                                                        <td>2</td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fas fa-trash text-secondary btn-icon"></i></td>
                                                        <td>CRONOLOGICA</td>
                                                        <td>n° 2 de Juan Perez</td>
                                                        <td>12345</td>
                                                        <td>4454</td>
                                                        <td>2233</td>
                                                        <td>2003</td>
                                                        <td>2</td>
                                                    </tr>

                                                </table> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--
                            #####################################################################
                            #####################################################################
                            #####################################################################
                        -->
                        </div> <!-- div tab content-->

                    </div> <!-- <div class="modal-body"> -->
                    <div class="modal-footer">
                        <div class="form-group row mb-1" style="display:<?=$_GET["readonly"] ? "none" : ""?>">
                            <div class="col-12 text-right">
                                <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                                &nbsp;&nbsp;&nbsp;
                                <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaParcela').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                            </div>
                        </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
</div>
<div id="divVentanaRegistroInmobiliario"></div>
<div id="divVentanaSuperficieAfectada"></div>
<div id="divVentanaPoligono"></div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_id_localidad = "<?=$localidad_id?>";
$(document).ready(function() {
    $.hideLoading();
    $('#tabs').tab();

    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaParcela').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmParcela #txtIDSigo").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
        //--
        $.post("app-add-registro-inmobiliario-x.php", {
                accion: "armar-tabla-registros-inmobiliarios",
                par_id: "<?=$id?>",
                readonly: <?=$_GET["readonly"]?>
            },
            function(response) {
                let json = $.parseJSON(response);
                $("#divParcelaResgistrosInmobiliarios").html(json["html"]);
            });
            $.getSuperficiesAfectadas(<?=$_GET["readonly"]?>);
    });
    $('#mdlVentanaParcela').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //--------------------------------------------------------------------------------------------------------------------------------
    //--- abrir modal alta de Parcela -------------------------------
    $.abrirVentanaRegistroInmobiliario = function() {
        if ($("#lstRegistroInmobiliarioTipo").val() != "") {
            $.showLoading();
            $("#divVentanaRegistroInmobiliario").load("app-add-registro-inmobiliario.php?par_id=<?=$id?>&tipo=" + encodeURIComponent($("#lstRegistroInmobiliarioTipo").val()), function() {
                $("#mdlVentanaRegistroInmobiliario").modal({
                    backdrop: "static",
                    keyboard: false
                });
            })
        }
    };
    //-----------------------------------------------------------------------------------------------------------------
    $.setCopyPaste = function() {
        let inp = $("#txtLocalidad").val() + " " + $("#txaDatosAdicionalesUbicacion").val();
        $.copyPaste(inp, '#txaUbicacionNormalizados');
    }
    //-----------------------------------------------------------------------------------------------------------------
    $.copyPaste = function(inp, out) {
        navigator.clipboard.writeText(inp).then(function() {
            navigator.clipboard.readText().then(text => document.querySelector(out).innerText = text);
        }, function() {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS</b></big><br><br>Es probable que hayas denegado oportunamente el permiso para copiar y pegar cuando el sistema te lo solicitó.</span>"
            });
        });
    }
    //----------------------------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------------------------
    $.getResoluciones = function(obr_id) {
        $.showLoading();
        $.post("app-add-parcela-x.php", {
                accion: "get-resoluciones",
                id: "<?=$id?>",
                obr_id: obr_id
            },
            function(response) {
                $.hideLoading();
                let json = $.parseJSON(response);
                if (json["res"] == 0) {
                    vex.dialog.alert({
                        unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                    });
                }
                if (json["res"] == 1) {
                    $('#divResoluciones').html(json["html"]);
                }
            });
    }
    $.getResoluciones("<?=$obras_arr[0]["obr_id"]?>");
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmParcela").submit(function(e) {
        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-parcela-x.php", {
                            id: "<?=$id?>",
                            id_localidad: g_id_localidad,
                            datastring: $("#frmParcela").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            //console.log(json["msg"]);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            if (json["res"] == 1) {
                                $('#mdlVentanaParcela').modal("hide");
                            }
                        });
                } else {}
            }
        })
    });
    //-----------------------------------------------------------------------------------------------------------------
    $.editarSuperficie = function(data) {
        $.showLoading();
        $("#divVentanaSuperficieAfectada").load("app-add-superficie-afectada-parcela.php?data=" + data, function() {
            $("#mdlVentanaSuperficieAfectada").modal({
                backdrop: "static",
                keyboard: false
            });
        })
    }
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#btnAgregarSuperficie").click(function(e){
        $.showLoading();
        $("#divVentanaSuperficieAfectada").load("app-add-superficie-afectada-parcela.php?par_id=<?=$id?>", function() {
            $("#mdlVentanaSuperficieAfectada").modal({
                backdrop: "static",
                keyboard: false
            });
        })
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $.cargaPoligonos = function(psup_id, psup_nombre) {
        $.showLoading();
        $("#divVentanaPoligono").load("app-add-poligono.php?par_id=<?=$id?>&psup_id="+psup_id+"&psup_nombre="+psup_nombre, function() {
            $("#mdlVentanaPligono").modal({
                backdrop: "static",
                keyboard: false
            });
        })
    };
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $.eliminarSuperficie = function(psup_id, fila) {
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-parcela-x.php", {
                            accion: "eliminar-superficie-afectada",
                            psup_id: psup_id,
                            par_id: "<?=$id?>",
                        },
                        function(response) {
                            $.hideLoading();
                            $("#tr" + fila).hide();
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Superficie afectada eliminada</span>"
                            });
                            $.getSuperficiesAfectadas(0);
                        });
                } else {}
            }
        })
    }
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $.getSuperficiesAfectadas = function(readonly) {
        $.post("app-add-parcela-x.php", {
                accion: "armar-tabla-superficies-parcela",
                par_id: "<?=$id?>",
                readonly: readonly
            },
            function(response) {
                // console.log(response)
                let json = $.parseJSON(response);
                $("#divSuperficiesAfectadasParcelas").html(json["html"]);
            });
    }
    //-----------------------------------------------------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmParcela #txtLocalidad").focus(function(e) {
        e.preventDefault();
        if ($("#txtLocalidad").val().length > 0) {
            //  1234|CARLOS PAZ|CORDOBA
            let arr = $("#txtLocalidad").val().split("|");
            if (arr.length > 1) {
                $("#txtLocalidad").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_localidad = arr[0];
            }
        }
    });
    //-----------------------------------------------------------------------------------------------------------------
    $.HabilitaUM = function() {
        document.getElementById("txtSuperficieTotalInmueble").value="";
        document.getElementById("txtpar_superficie_ha").value="";
        document.getElementById("txtpar_superficie_a").value="";
        document.getElementById("txtpar_superficie_ca").value="";
        document.getElementById("txtpar_superficie_dm2").value="";

        var xUM = document.getElementById("lstpar_superficie_umedida").value;
        if(xUM == 1){
            chabM2 = "";
            chabHa = "true";
            $("#frmParcela #txtSuperficieTotalInmueble").focus();
        } 
        else{
            chabM2 = "true";
            chabHa = "";
            $("#frmParcela #txtpar_superficie_ha").focus();
        }
     
        document.getElementById("txtSuperficieTotalInmueble").readOnly = chabM2;
        document.getElementById("txtpar_superficie_ha").readOnly = chabHa;
        document.getElementById("txtpar_superficie_a").readOnly = chabHa;
        document.getElementById("txtpar_superficie_ca").readOnly = chabHa;
        document.getElementById("txtpar_superficie_dm2").readOnly = chabHa;
         

    }
    //-----------------------------------------------------------------------------------------------------------------
    $.Calculo_UM = function() {
        var xUM = document.getElementById("lstpar_superficie_umedida").value;
        var x_m2 = document.getElementById("txtSuperficieTotalInmueble").value;
        var x_ha = document.getElementById("txtpar_superficie_ha").value;
        var x_a = document.getElementById("txtpar_superficie_a").value;
        var x_ca = document.getElementById("txtpar_superficie_ca").value;
        var x_dm2 = document.getElementById("txtpar_superficie_dm2").value;
        if(xUM == 1){
            document.getElementById("txtpar_superficie_ha").value="";
            document.getElementById("txtpar_superficie_a").value="";
            document.getElementById("txtpar_superficie_ca").value="";
            document.getElementById("txtpar_superficie_dm2").value="";
            if(x_m2 == ""){return}
            x_num = x_m2;
            x_entero = Math.trunc(x_num);  
             
            //x_decimal  = x_num - Math.trunc(x_num);
            x_decimal = x_num.toString().split('.')[1];
             
            x_ha = String(x_entero).substring(0, (String(x_entero).length)-4) ;
            if(x_ha ==  ""){x_ha = 0;}
            document.getElementById("txtpar_superficie_ha").value = x_ha;

            x_a = String(x_entero).substring((String(x_entero).length)-4, (String(x_entero).length)-2) ;
            if(x_a ==  ""){x_a = 0;}
            document.getElementById("txtpar_superficie_a").value = x_a;

            x_ca = String(x_entero).substring((String(x_entero).length)-2, (String(x_entero).length) ) ;
            if(x_ca ==  ""){x_ca = 0;}
            document.getElementById("txtpar_superficie_ca").value = x_ca;
         
            if(x_decimal == ""){
                x_dm2 = "0";    
            }
            else{
                x_dm2 =  x_decimal;
            }
            ;
            document.getElementById("txtpar_superficie_dm2").value = x_dm2;
        } 
        else{
            document.getElementById("txtSuperficieTotalInmueble").value="";
            if(x_ha == "" || x_a == "" || x_ca == "" || x_dm2 == ""){return}
            var xtot_m2 = (parseFloat(x_ha)*10000)+parseFloat(x_a)*100+parseFloat(x_ca)+(parseFloat(x_dm2)*0.01); 
            document.getElementById("txtSuperficieTotalInmueble").value=xtot_m2;
             
        }
       


    }
});
</script>