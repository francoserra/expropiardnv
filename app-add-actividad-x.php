<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- nuevo registro
if($_REQUEST["accion"] == 'insert') {
    //------------------------------------------------------------------------------------------------------------------
    //-- INSERT DE DATOS EN LA TABLA REPRESENTANTES
    //------------------------------------------------------------------------------------------------------------------
    date_default_timezone_set('America/Argentina/Buenos_Aires');
    $fechaHoy = date("Y-m-d");

    $cumplida = 0;
    if($fechaHoy > $dtFechaActividad) {
        $cumplida = 1;
    }

    $sql = "INSERT INTO actividades
    (par_id,
    act_fecha,
    ati_id,
    act_descripcion,
    act_cumplida,
    usuario,
    fecha)
    VALUES
    ($txtIdParcela,
    '$dtFechaActividad',
    $lstTipoActividad,
    UPPER('$txtDescripcion'),
    $cumplida,
    '" . $_SESSION["expropiar_usuario_login"] . "',
    Now())";
    $result = $db->ExecuteSQL($sql);

    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al registrar actividad (ERR $result).";
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 1;
        $arr["msg"] = "Actividad registrada con éxito.";
        echo json_encode($arr);
        exit;
    }
}

//---------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------

if(strtoupper($_SESSION["expropiar_usuario_distrito_nombre"]) == "CASA CENTRAL") {
    $login_central = "";
} else {
    if($_REQUEST["accion"] == 'listar_obras') {
        $login_central = " WHERE obr_id IN (SELECT obr_id FROM obras WHERE obr_distrito='" . $_SESSION["expropiar_usuario_distrito_nombre"] . "')";
    } else {
        $login_central = " AND par.obr_id IN (SELECT obr_id FROM obras WHERE obr_distrito='" . $_SESSION["expropiar_usuario_distrito_nombre"] . "')";
    }
}


if($_REQUEST["accion"] == 'get_parcelas_propietario') {
    $db->ExecuteSQL("SELECT parpro.*, par.* FROM parcelas_propietarios parpro, parcelas par 
    WHERE parpro.per_id_propietario = " . $_REQUEST["id_propietario"] . " 
    AND par.par_id  = parpro.par_id" . $login_central);
    $parcelas_arr = $db->getRows();
    
    $options = "";
    for ($i=0; $i < count($parcelas_arr); $i++) { 
        $data = encrypt_decrypt("encrypt", $parcelas_arr[$i]['par_id']);
        $options .= '<option value="'.$data.'">' . "Parcela " .$parcelas_arr[$i]['par_numero_orden'] . '</option>';
    }

    echo json_encode($options);
}
if($_REQUEST["accion"] == 'get_parcelas_numero') {
    $sql = "SELECT par.* FROM parcelas par WHERE par.par_numero_orden = '" . $_REQUEST["numero_parcela"] . "'" . $login_central;
    $db->ExecuteSQL($sql);
    $parcelas_arr = $db->getRows();

    if(count($parcelas_arr) > 0) {
        $parcelas_arr[0]["id_encriptado"] = encrypt_decrypt("encrypt", $parcelas_arr[0]['par_id']);
    }
    echo json_encode($parcelas_arr);
}
if($_REQUEST["accion"] == 'listar_obras') {
    $db->ExecuteSQL("SELECT obr.* FROM obras obr" . $login_central);
    $obras_arr = $db->getRows();

    $options = '<option value=""></option>';

    for ($i=0; $i < count($obras_arr); $i++) { 
        $options .= '<option value="'.$obras_arr[$i]['obr_id'].'">'.$obras_arr[$i]['obr_nombre'] . '</option>';
    }

    echo json_encode($options);
}
if($_REQUEST["accion"] == 'get_parcelas_obra') {
    $db->ExecuteSQL("SELECT par.* FROM parcelas par WHERE par.obr_id = " . $_REQUEST["id_obra"]);
    $parcelas_arr = $db->getRows();
    
    $options = "";
    for ($i=0; $i < count($parcelas_arr); $i++) { 
        $data = encrypt_decrypt("encrypt", $parcelas_arr[$i]['par_id']);
        $options .= '<option value="'.$data.'">'. "Parcela " .$parcelas_arr[$i]['par_numero_orden'] . '</option>';
    }

    echo json_encode($options);
}

