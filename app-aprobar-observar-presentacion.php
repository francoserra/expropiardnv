<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);





//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {exit;}

//-- los parametros que se le pasa a la ventana vienen encriptados
//$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$accion = $_GET["accion"];
$mpp_id = $_GET["mpp_id"];
$par_id = $_GET["par_id"];
$ente = $_GET["ente"];


?>
 
<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaObservarPresentacion">
    <div class="modal-dialog modal-lg modal-dialog-centered " role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title"><?=ucfirst($accion)." $ente"?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <form id="frmObservarPresentacion" name="frmObservarPresentacion">
                <div class="form-group row mb-1 required">
                        <label for="lstMotivo" class="col-3 col-form-label text-right">Motivo</label>
                        <div class="col-9">
                            <select  class="form-control  text-uppercase" id="lstMotivo" name="lstMotivo" >
                                <?php
                                if ( $accion=="aprobar" || $accion=="presentar" ){ 
                                ?>
                               <option value="">(NO APLICA)</option>
                                <?php
                                }else{
                                ?>
                                <option value="ERROR DE CARGA">ERROR DE CARGA</option>
                                <option value="DOCUMENTO ILEGIBLE">DOCUMENTO ILEGIBLE</option>
                                <option value="ERRORES DE MEDICIÓN">ERRORES DE MEDICIÓN</option>
                                <option value="OTROS">OTROS</option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div> 
   
                    <div class="form-group row mb-1 required" required>
                        <label for="dtFechaPresentacion" class="col-3 col-form-label text-right">Fecha</label>
                        <div class="col-9">
                            <input id="dtFechaPresentacion" name="dtFechaPresentacion" class="form-control" type="date" max="<?=date('Y-m-d')?>"required value="" />
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="uploadArchivo" class="col-3 col-form-label text-right">Archivo&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-md-9">
                            <div id="uploadArchivo" class="btn btn-primary" style="margin-bottom:-15px!important"><span class="fa fa-upload"></span>&nbsp;Adjuntar archivo</div>
                        </div>
                    </div>
                    <div class="form-group row mb-1 ">
                        <label for="txaObservaciones" class="col-3 col-form-label text-right">Observaciones&nbsp;&nbsp;&nbsp;</label>
                        <div class="col-9">
                            <textarea id="txaObservaciones" name="txaObservaciones" class="form-control text-uppercase" type="text" maxlength="500" rows="4" style="resize:none;overflow:hidden"></textarea>
                        </div>
                    </div>

                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaObservarPresentacion').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_archivo = "";
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaObservarPresentacion').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmObservarPresentacion #txtResolucion").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaObservarPresentacion').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmObservarPresentacion").submit(function(e) {
        e.preventDefault();

        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-aprobar-observar-presentacion-x.php", {
                            mpp_id: "<?=$mpp_id?>",
                            par_id: "<?=$par_id?>",
                            accion:"<?=$accion?>",
                            ente:"<?=$ente?>",
                            archivo: g_archivo,
                            datastring: $("#frmObservarPresentacion").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $("#mdlVentanaObservarPresentacion").modal('hide');
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
     //-----------------------------------------------------------------------------------------------------------------
    //-- upload
    var settings = {
        url: "app-aprobar-observar-presentacion-x.php",
        method: "POST",
        fileName: "myfile",
        uploadStr: '<i class="fa fa-paperclip"></i>&nbsp;&nbsp;click para adjuntar (max. 100MB)',
        multiple: false,
        showProgress: true,
        maxFileCount: 1,
        dragDrop: false,
        showDelete: true,
        showAbort: true,
        showError: 'pdf,dxf,doc',
        maxFileSize: 100000000,
        dynamicFormData: function() {
            var data = {
                accion: "upload-archivo",
                fuente: "<?=$fuente?>"
            };
            return data;
        },
        onSubmit: function(data, files) {
            $("#frmObservarPresentacion #uploadArchivo").hide();
        },
        onSelect: function(files) {
            //console.log(files);//json
        },
        afterUploadAll: function() {},
        onCancel: function() {
            $("#frmObservarPresentacion #uploadArchivo").show();
            uploadObj.reset();
            g_archivo = "";
        },
        onSuccess: function(files, data, xhr) {
            let json = $.parseJSON(data);
            if (json["res"] == 1) {
                g_archivo = json["archivo"];
                $("#frmObservarPresentacion #uploadArchivo").hide();
            } else if (json["res"] == 0) {
                $("#frmObservarPresentacion #uploadArchivo").show();
                uploadObj.reset();
                g_archivo = "";
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + json["msg"] + "</span>"
                });
            }
        },
        onError: function(files, status, errMsg) {
            $("#frmObservarPresentacion #uploadArchivo").show();
            g_archivo = "";
            uploadObj.reset();
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;UPS...</b></big><br><br>" + errMsg + "</span>"
            });
        },
        deleteCallback: function(data, pd) {
            g_archivo = "";
            $("#frmObservarPresentacion #uploadArchivo").show();
            pd.statusbar.hide();
        }
    };
    var uploadObj = $("#frmObservarPresentacion #uploadArchivo").uploadFile(settings);
});
</script>