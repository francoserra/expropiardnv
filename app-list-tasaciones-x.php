<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";

error_reporting(0);
set_time_limit(20);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {exit;}
//-- unserializamos los parametros del formulario
parse_str($_REQUEST["datastring"]);

//-- nombre de la tabla temporal que usamos para guardar todos los regidtros de la consulta, que luego paginaremos
$table_tmp = "_TMP_" . $_SESSION["expropiar_usuario_id"];

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//-- info vital para el paginador -------------------------------------------------------------------------------------

$pagina = $_GET["page"];
$pagina_anterior = $pagina - 1;
$pagina_siguiente = $pagina + 1;
$reg = $pagina * $GLOBALS["ITEMS_X_PAGINA"];
$limit = "LIMIT $reg," . $GLOBALS["ITEMS_X_PAGINA"];

//---------------------------------------------------------------------------------------------------------------------
// TIPOS DE TASACIONES
function getTipoTasacion($valor_buscado)
{ //$referencia = tipo de dato que viene en la variable $valor_buscado
    $objTiposTasaciones = (object) [
        1 => "NOTA DE VALOR",
        2 => "RECONSIDERACIÓN",
        3 => "ACTUALIZACIÓN NOTA DE VALOR",
        4 => "VALOR HISTÓRICO",
        5 => "ACTUALIZACIÓN POR CAMBIO DE SUPERFICIE AFECTACIÓN",
        6 => "RETROCESIÓN"
    ];
    return $objTiposTasaciones->$valor_buscado;
}

//---------------------------------------------------------------------------------------------------------------------
// ESTADOS COLORES
function getEstadoPresentacion($valor)
{ //$referencia = tipo de dato que viene en la variable $valor_buscado
    $objEstados = (object) [
        1 => "text-secondary",
        2 => "text-info",
        3 => "text-primary",
        4 => "text-warning",
        5 => "text-danger",
        6 => "text-success"
    ];
    return $objEstados->$valor;
}

// ESTADOS nombres
function getEstadoPresentacionNombre($valor)
{ //$referencia = tipo de dato que viene en la variable $valor_buscado
    $objEstadosNombre = (object) [
        1 => "TASACION SOLICITADA",
        2 => "OBSERVADA DNV",
        3 => "APROBADA DNV",
        4 => "PRESENTADA TTN",
        5 => "OBSERVADA TTN",
        6 => "APROBADA TTN",
        7 => "SOLICITUD TASADA"
    ];
    return $objEstadosNombre->$valor;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["accion"] == "get-archivo") {
    $ctx = stream_context_create(array('http' => array(
        'timeout' => 20000, //1200 Seconds is 20 Minutes
    ),
    ));

    if (file_exists($_REQUEST["archivo_pdf"])) {
        echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
    } else {
        echo "";
    }
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "delete-archivo") {
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id=" . $_REQUEST["arc_id"]);
    auditar($db, "archivo", "baja", $_REQUEST["arc_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "delete-presentacion") {
    //$db->ExecuteSQL("UPDATE parcelas_propietarios SET ppr_eliminado=1 WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    $db->ExecuteSQL("UPDATE mensura_presentaciones  SET mpp_eliminada=1 WHERE mpp_id=" . $_REQUEST["mpp_id"]);
    $db->ExecuteSQL("UPDATE archivos SET arc_eliminado=1 WHERE arc_id_fuente=" . $_REQUEST["mpp_id"]);
    auditar($db, "presentación", "baja", $_REQUEST["mpp_id"], json_encode($_REQUEST));
    echo true;
    exit;
}
//---------------------------------------------------------------------------------------------------------------------
else if ($_REQUEST["accion"] == "habilitar-tasacion") {
    //$db->ExecuteSQL("UPDATE parcelas_propietarios SET ppr_eliminado=1 WHERE ppr_id=" . $_REQUEST["ppr_id"]);
    $db->ExecuteSQL("UPDATE parcelas  SET par_tasacion_habilitada=".$_REQUEST["tipo_tasacion"].",par_tasacion_habilitada_fecha=Now() WHERE par_id=" . $_REQUEST["par_id"]);
    auditar($db, "presentación", "habilitación-tasación", $_REQUEST["par_id"], json_encode($_REQUEST));
    echo true;
    exit;
}

//---------------------------------------------------------------------------------------------------------------------

if ($_REQUEST["primera_vez"] == 1) {


    $filtro_dis = "";
    if ($_SESSION["expropiar_usuario_distrito_nombre"] != "CASA CENTRAL") {
        $filtro_dis = " AND obr_distrito ='".$_SESSION["expropiar_usuario_distrito_nombre"]."' ";
    }

    if ($lstObra != "") {
        $filtro_obra = " AND obr.obr_id='$lstObra' " . $filtro_dis ;
    } else {
        $filtro_obra = $filtro_dis;

    }

    if ($lstEstado != "" && $lstEstado != 1) {
        $filtro_estado = " AND ultimo_estado=$lstEstado";
    } else if ($lstEstado != "" || $lstEstado == 1) {
        $filtro_estado = " AND ultimo_estado IS NULL";
    }
    if ($txtNumeroOrden != "") {
        $filtro_numero_orden_parcela = " AND par.par_numero_orden=$txtNumeroOrden";
    }

    $sql = "SELECT 
                par.par_id,
                (SELECT ppt_estado FROM tasaciones_solicitudes WHERE par_id=par.par_id  ORDER BY ppt_estado DESC LIMIT 1) ultimo_estado,
                par.obr_id,
                par.par_numero_orden,
                par.fecha,
                par.usuario,
                obr.obr_idsigo,
                obr.obr_nombre
            FROM parcelas par, obras obr
            WHERE obr.obr_id=par.obr_id AND par.par_id IN (SELECT DISTINCT par_id FROM tasaciones_solicitudes)
                $filtro_obra
                $filtro_numero_orden_parcela
            HAVING 1
                $filtro_estado
            ORDER BY (SELECT tsol.ppt_fecha_solicitud FROM tasaciones_solicitudes tsol WHERE tsol.par_id=par.par_id ORDER BY tsol.ppt_fecha_solicitud DESC LIMIT 1) DESC";

    $db->ExecuteSQL("DROP TABLE $table_tmp");
    $result = $db->ExecuteSQL("CREATE TABLE $table_tmp ENGINE=MEMORY AS $sql");
    if ($result != 1) {
        $arr["res"] = 0;
        $arr["msg"] = "Error[98] en consulta a la base de datos ($result).";
        echo json_encode($arr);
        exit;
    }
    //-- para exportar a excel
    $_SESSION["sql"] = $sql;

    //-- informacion vital !!! para el paginador --------------------------------
    $result = $db->ExecuteSQL("SELECT COUNT(*) count FROM $table_tmp");
    $row = $db->getRows();
    $_SESSION["total_registros"] = $row[0]["count"];
    $_SESSION["total_paginas"] = ceil($row[0]["count"] / $GLOBALS["ITEMS_X_PAGINA"]);
    //---------------------------------------------------------------------------
}
//---------------------------------------------------------------------------------------------------------------------

$db->ExecuteSQL("SELECT * FROM $table_tmp $limit");
$parcelas_arr = $db->getRows();

// ENCABEZADOS DE LA GRILLA
$html = <<<eod
<table class="table ">
<caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions bold">Total de parcelas: {$number_format($_SESSION["total_registros"],0,",",".")}</mark></span></caption>
	<thead class="bg-dark text-light">
		<tr>
            <th class="vert-align" nowrap width="1%"></th>
            <th class="vert-align" nowrap>ID</th>
            <th class="vert-align" nowrap>ID Sigo</th>
            <th class="vert-align" nowrap>Nombre obra</th>
            <th class="vert-align" nowrap>N° de orden parcela</th>
            <th class="vert-align" nowrap>Fecha alta</th>
            <th class="vert-align" nowrap>Usuario alta</th>
		</tr>
	</thead>
eod;

// RECORRO TODAS LAS PARCELAS ENCONTRADAS
for ($i = 0; $i < count($parcelas_arr); $i++) {
    // BUSCO LAS SOLICITUDES DE LA PARCELA
    $db->ExecuteSQL("SELECT * FROM tasaciones_solicitudes WHERE par_id=" . $parcelas_arr[$i]["par_id"] . " ORDER BY ppt_fecha_solicitud DESC");
    $solicitudes_arr = $db->getRows();

    // SI TIENE SOLICITUDES DETALLE HABILITO EL TOGGLE PARA DESPLEGAR EL DETALLE
    $tool_toogle = <<<eod
            &nbsp;&nbsp;<a href="" ><i onClick="$('#trAdjuntos{$i}').toggle();$('#trSolicitudes{$i}').toggle();if ($(this).hasClass('fa-chevron-down')){ $(this).toggleClass('fa-chevron-up')};return false;"  data-toggle="tooltip" title="ver solicitudes de parcela" class="fa fa-chevron-down" style="font-size:0.9rem;color:#212121" ></i></a>&nbsp;&nbsp;
eod;

    // COMIENZO ARMANDO LA LISTA DE PARCELAS
    $html .= <<<eod
    <tr>
        <td nowrap class="vert-align">{$ti}{$tool_toogle}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_id"]}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["obr_idsigo"]}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["obr_nombre"]}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["par_numero_orden"]}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$ymd2dmy($parcelas_arr[$i]["fecha"])}{$tf}</td>
        <td nowrap class="vert-align">{$ti}{$parcelas_arr[$i]["usuario"]}{$tf}</td>
	</tr>
eod;

    if(count($solicitudes_arr) > 0){

        $html .= <<<eod
        <tr id="trSolicitudes{$i}" style="display:none">
            <td  colspan="2"></td>
            <td colspan="10">
                <table class="table table-sm table-borderless" style="font-size:0.8rem;background:#FFFDE7">
eod;

        for ($sol=0; $sol < count($solicitudes_arr); $sol++) { 

            $tools_solicitud = "";
            // BUSCO SI LA SOLICITUD TIENE UNA TASACION REALIZADA
            $db->ExecuteSQL("SELECT * FROM tasaciones WHERE tas_solicitud_id=" . $solicitudes_arr[$sol]["ppt_id"] ." AND tas_deleted=0 ORDER BY tas_fecha_tasacion DESC LIMIT 1");
            $tasaciones_arr = $db->getRows();

            //SI NO HAY TASACION MUESTRO EL BOTON PARA TASAR Y LA SOLICITUD ESTA APROBADA POR TTN
            if(count($tasaciones_arr) == 0 && $solicitudes_arr[$sol]["ppt_estado"] == 6){
                $tools_solicitud = <<<eod
                &nbsp;&nbsp;<button onClick="$.realizarTasacion({$solicitudes_arr[$sol]["ppt_id"]},{$parcelas_arr[$i]["par_id"]},'insert')" class="btn btn-sm btn-default" data-toggle="tooltip" title="REALIZAR TASACIÓN"><i class="fas fa-dollar-sign text-success btn-icon"></i></button>
eod;
            } else if(count($tasaciones_arr) == 1 && $solicitudes_arr[$sol]["ppt_estado"] == 7){
                $tools_solicitud = <<<eod
                &nbsp;&nbsp;<button onClick="$.realizarTasacion({$solicitudes_arr[$sol]["ppt_id"]},{$parcelas_arr[$i]["par_id"]},'show','tasacion')" class="btn btn-sm btn-default" data-toggle="tooltip" title="REALIZAR TASACIÓN"><i class="fas fa-search-dollar text-primary btn-icon"></i></button>
eod;

            } else {
                $tools_solicitud = "";
            }





            // BUSCO LOS DETALLES DE LA SOLICITUD
            $db->ExecuteSQL("SELECT * FROM tasaciones_solicitudes_detalle WHERE ppt_id=" . $solicitudes_arr[$sol]["ppt_id"]);
            $detalle_solicitud_arr = $db->getRows();
            
            $num_sol = intval($sol) + 1;
            $html .= <<<eod
            <tr id="trDetalleSolicitudes{$sol}">
                <td  colspan="2"></td>
                <td colspan="10">
                    <table class="table table-sm table-borderless" style="font-size:0.8rem;background:#FFFDE7">
                    <caption class="text-left" style="margin-bottom:-5px!important"><span class="label captions bold">Solicitud</span></caption>
                    <tr>
                        <th nowrap class="text-center"></th>
                        <th nowrap class="text-center"></th>
                        <th>ID</th>
                        <th nowrap>Tipo de solicitud</th>
                        <th nowrap>Usuario solicitante</th>
                        <th nowrap>Fecha de solicitud</th>
                        <th nowrap>Estado de solicitud</th>
                    </tr>
eod;
            // OBTENGO EL NOMBRE DEL TIPO DE TASACION SEGUN EL ID ASIGNADO A LA SOLICITUD
            $tipo_tasacion = getTipoTasacion($solicitudes_arr[$sol]["ppt_tipo_tasacion_solicitada"]);

            // OBTENGO EL NOMBRE DEL ESTADO DE LA SOLICITUD
            $estado_solicitud = getEstadoPresentacionNombre($solicitudes_arr[$sol]["ppt_estado"]);

            // VERIFICO LOS ESTADOS SEGUN EL DETALLE
            $clase_estatus_presentacion_plano = "";
            $clase_estatus_aprobada_mejoras = "";
            $clase_estatus_aprobada_croquis = "";

            
            

            for ($detest=0; $detest < count($detalle_solicitud_arr); $detest++) {
                

                $db->ExecuteSQL("SELECT tsdet.pptdet_estado, tsdet.pptdet_id, tsdet.mpp_id, arc.cat_id, arc.arc_id_fuente 
                                        FROM tasaciones_solicitudes_detalle tsdet, archivos arc 
                                        WHERE tsdet.pptdet_id=" . $detalle_solicitud_arr[$detest]["pptdet_id"]." AND arc.arc_id_fuente=tsdet.mpp_id");
                $detalle_estado = $db->getRows();

                // ESTADO DE PLANO
                if($detalle_estado[0]["cat_id"] == 13){
                    $clase_estatus_presentacion_plano = getEstadoPresentacion($detalle_estado[0]["pptdet_estado"]);
                } 
                // ESTADO DE MEJORAS
                if($detalle_estado[0]["cat_id"] == 25){
                    $clase_estatus_aprobada_mejoras = getEstadoPresentacion($detalle_estado[0]["pptdet_estado"]);
                } 
                // ESTADO DE CORQUIS
                if($detalle_estado[0]["cat_id"] == 26){
                    $clase_estatus_aprobada_croquis = getEstadoPresentacion($detalle_estado[0]["pptdet_estado"]);
                }
            }
            
            if($solicitudes_arr[$sol]["ppt_estado"] == 3) {
                $presentar_ttn = true;
            } else {
                $presentar_ttn = false;
            }

            // BOTON PARA ENVIAR A TTN
            if($presentar_ttn){
                $tools_solicitud = <<<eod
            <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion({$solicitudes_arr[$sol]["ppt_id"]},'presentar','TTN',{$solicitudes_arr[$sol]["ppt_id"]},{$detalle_solicitud_arr[$detsol]["pptdet_id"]});return false" data-toggle="tooltip" title="presentar tasación TTN"><i class="fas fa-arrow-alt-circle-left text-info btn-icon"></i></a>
eod;
            }

            switch ($atributo_perfil) {
                case 1:
                    $tools_solicitud = "";
                    break;
                case 2:
                    $tools_solicitud = "";
                    break;
                case 3:
                    $tools_solicitud = $tools_solicitud;
                    break;
                default:
                    $tools_solicitud = "";
                    break;
            }
            

            $html .= <<<eod
            <tr>
                <td class="text-center vert-align" width="5%">{$tools_solicitud}</td>
                <td nowrap class="vert-align text-center">{$ti}
                    <i  data-toogle="tooltip" title="Plano" class="fa fa-circle {$clase_estatus_presentacion_plano}"></i>
                    <i  data-toogle="tooltip" title="Mejoras" class="fa fa-circle {$clase_estatus_aprobada_mejoras}"></i>
                    <i  data-toogle="tooltip" title="Croquis" class="fa fa-circle {$clase_estatus_aprobada_croquis}"></i>
                {$tf}</td>
                <td nowrap class="vert-align">{$ti}{$solicitudes_arr[$sol]["ppt_id"]}{$tf}</td>
                <td nowrap class="vert-align" style="width: 20%;">{$ti}{$tipo_tasacion}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$solicitudes_arr[$sol]["ppt_usuario_solicitante"]}{$tf}</td>
                <td nowrap class="vert-align">{$ti}{$ymd2dmy($solicitudes_arr[$sol]["ppt_fecha_solicitud"])}{$tf}</td>
                <td nowrap class="vert-align">{$ti}<span class="badge badge-primary">{$estado_solicitud}</span>{$tf}</td>
            </tr>
eod;

            $html .= <<<eod
                <tr id="trDetalle{$detsol}">
                    <td  colspan="2"></td>
                    <td colspan="10">
                        <table class="table table-sm table-borderless" style="font-size:0.8rem;background:#FFFDE7">
                            <tr>
                                <th nowrap class="text-center"></th>
                                <th>ID Presentación</th>
                                <th>Fecha presentación</th>
                                <th nowrap>Presentó</th>
                                <th nowrap>Medio de presentación</th>
                                <th nowrap>Persona presentó</th>
                                <th>Observaciones presentación</th>
                                <th nowrap>Fecha alta</th>
                                <th nowrap>Usuario alta</th>
                            </tr>
eod;
    
            for ($detsol=0; $detsol < count($detalle_solicitud_arr); $detsol++) { 
                $tools = $tool_observar = $tool_aprobar = $tool_eliminar = $estado = "";

                $db->ExecuteSQL("SELECT *
                FROM
                    mensura_presentaciones mpp,
                    personas_fisicas per,
                    archivos arc,
                    mensura_estados mes,
                    categorizacion_archivos cat
                WHERE
                    mpp.per_id=per.per_id
                    AND mpp.mes_id=mes.mes_id
                    AND mpp.mpp_id=arc.arc_id_fuente
                    AND arc.cat_id=cat.cat_id
                    AND arc.arc_fuente='MENS'
                    AND mpp.mpp_eliminada=0
                    AND arc.arc_eliminado=0
                    AND mpp.mpp_id=" . $detalle_solicitud_arr[$detsol]["mpp_id"] . " ORDER BY mpp.mpp_id DESC");  

                $presentacion_detalle = $db->getRows();


                // BOTONES PARA APROBAR U OBSERVAR DNV
                if($detalle_solicitud_arr[$detsol]["pptdet_estado"] == "1"){
                    $tool_aprobar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion({$solicitudes_arr[$sol]["ppt_id"]},'aprobar','DNV',{$solicitudes_arr[$sol]["par_id"]},{$detalle_solicitud_arr[$detsol]["pptdet_id"]});return false" data-toggle="tooltip" title="aprobar tasación DNV"><i class="fas fa-check-circle text-info btn-icon"></i></a>
eod;
                $tool_observar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion({$solicitudes_arr[$sol]["ppt_id"]},'observar','DNV',{$solicitudes_arr[$sol]["par_id"]},{$detalle_solicitud_arr[$detsol]["pptdet_id"]});return false" data-toggle="tooltip" title="observar tasación DNV"><i class="fas fa-check-circle text-danger btn-icon"></i></a>
eod;
                }

                // BOTONES PARA APROBAR U OBSERVAR TTN
                if($detalle_solicitud_arr[$detsol]["pptdet_estado"] == "4"){
                    $tool_aprobar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion({$solicitudes_arr[$sol]["ppt_id"]},'aprobar','TTN',{$solicitudes_arr[$sol]["par_id"]},{$detalle_solicitud_arr[$detsol]["pptdet_id"]});return false" data-toggle="tooltip" title="aprobar tasación TTN"><i class="fas fa-check-circle text-info btn-icon"></i></a>
eod;
                $tool_observar = <<<eod
                <a class="btn btn-default btn-sm" onClick="$.presentarAprobarObservarPresentacion({$solicitudes_arr[$sol]["ppt_id"]},'observar','TTN',{$solicitudes_arr[$sol]["par_id"]},{$detalle_solicitud_arr[$detsol]["pptdet_id"]});return false" data-toggle="tooltip" title="observar tasación TTN"><i class="fas fa-check-circle text-danger btn-icon"></i></a>
eod;
                }

                switch ($atributo_perfil) {
                    case 1:
                        $tools = "";
                        break;
                    case 2:
                        $tools = "";
                        break;
                    case 3:
                        $tools = $tool_aprobar . $tool_observar . $tool_eliminar;
                        break;
                    default:
                        $tools = "";
                        break;
                }

                // COMIENZO ARMANDO LA LISTA DE PRESENTACIONES
                $html .= <<<eod
                <tr>
                    <td class="text-center vert-align"  width="8%">{$tools}</td>
                    <td nowrap class="vert-align" width="10%">{$presentacion_detalle[0]["mpp_id"]}</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentacion_detalle[0]["mpp_fecha_presentacion"])}</td>
                    <td nowrap class="vert-align">
                        {$presentacion_detalle[0]["cat_descripcion"]}&nbsp;<button onClick="$.descargarArchivo('{$presentacion_detalle[0]["arc_path"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                    
                    </td>
                    <td nowrap class="vert-align">{$presentacion_detalle[0]["mpp_medio"]} {$presentacion_detalle[0]["mpp_mesa_entrada_nota"]}</td>
                    <td nowrap nowrap class="vert-align">{$presentacion_detalle[0]["per_nombre"]} {$presentacion_detalle[0]["per_apellido"]}</td>
                    <td nowrap class="vert-align">{$presentacion_detalle[0]["mpp_observaciones"]}</td>
                    <td nowrap class="vert-align">{$ymd2dmy($presentacion_detalle[0]["fecha"])}</td>
                    <td nowrap class="vert-align">{$presentacion_detalle[0]["usuario"]}</td>
                </tr>
eod;


                $html_aprobada_dnv = "";
                if(!is_null($detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_fecha"])) {
                    $html_aprobada_dnv = <<<eod
                    <tr>
                        <td nowrap class="vert-align bold">aprobada DNV</td>
                        <td nowrap class="vert-align">{$ymd2dmy($detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_fecha"])}</td>
                        <td nowrap class="vert-align">observaciones: <b>{$detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_observacion"]}</b></td>
                        <td nowrap class="vert-align">usuario alta: {$detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_usuario"]}</td>
eod;            
                    $archivo_aprobada = "";
                    if($detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_archivo"] != "") {            
                        $archivo_aprobada = <<<eod
                            <td nowrap class="vert-align">
                                <button onClick="$.descargarArchivo('{$detalle_solicitud_arr[$detsol]["pptdet_aprobada_dnv_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                            </td>
                        </tr>
eod;
                    } else {
                        $archivo_aprobada = "";
                    }
                    $html_aprobada_dnv .= $archivo_aprobada;
                }


                $html_observada_dnv = "";
                if(!is_null($detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_fecha"])) {
                    $html_observada_dnv = <<<eod
                    <tr>
                        <td nowrap class="vert-align bold">observada DNV</td>
                        <td nowrap class="vert-align">{$ymd2dmy($detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_fecha"])}</td>
                        <td nowrap class="vert-align">observaciones: <b>{$detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_observacion"]}</b></td>
                        <td nowrap class="vert-align">usuario alta: {$detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_usuario"]}</td>
eod;            
                    $archivo_observada = "";
                    if($detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_archivo"] != "") {            
                        $archivo_observada = <<<eod
                            <td nowrap class="vert-align">
                                <button onClick="$.descargarArchivo('{$detalle_solicitud_arr[$detsol]["pptdet_observada_dnv_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                            </td>
                        </tr>
eod;
                    } else {
                        $archivo_observada = "";
                    }
                    $html_observada_dnv .= $archivo_observada;
                }


                $html_presentada_ttn = "";
                if(!is_null($detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_fecha"])) {
                    $html_presentada_ttn = <<<eod
                    <tr>
                        <td nowrap class="vert-align bold">presentada TTN</td>
                        <td nowrap class="vert-align">{$ymd2dmy($detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_fecha"])}</td>
                        <td nowrap class="vert-align">observaciones: <b>{$detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_observacion"]}</b></td>
                        <td nowrap class="vert-align">usuario alta: {$detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_usuario"]}</td>
eod;            
                    $archivo_presentadattn = "";
                    if($detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_archivo"] != "") {            
                        $archivo_presentadattn = <<<eod
                            <td nowrap class="vert-align">
                                <button onClick="$.descargarArchivo('{$detalle_solicitud_arr[$detsol]["pptdet_presentada_ttn_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                            </td>
                        </tr>
eod;
                    } else {
                        $archivo_presentadattn = "";
                    }
                    $html_presentada_ttn .= $archivo_presentadattn;
                }


                $html_aprobada_ttn = "";
                if(!is_null($detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_fecha"])) {
                    $html_aprobada_ttn = <<<eod
                    <tr>
                        <td nowrap class="vert-align bold">aprobada TTN</td>
                        <td nowrap class="vert-align">{$ymd2dmy($detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_fecha"])}</td>
                        <td nowrap class="vert-align">observaciones: <b>{$detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_observacion"]}</b></td>
                        <td nowrap class="vert-align">usuario alta: {$detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_usuario"]}</td>
eod;            
                    $archivo_aprobadattn = "";
                    if($detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_archivo"] != "") {            
                        $archivo_aprobadattn = <<<eod
                            <td nowrap class="vert-align">
                                <button onClick="$.descargarArchivo('{$detalle_solicitud_arr[$detsol]["pptdet_aprobada_ttn_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                            </td>
                        </tr>
eod;
                    } else {
                        $archivo_aprobadattn = "";
                    }
                    $html_aprobada_ttn .= $archivo_aprobadattn;
                }


                $html_observada_ttn = "";
                if(!is_null($detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_fecha"])) {
                    $html_observada_ttn = <<<eod
                    <tr>
                        <td nowrap class="vert-align bold">observada TTN</td>
                        <td nowrap class="vert-align">{$ymd2dmy($detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_fecha"])}</td>
                        <td nowrap class="vert-align">observaciones: <b>{$detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_observacion"]}</b></td>
                        <td nowrap class="vert-align">usuario alta: {$detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_usuario"]}</td>
eod;            
                    $archivo_observadattn = "";
                    if($detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_archivo"] != "") {            
                        $archivo_observadattn = <<<eod
                            <td nowrap class="vert-align">
                                <button onClick="$.descargarArchivo('{$detalle_solicitud_arr[$detsol]["pptdet_observada_ttn_archivo"]}')" class="btn btn-sm btn-default" data-toggle="tooltip" title="descargar archivo"><i class="fas fa-download text-primary"></i></button>&nbsp;&nbsp;
                            </td>
                        </tr>
eod;
                    } else {
                        $archivo_observadattn = "";
                    }
                    $html_observada_ttn .= $archivo_observadattn;
                }

                if ($detalle_solicitud_arr[$detsol]["pptdet_estado"] == 3) {
                    $html .= <<<eod
                    <tr style="font-size:0.8em">
                        <td colspan="2"></td>
                        <td colspan="10">
                            <table style=" border: 1px solid #ccc;border-collapse: collapse">
                                {$html_aprobada_dnv}
                            </table>
                        </td>
                    </tr>
eod;
                }
                if ($detalle_solicitud_arr[$detsol]["pptdet_estado"] == 2) {
                    $html .= <<<eod
                    <tr style="font-size:0.8em">
                        <td colspan="2"></td>
                        <td colspan="10">
                            <table style=" border: 1px solid #ccc;border-collapse: collapse">
                                {$html_observada_dnv}
                            </table>
                        </td>
                    </tr>
eod;
                }

                if ($detalle_solicitud_arr[$detsol]["pptdet_estado"] == 4) {
                    $html .= <<<eod
                    <tr style="font-size:0.8em">
                        <td colspan="2"></td>
                        <td colspan="10">
                            <table style=" border: 1px solid #ccc;border-collapse: collapse">
                                {$html_aprobada_dnv}
                                {$html_presentada_ttn}
                            </table>
                        </td>
                    </tr>
eod;
                }
                if ($detalle_solicitud_arr[$detsol]["pptdet_estado"] == 5) {
                    $html .= <<<eod
                    <tr style="font-size:0.8em">
                        <td colspan="2"></td>
                        <td colspan="10">
                            <table style=" border: 1px solid #ccc;border-collapse: collapse">
                                {$html_aprobada_dnv}
                                {$html_presentada_ttn}
                                {$html_observada_ttn}
                            </table>
                        </td>
                    </tr>
eod;
                }
                if ($detalle_solicitud_arr[$detsol]["pptdet_estado"] == 6) {
                    $html .= <<<eod
                    <tr style="font-size:0.8em">
                        <td colspan="2"></td>
                        <td colspan="10">
                            <table style=" border: 1px solid #ccc;border-collapse: collapse">
                                {$html_aprobada_dnv}
                                {$html_presentada_ttn}
                                {$html_aprobada_ttn}
                            </table>
                        </td>
                    </tr>
eod;
                }
            }
            
            $html .= <<<eod
                    </table>
                </td>
            </tr>
eod;
            $html .= <<<eod
                    </table>
                </td>
            </tr>
eod;
        }
        $html .= <<<eod
            </table>
            </td>
        </tr>
eod;
    }

}    

$html .= <<<eod
    </table>
eod;

//-- paginador: incluir siempre !!!! ----------------------------------------
include "inc/paginador.php";
//----------------------------------------------------------------------------

$arr["res"] = 1;
$arr["html"] = $html;
echo json_encode($arr);
exit;
