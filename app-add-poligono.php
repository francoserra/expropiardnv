<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- los parametros que se le pasa a la ventana vienen encriptados
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];

$par_id = $_GET["par_id"];
$psup_id= $_GET["psup_id"];
$psup_nombre = $_GET["psup_nombre"];

//-- chequeamos que el usuario se haya logeado y tengas los permisos para ejecutar este script, ESTO VA SIEMPRE !!!!!!
//$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil == 0) {;exit;}
//if ($perfil_readonly) {exit;}

//-- abrimos la base de datos
$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");

//$db->ExecuteSQL("SELECT * FROM parcelas_poligonos WHERE poligono_id = $id");
//$psup_datos = $db->getRows();

?>

<div class="modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaPligono" style="z-index:99999999!important">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content  box-shadow--8dp">
            <div class="modal-header">
                <h5 class="modal-title">Nuevo Polígono Catastral</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmPoligono" name="frmPoligono">
                    <input type="hidden" name="poligono_id" id="poligono_id" value="">
                    <div class="form-group row mb-3">
                        <label for="lstlado" class="col-1 col-form-label text-right">Lado</label>
                        <div class="col-2">
                            <select class="form-control  text-uppercase" id="lstlado" name="lstlado">

                                <option value="Norte">Norte</option>
                                <option value="Este">Este</option>
                                <option value="Sur">Sur</option>
                                <option value="Oeste">Oeste</option>
                                

                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-0 ">
                        <label for="txtpunto" class="col-1 col-form-label text-right">Punto</label>
                        <div class="col-2">
                            <input id="txtpunto" name="txtpunto" class="form-control  text-uppercase" type="text" maxlength="2" value="" required>
                        </div>

                        <label for="txtpunto_sig" class="col-3 col-form-label text-right">Punto Siguiente Segmento</label>
                        <div class="col-2">
                            <input id="txtpunto_sig" name="txtpunto_sig" class="form-control  text-uppercase" type="TEXT" maxlength="2" value="" required>
                        </div>  


                    </div>
                    <hr style="color: #0056b2;" />
                    <div class="form-group row mb-0 ">
                        <b><label for="txtangulo_grad" class="col-1 col-form-label text-right">Angulo</label></b>
                    </div>
                    <div class="form-group row mb-3 ">
                        <label for="txtangulo_grad" class="col-1 col-form-label text-right">Grados</label>
                        <div class="col-2">
                            <input id="txtangulo_grad" name="txtangulo_grad" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="" >
                        </div>

                         <label for="txtangulo_min" class="col-1 col-form-label text-right">Minutos</label>
                        <div class="col-2">
                            <input id="txtangulo_min" name="txtangulo_min" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="" >
                        </div>  
                        <label for="txtangulo_seg" class="col-1 col-form-label text-right">Segundos</label>
                         <div class="col-2">
                            <input id="txtangulo_seg" name="txtangulo_seg" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="" >
                        </div>  
                        <label for="txtlongitud" class="col-1 col-form-label text-right">Longitud</label>
                         <div class="col-2">
                            <input id="txtlongitud" name="txtlongitud" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="" required>
                        </div>  


                    </div>
                    <hr style="color: #0056b2;" />
                    <div class="form-group row mb-3">
                        <label for="lsttipo" class="col-1 col-form-label text-right">Tipo</label>
                        <div class="col-2">
                            <select class="form-control  text-uppercase" id="lsttipo" name="lsttipo">

                                <option value="Línea">Línea</option>
                                <option value="Arco">Arco</option>
 
                            </select>
                        </div>
                        <label for="txtcuerda" class="col-1 col-form-label text-right">Cuerda</label>
                        <div class="col-2">
                            <input id="txtcuerda" name="txtcuerda" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="">
                        </div>

                         <label for="txtflecha" class="col-1 col-form-label text-right">Flecha</label>
                        <div class="col-2">
                            <input id="txtflecha" name="txtflecha" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="">
                        </div>  
                        <label for="txtradio" class="col-1 col-form-label text-right">Radio</label>
                         <div class="col-2">
                            <input id="txtradio" name="txtradio" class="form-control  text-uppercase" type="number" min="0.00" step="0.00001" value="">
                        </div>  
                    </div>
 

                    <div class="form-group row mb-3 ">
                        <label for="txtlindero" class="col-1 col-form-label text-right">Lindero</label>
                        <div class="col-12">
                            <textarea id="txtlindero" name="txtlindero" class="form-control text-uppercase" type="text" maxlength="255" rows="2" style="resize:none;overflow-y:scroll"  ></textarea >
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptar" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelar" type="button" class="btn btn-secondary btn-lg" onClick="$('#mdlVentanaPligono').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>
                </form>

                <div class="form-group row mb-1">
                    <b><label for="lstRegistroInmobiliarioTipo" class="col-12 col-form-label text-right">Poligonos Catastrales</label></b>
                </div>
                <div class="form-group row mb-1">
                    
                    <div class="col-12">
                        <div id="divPoligonos" style="max-height:40vh ;overflow-y: auto;">

                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
$(document).ready(function() {
    $.hideLoading();
    //-----------------------------------------------------------------------------------------------------------------
    $('#mdlVentanaPligono').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmPoligono #lstlado").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
        $.getPoligonos(<?=$_GET["readonly"]?>);
    });
    $('#mdlVentanaPligono').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmPoligono").submit(function(e) {
        
        if ($("#lsttipo").val() == "Arco"){
            if ($("#txtcuerda").val() == ""){
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Para Tipo Arco debe Completar el valor Cuerda</span>"
                });
                $("#frmPoligono #cuerda").focus();
                return false;
            }
            if ($("#txtflecha").val() == ""){
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Para Tipo Arco debe Completar el valor Flecha</span>"
                });
                $("#frmPoligono #flecha").focus();
                return false;
            }
            if ($("#txtradio").val() == ""){
                vex.dialog.alert({
                    unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Para Tipo Arco debe Completar el valor Radio</span>"
                });
                $("#frmPoligono #radio").focus();
                return false;
            }
        }


        e.preventDefault();
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-poligono-x.php", {
                            par_id: "<?=$par_id?>",
                            psup_id: "<?=$psup_id?>",
                            psup_nombre: "<?=$psup_nombre?>",
                            accion: "add-poligono",
                            datastring: $("#frmPoligono").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            //console.log(json["msg"]);
                            if(json["res"]==1){
                                $.limpiar();
                                $.getPoligonos();
                                $.getSuperficiesAfectadas();
                                $("#frmPoligono #lstlado").focus();
                            }
                        });
                } else {}
            }
        })
    });
    //-----------------------------------------------------------------------------------------------------------------
    $.limpiar = function() {
      
        $("#poligono_id").val(""); 
        $("#txtpunto").val(""); 
        $("#txtpunto_sig").val(""); 
        $("#txtangulo_grad").val(""); 
        $("#txtangulo_min").val(""); 
        $("#txtangulo_seg").val(""); 
        $("#txtlongitud").val(""); 
       
        $("#txtcuerda").val(""); 
        $("#txtflecha").val(""); 
        $("#txtradio").val(""); 
        $("#txtlindero").val(""); 
    }
    //-----------------------------------------------------------------------------------------------------------------
    $.getPoligonos = function(readonly) {
        //console.log("<?=$par_id?>","<?=$psup_id?>");
        $.post("app-add-poligono-x.php", {
                accion: "armar-tabla-poligonos",
                par_id: "<?=$par_id?>",
                psup_id: "<?=$psup_id?>",
                readonly: readonly
            },
            function(response) {
                // console.log(response)
                let json = $.parseJSON(response);
                $("#divPoligonos").html(json["html"]);
            });
    }
    //-----------------------------------------------------------------------------------------------------------------
    $.eliminarPoligonos = function(psup_id, fila) {
         vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-add-poligono-x.php", {
                            accion: "eliminar",
                            poligono_id: psup_id,
                        },
                        function(response) {
                            $.hideLoading();
                            $("#tr" + fila).hide();
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Polígono eliminado</span>"
                            });
                           // $.getPoligonos(0);
                        });
                } else {}
            }
        }) 
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    $.editarPoligonos = function(ppol_id, fila) {
       $.post("app-add-poligono-x.php", {
            accion: "get-poligono",
            ppol_id: ppol_id 
        },
        function(response) {
            
            let json = $.parseJSON(response);
            $("#poligono_id").val(json[0]["poligono_id"]);
            
            document.getElementById("lstlado").value = json[0]["lado"];
            $("#txtpunto").val(json[0]["punto"]);
            $("#txtpunto_sig").val(json[0]["punto_sig"]);
            $("#txtangulo_grad").val(json[0]["angulo_grad"]);
            $("#txtangulo_min").val(json[0]["angulo_min"]);
            $("#txtangulo_seg").val(json[0]["angulo_seg"]);
            $("#txtlongitud").val(json[0]["longitud"]);
            
            document.getElementById("lsttipo").value = json[0]["tipo"];
            $("#txtcuerda").val(json[0]["cuerda"]);
            $("#txtflecha").val(json[0]["flecha"]);
            $("#txtradio").val(json[0]["radio"]);
            $("#txtlindero").val(json[0]["lindero"]);

            //$("#divPoligonos").html(json["html"]);
        });
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    $.subir = function(ppol_id, fila) {
       $.post("app-add-poligono-x.php", {
            accion: "subir-poligono",
            ppol_id: ppol_id 
        },
        function(response) {
            $.getPoligonos();
            $.getSuperficiesAfectadas();
        });
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    $.bajar = function(ppol_id, fila) {
       $.post("app-add-poligono-x.php", {
            accion: "bajar-poligono",
            ppol_id: ppol_id 
        },
        function(response) {
             
            $.getPoligonos();
            $.getSuperficiesAfectadas();
        });
    }
    //--------------------------------------------------------------------------------------------------------------------------------
});
 
</script>