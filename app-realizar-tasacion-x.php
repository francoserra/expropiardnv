<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb_sqlsrv.php";
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- chequeo que usuario tenga permisos para ABM
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil < 2) {exit;}

parse_str($_REQUEST["datastring"]);

$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "get-archivo") {
    $ctx = stream_context_create(array('http' => array(
        'timeout' => 20000, //1200 Seconds is 20 Minutes
    ),
    ));

    if (file_exists($_REQUEST["archivo_pdf"])) {
        echo "data:application/pdf;base64," . base64_encode(file_get_contents($_REQUEST["archivo_pdf"], false, $ctx));
    } else {
        echo "";
    }
    exit;
}
//--------------------------------------------------------------------------------------------------------------------
if($_REQUEST["accion"] == "validar-superficie") {
    // BUSCO LOS DETALLES DE LA SOLICITUD
    $db->ExecuteSQL("SELECT * FROM parcelas WHERE par_id=" . $_REQUEST["par_id"]);
    $parcela = $db->getRows();

    if($parcela[0]["par_superficie_afectacion"] == $_REQUEST["superficie"]) {
        $arr["res"] = 1;
        $arr["msg"] = "Superficie correcta" ;
        echo json_encode($arr);
        exit;
    } else {
        $arr["res"] = 0;
        $arr["msg"] = "Las superficie ingresada no corresponde con la de la parcela ({$parcela[0]["par_superficie_afectacion"]})" ;
        echo json_encode($arr);
        exit;
    }
}

//--------------------------------------------------------------------------------------------------------------------
if ($_REQUEST["accion"] == "upload-archivo") {

    $month = date("m");
    $year = date("Y");
    $dir = "$month-$year";
    if (!file_exists("archivos/" . $dir)) {
        mkdir("archivos/" . $dir, 0700);
        chmod("archivos/" . $dir, 0777);
    }
    $output_dir = "archivos/" . $dir;
    $partes_ruta = pathinfo($_FILES['myfile']['name']);
    $archivo = session_id() . date("dmYHis") . "." . $partes_ruta['extension'];
    $uploadfile = $output_dir . "/" . $archivo;
    $ret = array();
    if (isset($_FILES["myfile"])) {
        $error = $_FILES["myfile"]["error"];
        unlink($uploadfile);
        if (move_uploaded_file($_FILES['myfile']['tmp_name'], $uploadfile)) {
            $_FILES["myfile"]["error"] = "";
            $arr["res"] = 1;
            $arr["archivo"] = $uploadfile;
            print json_encode($arr);
            exit;
        }
    }

    $arr["res"] = 0;
    $arr["msg"] = "Error al adjuntar archivo (" . $_FILES["myfile"]["error"] . ")";
    print json_encode($arr);
    exit;
}

if ($_REQUEST["accion"] == "insert-tasacion") {
    $db->ExecuteSQL("START TRANSACTION");
    $insert = "INSERT INTO tasaciones (tas_valor_tasacion,tas_valor_mejoras,tas_valor_afectacion,tas_fecha_tasacion,tas_superficie_tasacion,tas_numero_nota_if,tas_archivo_adjunto,tas_numero_especial_ttn,par_id,tas_solicitud_id,tas_estado_tasacion,tas_usuario_tasacion) VALUES (
        $txtValorTasacion,
        $txtValorTasacionMejora,
        $txtValorTasacionAfectacion,
        Now(),
        $txtSuperficieTasación,
        '$txtNumeroNotaIf','"
        .$_REQUEST["archivo"]."',
        '$txtNumeroEspecialTTN',"
        .$_REQUEST["par_id"] . "," . $_REQUEST["ppt_id"] .",1,'" . $_SESSION["expropiar_usuario_login"] ."')";


    $result = $db->ExecuteSQL($insert);

    if ($result != 1) {
        $db->ExecuteSQL("ROLLBACK");
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al realizar tasación (ERR $result)." ;
        echo json_encode($arr);
        exit;
    } else {
        $update_solicitud = "UPDATE tasaciones_solicitudes SET ppt_estado = 7 WHERE ppt_id = " . $_REQUEST["ppt_id"];
        $result = $db->ExecuteSQL($update_solicitud);
        $db->ExecuteSQL("UPDATE parcelas SET par_tasacion_habilitada = 3 WHERE par_id=" . $_REQUEST["par_id"]);
        if($result != 1) {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[66] al realizar tasación (ERR $result)." ;
            echo json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("COMMIT");
            $arr["res"] = 1;
            $arr["msg"] = "Tasación realizada con éxito.";
            echo json_encode($arr);
            exit;
        }
    }
}

if ($_REQUEST["accion"] == "delete-tasacion") {
    $db->ExecuteSQL("START TRANSACTION");
    $delete = "UPDATE tasaciones SET tas_deleted=1 WHERE tas_id=".$_REQUEST["tas_id"];

    $result = $db->ExecuteSQL($delete);

    if ($result != 1) {
        $db->ExecuteSQL("ROLLBACK");
        $arr["res"] = 0;
        $arr["msg"] = "Error[66] al eliminar tasación (ERR $result)." ;
        echo json_encode($arr);
        exit;
    } else {
        $update_solicitud = "UPDATE tasaciones_solicitudes SET ppt_estado = 6 WHERE ppt_id = " . $_REQUEST["ppt_id"];
        $result = $db->ExecuteSQL($update_solicitud);
        if($result != 1) {
            $db->ExecuteSQL("ROLLBACK");
            $arr["res"] = 0;
            $arr["msg"] = "Error[66] al eliminar tasación (ERR $result)." ;
            echo json_encode($arr);
            exit;
        } else {
            $db->ExecuteSQL("COMMIT");
            $arr["res"] = 1;
            $arr["msg"] = "Tasación eliminada con éxito.";
            echo json_encode($arr);
            exit;
        }
        
    }
}
