<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
include "inc/cdb.php";
include "app-config.php";
include "inc/common.php";
include "app-lib.php";
error_reporting(0);
set_time_limit(10);

//-- recibe encriptados los datos de la grilla desxdxe la cual se abre esta cventana
$arr = explode("|", encrypt_decrypt("decrypt", $_GET["data"]));
$id = $arr[0];
//-- chequeo que usuario tenga permisos 
$atributo_perfil = checkAutorizacion($_SESSION["expropiar_usuario_perfil"], basename($_SERVER["SCRIPT_FILENAME"], '.php'));if ($atributo_perfil <2) {;exit;}


$db = new clsDB();
$db->setHost($GLOBALS["HOST_DB"]);
$db->setUsername($GLOBALS["USUARIO_DB"]);
$db->setPassword($GLOBALS["PASSWORD_DB"]);
$db->setDatabase($GLOBALS["DATABASE_DB"]);
$db->setDebugmode(false);
$db->openDB();
$db->ExecuteSQL("SET NAMES 'utf8'");


$db->ExecuteSQL("SELECT * FROM parcela_relacion");
$parcela_relacion_arr=$db->getRows();


$db->ExecuteSQL("SELECT SUM(ppr_porcentaje) total_porcentaje_asignado FROM parcelas_propietarios WHERE ppr_eliminado=0  AND par_id=$id");
$total_porcentaje_asignado=100-$db->getRows()[0]["total_porcentaje_asignado"];

?>
<style>
/*.custom-class-assignedto-modal .modal-dialog {
    width: 60%;
}

.custom-class-assignedto-modal .modal-body {
    height: 70vh;
    overflow: hidden;
}*/
</style>
<div class="modal custom-class-assignedto-modal box-shadow--8dp" tabindex="-1" role="dialog" id="mdlVentanaAsignarPropietario">
    <div class="modal-dialog  modal-lg modal-dialog-centered " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Asignar propietario parcela</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="divVentanaModalSearch"></div>
                <form id="frmPropietario" name="frmPropietario">
                    <div class="form-group row mb-1 required">
                        <label for="lstTipoPersona" class="col-4 col-form-label text-right">Tipo de persona</label>
                        <div class="col-8">
                            <select class="form-control  text-uppercase" id="lstTipoPersona" name="lstTipoPersona" required>
                                <option value="PF" selected>Persona Física</option>
                                <option value="PJ">Persona Jurídica</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtPersona" class="col-4 col-form-label text-right">Persona (nombre o apellido)</label>
                        <div class="col-8">
                            <div class="input-group">
                                <input required id="txtPersona" name="txtPersona" class="form-control  text-uppercase" type="text" pattern="\d*" value="" readonly style="background:transparent!important">
                                <div>
                                    &nbsp;<button type="button" class="btn btn-primary" id="" name="" onClick="$.buscarPersonas()" ;return false"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstRol" class="col-4 col-form-label text-right">Relación con el bien</label>
                        <div class="col-8">
                            <select class="form-control  text-uppercase" id="lstRol" name="lstRol" onChange="$.bloquearPorcentaje()" required>
                                <?php
                                for ($i=0; $i <count($parcela_relacion_arr) ; $i++) { 
                                ?>
                                <option value="<?=$parcela_relacion_arr[$i]["pre_id"]?>"><?=$parcela_relacion_arr[$i]["pre_relacion"]?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="txtPorcentaje" class="col-4 col-form-label text-right">Porcentaje propiedad</label>
                        <div class="col-8">
                            <input required id="txtPorcentaje" name="txtPorcentaje" class="form-control  text-uppercase" type="number" min=0 max=<?=$total_porcentaje_asignado?> step="0.01" value="<?=$total_porcentaje_asignado?>">
                        </div>
                    </div>
                    <div class="form-group row mb-1 required">
                        <label for="lstTieneRepresentante" class="col-4 col-form-label text-right">Tiene representante ?</label>
                        <div class="col-8">
                            <select class="form-control  text-uppercase" id="lstTieneRepresentante" name="lstTieneRepresentante" required>
                                <option value="0">No</option>
                                <option value="1">Si</option>
                            </select>
                        </div>
                    </div>

                    <br>
                    <hr>
                    <div class="form-group row mb-1">
                        <div class="col-12 text-right">
                            <button id="btnAceptarEditarDato" type="submit" class="btn btn-primary btn-lg"><i class="fas fa-check"></i>&nbsp;&nbsp;Aceptar</button>
                            &nbsp;&nbsp;&nbsp;
                            <button id="btnCancelarEditarDato" type="button" class="btn btn-secondary btn-lg" onClick="	$('#mdlVentanaAsignarPropietario').modal('hide');"><i class="fas fa-ban"></i>&nbsp;&nbsp;Cancelar</button>

                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<script src="js/jquery-ui.min.js"></script>
<script>
var g_archivo = "";
var g_id_persona = 0;
$(document).ready(function() {
    $.hideLoading();
    $('#mdlVentanaAsignarPropietario').on('shown.bs.modal', function() {
        $(this).find('form')[0].reset();
        setTimeout(() => {
            $("#frmPropietario #lstTipoAlstTipoPersonarchivo").focus();
        }, 250);
        $(".modal-dialog").draggable({
            cursor: "move",
            handle: ".dragable_touch",
        });
    });
    $('#mdlVentanaAsignarPropietario').on('hidden.bs.modal', function() {
        $(".modal-dialog").draggable("disable");
    });
    //---------------------------------------------------------------------
    //--ABRIMOS BUSCADOR DE PERSONAS
    $.buscarPersonas = function() {
        var tabla_busqueda = "";
        var filtro_busqueda1 = "";
        var filtro_busqueda2 = "";
        if ($('#lstTipoPersona').val() == 'PF') {
            tabla_busqueda = 'personas_fisicas';
            filtro_busqueda1 = "per_nombre";
            filtro_busqueda2 = "per_apellido";
            campo_mostrar1 = "per_dni";
            campo_mostrar2 = "per_nombre";
            campo_mostrar3 = "per_apellido";
        } else {
            tabla_busqueda = 'personas_juridicas';
            filtro_busqueda1 = "per_cuit";
            filtro_busqueda2 = "per_razon_social";
            campo_mostrar1 = "per_cuit";
            campo_mostrar2 = "per_razon_social";
            campo_mostrar3 = "";
        } 
        $.mostrarVentanaSearch('Buscar persona', tabla_busqueda, 'per_id', filtro_busqueda1, filtro_busqueda2, campo_mostrar1, campo_mostrar2, campo_mostrar3, 'txtPersona');
    }
    //---------------------------------------------------------------------
    //-- procesamos lo que se selecciono en la ventana de busqueda
    $("#frmPropietario #txtPersona").focus(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val().length > 0) {
            //  1234|JUAN|PEREZ
            let arr = $("#txtPersona").val().split("|");
            if (arr.length > 1) {
                $("#txtPersona").val(arr[1] + ", " + arr[2] + ", " + arr[3]);
                g_id_persona = arr[0];
            }
        }
    });
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $.bloquearPorcentaje = function() {
        if ($("#lstRol").val() == 5 || $("#lstRol").val() == 9 || $("#lstRol").val() == 10) {
            if($("#lstRol").val() == 10) {
                $("#txtPorcentaje").val(0)
            }
            $("#txtPorcentaje").attr("readonly", "readonly");
        } else {
            $("#txtPorcentaje").removeAttr("readonly");
        }
    }
    //-------------------------------------------------------------------------------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------
    $("#frmPropietario").submit(function(e) {
        e.preventDefault();
        if ($("#txtPersona").val() == "") {
            vex.dialog.alert({
                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>Tenés que seleccionar una persona.</span>"
            });
            return false;
        }
        vex.dialog.confirm({
            message: 'Confirmás ?',
            callback: function(value) {
                if (value) {
                    $.showLoading();
                    $.post("app-asignar-propietario-x.php", {
                            id: "<?=$id?>",
                            id_persona: g_id_persona,
                            accion: 'insert',
                            datastring: $("#frmPropietario").serialize()
                        },
                        function(response) {
                            $.hideLoading();
                            let json = $.parseJSON(response);
                            vex.dialog.alert({
                                unsafeMessage: "<big><b><i class='fas fa-exclamation-circle'></i>&nbsp;INFO</b></big><br><br>" + json["msg"] + "</span>"
                            });
                            $.clearForm("frmPropietario");
                            $("#txtPorcentaje").val(json["total_porcentaje_asignado"]);
                            $("#txtPorcentaje").attr("max", json["total_porcentaje_asignado"]);
                        });
                } else {}
            }
        }) //vex.dialog.confirm({
    });
});
</script>